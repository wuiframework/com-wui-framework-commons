/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons {
    "use strict";

    export class SeleniumTestEnvironmentArgs extends EnvironmentArgs {
        public Load($appConfig : any, $handler : () => void) : void {
            super.Load($appConfig, $handler);
        }

        protected getConfigPaths() : string[] {
            return [];
        }
    }

    export class SeleniumTestLoader extends Loader {
        protected initEnvironment() : SeleniumTestEnvironmentArgs {
            return new SeleniumTestEnvironmentArgs();
        }
    }

    export class SeleniumTestRunner extends Com.Wui.Framework.UnitTestRunner {
        protected initLoader() : void {
            super.initLoader(SeleniumTestLoader);
        }
    }
}
