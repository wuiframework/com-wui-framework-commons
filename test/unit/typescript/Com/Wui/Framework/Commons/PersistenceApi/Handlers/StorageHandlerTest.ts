/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.PersistenceApi.Handlers {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;

    export class StorageHandlerTest extends UnitTestRunner {

        public testConstructor() : void {
            const handler1 : StorageHandler = new StorageHandler();
            const handler2 : StorageHandler = new StorageHandler();
            assert.notEqual(handler1.getSessionId(), handler2.getSessionId());

            const handler3 : StorageHandler = new StorageHandler("sessionId");
            assert.equal(handler3.getSessionId(), "sessionId");
        }

        public testgetLoadTime() : void {
            const handler : StorageHandler = new StorageHandler();
            assert.equal(handler.getLoadTime(), 0);
        }

        public testExists() : void {
            const handler : StorageHandler = new StorageHandler();
            assert.ok(!handler.Exists("key"));
            handler.Variable("key", "value");
            assert.ok(handler.Exists("key"));
        }

        public testgetSize() : void {
            const handler1 : StorageHandler = new StorageHandler("storage4");
            const variablesList : ArrayList<any> = new ArrayList<any>();
            variablesList.Add("value", "key1");
            variablesList.Add("value2", "key2");
            handler1.Variable(variablesList);
            assert.equal(handler1.getSize(), 0);

            const handler2 : StorageHandler = new StorageHandler("storage4");
            assert.equal(handler2.Variable("key2"), "value2");
            assert.equal(handler2.getSize(), 268);
        }

        public testVariable() : IUnitTestRunnerPromise {
            const handler : StorageHandler = new StorageHandler("storage2");
            assert.ok(!handler.Exists("key"));
            handler.Variable("key", "value");
            assert.ok(handler.Exists("key"));

            assert.ok(!handler.Exists("key1"));
            assert.ok(!handler.Exists("key2"));
            const variablesList : ArrayList<any> = new ArrayList<any>();
            variablesList.Add("value", "key1");
            variablesList.Add("value", "key2");
            handler.Variable(variablesList);
            assert.ok(handler.Exists("key"));
            assert.ok(handler.Exists("key1"));
            assert.ok(handler.Exists("key2"));
            const asyncHandler : any = () : void => {
                const data : any = "data";
            };

            const storage : StorageHandler = new StorageHandler("storage1");
            (<any>BasePersistenceHandler).crcCheckEnabled = true;
            assert.throws(() : void => {
                storage.Variable(null, asyncHandler);
            }, /Persistence name is supposed to be type of string or integer/);

            return ($done : () => void) : void => {
                handler.Variable("key2", "value", () : void => {
                    assert.ok(handler.Exists("key2"));
                    ExceptionsManager.Clear();
                });
                this.initSendBox();
                $done();
            };
        }

        public testLoadPersistenceAsynchronously() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const handler : StorageHandler = new StorageHandler();
                handler.LoadPersistenceAsynchronously(() : void => {
                    assert.equal(handler.Variable("key"), null);
                });
                ExceptionsManager.Clear();
                this.initSendBox();
                $done();
            };
        }

        public __IgnoretestLoadPersistenceAsynchronously2() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const handler : StorageHandler = new StorageHandler();
                handler.LoadPersistenceAsynchronously(
                    () : void => {
                        assert.equal(handler.Variable("key"), "value");
                        $done();
                    }, "file://" + this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Commons/StoragePersistence.jsonp");
            };
        }

        public __IgnoretestLoadPersistenceAsynchronously3() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const handler : StorageHandler = new StorageHandler("storage2");
                handler.LoadPersistenceAsynchronously(() : void => {
                    assert.equal(handler.Variable("loading"), null);
                    $done();
                }, "file://" + this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Commons/ValidData2.jsonp");

            };
        }

        public testLoadPersistenceAsynchronously4() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const handler : StorageHandler = new StorageHandler("storage2");
                handler.LoadPersistenceAsynchronously(() : void => {
                    assert.equal(handler.Variable(null), null);
                    ExceptionsManager.Clear();

                }, "file://" + this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Commons/EmptyObject.jsonp");
                this.initSendBox();
                $done();
            };
        }

        public testLoadPersistenceAsynchronously5() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const handler : StorageHandler = new StorageHandler();
                const list : ArrayList<any> = new ArrayList<any>();
                list.Add("test", 0);
                list.Add("test1", 1);
                (<any>StorageHandler).session = list;
                handler.LoadPersistenceAsynchronously(() : void => {
                    $done();
                });
                ExceptionsManager.Clear();
            };
        }

        public testDestroy() : void {
            const handler : StorageHandler = new StorageHandler();
            handler.Variable("key", "value");
            handler.Variable("key1", "value");
            handler.Variable("key2", "value");
            handler.Variable("key3", "value");

            assert.ok(handler.Exists("key"));
            assert.ok(handler.Exists("key1"));
            assert.ok(handler.Exists("key2"));
            assert.ok(handler.Exists("key3"));

            handler.Destroy("key");
            const variablesList : ArrayList<string> = new ArrayList<string>();
            variablesList.Add("key1");
            variablesList.Add("key2");
            variablesList.Add("key5");
            handler.Destroy(variablesList);

            assert.ok(!handler.Exists("key"));
            assert.ok(!handler.Exists("key1"));
            assert.ok(!handler.Exists("key2"));
            assert.ok(handler.Exists("key3"));

            const handler3 : StorageHandler = new StorageHandler("storage3");
            handler3.Destroy(null);
            assert.ok(!handler3.Exists("key3"));
            ExceptionsManager.Clear();
        }

        public testClear() : void {
            const handler : StorageHandler = new StorageHandler();
            handler.Variable("key", "value");
            handler.Variable("key1", "value");
            handler.Variable("key2", "value");

            assert.ok(handler.Exists("key"));
            assert.ok(handler.Exists("key1"));
            assert.ok(handler.Exists("key2"));

            handler.Clear();

            assert.ok(!handler.Exists("key"));
            assert.ok(!handler.Exists("key1"));
            assert.ok(!handler.Exists("key2"));
            ExceptionsManager.Clear();
        }

        public testToString() : void {
            const handler : StorageHandler = new StorageHandler();
            handler.Variable("key", "value");
            this.resetCounters();
            assert.equal(handler.ToString(),
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ \"key\" ]&nbsp;&nbsp;&nbsp;&nbsp;value<br/>" +
                "</span>");
            ExceptionsManager.Clear();
        }

        public testgetRawData() : void {
            const handler : StorageHandler = new StorageHandler();
            handler.Variable("key", "value");
            assert.equal(handler.getRawData(),
                "m:60:a:55:i:0;s:46:Com.Wui.Framework.Commons.Primitives.ArrayListc:0:166:o:160:s:4:sizei:2;s:4:keysa:28:i:0;" +
                "s:9:variablesi:1;s:3:crcs:4:dataa:94:i:0;c:0:67:o:62:s:4:sizei:1;" +
                "s:4:keysa:11:i:0;s:3:keys:4:dataa:13:i:0;s:5:valuei:1;i:438593955;");

            const handler2 : StorageHandler = new StorageHandler("storage1");
            const asyncHandler : any = () : void => {
                const data : string = "data";
            };
            assert.deepEqual(handler2.getRawData(asyncHandler), "");

            handler.Variable("key1", "value1");
            const crcstring : string = handler.getRawData();
            assert.equal(handler.getRawData(), crcstring);
            this.initSendBox();
        }

        public testtoString() : void {
            const handler3 : StorageHandler = new StorageHandler("storage3");
            this.resetCounters();
            assert.equal(handler3.toString(),
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>");
            ExceptionsManager.Clear();
        }

        public testIsSupported() : void {
            const storage : any = localStorage;
            assert.equal(StorageHandler.IsSupported(), true);
            localStorage = null;
            assert.equal(StorageHandler.IsSupported(), false);
            localStorage = storage;
            ExceptionsManager.Clear();
        }
    }
}
