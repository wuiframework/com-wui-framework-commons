/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.PersistenceApi {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;

    export class CacheManagerTest extends UnitTestRunner {

        public testConstructor() : void {
            assert.doesNotThrow(() : void => {
                const instance : CacheManager = new CacheManager();
            });
            ExceptionsManager.Clear();
        }

        public testProcess() : void {
            this.resetCounters();
            assert.resolveEqual(CacheManager, "" +
                "<head></head>" +
                "<body>" +
                "<div id=\"Content\"><span guitype=\"HtmlAppender\"><br>" +
                "<h1>Local cache manager</h1></span><span guitype=\"HtmlAppender\"><br>" +
                "<h2>Domain cookies</h2></span><span guitype=\"HtmlAppender\"><br>" +
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>" +
                "</span><span guitype=\"HtmlAppender\"><br>" +
                "<span style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\" " +
                "onclick=\"Com.Wui.Framework.Commons.PersistenceApi.CacheManager.clearCookies();\">Clear cookies</span></span>" +
                "<span guitype=\"HtmlAppender\"><br>" +
                "<h2>Local storage items</h2></span><span guitype=\"HtmlAppender\"><br>" +
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_1\').style.display=" +
                "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>" +
                "</span><span guitype=\"HtmlAppender\"><br>" +
                "<span style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\" " +
                "onclick=\"Com.Wui.Framework.Commons.PersistenceApi.CacheManager.clearStorage();\">Clear storage</span></span>" +
                "</div>" +
                "</body>");
        }

        public testclearCookies() : void {
            document.cookie = "testItem=testValue; expires=" + Convert.TimeToGMTformat("+1 min") + "; path=/;";
            assert.notEqual(document.cookie, "");

            assert.doesNotThrow(() : void => {
                (<any>CacheManager).clearCookies();
            });
            this.getHttpManager().Refresh();
            assert.equal(document.cookie, "");
        }

        public testclearStorage() : void {
            this.setStorageItem("testItem", "testValue");
            assert.ok(!this.getHttpManager().getRequest().getStorageItems().IsEmpty());
            assert.doesNotThrow(() : void => {
                (<any>CacheManager).clearStorage();
            });
            this.getHttpManager().Refresh();
            assert.ok(this.getHttpManager().getRequest().getStorageItems().IsEmpty());
        }

        protected setUp() : void {
            this.clearAll();
            window.location.reload = (forcedReload? : boolean) : void => {
                // mock implementation for missing native API
            };
        }

        protected after() : void {
            this.clearAll();
            window.location.reload = null;
        }

        private clearAll() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
            (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
                return true;
            });
            StringUtils.Split(document.cookie, ";").forEach(($value : any, $key? : any) : void => {
                document.cookie = $key + "=; expires=" + Convert.TimeToGMTformat(Property.Time(null, "-1 year"));
            });
            localStorage.clear();
        }
    }
}
