/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.PersistenceApi.Handlers {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    class MockPersistenceHandler extends BasePersistenceHandler {
        constructor($sessionId? : string) {
            super($sessionId);
            this.variables = new ArrayList<any>();
        }
    }

    export class BasePersistenceHandlerTest extends UnitTestRunner {

        public testConstructor() : void {
            const handler1 : MockPersistenceHandler = new MockPersistenceHandler("sessionName");
            assert.equal(handler1.getSessionId(), "sessionName");

            const handler2 : MockPersistenceHandler = new MockPersistenceHandler(null);
            assert.equal(handler2.getSessionId(), "");
        }

        public testgetSize() : void {
            const handler : MockPersistenceHandler = new MockPersistenceHandler();
            assert.equal(handler.getSize(), 0);
        }

        public testgetLoadTime() : void {
            const handler : MockPersistenceHandler = new MockPersistenceHandler();
            assert.equal(handler.getLoadTime(), 0);
        }

        public testDisableCRC() : void {
            const handler : MockPersistenceHandler = new MockPersistenceHandler();
            assert.ok((<any>handler).crcCheckEnabled);
            handler.DisableCRC();
            assert.ok(!(<any>handler).crcCheckEnabled);
        }

        public testExpireTime() : void {
            const handler1 : MockPersistenceHandler = new MockPersistenceHandler();
            assert.equal(handler1.ExpireTime(10380), 10380);

            const handler2 : MockPersistenceHandler = new MockPersistenceHandler();
            assert.equal(handler2.ExpireTime(), null);

            const handler3 : MockPersistenceHandler = new MockPersistenceHandler();
            assert.equal(handler3.ExpireTime("+80103"), 2465639305200000);

            const handler4 : MockPersistenceHandler = new MockPersistenceHandler();
            assert.equal(handler4.ExpireTime(null), null);

            const handler5 : MockPersistenceHandler = new MockPersistenceHandler();
            assert.equal(handler5.ExpireTime("80103"), "+80103");

            const handler6 : MockPersistenceHandler = new MockPersistenceHandler();
            assert.equal(handler6.ExpireTime("-80103"), 2465639305200000);
        }

        public __IgnoretestLoadPersistenceAsynchronously() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const handler : MockPersistenceHandler = new MockPersistenceHandler();
                handler.LoadPersistenceAsynchronously(
                    () : void => {
                        assert.equal(handler.Variable("key"), "value");
                        $done();
                    },
                    "file://" + this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Commons/BasePersistence.jsonp");
            };
        }

        public __IgnoretestLoadPersistenceAsynchronously2() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const handler : MockPersistenceHandler = new MockPersistenceHandler("cookie2");
                handler.LoadPersistenceAsynchronously(() : void => {
                    // async load handler
                }, "file://" + this.getAbsoluteRoot() + "/test/resource/data/Com/Wui/Framework/Commons/EmptyObject.jsonp");
            };
        }

        public testDestroy() : void {
            const handler : MockPersistenceHandler = new MockPersistenceHandler();
            handler.Variable("key", "value");
            assert.equal(this.getVariablesCount(handler), 1);
            handler.Destroy("key");
            assert.equal(this.getVariablesCount(handler), 0);

            const handler3 : MockPersistenceHandler = new MockPersistenceHandler("storage3");
            handler3.Destroy(null);
            assert.ok(!handler3.Exists("key3"));
        }

        public testVariable() : void {
            const handler : MockPersistenceHandler = new MockPersistenceHandler();
            handler.Variable("key1", "value");
            assert.equal(this.getVariablesCount(handler), 1);
            handler.Variable("key2", "value");
            assert.equal(this.getVariablesCount(handler), 2);
            handler.Variable("key1", "value2");
            assert.equal(this.getVariablesCount(handler), 2);
            assert.equal(handler.Variable("key1"), "value2");
            const handler2 : MockPersistenceHandler = new MockPersistenceHandler();
            assert.equal(handler2.Variable(null), null);
        }

        public testExists() : void {
            const handler : MockPersistenceHandler = new MockPersistenceHandler();
            assert.equal(handler.Exists("key"), false);
            handler.Variable("key", "value");
            assert.equal(handler.Exists("key"), true);
        }

        public testClear() : void {
            const handler : MockPersistenceHandler = new MockPersistenceHandler();
            handler.Variable("key", "value");
            assert.ok(this.getVariablesCount(handler) > 0);
            handler.Clear();
            assert.equal((<any>handler).variables, null);
        }

        public testToString() : void {
            const handler : MockPersistenceHandler = new MockPersistenceHandler();
            handler.Variable("key", "value");
            this.resetCounters();
            const expected : string = (<any>handler).variables.ToString();
            this.resetCounters();
            assert.equal(handler.ToString(), expected);

            const handler7 : MockPersistenceHandler = new MockPersistenceHandler();
            this.resetCounters();
            assert.equal(handler7.ToString("", true),
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>");

            delete (<any>handler).variables;
            this.resetCounters();
            assert.equal(handler.ToString("", true),
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>");
        }

        public testtoString() : void {
            const handler : MockPersistenceHandler = new MockPersistenceHandler();
            handler.Variable("key", "value");
            this.resetCounters();
            const expected : string = (<any>handler).variables.toString();
            this.resetCounters();
            assert.equal(handler.toString(), expected);
        }

        public testgetRawData() : void {
            const handler : MockPersistenceHandler = new MockPersistenceHandler();
            handler.Variable("key", "value");
            assert.equal(handler.getRawData(),
                "m:60:a:55:i:0;s:46:Com.Wui.Framework.Commons.Primitives.ArrayListc:0:67:o:62:s:4:sizei:1;" +
                "s:4:keysa:11:i:0;s:3:keys:4:dataa:13:i:0;s:5:value");

            const handler2 : MockPersistenceHandler = new MockPersistenceHandler("cookie1");
            const asyncHandler : any = () : void => {
                const data : string = "data";
            };
            assert.deepEqual(handler2.getRawData(asyncHandler), "");

            const handler3 : MockPersistenceHandler = new MockPersistenceHandler();
            const list : ArrayList<string> = new ArrayList<string>();
            list.Add("test");
            list.Add("message");
            handler.Variable(list);
            assert.equal(handler3.getRawData(),
                "m:60:a:55:i:0;s:46:Com.Wui.Framework.Commons.Primitives.ArrayListc:0:41:o:36:s:4:sizei:0;s:4:keysa:0:s:4:dataa:0:");
            this.initSendBox();
        }

        private getVariablesCount($handler : MockPersistenceHandler) : number {
            return (<any>$handler).variables.Length();
        }
    }
}
