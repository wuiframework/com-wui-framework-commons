/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Exceptions.Type {
    "use strict";
    import ExceptionCode = Com.Wui.Framework.Commons.Enums.ExceptionCode;

    export class OutOfRangeExceptionTest extends UnitTestRunner {

        public testConstructor() : void {
            assert.doesNotThrow(() : void => {
                const instance : OutOfRangeException = new OutOfRangeException();
            });
            const exception : OutOfRangeException = new OutOfRangeException("message");
            assert.equal(exception.Message(), "message");
        }

        public testCode() : void {
            const exception : OutOfRangeException = new OutOfRangeException();
            assert.equal(exception.Code(), ExceptionCode.OUT_OF_RANGE);
        }
    }
}
