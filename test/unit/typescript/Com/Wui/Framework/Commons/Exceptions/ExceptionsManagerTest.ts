/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Exceptions {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ResolverFatalException = Com.Wui.Framework.Commons.Exceptions.Type.ResolverFatalException;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ErrorPageException = Com.Wui.Framework.Commons.Exceptions.Type.ErrorPageException;
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import FatalError = Com.Wui.Framework.Commons.Exceptions.Type.FatalError;
    import LogLevel = Com.Wui.Framework.Commons.Enums.LogLevel;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IllegalArgumentException = Com.Wui.Framework.Commons.Exceptions.Type.IllegalArgumentException;
    import NullPointerException = Com.Wui.Framework.Commons.Exceptions.Type.NullPointerException;
    import OutOfRangeException = Com.Wui.Framework.Commons.Exceptions.Type.OutOfRangeException;

    export class ExceptionsManagerTest extends UnitTestRunner {

        public testThrowMessage() : void {
            try {
                ExceptionsManager.Throw("test", "string message");
            } catch (ex) {
                assert.equal(ExceptionsManager.getAll().ToString("", false),
                    "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                    "[ 0 ]    string message\r\n");
            }
        }

        public testThrowError() : void {
            try {
                ExceptionsManager.Throw("test", new Error("error message"));
            } catch (ex) {
                assert.patternEqual(
                    ExceptionsManager.getAll().ToString("", false),
                    "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                    "[ 0 ]    error message, stack: \r\n" +
                    "Error: error message\r\n" +
                    "    at ExceptionsManagerTest.testThrowError (*:*:*)\r\n" +
                    "    at *.js:*:*\r\n");
            }
        }

        public testThrowFatal() : void {
            try {
                ExceptionsManager.Throw("test", new ResolverFatalException());
            } catch (ex) {
                assert.equal(ExceptionsManager.getAll().ToString("", false),
                    "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                    "[ 0 ]    ResolverFatalException - RESOLVER_EXCEPTION (6)\r\n");
            }
        }

        public testThrowSelferror() : void {
            try {
                ExceptionsManager.Throw("test", new ErrorPageException());
            } catch (ex) {
                assert.equal(ExceptionsManager.getAll().ToString("", false),
                    "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                    "[ 0 ]    ErrorPageException - ERROR_PAGE_EXCEPTION (5)\r\n");
            }
        }

        public testThrowExit() : void {
            try {
                assert.ok(true, "This line should be reachable");
                ExceptionsManager.ThrowExit();
                assert.ok(false, "This line should be unreachable");
            } catch (ex) {
                assert.ok(ExceptionsManager.getAll().IsEmpty());
            }
        }

        public testIsNativeException() : void {
            assert.ok(ExceptionsManager.IsNativeException(new Error()));
            assert.ok(ExceptionsManager.IsNativeException("message"));
            try {
                ExceptionsManager.ThrowExit();
            } catch (ex) {
                assert.ok(!ExceptionsManager.IsNativeException(ex));
            }
            try {
                ExceptionsManager.Throw("test", "message");
            } catch (ex) {
                assert.ok(!ExceptionsManager.IsNativeException(ex));
            }
        }

        public testgetAll() : void {
            try {
                ExceptionsManager.Throw("test", "message1");
            } catch (ex) {
                // register exception
            }
            try {
                ExceptionsManager.Throw("test", "message2");
            } catch (ex) {
                // register exception
            }
            assert.equal(ExceptionsManager.getAll().Length(), 2);
            assert.equal(ExceptionsManager.getAll(), (<any>ExceptionsManager).exceptionList);

            const list : ArrayList<any> = (<any>ExceptionsManager).exceptionList;
            const propertyName : string = "exceptionList";
            delete ExceptionsManager[propertyName];
            assert.equal(ExceptionsManager.getAll().Length(), 0);
            (<any>ExceptionsManager).exceptionList = list;
        }

        public testgetLast() : void {
            try {
                ExceptionsManager.Throw("test", "message1");
            } catch (ex) {
                // register exception
            }
            try {
                ExceptionsManager.Throw("test", "message2");
            } catch (ex) {
                // register exception
            }
            assert.equal(ExceptionsManager.getAll().Length(), 2);
            assert.equal(ExceptionsManager.getLast(), (<any>ExceptionsManager).exceptionList.getLast());

            const list : ArrayList<any> = (<any>ExceptionsManager).exceptionList;
            const propertyName : string = "exceptionList";
            delete ExceptionsManager[propertyName];
            assert.equal(ExceptionsManager.getLast(), null);
            (<any>ExceptionsManager).exceptionList = list;
        }

        public testHandleExceptionMessage() : void {
            LogIt.setLevel(LogLevel.ERROR);
            UnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-commons", version: "1.0.0"
            });

            try {
                ExceptionsManager.Throw("test", "message");
            } catch (ex) {
                assert.doesNotThrow(() : void => {
                    ExceptionsManager.HandleException(ex);
                });
            }
            this.initSendBox();
        }

        public testHandleExceptionWithMoreInfo() : void {
            try {
                ExceptionsManager.Throw("test", "message", 58, "ExceptionManagerTest", 120);
            } catch (ex) {
                assert.doesNotThrow(() : void => {
                    ExceptionsManager.HandleException(ex);
                });
            }
        }

        public __IgnoretestHandleExceptionNative() : void {
            try {
                throw new Error("error message exception");
            } catch (ex) {
                assert.doesNotThrow(() : void => {
                    ExceptionsManager.HandleException(ex);
                });
            }
        }

        public testHandleExceptionExit() : void {
            try {
                ExceptionsManager.ThrowExit();
            } catch (ex) {
                assert.doesNotThrow(() : void => {
                    ExceptionsManager.HandleException(ex);
                });
            }
        }

        public testToString() : void {
            try {
                ExceptionsManager.Throw("test", new Error("message"));
            } catch (ex) {
                // register exception
            }

            assert.patternEqual(ExceptionsManager.ToString(),
                "<h1>Oops, something went wrong...</h1>" +
                "thrown by: <b>test</b>: <br/>" +
                "message<br/>" +
                "<span onclick=\"" +
                "document.getElementById('ContentBlock_0').style.display=" +
                "document.getElementById('ContentBlock_0').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Stack trace</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Error: message<br/>" +
                "    at ExceptionsManagerTest.testToString (*:*:*)<br/>" +
                "    at *<br/>" +
                "*</span><br/>");
            this.resetCounters();
            assert.patternEqual(ExceptionsManager.ToString("", false),
                "Oops, something went wrong...\r\n" +
                "thrown by: test\r\n" +
                "message, stack: \r\n" +
                "Error: message\r\n" +
                "    at ExceptionsManagerTest.testToString (*:*:*)\r\n" +
                "    at *\r\n");
        }

        public testToStringWithFatal() : void {
            try {
                ExceptionsManager.Throw("test", new FatalError("message"));
            } catch (ex) {
                // register exception
            }
            assert.equal(ExceptionsManager.ToString(),
                "<h1>FATAL Error!</h1>" +
                "thrown by: <b>test</b>: <br/>" +
                "message<br/><br/>");
            this.resetCounters();
            assert.equal(ExceptionsManager.ToString("", false),
                "FATAL Error!\r\n" +
                "thrown by: test\r\n" +
                "message\r\n");
        }

        public testToString2() : void {
            try {
                ExceptionsManager.Throw("some owner", null, null);
            } catch (ex) {
                // register exception
            }
            assert.equal(ExceptionsManager.ToString(""), "");
        }

        public testToString3() : void {
            this.resetCounters();
            try {
                ExceptionsManager.Throw(null, "string message");
            } catch (ex) {
                assert.deepEqual((<any>ExceptionsManager).exceptionList.ToString(),
                    "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                    "<span onclick=\"" +
                    "document.getElementById(\'ContentBlock_0\').style.display=" +
                    "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                    "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                    "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                    "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;string message<br/><br/></span>");
            }
        }

        public testToStringNullExeptionList() : void {
            this.resetCounters();
            const propertyName : string = "exceptionList";
            delete ExceptionsManager[propertyName];
            assert.equal(ExceptionsManager.ToString(""), "");
        }

        public testToString4() : void {
            const exception : Exception = new Exception();
            const exception1 : Exception = new Exception();
            const exception2 : ErrorPageException = new ErrorPageException();
            const exception3 : IllegalArgumentException = new IllegalArgumentException();
            const exception4 : NullPointerException = new NullPointerException();
            const exception5 : ResolverFatalException = new ResolverFatalException();

            const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
            exceptionList.Add(exception, 0);
            exceptionList.Add(exception1, 1);
            exceptionList.Add(exception2, 2);
            exceptionList.Add(exception3, 3);
            exceptionList.Add(exception4, 4);
            exceptionList.Add(exception5, 5);

            const storage : any = (<any>ExceptionsManager).exceptionList;
            (<any>ExceptionsManager).exceptionList = exceptionList;
            this.resetCounters();
            assert.equal((<any>ExceptionsManager).exceptionList.ToString(),
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i>" +
                " <span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 5 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/></span>");
            (<any>ExceptionsManager).exceptionList = storage;
        }

        public testHandleException() : void {
            const error : Error = new Error("this is my message");
            const exception : Exception = new Exception();
            const exception1 : Exception = new Exception();
            const exception2 : ErrorPageException = new ErrorPageException();
            const exception3 : IllegalArgumentException = new IllegalArgumentException();
            const exception4 : NullPointerException = new NullPointerException();
            const exception5 : ResolverFatalException = new ResolverFatalException();
            const exception6 : ErrorPageException = new ErrorPageException();
            const exception7 : IllegalArgumentException = new IllegalArgumentException();
            const exception8 : NullPointerException = new NullPointerException();
            const exception9 : OutOfRangeException = new OutOfRangeException();
            const exception10 : ResolverFatalException = new ResolverFatalException();
            const exception11 : Exception = new Exception("Try again");
            const exception12 : Exception = new Exception("Exception");
            const exception13 : Exception = new Exception("Wrong arguments");
            const exception14 : OutOfRangeException = new OutOfRangeException();
            const exception15 : NullPointerException = new NullPointerException();

            const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
            exceptionList.Add(exception, 0);
            exceptionList.Add(exception1, 1);
            exceptionList.Add(exception2, 2);
            exceptionList.Add(exception3, 3);
            exceptionList.Add(exception4, 4);
            exceptionList.Add(exception5, 5);
            exceptionList.Add(exception6, 6);
            exceptionList.Add(exception7, 7);
            exceptionList.Add(exception8, 8);
            exceptionList.Add(exception9, 9);
            exceptionList.Add(exception10, 10);
            exceptionList.Add(exception11, 11);
            exceptionList.Add(exception12, 12);
            exceptionList.Add(exception13, 13);
            exceptionList.Add(exception14, 14);
            exceptionList.Add(exception15, 15);

            (<any>ExceptionsManager).exceptionList = exceptionList;
            assert.equal((<any>ExceptionsManager).exceptionList.ToString(),
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i>" +
                " <span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 5 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 6 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 7 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 8 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 9 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 10 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 11 ]&nbsp;&nbsp;&nbsp;&nbsp;Try again<br/><br/>" +
                "[ 12 ]&nbsp;&nbsp;&nbsp;&nbsp;Exception<br/><br/>" +
                "[ 13 ]&nbsp;&nbsp;&nbsp;&nbsp;Wrong arguments<br/><br/>" +
                "[ 14 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                "[ 15 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/></span>");
        }

        public __IgnoretestToString20() : void {
            const error : Error = new Error("this is my message");
            const exception : Exception = new Exception();
            const exception1 : Exception = new Exception();
            const exception2 : ErrorPageException = new ErrorPageException();
            const exception3 : IllegalArgumentException = new IllegalArgumentException();
            const exception4 : NullPointerException = new NullPointerException();
            const exception5 : IllegalArgumentException = new IllegalArgumentException();
            const exception6 : ErrorPageException = new ErrorPageException();
            const exception7 : IllegalArgumentException = new IllegalArgumentException();
            const exception8 : NullPointerException = new NullPointerException();
            const exception9 : OutOfRangeException = new OutOfRangeException();
            const exception10 : NullPointerException = new NullPointerException();
            const exception11 : Exception = new Exception();
            const exception12 : Exception = new Exception();
            const exception13 : Exception = new Exception();
            const exception14 : OutOfRangeException = new OutOfRangeException();
            const exception15 : NullPointerException = new NullPointerException();

            const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
            exceptionList.Add(exception, 0);
            exceptionList.Add(exception1, 1);
            exceptionList.Add(exception2, 2);
            exceptionList.Add(exception3, 3);
            exceptionList.Add(exception4, 4);
            exceptionList.Add(exception5, 5);
            exceptionList.Add(exception6, 6);
            exceptionList.Add(exception7, 7);
            exceptionList.Add(exception8, 8);
            exceptionList.Add(exception9, 9);
            exceptionList.Add(exception10, 10);
            exceptionList.Add(exception11, 11);
            exceptionList.Add(exception12, 12);
            exceptionList.Add(exception13, 13);
            exceptionList.Add(exception14, 14);
            exceptionList.Add(exception15, 15);

            const storage : any = (<any>ExceptionsManager).exceptionList;
            (<any>ExceptionsManager).exceptionList = exceptionList;
            try {
                throw new Error("error message exception");
            } catch (ex) {
                assert.doesNotThrow(() : void => {
                    ExceptionsManager.HandleException(ex);
                });
                this.resetCounters();
                assert.equal(ExceptionsManager.ToString(), "<h1>Oops, something went wrong...</h1>thrown by: <b></b>: <br/>" +
                    "<br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/>" +
                    "<br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/>" +
                    "<br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/>" +
                    "<br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/>" +
                    "<br/><br/>thrown by: <b></b>: <br/><br/><br/>thrown by: <b></b>: <br/><br/><br/><br/>" +
                    "... and more 2 exceptions not printed");
                this.resetCounters();
                assert.patternEqual(
                    (<any>ExceptionsManager).exceptionList.ToString(),
                    "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                    "<span onclick=\"" +
                    "document.getElementById(\'ContentBlock_0\').style.display=" +
                    "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                    "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                    "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                    "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 5 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 6 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 7 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 8 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 9 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 10 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 11 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 12 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 13 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 14 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 15 ]&nbsp;&nbsp;&nbsp;&nbsp;<br/><br/>" +
                    "[ 16 ]&nbsp;&nbsp;&nbsp;&nbsp;error message exception<br/>" +
                    "<span onclick=\"" +
                    "document.getElementById(\'ContentBlock_1\').style.display=" +
                    "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
                    "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Stack trace</span>" +
                    "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
                    "Error: error message exception<br/>    " +
                    "at ExceptionsManagerTest.testToString20 (*)<br/>" +
                    "    at ExceptionsManagerTest.UnitTestRunner.handleWuiExceptions (*)<br/>" +
                    "    at Test.test (*)<br/>" +
                    "    at *<br/>" +
                    "    at *<br/>" +
                    "    at runCallbacks (*)<br/>" +
                    "    at *<br/>" +
                    "    at run (*)<br/>" +
                    "    at *<br/>" +
                    "    at _combinedTickCallback (internal/process/next_tick.js:*)</span><br/></span>");
                (<any>ExceptionsManager).exceptionList = storage;
            }
        }

        public __IgnoretestToString30() : void {
            const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
            const storage : any = (<any>ExceptionsManager).exceptionList;
            (<any>ExceptionsManager).exceptionList = exceptionList;

            assert.equal(ExceptionsManager.ToString(), "");
            assert.equal((<any>ExceptionsManager).exceptionList.ToString(),
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i>" +
                " <span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "Data object <b>EMPTY</b></span>");
            (<any>ExceptionsManager).exceptionList = storage;
        }

        public __IgnoretestHandleException10() : void {
            const error : Error = new Error("this is my message");
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("test", "key");
            this.getHttpManager().ReloadTo("/some/other/link", data, true);
            ExceptionsManager.HandleException(error);
            this.resetCounters();
            assert.patternEqual(ExceptionsManager.ToString(""), "");
        }

        protected setUp() : void {
            ExceptionsManager.Clear();
            this.resetCounters();
        }

        protected tearDown() : void {
            ExceptionsManager.Clear();
        }
    }
}
