/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Exceptions.Type {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ExceptionCode = Com.Wui.Framework.Commons.Enums.ExceptionCode;

    class MockBaseObject extends BaseObject {
    }

    export class ExceptionTest extends UnitTestRunner {

        public testConstructor() : void {
            assert.doesNotThrow(() : void => {
                const instance : Exception = new Exception();
            });
            const exception : Exception = new Exception("message");
            assert.equal(exception.Message(), "message");
        }

        public testCode() : void {
            const exception : Exception = new Exception();
            assert.equal(exception.Code(), ExceptionCode.GENERAL);
            assert.equal(exception.Code(ExceptionCode.EXIT), ExceptionCode.EXIT);
        }

        public testMessage() : void {
            const exception : Exception = new Exception();
            assert.equal(exception.Message("test of exception"), "test of exception");
        }

        public testStack() : void {
            const exception : Exception = new Exception();
            assert.equal(exception.Stack("stack"), "stack");
        }

        public testOwner() : void {
            const exception : Exception = new Exception();
            assert.equal(exception.Owner(), "");
            exception.Owner("testOwner");
            assert.equal(exception.Owner(), "testOwner");
            const owner : BaseObject = new MockBaseObject();
            exception.Owner(owner);
            assert.equal(exception.Owner(), owner.getClassName());
        }

        public testLine() : void {
            const exception : Exception = new Exception();
            assert.equal(exception.Line(25), 25);
        }

        public testToString() : void {
            const exception : Exception = new Exception();
            exception.File("exception.txt");
            exception.Line(5);
            assert.equal(exception.ToString("", true), "<br/>file: exception.txt<br/>at line: 5<br/>");

            exception.File("testing.txt");
            exception.Line(6);
            assert.equal(exception.ToString("", false), ", file: testing.txt, at line: 6");
        }

        public testtoString() : void {
            const exception : Exception = new Exception();
            assert.equal(exception.toString(), "<br/>");
        }
    }
}
