/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import LogLevel = Com.Wui.Framework.Commons.Enums.LogLevel;
    import HttpResolver = Com.Wui.Framework.Commons.HttpProcessor.HttpResolver;

    class MockErrorLoader extends UnitTestLoader {
        protected initResolver() : HttpResolver {
            throw new Error("test");
        }
    }

    export class LoaderTest extends UnitTestRunner {
        public testInfoLevelInProduction() : void {
            LogIt.setLevel(LogLevel.INFO);
            UnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-commons", version: "1.0.0"
            });
            assert.equal((<any>LogIt).level, LogLevel.ERROR);
        }

        public testDebugLevel() : void {
            LogIt.setLevel(LogLevel.DEBUG);
            UnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString(), type: "dev"}, name: "com-wui-framework-commons", version: "1.0.0"
            });
            assert.equal((<any>LogIt).level, LogLevel.DEBUG);
        }

        public testErrorLevelForProd() : void {
            LogIt.setLevel(LogLevel.DEBUG);
            UnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-commons", version: "1.0.0"
            });
            assert.equal((<any>LogIt).level, LogLevel.ERROR);
        }

        public testLoadWithWarningLevel() : void {
            LogIt.setLevel(LogLevel.WARNING);
            UnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString(), type: "dev"}, name: "com-wui-framework-commons", version: "1.0.0"
            });
            assert.equal((<any>LogIt).level, LogLevel.WARNING);
        }

        public __IgnoretestLoadException() : void {
            assert.doesNotThrow(() : void => {
                MockErrorLoader.Load(<any>{
                    build: {time: new Date().toTimeString(), type: "dev"}, name: "com-wui-framework-commons", version: "1.0.0"
                });
            });
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
