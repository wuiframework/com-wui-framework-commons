/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events.Args {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;

    export class EventArgsTest extends UnitTestRunner {

        public testConstructor() : void {
            const args : EventArgs = new EventArgs();
            assert.equal(args.Owner(BaseObject), BaseObject);
        }

        public testOwner() : void {
            const args : EventArgs = new EventArgs();
            assert.equal(args.Owner("owner"), "owner");
        }

        public testType() : void {
            const args : EventArgs = new EventArgs();
            assert.equal(args.Type(), "");
            args.Type(EventType.BEFORE_LOAD);
            assert.equal(args.Type(), EventType.BEFORE_LOAD);
        }

        public testNativeEventArgs() : void {
            const args : EventArgs = new EventArgs();
            const nativeEventArgs : any = {type: "MouseEvent"};
            assert.equal(args.NativeEventArgs(nativeEventArgs), nativeEventArgs);
        }

        public testPreventDefaultOnUndefinedNativeArgs() : void {
            const args : EventArgs = new EventArgs();
            assert.doesNotThrow(() : void => {
                assert.equal(args.NativeEventArgs(), undefined);
                args.PreventDefault();
            });
        }

        public testPreventDefaultWithoutRequiredAPI() : void {
            const args : EventArgs = new EventArgs();
            const nativeEventArgs : any = {
                type: "MouseEvent"
            };
            args.NativeEventArgs(nativeEventArgs);
            assert.equal(args.NativeEventArgs(), nativeEventArgs);
            assert.doesNotThrow(() : void => {
                args.PreventDefault();
            });
            assert.equal(nativeEventArgs.returnValue, false);
        }

        public testPreventDefaultWithRequiredAPI() : void {
            const args : EventArgs = new EventArgs();
            const nativeEventArgs : any = {
                preventDefault() : void {
                    assert.equal(this.type, "MouseEvent");
                },
                type: "MouseEvent"
            };
            args.NativeEventArgs(nativeEventArgs);
            assert.equal(args.NativeEventArgs(), nativeEventArgs);
            assert.doesNotThrow(() : void => {
                args.PreventDefault();
            });
        }

        public testToStringHTML() : void {
            const args : EventArgs = new EventArgs();
            this.resetCounters();
            assert.equal(this.stripInstrumentation(args.ToString()),
                "[Type] EMPTY<br/>" +
                "[NativeEventArgs] NOT DEFINED<br/>" +
                "[PreventDefault] NOT DEFINED<br/>" +
                "[StopAllPropagation] NOT DEFINED<br/>" +
                "[getClassName] Com.Wui.Framework.Commons.Events.Args.EventArgs<br/>" +
                "[getNamespaceName] Com.Wui.Framework.Commons.Events.Args<br/>" +
                "[getClassNameWithoutNamespace] EventArgs<br/>" +
                "[IsMemberOf] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "[Implements] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "[getHash] <i>Object type:</i> number. <i>Return value:</i> -684756430<br/>" +
                "[excludeSerializationData] <i>Object type:</i> array. <i>Return values:</i><br/>" +
                "<i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;objectNamespace<br/>" +
                "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;objectClassName<br/></span><br/>" +
                "[excludeIdentityHashData] <i>Object type:</i> array. <i>Return values:</i><br/>" +
                "<i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_1\').style.display=" +
                "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span><br/>");
        }

        public testToStringPlainText() : void {
            const args : EventArgs = new EventArgs();
            this.resetCounters();
            assert.equal(this.stripInstrumentation(args.ToString("", false)),
                "[Type] EMPTY\r\n" +
                "[NativeEventArgs] NOT DEFINED\r\n" +
                "[PreventDefault] NOT DEFINED\r\n" +
                "[StopAllPropagation] NOT DEFINED\r\n" +
                "[getClassName] Com.Wui.Framework.Commons.Events.Args.EventArgs\r\n" +
                "[getNamespaceName] Com.Wui.Framework.Commons.Events.Args\r\n" +
                "[getClassNameWithoutNamespace] EventArgs\r\n" +
                "[IsMemberOf] Object type: boolean. Return value: false\r\n" +
                "[Implements] Object type: boolean. Return value: false\r\n" +
                "[getHash] Object type: number. Return value: -684756430\r\n" +
                "[excludeSerializationData] Object type: array. Return values: \r\n" +
                "Array object\r\n" +
                "[ 0 ]    objectNamespace\r\n" +
                "[ 1 ]    objectClassName\r\n\r\n" +
                "[excludeIdentityHashData] Object type: array. Return values: \r\nArray object\r\nData object EMPTY\r\n");
        }

        public testtoString() : void {
            const args : EventArgs = new EventArgs();
            this.resetCounters();
            const value : string = this.stripInstrumentation(args.ToString());
            this.resetCounters();
            assert.equal(this.stripInstrumentation(args.toString()), value);
        }
    }
}
