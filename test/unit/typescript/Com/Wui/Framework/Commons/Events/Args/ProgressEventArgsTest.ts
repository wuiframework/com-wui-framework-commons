/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events.Args {
    "use strict";

    export class ProgressEventArgsTest extends UnitTestRunner {

        public testConstructor() : void {
            const event : ProgressEventArgs = new ProgressEventArgs();
            assert.equal(event.getClassName(), "Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs");
        }

        public testRangeStart() : void {
            const event : ProgressEventArgs = new ProgressEventArgs();
            assert.equal(event.RangeStart(6), 6);
        }

        public testRangeEnd() : void {
            const event : ProgressEventArgs = new ProgressEventArgs();
            assert.equal(event.RangeEnd(100), 100);
        }

        public testCurrentValue() : void {
            const event : ProgressEventArgs = new ProgressEventArgs();
            assert.equal(event.CurrentValue(6), 6);
        }
    }
}
