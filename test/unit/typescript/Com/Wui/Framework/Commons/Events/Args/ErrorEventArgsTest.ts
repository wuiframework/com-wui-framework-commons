/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events.Args {
    "use strict";
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;

    export class ErrorEventArgsTest extends UnitTestRunner {

        public testConstructorWithString() : void {
            const error : ErrorEventArgs = new ErrorEventArgs("Error message");
            assert.equal(error.ToString("", false),
                "Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs\r\n" +
                "[\"Message\"] Error message\r\n" +
                "[\"Owner\"] Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs\r\n");
        }

        public testConstructorWithException() : void {
            const error : ErrorEventArgs = new ErrorEventArgs(new Exception());
            assert.equal(error.ToString("", false),
                "Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs\r\n" +
                "[\"Message\"] \r\n" +
                "[\"Owner\"] Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs\r\n");
        }

        public testMessage() : void {
            const error : ErrorEventArgs = new ErrorEventArgs("Error message");
            assert.equal(error.Message(), "Error message");
            error.Message("new error message");
            assert.equal(error.Message(), "new error message");
        }

        public testExceptionFromString() : void {
            const error : ErrorEventArgs = new ErrorEventArgs("test message");
            assert.equal(error.Exception().getClassName(), Exception.ClassName());
            assert.equal(error.Exception().Message(), "test message");
            assert.equal(error.Exception().Line(10), 10);
        }

        public testExceptionFromWuiException() : void {
            const error : ErrorEventArgs = new ErrorEventArgs(new Exception("test message"));
            assert.equal(error.Exception().getClassName(), Exception.ClassName());
            assert.equal(error.Exception().Message(), "test message");
            assert.equal(error.Exception().Stack("info message"), "info message");
        }

        public testExceptionFromNativeError() : void {
            const error : ErrorEventArgs = new ErrorEventArgs(new Error("native test message"));
            assert.equal(error.Exception().getClassName(), Exception.ClassName());
            assert.equal(error.Exception().Message(), "native test message");
            assert.patternEqual(
                error.Exception().ToString("", false),
                "native test message, stack: \r\n" +
                "Error: native test message\r\n" +
                "    at ErrorEventArgsTest.testExceptionFromNativeError (*:*:*)\r\n" +
                "    at *");
        }

        public testExceptionFromNativeErrorWithoutStack() : void {
            const nativeError : Error = new Error("native test message");
            const property : string = "stack";
            delete nativeError[property];
            const error : ErrorEventArgs = new ErrorEventArgs(nativeError);
            assert.equal(error.Exception().getClassName(), Exception.ClassName());
            assert.equal(error.Exception().Message(), "native test message");
            assert.equal(error.Exception().ToString("", false), "native test message");
        }

        public testExceptionFromNull() : void {
            const error : ErrorEventArgs = new ErrorEventArgs(null);
            assert.equal(error.Exception().getClassName(), Exception.ClassName());
            assert.equal(error.Exception().Message(), "");
            assert.equal(error.Exception().Owner("owner"), "owner");
        }

        public testToStringPlainText() : void {
            const error : ErrorEventArgs = new ErrorEventArgs("Error Message");
            assert.equal(error.ToString("__", false),
                "__Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs\r\n" +
                "__[\"Message\"] Error Message\r\n" +
                "__[\"Owner\"] Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs\r\n");
        }

        public testToStringHTML() : void {
            const error : ErrorEventArgs = new ErrorEventArgs("Error Message");
            error.Owner("test");
            assert.equal(error.ToString(),
                "Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs<br/>" +
                "[\"Message\"] Error Message<br/>" +
                "[\"Owner\"] test<br/>");
        }
    }
}
