/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events.Args {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;

    export class HttpRequestEventArgsTest extends UnitTestRunner {

        public testConstructorWithoutPost() : void {
            const args : HttpRequestEventArgs = new HttpRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/");
            assert.equal(args.getClassName(), "Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs");
            assert.equal(args.Url(), "http://localhost:8888/UnitTestEnvironment.js/");
            assert.ok(args.POST().IsEmpty());
        }

        public testConstructorWithPost() : void {
            const data : ArrayList<string> = new ArrayList<string>();
            data.Add("value1", "key1");
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            const args : HttpRequestEventArgs = new HttpRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/", data);
            assert.equal(args.getClassName(), "Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs");
            assert.equal(args.Url(), "http://localhost:8888/UnitTestEnvironment.js/");
            assert.equal(args.POST().Length(), 3);
            assert.equal(args.POST().getItem("key1"), "value1");
            assert.equal(args.POST().getItem("key2"), "value2");
            assert.equal(args.POST().getItem("key3"), "value3");
        }

        public testUrl() : void {
            const args : HttpRequestEventArgs = new HttpRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/");
            assert.equal(args.Url(), "http://localhost:8888/UnitTestEnvironment.js/");
        }

        public testStatus() : void {
            const args : HttpRequestEventArgs = new HttpRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/");
            args.Status(HttpStatusType.SUCCESS);
            assert.equal(args.Status(), HttpStatusType.SUCCESS);
        }

        public testGET() : void {
            const args : HttpRequestEventArgs = new HttpRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/");
            const data : ArrayList<string> = new ArrayList<string>();
            data.Add("value1", "key1");
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            assert.ok(args.GET().IsEmpty());
            args.GET(data);
            assert.deepEqual(args.GET(), data);
        }

        public testPOST() : void {
            const args : HttpRequestEventArgs = new HttpRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/");
            const data : ArrayList<string> = new ArrayList<string>();
            data.Add("value1", "key1");
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            assert.ok(args.POST().IsEmpty());
            args.POST(data);
            assert.deepEqual(args.POST(), data);
        }

        public testToStringHTML() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value1", "key1");
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            const args : HttpRequestEventArgs = new HttpRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/", data);
            args.Owner("test");
            this.resetCounters();
            assert.equal(args.ToString(),
                "Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs<br/>" +
                "[\"Url\"] http://localhost:8888/UnitTestEnvironment.js/<br/>" +
                "[\"Owner\"] test<br/>" +
                "[\"Status\"] 200<br/>" +
                "[\"GET\"] <br/><i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "&nbsp;&nbsp;&nbsp;Data object <b>EMPTY</b></span><br/>[\"POST\"] <br/>" +
                "<i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_1\').style.display=" +
                "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
                "&nbsp;&nbsp;&nbsp;[ \"key1\" ]&nbsp;&nbsp;&nbsp;&nbsp;value1<br/>" +
                "&nbsp;&nbsp;&nbsp;[ \"key2\" ]&nbsp;&nbsp;&nbsp;&nbsp;value2<br/>" +
                "&nbsp;&nbsp;&nbsp;[ \"key3\" ]&nbsp;&nbsp;&nbsp;&nbsp;value3<br/>" +
                "</span>");
        }

        public testToStringPlainText() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("value1", "key1");
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            const args : HttpRequestEventArgs = new HttpRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/", data);
            args.GET(data);
            assert.equal(args.ToString("__", false),
                "__Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs\r\n" +
                "__[\"Url\"] http://localhost:8888/UnitTestEnvironment.js/\r\n" +
                "__[\"Owner\"] Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs\r\n" +
                "__[\"Status\"] 200\r\n" +
                "__[\"GET\"] \r\n" +
                "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                "    [ \"key1\" ]    value1\r\n" +
                "    [ \"key2\" ]    value2\r\n" +
                "    [ \"key3\" ]    value3\r\n\r\n" +
                "__[\"POST\"] \r\n" +
                "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                "    [ \"key1\" ]    value1\r\n" +
                "    [ \"key2\" ]    value2\r\n" +
                "    [ \"key3\" ]    value3\r\n");
        }
    }
}
