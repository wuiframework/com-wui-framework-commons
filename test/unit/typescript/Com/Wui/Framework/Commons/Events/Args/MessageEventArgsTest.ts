/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events.Args {
    "use strict";

    export class MessageEventArgsTest extends UnitTestRunner {

        public testConstructor() : void {
            const event : MessageEventArgs = new MessageEventArgs();
            assert.equal(event.getClassName(), "Com.Wui.Framework.Commons.Events.Args.MessageEventArgs");
        }

        public testNativeEventArgs() : void {
            const event : MessageEventArgs = new MessageEventArgs();
            const messageEvent : any = {origin: "test"};
            event.NativeEventArgs(messageEvent);
            assert.equal(event.NativeEventArgs(), messageEvent);
        }
    }
}
