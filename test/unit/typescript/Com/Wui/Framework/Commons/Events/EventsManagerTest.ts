/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import IEventArgs = Com.Wui.Framework.Commons.Interfaces.IEventArgs;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IEventsManager = Com.Wui.Framework.Commons.Interfaces.IEventsManager;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ExceptionCode = Com.Wui.Framework.Commons.Enums.ExceptionCode;
    import HttpRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs;
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;
    import MessageEventArgs = Com.Wui.Framework.Commons.Events.Args.MessageEventArgs;

    export class EventsManagerTest extends UnitTestRunner {

        public testgetInstanceSingleton() : void {
            assert.ok(EventsManager.getInstanceSingleton().IsMemberOf(EventsManager));
            assert.ok(EventsManager.getInstanceSingleton().Implements(IEventsManager));
        }

        public __IgnoretestBindOnerror() : void {
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_ERROR,
                ($eventArgs : ErrorEventArgs) : void => {
                    assert.equal($eventArgs.Message(), "win error");
                    assert.equal($eventArgs.Exception().File(), "EventsManagerTest.ts");
                    assert.equal($eventArgs.Exception().Code(), ExceptionCode.GENERAL);
                    assert.equal($eventArgs.Exception().Line(), 30);
                });
            window.onerror("win error", "EventsManagerTest.ts", 30, 62);
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.ON_ERROR);
        }

        public __IgnoretestBindOnerror2() : void {
            const error : Error = new Error("Test");
            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_ERROR,
                ($eventArgs : ErrorEventArgs) : void => {
                    assert.equal($eventArgs.Exception().Message(), "Test");
                    assert.patternEqual($eventArgs.Exception().Stack(),
                        "Error: Test\n" +
                        "    at *\n" +
                        "*)");
                });
            window.onerror("Test", "EventsManagerTest.ts", 30, 15, error);
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.ON_ERROR);
        }

        public __IgnoretestBindOnerror3() : void {
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.ON_ERROR);
            EventsManager.getInstanceSingleton();
            assert.equal(window.onerror(ExceptionsManager.ClassName() + ".ExRethrow"), true);
        }

        public __IgnoretestBindOnErrorException() : void {
            assert.doesHandleException(() : void => {
                window.onerror(<any>{});
            }, "this.exception.Message is not a function");
        }

        public __IgnoretestBindOnload() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_LOAD,
                    ($eventArgs : ErrorEventArgs) : void => {
                        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_LOAD);
                        assert.deepEqual($eventArgs.Owner(), $eventArgs);
                        assert.equal($eventArgs.Type(), EventType.ON_LOAD);
                        $done();
                    });
                window.onload(null);
            };
        }

        public __IgnoretestBindOnloadException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.onRedirect(
                    () : void => {
                        const property : string = "ON_LOAD";
                        delete (<any>EventType)[property];
                        window.onload(null);
                    },
                    () : void => {
                        assert.equal(ExceptionsManager.getLast().Message(), "test async exception");
                    },
                    () : void => {
                        (<any>EventType).ON_LOAD = "onload";
                        $done();
                    });
            };
        }

        public __IgnoretestBindOnbeforeunload() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const nativeEventArgs : any = {};
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.BEFORE_REFRESH,
                    ($eventArgs : ErrorEventArgs) : void => {
                        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.BEFORE_REFRESH);
                        assert.equal($eventArgs.Type(), EventType.BEFORE_REFRESH);
                        assert.equal($eventArgs.NativeEventArgs(), nativeEventArgs);
                        $done();
                    });
                window.onbeforeunload(nativeEventArgs);
            };
        }

        public __IgnoretestBindOnbeforeunloadException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.onRedirect(
                    () : void => {
                        const property : string = "BEFORE_REFRESH";
                        delete (<any>EventType)[property];
                        window.onbeforeunload(<any>{});
                    },
                    () : void => {
                        assert.equal(ExceptionsManager.getLast().Message(), "bindOnbeforeunload Exception");
                    },
                    () : void => {
                        (<any>EventType).BEFORE_REFRESH = "beforerefresh";
                        $done();
                    });
            };
        }

        public __IgnoretestBindOnhashchange() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                window.onhashchange = null;
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST,
                    ($eventArgs : HttpRequestEventArgs) : void => {
                        assert.equal($eventArgs.Type(), EventType.ON_HTTP_REQUEST);
                        this.initSendBox();
                        $done();
                    });
                window.onhashchange(<any>{});
            };
        }

        public __IgnoretestBindOnhashchange2() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                window.onhashchange = null;
                this.setUserAgent(BrowserType.INTERNET_EXPLORER, true);
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST,
                    ($eventArgs : HttpRequestEventArgs) : void => {
                        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST);
                        assert.equal($eventArgs.Type(), EventType.ON_HTTP_REQUEST);
                        this.initSendBox();
                        $done();
                    });
                window.onhashchange(<any>{});
            };
        }

        public __IgnoretestBindOnpopstate() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                delete window.onhashchange;
                window.onpopstate = null;
                this.setUserAgent(BrowserType.INTERNET_EXPLORER, false);
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST,
                    ($eventArgs : HttpRequestEventArgs) : void => {
                        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST);
                        delete window.onpopstate;
                        window.onhashchange = null;
                        EventsManager.getInstanceSingleton();
                        assert.equal($eventArgs.Type(), EventType.ON_HTTP_REQUEST);
                        this.initSendBox();
                        $done();
                    });
                window.onpopstate(<any>{});
            };
        }

        public __IgnoretestBindHttpRequest() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                delete window.onhashchange;
                delete window.onpopstate;
                assert.ok(!ObjectValidator.IsSet(window.onhashchange));
                assert.ok(!ObjectValidator.IsSet(window.onpopstate));
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST,
                    ($eventArgs : HttpRequestEventArgs) : void => {
                        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST);
                        ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST);
                        window.onhashchange = null;
                        EventsManager.getInstanceSingleton();
                        assert.equal($eventArgs.Type(), EventType.ON_HTTP_REQUEST);
                        $done();
                    });
            };
        }

        public __IgnoretestBindOnmessage() : void {
            const messageEvent : any = {data: "EventFactory", origin: "Message"};

            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_MESSAGE,
                ($eventArgs : MessageEventArgs) : void => {
                    assert.equal($eventArgs.Type(), EventType.ON_MESSAGE);
                    assert.deepEqual($eventArgs.NativeEventArgs(), messageEvent);
                });
            window.onmessage(messageEvent);
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.ON_MESSAGE);
        }

        public __IgnoretestBindOnmessage2() : void {
            let messageEvent : any; // tslint:disable-line

            EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.BODY, EventType.ON_MESSAGE,
                ($eventArgs : MessageEventArgs) : void => {
                    assert.equal($eventArgs.Type(), EventType.ON_MESSAGE);
                    assert.equal($eventArgs.NativeEventArgs(), undefined);
                });
            window.onmessage(messageEvent);
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.BODY, EventType.ON_MESSAGE);
        }

        public testsetEvent() : void {
            const manager : EventsManager = new EventsManager();
            manager.setEvent("test", "type", () : void => {
                LogIt.Info("message 1");
            });
            manager.setEvent("test", "type", () : void => {
                LogIt.Info("message 2");
            });
            const args : EventArgs = new EventArgs();
            const handler : any = () : void => {
                LogIt.Info("handler script");
            };
            manager.setEvent("test1", "type1", handler, args);

            assert.equal(manager.getAll().Length(), 2);
            assert.equal(manager.getAll().getItem("test").Length(), 1);
            assert.equal(manager.getAll().getItem("test1").Length(), 1);
            assert.equal(manager.getAll().getItem("test").getItem("type").Length(), 2);
            assert.equal(manager.getAll().getItem("test1").getItem("type1").Length(), 1);
            assert.ok(manager.SerializationData(), "eventsList");
        }

        public testFireEventSync() : void {
            const manager : EventsManager = new EventsManager();
            const args : EventArgs = new EventArgs();
            args.Owner("syncTestOwner");
            const handler : any = ($eventArgs : EventArgs) : void => {
                assert.equal($eventArgs.Type(), EventType.ON_ERROR);
                assert.equal($eventArgs.Owner(), "syncTestOwner");
                assert.equal($eventArgs.NativeEventArgs(), (<any>EventArgs).args);
            };

            manager.setEvent("syncTestOwner", EventType.ON_ERROR, handler);
            manager.FireEvent("syncTestOwner", EventType.ON_ERROR, args, false);
            manager.FireEvent("syncTestOwner", EventType.ON_ERROR, false);
            assert.throws(() : void => {
                manager.setEvent(null, null, handler, null);
                manager.FireEvent(null, null, null, null);
            }, /Cannot read property 'KeyExists' of null/);
        }

        public testFireEventAsync() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const manager : EventsManager = new EventsManager();
                const args : EventArgs = new EventArgs();
                const handler : any = ($eventArgs : EventArgs) : void => {
                    assert.equal($eventArgs.Type(), EventType.ON_ERROR);
                    assert.equal($eventArgs.Owner(), args);
                    $done();
                };
                manager.setEvent("asyncTestOwner", EventType.ON_ERROR, handler);
                manager.FireEvent("asyncTestOwner", EventType.ON_ERROR, args);
            };
        }

        public testFireEventAPIoptions() : void {
            const manager : EventsManager = new EventsManager();
            const args : EventArgs = new EventArgs();
            args.Owner("testArgsOwner");
            args.Type("testType");
            manager.setEvent("testArgsOwner", "testType", (args : EventArgs) : void => {
                // mock event handler
            });
            manager.FireEvent("testArgsOwner", "testType");
            manager.FireEvent("testArgsOwner", "testType", args);
            manager.FireEvent("testArgsOwner", "testType", false);
            manager.FireEvent("testArgsOwner", "testType", true);
            manager.FireEvent("testArgsOwner", "testType", args, false);
            manager.FireEvent("testArgsOwner", "testType", args, true);
        }

        public testFireEvent() : void {
            const manager : EventsManager = new EventsManager();
            const args : EventArgs = new EventArgs();

            manager.setEvent("testArgsOwner", "testType", () : void => {
                // mock event handler
            });

            manager.FireEvent("testArgsOwner", "testType");
            manager.FireEvent("testArgsOwner", "testType", args);
            manager.FireEvent("testArgsOwner", "testType", false);
            manager.FireEvent("testArgsOwner", "testType", true);
            manager.FireEvent("testArgsOwner", "testType", args, false);
            manager.FireEvent("testArgsOwner", "testType", args, true);
        }

        public testFireEvent5() : void {
            const manager : EventsManager = new EventsManager();
            const listArgs : ArrayList<IEventArgs> = new ArrayList<IEventArgs>();
            const arg : EventArgs = new EventArgs();
            arg.Owner(GeneralEventOwner.WINDOW);
            arg.Type(EventType.ON_START);
            listArgs.Add(arg, "testType");
            (<any>EventsManager).argsList = listArgs;
            manager.setEvent("testArgsOwner", "testType", () : void => {
                // mock event handler
            });
            assert.equal(manager.Exists("testArgsOwner", "testType"), true);
            manager.FireEvent("testArgsOwner", "testType", arg);
            manager.FireEvent("testArgsOwner", "testType", <any>(new ArrayList<any>()));
        }

        public testFireEvents6() : void {
            const manager : EventsManager = new EventsManager();
            manager.setEvent("testArgsOwner", "testType", ($eventArgs : EventArgs) : void => {
                assert.ok(true);
            });
            const args : ArrayList<IEventArgs> = new ArrayList<IEventArgs>();
            args.Add(null, "testType");
            (<any>manager).argsList.Add(args, "testArgsOwner");
            manager.FireEvent("testArgsOwner", "testType");
        }

        public testFireEvents7() : void {
            const manager : EventsManager = new EventsManager();
            manager.setEvent("testArgsOwner", "testType", ($eventArgs : EventArgs) : void => {
                assert.ok(true);
            });
            const property : string = "argsList";
            delete (<any>manager)[property];
            manager.FireEvent("testArgsOwner", "testType");
            manager.FireEvent("testArgsOwner", "testType");
        }

        public __IgnoretestFireEvents8() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.doesHandleException(
                    () : void => {
                        const manager : EventsManager = new EventsManager();
                        ExceptionsManager.Clear();
                        manager.setEvent("testExceptionOwner", "testType", () : void => {
                            $done();
                            throw new Error("test fire async exception");
                        });
                        manager.FireEvent("testExceptionOwner", "testType");
                    }, "test fire async exception");
            };
        }

        public testRemoveHandler() : void {
            const manager : EventsManager = new EventsManager();
            const handler : any = () : void => {
                LogIt.Info("handler script");
            };
            manager.setEvent("test", "type", handler);

            assert.equal(manager.getAll().getItem("test").getItem("type").Length(), 1);
            manager.RemoveHandler("test", "type", () : void => {
                // mock event handler
            });
            assert.equal(manager.getAll().getItem("test").getItem("type").Length(), 1);
            manager.RemoveHandler("test2", "test", handler);
            assert.equal(manager.getAll().getItem("test").getItem("type").Length(), 1);

            manager.RemoveHandler("test", "type", handler);
            assert.equal(manager.getAll().getItem("test").getItem("type").Length(), 0);
        }

        public testClear() : void {
            const manager : EventsManager = new EventsManager();
            const args : EventArgs = new EventArgs();
            const handler : any = ($eventArgs : EventArgs) : void => {
                args.Owner(GeneralEventOwner.WINDOW);
                args.Type(EventType.ON_START);
                assert.equal(GeneralEventOwner.WINDOW, "window");
                assert.equal(EventType.ON_START, "onstart");
            };

            manager.setEvent("test1", "type1", handler);
            manager.setEvent("test1", "type2", handler);
            manager.setEvent("test1", "type3", handler);
            manager.setEvent("test2", "type1", handler);
            manager.setEvent("test3", "type1", handler);
            manager.setEvent("45", "type", handler);

            assert.equal(manager.getAll().getItem("test1").Length(), 3);
            manager.Clear("test1", "type1");
            assert.equal(manager.getAll().getItem("test1").Length(), 2);
            manager.Clear("test1");
            assert.equal(manager.getAll().KeyExists("test1"), false);
            manager.Clear(45);
            assert.equal(manager.getAll().KeyExists(45), false);
            manager.Clear();
            assert.equal(manager.getAll().Length(), 0);
        }

        public testClearSpecificThread() : void {
            const manager : EventsManager = new EventsManager();
            const threadNumber : number = manager.FireAsynchronousMethod(() : void => {
                // generate thread number
            });
            manager.Clear(threadNumber);
        }

        public testToString() : void {
            const manager : EventsManager = new EventsManager();
            const handler : any = () : void => {
                // mock event handler
            };
            manager.setEvent("test1", "type1", handler);
            assert.equal(manager.toString(), "Registered events list:<br/>&nbsp;&nbsp;&nbsp;" +
                "[\"test1\"][\"type1\"] hooked handlers count: 1<br/>");
            assert.equal(manager.ToString("", false), "Registered events list:\r\n    [\"test1\"][\"type1\"]" +
                " hooked handlers count: 1\r\n");
        }

        public testsetEventArgs() : void {
            const manager : EventsManager = new EventsManager();
            const args : EventArgs = new EventArgs();
            args.Owner();
            delete (<any>args).owner;
            args.Type("ON_LOAD");
            manager.setEvent("test0", "type0");
            assert.ok(manager.Exists("test0", "type0"));
            assert.ok(ObjectValidator.IsEmptyOrNull(args.Owner()));
            manager.setEventArgs("test0", "type0", args);
            manager.setEventArgs("", "", args);
            manager.setEventArgs("test1", "type1", args);
        }

        public testFireAsynchronousMethod() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const manager : EventsManager = new EventsManager();
                const handler : any = ($eventArgs : EventArgs) : void => {
                    // mock handler
                };

                manager.setEvent("Owner", EventType.ON_START, handler);
                manager.FireAsynchronousMethod(handler);
                manager.FireAsynchronousMethod(handler, 50);
                manager.FireAsynchronousMethod(handler, true);
                manager.FireAsynchronousMethod(handler, true, 50);
                manager.FireAsynchronousMethod(handler, false);
                manager.FireAsynchronousMethod(() : void => {
                    $done();
                }, false, 50);
            };
        }

        public testFireAsynchronousMethod2() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const manager : EventsManager = new EventsManager();
                const handler : any = () : void => {
                    // function do nothing
                };
                manager.setEvent("owner", "type1", handler);
                const property : string = "threadsRegister";
                delete (<any>manager)[property];
                manager.FireAsynchronousMethod(handler, true, 50);
                manager.FireAsynchronousMethod(handler, true, 50);
                assert.equal(manager.Exists("owner", "type1"), true);
                $done();
            };
        }

        public testFireAsynchronousMethodThreadNumber() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const manager : EventsManager = new EventsManager();
                const threadNumber : number = manager.FireAsynchronousMethod(() : void => {
                    // generate thread number
                }, 100);
                assert.doesNotThrow(() : void => {
                    clearTimeout(threadNumber);
                });
                $done();
            };
        }

        public __IgnoretestFireAsynchronousMethodException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.onRedirect(
                    () : void => {
                        const manager : EventsManager = new EventsManager();
                        ExceptionsManager.Clear();
                        manager.FireAsynchronousMethod(() : void => {
                            throw new Error("test async exception");
                        });
                    },
                    () : void => {
                        assert.equal(ExceptionsManager.getLast().Message(), "test async exception");
                    },
                    $done);
            };
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
