/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events {
    "use strict";
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;
    import HttpManager = Com.Wui.Framework.Commons.HttpProcessor.HttpManager;

    export class ThreadPoolTest extends UnitTestRunner {
        private tmpHref : string;

        public testAddThread() : void {
            assert.equal(ThreadPool.IsRunning("owner1", "type1"), false);
            ThreadPool.AddThread("owner1", "type1", () : void => {
                ThreadPool.RemoveThread("owner1", "type1");
            });
            assert.equal(ThreadPool.IsRunning("owner1", "type1"), true);
        }

        public testRemoveThread() : void {
            assert.equal(ThreadPool.IsRunning("owner2", "type2"), false);
            ThreadPool.AddThread("owner2", "type2", () : void => {
                // mock thread callback
            });
            assert.equal(ThreadPool.IsRunning("owner2", "type2"), true);
            ThreadPool.RemoveThread("owner2", "type2");
            assert.equal(ThreadPool.IsRunning("owner2", "type2"), false);
        }

        public testIsRunning() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.equal(ThreadPool.IsRunning("owner3", "type3"), false);
                ThreadPool.AddThread("owner3", "type3", () : void => {
                    assert.equal(ThreadPool.IsRunning("owner3", "type3"), true);
                    this.initSendBox();
                    $done();
                });
                ThreadPool.Execute();
            };
        }

        public testIsNotRunning() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.equal(ThreadPool.IsRunning(GeneralEventOwner.WINDOW, "myEventType1"), false);
                assert.equal(ThreadPool.IsRunning(GeneralEventOwner.WINDOW, "myEventType2"), false);
                ThreadPool.AddThread(GeneralEventOwner.WINDOW, "myEventType2", () : void => {
                    assert.equal(ThreadPool.IsRunning(GeneralEventOwner.WINDOW, "myEventType1"), false);
                    assert.equal(ThreadPool.IsRunning(GeneralEventOwner.WINDOW, "myEventType2"), true);
                    ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, "myEventType2");
                    this.initSendBox();
                    $done();
                });
                ThreadPool.Execute();
            };
        }

        public __IgnoretestsetThreadArgs() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const args : EventArgs = new EventArgs();
                ThreadPool.AddThread(GeneralEventOwner.WINDOW, "myEventType3", ($eventArgs : EventArgs) : void => {
                    ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, "myEventType3");
                    assert.deepEqual($eventArgs, args);
                    $done();
                });
                ThreadPool.setThreadArgs(GeneralEventOwner.WINDOW, "myEventType3", args);
                ThreadPool.Execute();
            };
        }

        public testClear() : void {
            assert.doesNotThrow(() : void => {
                ThreadPool.Clear();
            });
        }

        protected before() : void {
            this.tmpHref = window.location.href;
            this.setUrl("http://localhost.wuiframework.com/index.html#/project-name/unit/ThreadPoolTest");
            const manager : HttpManager = new HttpManager(new HttpRequestParser("project-name"));
        }

        protected after() : void {
            ThreadPool.RemoveThread("owner", "type1");
            ThreadPool.RemoveThread("owner", "type2");
            ThreadPool.RemoveThread("owner", "type3");
            ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, "myEventType1");
            ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, "myEventType2");
            ThreadPool.RemoveThread(GeneralEventOwner.WINDOW, "myEventType3");

            this.setUrl(this.tmpHref);
            const manager : HttpManager = new HttpManager(new HttpRequestParser());
        }
    }
}
