/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events.Args {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export class AsyncRequestEventArgsTest extends UnitTestRunner {

        public testConstructorWithoutPost() : void {
            const async : AsyncRequestEventArgs = new AsyncRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/");
            assert.equal(async.getClassName(), "Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs");
            assert.equal(async.Url(), "http://localhost:8888/UnitTestEnvironment.js/");
            assert.ok(async.POST().IsEmpty());
        }

        public testConstructorWithPost() : void {
            const data : ArrayList<string> = new ArrayList<string>();
            data.Add("value1", "key1");
            data.Add("value2", "key2");
            data.Add("value3", "key3");
            const async : AsyncRequestEventArgs = new AsyncRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/", data);
            assert.equal(async.getClassName(), "Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs");
            assert.equal(async.Url(), "http://localhost:8888/UnitTestEnvironment.js/");
            assert.equal(async.POST().Length(), 3);
            assert.equal(async.POST().getItem("key1"), "value1");
            assert.equal(async.POST().getItem("key2"), "value2");
            assert.equal(async.POST().getItem("key3"), "value3");
        }

        public testResult() : void {
            const async : AsyncRequestEventArgs = new AsyncRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/");
            async.Result(true);
            assert.equal(async.Result(), true);
        }
    }
}
