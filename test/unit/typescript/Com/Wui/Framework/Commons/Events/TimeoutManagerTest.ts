/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events {
    "use strict";
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;

    export class TimeoutManagerTest extends UnitTestRunner {
        private timeout : TimeoutManager;

        public testgetId() : void {
            const timeout : TimeoutManager = new TimeoutManager();
            assert.notEqual(this.timeout.getId(), timeout.getId());
        }

        public testLength() : void {
            assert.equal(this.timeout.Length(), 0);
        }

        public testExecute() : void {
            assert.ok(this.timeout.Execute(), this.timeout.getId());
        }

        public __IgnoretestExecuteWithException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.onRedirect(
                    () : void => {
                        this.timeout.Add(() : void => {
                            throw new Error("test async exception");
                        });
                        this.timeout.Execute();
                    },
                    () : void => {
                        assert.equal(ExceptionsManager.getLast().Message(), "test async exception");
                    },
                    $done);
            };
        }

        protected before() : void {
            this.timeout = new TimeoutManager();
        }
    }
}
