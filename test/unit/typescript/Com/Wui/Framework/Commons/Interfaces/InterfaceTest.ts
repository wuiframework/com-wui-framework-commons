/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* tslint:disable: variable-name */
/* tslint:disable: no-use-before-declare only-arrow-functions */
namespace Com.Wui.Framework.Commons.Interfaces.Test {
    "use strict";
    export let IMockTestInterface : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnTest",
                "OnClose"
            ]);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces.Test {
    "use strict";
    export let IMockTestInterface2 : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnTest",
                "Add",
                "GetAll",
                "Contains"
            ], IMockTestInterface);
        }();
}
/* tslint:enable */

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    import IMockTestInterface = Com.Wui.Framework.Commons.Interfaces.Test.IMockTestInterface;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import IMockTestInterface2 = Com.Wui.Framework.Commons.Interfaces.Test.IMockTestInterface2;
    import Constants = Com.Wui.Framework.Commons.Enums.SyntaxConstants;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class InterfaceTest extends UnitTestRunner {

        public testgetInstance() : void {
            if (!ObjectValidator.IsSet((<any>Reflection).singleton)) {
                assert.equal(IMockTestInterface.ClassName(), ".");
                assert.equal(IMockTestInterface.NamespaceName(), "");
                assert.equal(IMockTestInterface.ClassNameWithoutNamespace(), "");
                assert.equal(IMockTestInterface.ClassNameWithoutNamespace(), "");
            }

            delete (<any>Reflection).singleton;
            Reflection.getInstance();

            assert.equal(IMockTestInterface.ClassName(), "Com.Wui.Framework.Commons.Interfaces.Test.IMockTestInterface");
            assert.equal(IMockTestInterface.NamespaceName(), "Com.Wui.Framework.Commons.Interfaces.Test");
            assert.equal(IMockTestInterface.ClassNameWithoutNamespace(), "IMockTestInterface");

            const output : string[] = [];
            let parameterIndex : number = 0;
            let parameter : any;
            for (parameter in IMockTestInterface2) {
                if (typeof IMockTestInterface2[parameter] !== Constants.FUNCTION) {
                    output[parameterIndex++] = parameter;
                }
            }

            assert.deepEqual(output, [
                "classNamespace",
                "className",
                "OnTest",
                "OnClose",
                "Add",
                "GetAll",
                "Contains"
            ]);
        }

        public testClassName() : void {
            assert.deepEqual(Interface.getInstance([], IBaseObject).ClassName(), "Com.Wui.Framework.Commons.Interfaces.IBaseObject");

            const inter : Interface = new Interface();
            assert.equal(inter.ClassName(), null);
        }

        public testNamespaceName() : void {
            assert.deepEqual(Interface.getInstance([], IBaseObject).NamespaceName(), "Com.Wui.Framework.Commons.Interfaces");

            const inter : Interface = new Interface();
            assert.equal(inter.NamespaceName(), null);
        }

        public testClassNameWithoutNamespace() : void {
            assert.deepEqual(Interface.getInstance([], IBaseObject).ClassNameWithoutNamespace(), "IBaseObject");

            const inter : Interface = new Interface();
            assert.equal(inter.ClassNameWithoutNamespace(), null);
        }

        public testToString() : void {
            const inter : Interface = new Interface();
            assert.equal(inter.ToString(), null);
        }

        public testtoString() : void {
            const inter : Interface = new Interface();
            assert.equal(inter.toString(), null);
        }
    }
}
