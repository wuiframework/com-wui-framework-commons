/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Connectors {
    "use strict";
    import IWuiBuilderEvents = Com.Wui.Framework.Commons.Interfaces.Events.IWuiBuilderEvents;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    export class WuiBuilderConnectorTest extends UnitTestRunner {

        public testConnect() : void {
            const singleton : WuiBuilderConnector = WuiBuilderConnector.Connect();
            assert.equal(WuiBuilderConnector.Connect(), singleton);
            EventsManager.getInstanceSingleton().Clear(singleton.getId().toString());
        }

        public testConstructor() : void {
            assert.doesNotThrow(() : void => {
                const instance : WuiBuilderConnector = new WuiBuilderConnector();
                EventsManager.getInstanceSingleton().Clear(instance.getId().toString());
            });
            assert.doesNotThrow(() : void => {
                const instance2 : WuiBuilderConnector = new WuiBuilderConnector(false);
                EventsManager.getInstanceSingleton().Clear(instance2.getId().toString());
            });
        }

        public testgetEvents() : void {
            const connect : WuiBuilderConnector = new WuiBuilderConnector();
            const events : IWuiBuilderEvents = connect.getEvents();
            const types : string[] = [];
            let property : string;
            for (property in events) {
                if (events.hasOwnProperty(property)) {
                    types.push(property);
                }
            }
            assert.deepEqual(types, [
                "OnClose",
                "OnError",
                "OnStart",
                "OnTimeout",
                "OnBuildStart",
                "OnBuildComplete",
                "OnWarning",
                "OnFail",
                "OnMessage"
            ]);
            EventsManager.getInstanceSingleton().Clear(connect.getId().toString());
        }

        public testgetId() : void {
            const connect : WuiBuilderConnector = new WuiBuilderConnector(true);
            assert.ok(connect.getId(), "1924497448");
            EventsManager.getInstanceSingleton().Clear(connect.getId().toString());
        }

        public __IgnoretestSend() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const connect : WuiBuilderConnector = new WuiBuilderConnector(true);
                connect.Send("Cmd", {
                    args: ["--version"],
                    cmd : "wui"
                }, ($data : any) : void => {
                    assert.equal($data.data, "WzBd");
                    assert.equal($data.status, 200);
                    assert.equal($data.type, "Cmd");
                    (<any>connect).client.StopCommunication();
                    EventsManager.getInstanceSingleton().Clear(connect.getId().toString());
                    $done();
                });
            };
        }

        public testAddEventListener() : void {
            const connect : WuiBuilderConnector = new WuiBuilderConnector(true);
            const handler : any = () : void => { // tslint:disable-line
            };
            connect.AddEventListener("AddEventListener", handler);
            connect.AddEventListener("AddEventListener", handler);
            EventsManager.getInstanceSingleton().Clear(connect.getId().toString());
        }

        public testRemoveEventListener() : void {
            const connect : WuiBuilderConnector = new WuiBuilderConnector(true);
            const handler : any = () : void => {
                let event : IWuiBuilderEvents; // tslint:disable-line
            };
            connect.AddEventListener("Listener", handler);
            connect.AddEventListener("Event", handler);
            connect.RemoveEventListener("Event");
            connect.RemoveEventListener("Try");
            EventsManager.getInstanceSingleton().Clear(connect.getId().toString());
        }
    }
}
