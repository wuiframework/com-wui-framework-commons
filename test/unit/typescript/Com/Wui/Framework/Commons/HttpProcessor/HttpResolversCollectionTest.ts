/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.HttpProcessor {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import BaseHttpResolver = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver;

    export class HttpResolversCollectionTest extends UnitTestRunner {

        public testConstructor() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("parent", "parentClass");
            assert.equal(collection.Pattern(), "parent");
            assert.equal(collection.ResolverClassName(), "parentClass");
            assert.equal(collection.ParameterName(), null);
            assert.equal(collection.IgnoreCase(), true);
            assert.equal(collection.LevelsCount(), "0");
            assert.ok(collection.getChildrenList().IsEmpty());

            assert.throws(() : void => {
                const instance : HttpResolversCollection = new HttpResolversCollection(null, null);
            }, /HttpKeyPattern can not be null./);
        }

        public testPattern() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            assert.equal(collection.Pattern(), "pattern");
        }

        public testContains() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.AddChild(new HttpResolversCollection("child"));
            assert.ok(collection.Contains("child"));
            assert.equal(collection.Contains("other"), false);
        }

        public testResolverClassName() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern", "className");
            assert.equal(collection.ResolverClassName(), "className");
        }

        public testParameterName() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.ParameterName("parameter");
            assert.equal(collection.ParameterName(), "parameter");
        }

        public testIgnoreCase() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.IgnoreCase(true);
            assert.equal(collection.IgnoreCase(), true);
        }

        public testLevelsCountFlexible() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.LevelsCount(254, true);
            assert.equal(collection.LevelsCount(), "254+");
        }

        public testLevelsCountNonFlexible() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.LevelsCount(254, false);
            assert.equal(collection.LevelsCount(), "254");
        }

        public testLevelsCountLastNumber() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.LevelsCount("253+", true);
            collection.LevelsCount("254+", true);
            assert.equal(collection.LevelsCount(), "254+");
        }

        public testLevelsCountFromString() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.LevelsCount("fifty", true);
            assert.equal(collection.LevelsCount(), "0+");
        }

        public testLevelsCountFromNullNumber() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.LevelsCount(0, true);
            assert.equal(collection.LevelsCount(), "0+");
        }

        public testLevelsCountFromNullString() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.LevelsCount("", true);
            assert.equal(collection.LevelsCount(), "0");
        }

        public testLevelsCountFromNonNumberFlexible() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.LevelsCount("456test", true);
            assert.equal(collection.LevelsCount(), "456+");
        }

        public testLevelsCountFromNonNumber() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            collection.LevelsCount("456test", false);
            assert.equal(collection.LevelsCount(), "456");
        }

        public testAddChild() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            assert.equal(collection.getChildrenList().IsEmpty(), true);
            collection.AddChild(new HttpResolversCollection("child"));
            assert.equal(collection.getChildrenList().Length(), 1);
            collection.AddChild(null);
            assert.equal(collection.getChildrenList().Length(), 1);
        }

        public testgetChildrenList() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            const child : HttpResolversCollection = new HttpResolversCollection("child");
            collection.AddChild(child);
            assert.equal(collection.getChildrenList().Length(), 1);
            assert.deepEqual(collection.getChildrenList().getFirst(), child);
        }

        public testgetChild() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern");
            const child : HttpResolversCollection = new HttpResolversCollection("child");
            collection.AddChild(child);
            assert.deepEqual(collection.getChild("child"), child);
            assert.equal(collection.getChild("other"), null);
        }

        public testCollection() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("web");
            const collection2 : HttpResolversCollection = new HttpResolversCollection("test");
            collection2.AddChild(new HttpResolversCollection("*te*st*"));
            collection.AddChild(collection2);
            collection.AddChild(new HttpResolversCollection("/7t77est7"));
            collection.AddChild(new HttpResolversCollection("6te6st6"));
            collection.AddChild(new HttpResolversCollection("**"));
            collection.AddChild(new HttpResolversCollection("t*est"));
            collection.AddChild(new HttpResolversCollection("te*st"));
            collection.AddChild(new HttpResolversCollection("tes*t"));
            collection.AddChild(new HttpResolversCollection("test*"));
            collection.AddChild(new HttpResolversCollection("*"));
            collection.getChild("6te6st6").AddChild(new HttpResolversCollection("8t88est8"));
            collection.getChild("6te6st6").AddChild(new HttpResolversCollection("7te7st7"));
            collection.getChild("6te6st6").AddChild(new HttpResolversCollection("test"));
            collection.getChild("te*st").AddChild(new HttpResolversCollection("va*lue"));
            collection.getChild("te*st").AddChild(new HttpResolversCollection("valu*e120"));
            collection.getChild("te*st").AddChild(new HttpResolversCollection("value*2556"));
            collection.getChild("6te6st6").getChild("8t88est8").AddChild(new HttpResolversCollection("5klm6455"));
            collection.getChild("6te6st6").getChild("8t88est8").AddChild(new HttpResolversCollection("ctest"));
            collection.getChild("6te6st6").getChild("8t88est8").AddChild(new HttpResolversCollection("atest"));
            collection.getChild("6te6st6").getChild("8t88est8").AddChild(new HttpResolversCollection("btest"));
            collection.getChild("6te6st6").getChild("8t88est8").AddChild(new HttpResolversCollection("test"));
            collection.getChild("te*st").getChild("va*lue").AddChild(new HttpResolversCollection("golden5gate7"));
            collection.getChild("te*st").getChild("va*lue").AddChild(new HttpResolversCollection("black6hacker6"));
            collection.getChild("te*st").getChild("va*lue").AddChild(new HttpResolversCollection("black6hacker8"));
            collection.OrderChildrenCollection();
            collection.getChild("te*st").OrderChildrenCollection();
            collection.getChild("te*st").getChild("va*lue").OrderChildrenCollection();
            collection.getChild("6te6st6").getChild("8t88est8").OrderChildrenCollection();
        }

        public testOrderChildrenCollection() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("web");
            const collection2 : HttpResolversCollection = new HttpResolversCollection("test");
            collection2.AddChild(new HttpResolversCollection("*te*st*"));
            collection.AddChild(collection2);
            collection.AddChild(new HttpResolversCollection("/7t77est7"));
            collection.AddChild(new HttpResolversCollection("6te6st6"));
            collection.AddChild(new HttpResolversCollection("**"));
            collection.AddChild(new HttpResolversCollection("t*est"));
            collection.AddChild(new HttpResolversCollection("te*st"));
            collection.AddChild(new HttpResolversCollection("tes*t"));
            collection.AddChild(new HttpResolversCollection("test*"));
            collection.AddChild(new HttpResolversCollection("*"));

            assert.deepEqual(collection.getChildrenList().ToString("", false),
                "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                "[ 0 ]    \"test\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                "        [ 0 ]    \"*te*st*\" => resolver not assigned   \r\n" +
                "                ignore case: true\r\n" +
                "                count of levels: 0\r\n" +
                "                parameter name: NULL\r\n" +
                "                child list: EMPTY\r\n" +
                "[ 1 ]    \"/7t77est7\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 2 ]    \"6te6st6\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 3 ]    \"**\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 4 ]    \"t*est\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 5 ]    \"te*st\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 6 ]    \"tes*t\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 7 ]    \"test*\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 8 ]    \"*\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n");
            collection.OrderChildrenCollection();
            assert.deepEqual(collection.getChildrenList().ToString("", false),
                "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                "[ 0 ]    \"/7t77est7\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 1 ]    \"6te6st6\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 2 ]    \"test\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                "        [ 0 ]    \"*te*st*\" => resolver not assigned   \r\n" +
                "                ignore case: true\r\n" +
                "                count of levels: 0\r\n" +
                "                parameter name: NULL\r\n" +
                "                child list: EMPTY\r\n" +
                "[ 3 ]    \"test*\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 4 ]    \"tes*t\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 5 ]    \"te*st\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 6 ]    \"t*est\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 7 ]    \"*\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 8 ]    \"**\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n");

            (<any>HttpResolversCollection).isOrdered = true;
            collection.OrderChildrenCollection();
            assert.deepEqual(collection.getChildrenList().ToString("", false),
                "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n" +
                "[ 0 ]    \"/7t77est7\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 1 ]    \"6te6st6\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 2 ]    \"test\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: Com.Wui.Framework.Commons.Primitives.ArrayList object\r\n        " +
                "[ 0 ]    \"*te*st*\" => resolver not assigned   \r\n" +
                "                ignore case: true\r\n" +
                "                count of levels: 0\r\n" +
                "                parameter name: NULL\r\n" +
                "                child list: EMPTY\r\n" +
                "[ 3 ]    \"test*\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 4 ]    \"tes*t\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 5 ]    \"te*st\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 6 ]    \"t*est\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 7 ]    \"*\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n" +
                "[ 8 ]    \"**\" => resolver not assigned   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n");

            const collection3 : HttpResolversCollection = new HttpResolversCollection("web");
            collection3.OrderChildrenCollection();
            assert.deepEqual(collection3.getChildrenList().ToString("", false),
                "Com.Wui.Framework.Commons.Primitives.ArrayList object\r\nData object EMPTY");

            const collection5 : HttpResolversCollection = new HttpResolversCollection("way");
            collection.getChild("test").AddChild(collection5);
            collection.getChild("test").AddChild(collection5);
            collection.OrderChildrenCollection();
            assert.equal(collection.getChild("test"), collection2);
            assert.equal(collection.getChild("test"), collection2);
            collection.OrderChildrenCollection();
            assert.equal(collection.getChild("test").getChild("way"), collection5);
        }

        public testToString() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("child", BaseHttpResolver);
            this.resetCounters();
            assert.equal(collection.ToString("", true),
                "\"child\" => \"Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver\"&nbsp;&nbsp;&nbsp;" +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_1\').style.display=" +
                "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>ignore case:</i> true<br/>" +
                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>count of levels:</i> 0<br/>" +
                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>parameter name:</i> NULL<br/>" +
                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<i>child list:</i> <i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Data object <b>EMPTY</b></span></span>");
        }

        public testtoString() : void {
            const collection : HttpResolversCollection = new HttpResolversCollection("pattern", BaseHttpResolver.ClassName());
            collection.AddChild(new HttpResolversCollection("child"));
            this.resetCounters();
            const value : string = collection.ToString();
            this.resetCounters();
            assert.equal(collection.toString(), value);
        }
    }
}
