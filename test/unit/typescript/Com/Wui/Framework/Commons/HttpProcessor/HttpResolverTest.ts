/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.HttpProcessor {
    "use strict";
    import HttpRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs;
    import BaseHttpResolver = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver;
    import HttpRequestConstants = Com.Wui.Framework.Commons.Enums.HttpRequestConstants;
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ExceptionErrorPage = Com.Wui.Framework.Commons.ErrorPages.ExceptionErrorPage;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    class MockHttpResolver extends BaseHttpResolver {
        public Process() : void {
            throw new Error("test resolver exception");
        }
    }

    class MockExceptionResolver extends BaseHttpResolver {
        public static asyncProcessor : () => void;

        public Process() : void {
            if (this.RequestArgs().POST().KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
                const exception : Exception = this.RequestArgs().POST().getItem(HttpRequestConstants.EXCEPTIONS_LIST).getLast();
                this.getHttpManager().OverrideResolver(
                    "/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}", MockExceptionResolver);
                assert.equal(exception.Message(), "test resolver exception");
                MockExceptionResolver.asyncProcessor();
            }
            // override process for ability to discard default behavior
        }
    }

    class MockBaseHttpResolver extends BaseHttpResolver {
        public Process() : void {
            assert.equal(this.RequestArgs().GET().Length(), 1);
            assert.equal(this.RequestArgs().GET().getItem("testKey"), "testValue");
            super.Process();
        }
    }

    class MockResolver extends HttpResolver {
        protected getStartupResolvers() : any {
            const resolvers : any = super.getStartupResolvers();
            resolvers["/empty"] = "";
            resolvers["/ServerError/Http/DefaultPage"] = MockHttpResolver;
            resolvers["/ServerError/Exception/{" + HttpRequestConstants.EXCEPTION_TYPE + "}"] = MockExceptionResolver;
            return resolvers;
        }
    }

    class MockException extends ExceptionErrorPage {
        protected getPageBody() : any {
            assert.throws(() : void => {
                this.getClassName();
            }, /Exceptions/);
        }
    }

    export class HttpResolverTest extends UnitTestRunner {

        public testConstructorWithArgs() : void {
            ClientUnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString()}, name: "com-wui-framework-commons", version: "1.0.0"
            });
            window.location.hash = "";
            const instance : HttpResolver = new HttpResolver("projectName/basePath");
            assert.equal(window.location.hash, "/projectName/basePath/");
            assert.equal(instance.getManager().getClassName(), HttpManager.ClassName());
            window.location.hash = "";
        }

        public testResolverRequestAsync() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            const resolver : HttpResolver = new HttpResolver();
            data.Add("http://localhost:8888/baseAddress/UnitTestEnvironment.js#/projectName/pageName/task",
                HttpRequestConstants.EXCEPTIONS_LIST);
            this.setUserAgent(window.navigator.userAgent + " com-wui-framework-jre");
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
            resolver.ResolveRequest(args);

            EventsManager.getInstanceSingleton().FireEvent(GeneralEventOwner.WINDOW, EventType.ON_HTTP_REQUEST, false);
            this.initSendBox();
        }

        public testResolverRequestWithoutArgs() : void {
            assert.doesHandleException(() : void => {
                const resolver : HttpResolver = new HttpResolver();
                resolver.ResolveRequest(null);
            }, "");
            this.initSendBox();
        }

        public testResolverRequestSameUrlTwice() : void {
            assert.doesNotThrow(() : void => {
                const resolver : HttpResolver = new HttpResolver();
                this.setUserAgent(window.navigator.userAgent + " com-wui-framework-jre");
                resolver.ResolveRequest(new HttpRequestEventArgs(this.getRequest().getUrl()));
                this.initSendBox();
            });
        }

        public testResolverRequestCreateDefaultResolver() : void {
            const resolver : HttpResolver = new HttpResolver();
            const child : HttpResolversCollection = resolver.getManager().getResolversCollection().getChildrenList().getFirst()
                .getChild("ServerError").getChild("Http").getChild("DefaultPage");
            const defaultResolverClass : string = child.ResolverClassName();
            resolver.ResolveRequest(new HttpRequestEventArgs("/test/test1"));
            child.ResolverClassName("");
            resolver.ResolveRequest(new HttpRequestEventArgs("/test/test2"));
            child.ResolverClassName(defaultResolverClass);
            this.initSendBox();
        }

        public testrequestResolver() : void {
            assert.doesNotThrow(() : void => {
                const resolver : HttpResolver = new HttpResolver("http://localhost:8888/baseAddress/UnitTestEnvironment.js");
                this.setUserAgent(window.navigator.userAgent + " com-wui-framework-jre");
                resolver.getManager().RegisterResolver("/test/MockBaseHttpResolver", MockBaseHttpResolver);
                assert.equal(resolver.getManager().getResolverClassName("/test/MockBaseHttpResolver"), MockBaseHttpResolver);
                const requestArgs : HttpRequestEventArgs = new HttpRequestEventArgs("/test/MockBaseHttpResolver");
                requestArgs.GET().Add("testValue", "testKey");
                resolver.ResolveRequest(requestArgs);
                this.initSendBox();
            });
        }

        public testtoString() : void {
            const resolver : HttpResolver = new HttpResolver();
            assert.equal(resolver.toString(), "object type of \'Com.Wui.Framework.Commons.HttpProcessor.HttpResolver\'");
        }

        protected tearDown() : void {
            this.initSendBox();
            ExceptionsManager.Clear();
        }
    }
}
