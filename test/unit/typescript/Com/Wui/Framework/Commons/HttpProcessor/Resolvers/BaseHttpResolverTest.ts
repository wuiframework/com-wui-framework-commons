/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.HttpProcessor.Resolvers {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    class MockResolveExit extends BaseHttpResolver {
        protected resolver() : void {
            Echo.Print("MockResolveExit reachable");
            assert.throws(() : void => {
                this.exit();
                Echo.Print("MockResolveExit not reachable");
            }, /Com.Wui.Framework.Commons.Exceptions.ExceptionsManager.Exit/);
        }
    }

    class MockRegisterResolver extends BaseHttpResolver {
        protected resolver() : void {
            Echo.Print("MockRegisterResolver test");
            this.registerResolver("test3", "dynamicResolver", false);
            assert.equal(this.getHttpManager().getResolversCollection().getChild("test3").ToString("", false),
                "\"test3\" => \"dynamicResolver\"   \r\n" +
                "        ignore case: false\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n");
        }
    }

    class MockOverrideResolver extends BaseHttpResolver {
        protected resolver() : void {
            Echo.Print("MockOverrideResolver test");
            this.registerResolver("test", "resolver");
            assert.patternEqual(this.getHttpManager().getResolversCollection().getChild("test").ToString("", false),
                "\"test\" => \"resolver\"   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n");

            this.overrideResolver("test", "resolver2");
            assert.patternEqual(this.getHttpManager().getResolversCollection().getChild("test").ToString("", false),
                "\"test\" => \"resolver2\"   \r\n" +
                "        ignore case: true\r\n" +
                "        count of levels: 0\r\n" +
                "        parameter name: NULL\r\n" +
                "        child list: EMPTY\r\n");
        }
    }

    class MockBaseHttpResolverClass extends BaseHttpResolver {
    }

    export class BaseHttpResolverTest extends UnitTestRunner {

        public testConstructor() : void {
            assert.resolveEqual(MockBaseHttpResolverClass, "" +
                "<head></head>" +
                "<body>" +
                "<div id=\"Content\"><span guitype=\"HtmlAppender\">" +
                "<br>This is abstract BaseHttpResolver, which has been executed at: UnitTestLoader" +
                "</span>" +
                "</div>" +
                "</body>");
            this.initSendBox();
        }

        public testtoString() : void {
            const async : BaseHttpResolver = new MockBaseHttpResolverClass();
            assert.equal(async.toString(), "object type of \'Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver\'");
        }

        public testexit() : void {
            assert.resolveEqual(MockResolveExit, "" +
                "<head></head><body><div id=\"Content\"><span guitype=\"HtmlAppender\">MockResolveExit reachable</span></div></body>");
        }

        public testregisterResolver() : void {
            assert.resolveEqual(MockRegisterResolver, "" +
                "<head></head><body><div id=\"Content\"><span guitype=\"HtmlAppender\">MockRegisterResolver test</span></div></body>");
        }

        public testoverrideResolver() : void {
            assert.resolveEqual(MockOverrideResolver, "" +
                "<head></head>" +
                "<body>" +
                "<div id=\"Content\">" +
                "<span guitype=\"HtmlAppender\">MockOverrideResolver test</span>" +
                "</div>" +
                "</body>");
        }
    }
}
