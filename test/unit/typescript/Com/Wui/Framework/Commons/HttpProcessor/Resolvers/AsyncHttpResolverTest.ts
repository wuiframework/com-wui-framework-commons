/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.HttpProcessor.Resolvers {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    class MockAsyncHttpResolver extends AsyncHttpResolver {
    }

    export class AsyncHttpResolverTest extends UnitTestRunner {

        public testConstructor() : void {
            const async : AsyncHttpResolver = new MockAsyncHttpResolver();
            assert.equal(async.getClassName(), "Com.Wui.Framework.Commons.HttpProcessor.Resolvers.AsyncHttpResolver");
        }

        public __IgnoretestProcess() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const data : ArrayList<any> = new ArrayList<any>();
                data.Add("value1", "key");
                data.Add("value2", "key2");
                const args : AsyncRequestEventArgs = new AsyncRequestEventArgs("http://localhost:8888/UnitTestEnvironment.js/", data);
                EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.ASYNC_REQUEST);
                let started : boolean = false;
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_START,
                    ($eventArgs : AsyncRequestEventArgs) : void => {
                        if (!started) {
                            started = true;
                            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.ASYNC_REQUEST);
                            assert.deepEqual($eventArgs.Owner(), $eventArgs);
                            assert.equal($eventArgs.Type(), EventType.ON_START);
                            $done();
                        }
                    });
                assert.resolveEqual(AsyncHttpResolver, "", args);
            };
        }

        public __IgnoretestProcess2() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const data : ArrayList<string> = new ArrayList<string>();
                data.Add("value2", "key2");
                data.Add("value3", "key3");
                data.Add("value4", "key4");

                const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(this.getRequest().getScriptPath(), data);
                EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.ASYNC_REQUEST);
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_SUCCESS,
                    () : void => {
                        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.ASYNC_REQUEST);
                        assert.ok(false, "Test should be failing!");
                        $done();
                    });
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_ERROR,
                    ($eventArgs : AsyncRequestEventArgs) : void => {
                        EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.ASYNC_REQUEST);
                        assert.equal($eventArgs.Result(),
                            "<h1>Missing async call subscribe data for resolver " +
                            "\"Com.Wui.Framework.Commons.HttpProcessor.Resolvers.AsyncHttpResolver\"!</h1>");
                        $done();
                    });
                assert.resolveEqual(AsyncHttpResolver, "", args);
            };
        }
    }
}
