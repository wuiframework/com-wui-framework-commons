/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.HttpProcessor {
    "use strict";
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;

    export class HttpRequestParserTest extends UnitTestRunner {

        public testgetInstance() : void {
            assert.deepEqual(this.getHttpResolver().CreateRequest("/project/baseUrl"), new HttpRequestParser("/project/baseUrl"));
        }

        public testConstructor() : void {
            let request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.getRelativeRoot(), "");
            request = new HttpRequestParser("/project/baseUrl");
            assert.equal(request.getRelativeRoot(), "/project/baseUrl");
        }

        public testgetUri() : void {
            let request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.getUri(), "http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");

            this.setUrl("http://localhost/UnitTestEnvironment.js");
            window.location.port = "";
            request = new HttpRequestParser();
            assert.equal(request.getUri(), "http://localhost/UnitTestEnvironment.js");
        }

        public testgetUrl() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.getUrl(), "UnitTestLoader");
        }

        public testgetHostUrl() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.getHostUrl(), "http://localhost:8888/UnitTestEnvironment.js");
        }

        public testgetBaseUrl() : void {
            let request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.getBaseUrl(), "http://localhost:8888/UnitTestEnvironment.js/");

            this.setUrl("http://localhost:8888/baseAddress/UnitTestEnvironment.js#/projectName/pageName/task");
            request = new HttpRequestParser();
            assert.equal(request.getBaseUrl(), "http://localhost:8888/baseAddress/UnitTestEnvironment.js/projectName/pageName/");
        }

        public testgetScriptPath() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?urlArg1=test#/projectName/pageName/task");
            const request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.getScriptPath(), "/projectName/pageName/task");
        }

        public testgetUrlArgs() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?sessionid=5&urlArg2=test2#/projectName");
            let request : HttpRequestParser = new HttpRequestParser();
            let args : ArrayList<string> = request.getUrlArgs();
            assert.equal(args.Length(), 2);
            assert.equal(args.getItem("sessionid"), "5");
            assert.equal(args.getItem("urlArg2"), "test2");

            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?urlArg1=test&urlArg2=test2&urlArg3=test3");
            request = new HttpRequestParser();
            args = request.getUrlArgs();
            assert.equal(args.Length(), 3);
            assert.equal(args.getItem("urlArg1"), "test");
            assert.equal(args.getItem("urlArg2"), "test2");
            assert.equal(args.getItem("urlArg3"), "test3");

            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?urlArg1=test&=justValue");
            request = new HttpRequestParser();
            args = request.getUrlArgs();
            assert.equal(args.Length(), 1);
            assert.equal(args.getItem("urlArg1"), "test");
        }

        public testgetRelativeDirectory() : void {
            this.setUrl("http://localhost:8888/baseAddress/UnitTestEnvironment.js#/projectName/pageName/task");
            const request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.getRelativeDirectory(), "/projectName/pageName");
        }

        public testgetRelativeRoot() : void {
            const request : HttpRequestParser = new HttpRequestParser("/projectRoot");
            assert.equal(request.getRelativeRoot(), "/projectRoot");
        }

        public testgetClientIP() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.getClientIP(), "127.0.0.1");
        }

        public testIsOnServer() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.IsOnServer(), true);
            const request2 : HttpRequestParser = new HttpRequestParser();
            assert.equal(request2.IsOnServer(false), true);
            const request3 : HttpRequestParser = new HttpRequestParser();
            assert.equal(request3.IsOnServer(true), false);
        }

        public testIsSearchBot() : void {
            let request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.IsSearchBot(), false);

            this.setUserAgent("some bot client v1.0.0");
            request = new HttpRequestParser();
            assert.equal(request.IsSearchBot(), true);
        }

        public testIsMobileDevice() : void {
            let request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.IsMobileDevice(), false);

            this.setUserAgent("Android Mobile v1.0.0");
            request = new HttpRequestParser();
            assert.equal(request.IsMobileDevice(), true);
        }

        public testIsWuiJre() : void {
            let request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.IsWuiJre(), false);

            this.setUserAgent(window.navigator.userAgent + " com-wui-framework-jre");
            request = new HttpRequestParser();
            assert.equal(request.IsWuiJre(), true);
            assert.equal(request.IsWuiJre(true), false);

            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?debug=JRESimulator");
            request = new HttpRequestParser();
            assert.equal(request.IsWuiJre(true), true);
        }

        public testIsWuiPlugin() : void {
            let request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.IsWuiPlugin(), false);

            this.setUserAgent(window.navigator.userAgent + " com-wui-framework-plugin");
            request = new HttpRequestParser();
            assert.equal(request.IsWuiPlugin(), true);
        }

        public testIsWuiConnector() : void {
            let request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.IsWuiConnector(), false);

            (<MockWindow>window).location.headers = "server: WuiConnector v1.0.0";
            request = new HttpRequestParser();
            assert.equal(request.IsWuiConnector(), false);

            const request2 : HttpRequestParser = new HttpRequestParser();
            assert.equal(request2.IsWuiConnector(false), false);

            const request3 : HttpRequestParser = new HttpRequestParser();
            assert.equal(request3.IsWuiConnector(true), false);
        }

        public testIsIdeaHost() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.IsIdeaHost(), false);
        }

        public testIsCookieEnabled() : void {
            let request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.IsCookieEnabled(), true);
            (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
                return false;
            });
            request = new HttpRequestParser();
            assert.equal(request.IsCookieEnabled(), false);
            (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
                return true;
            });
            request = new HttpRequestParser();
            assert.equal(request.IsCookieEnabled(), true);
        }

        public testgetUserAgent() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            assert.patternEqual(request.getUserAgent(), "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0");
        }

        public testgetBrowserType() : void {
            let request : HttpRequestParser = new HttpRequestParser();
            assert.ok(request.getBrowserType() >= 0);

            this.setUserAgent(
                "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; " +
                "Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; " +
                "Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E)");
            request = new HttpRequestParser();
            assert.equal(request.getBrowserType(), BrowserType.INTERNET_EXPLORER);
            assert.equal(request.getBrowserVersion(), 7);

            this.setUserAgent(
                "Mozilla/4.0 (.NET CLR 3.5.30729; .NET CLR 3.0.30729; " +
                "Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E; rv:7.0)");
            request = new HttpRequestParser();
            assert.equal(request.getBrowserType(), BrowserType.INTERNET_EXPLORER);
            assert.equal(request.getBrowserVersion(), 7);

            this.setUserAgent(
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                "Chrome/52.0.2743.116 Safari/537.36 OPR/39.0.2256.71");
            request = new HttpRequestParser();
            assert.equal(request.getBrowserType(), BrowserType.OPERA);
            assert.equal(request.getBrowserVersion(), 39);

            this.setUserAgent(
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                "Chrome/52.0.2743.116 Safari/537.36 Opera/39.0.2256.71");
            request = new HttpRequestParser();
            assert.equal(request.getBrowserType(), BrowserType.OPERA);
            assert.equal(request.getBrowserVersion(), -1);

            this.setUserAgent(
                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) " +
                "Chrome/52.0.2743.116 Safari/537.36");
            request = new HttpRequestParser();
            assert.equal(request.getBrowserType(), BrowserType.GOOGLE_CHROME);
            assert.equal(request.getBrowserVersion(), 52);

            this.setUserAgent(
                "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2");
            request = new HttpRequestParser();
            assert.equal(request.getBrowserType(), BrowserType.SAFARI);
            assert.equal(request.getBrowserVersion(), 5);

            this.setUserAgent(
                "Mozilla/5.0 Safari (Windows NT 6.1; WOW64; Version/5.1.7)");
            request = new HttpRequestParser();
            assert.equal(request.getBrowserType(), BrowserType.SAFARI);
            assert.equal(request.getBrowserVersion(), 5);

            this.setUserAgent(
                "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:48.0) Gecko/20100101 Firefox/48.0");
            request = new HttpRequestParser();
            assert.equal(request.getBrowserType(), BrowserType.FIREFOX);
            assert.equal(request.getBrowserVersion(), 48);
        }

        public testgetBrowserVersion() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            assert.ok(request.getBrowserVersion() >= -1);
        }

        public testgetHeadersXMLHttpRequestAPI() : void {
            ClientUnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString()}, name: "com-wui-framework-commons", version: "1.0.0"
            });
            (<MockWindow>window).location.headers = "Connection:Keep-Alive\n";
            const request : HttpRequestParser = new HttpRequestParser();
            assert.ok(!request.getHeaders().IsEmpty());
            this.initSendBox();
        }

        public __IgnoretestgetHeadersAll() : void {
            this.setUrl("http://10.171.88.56:57961");
            window.location.protocol = "file:";
            (<MockWindow>window).location.headers = "";
            this.setUserAgent(
                "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; " +
                "Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; " +
                "Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E)");

            const request : HttpRequestParser = new HttpRequestParser();
            (<MockWindow>window).location.headers =
                "Connection:Keep-Alive\n" +
                "Cookie: $Version=1; Skin=new;\n" +
                "Content-Type: application/x-www-form-urlencoded\n" +
                "X-Requested-With: XMLHttpRequest\n" +
                "HTTP_IF_NONE_MATCH:sfsdf\n" +
                "X-Forwarded-Proto: https\n" +
                "Cache-Control: max-age=3600\n";

            request.getHeaders().Add("Keep-Alive", "Connection");
            request.getHeaders().Add("$Version=1; Skin=new;", "Cookie");
            request.getHeaders().Add("application/x-www-form-urlencoded", "Content-Type");
            request.getHeaders().Add("XMLHttpRequest", "X-Requested-With");
            request.getHeaders().Add("sfsdf", "HTTP_IF_NONE_MATCH");
            request.getHeaders().Add("https", "X-Forwarded-Proto");
            request.getHeaders().Add("max-age=3600", "Cache-Control");

            const headers : ArrayList<string> = request.getHeaders();
            assert.equal(headers.getItem("Connection"), "Keep-Alive");
            assert.equal(headers.getItem("Cookie"), "$Version=1; Skin=new;");
            assert.equal(headers.getItem("Content-Type"), "application/x-www-form-urlencoded");
            assert.equal(headers.getItem("X-Requested-With"), "XMLHttpRequest");
            assert.equal(headers.getItem("HTTP_IF_NONE_MATCH"), "sfsdf");

            assert.equal(request.getHeaders(), headers);
            assert.deepEqual(request.getHeaders().getAll(),
                [
                    "Keep-Alive", "$Version=1; Skin=new;", "application/x-www-form-urlencoded",
                    "XMLHttpRequest", "sfsdf", "https", "max-age=3600"
                ]);
        }

        public __IgnoretestgetEtags() : void {
            this.setUrl("http://10.171.88.56:57961");
            (<MockWindow>window).location.headers =
                "HTTP_IF_NONE_MATCH: c0947-b1-4d0258df1f625," +
                " \"618bbc92e2d35ea1945008b42799b0e7\"," +
                " W/\"b749c4dd1b20885128f9d9a1a8ba70b6\"\n";
            const request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.getEtags().IsEmpty(), true);
            request.getEtags().Add("c0947-b1-4d0258df1f625", 0);
            request.getEtags().Add("\"618bbc92e2d35ea1945008b42799b0e7\"", 1);
            request.getEtags().Add("W/\"b749c4dd1b20885128f9d9a1a8ba70b6\"", 2);
            const etags : ArrayList<string> = request.getEtags();
            assert.equal(etags.Length(), 3);
            assert.equal(etags.getItem(0), "c0947-b1-4d0258df1f625");
            assert.equal(etags.getItem(1), "\"618bbc92e2d35ea1945008b42799b0e7\"");
            assert.equal(etags.getItem(2), "W/\"b749c4dd1b20885128f9d9a1a8ba70b6\"");
            assert.equal(request.getEtags(), etags);
            assert.deepEqual(request.getEtags().getAll(),
                ["c0947-b1-4d0258df1f625", "\"618bbc92e2d35ea1945008b42799b0e7\"", "W/\"b749c4dd1b20885128f9d9a1a8ba70b6\""]);
            request.getEtags().Clear();
        }

        public testgetLastModifiedTime() : void {
            UnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString()}, name: "com-wui-framework-commons", version: "1.0.0"
            });
            let request : HttpRequestParser = new HttpRequestParser();
            assert.ok(request.getLastModifiedTime(), Convert.TimeToGMTformat(new Date()));

            const timestamp : string = Convert.TimeToGMTformat(new Date());
            (<MockWindow>window).location.headers = "HTTP_IF_MODIFIED_SINCE: " + timestamp + "\n";
            request = new HttpRequestParser();
            assert.equal(request.getLastModifiedTime(), timestamp);
            this.initSendBox();
        }

        public testgetCookies() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
            (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
                return true;
            });
            document.cookie = "username1=John Doe; expires=Thu, 18 Dec 2013 12:00:00 UTC; path=/;";
            document.cookie = "username2=John Doe 2; path=/;";
            document.cookie = "username3=John Doe 3; expires=" + Convert.TimeToGMTformat("+10min") + "; path=/;";
            document.cookie = "username4=John Doe 4; path=/index;";
            let request : HttpRequestParser = new HttpRequestParser();
            const cookies : ArrayList<string> = request.getCookies();
            assert.equal(cookies.Length(), 2);
            assert.equal(request.getCookies().getItem("username1"), null);
            assert.equal(request.getCookies().getItem("username2"), "John Doe 2");
            // assert.equal(cookies.getItem("path"), "/");
            assert.equal(request.getCookies().getItem("username3"), "John Doe 3");
            assert.equal(request.getCookies().getItem("expires"), null);

            (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
                return false;
            });
            request = new HttpRequestParser();
            assert.ok(request.getCookies().IsEmpty());
            (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
                return true;
            });
            document.cookie = "username2=; expires=" + Convert.TimeToGMTformat("-10min") + "; path=/;";
            document.cookie = "username3=; expires=" + Convert.TimeToGMTformat("-10min") + "; path=/;";
        }

        public testgetCookie() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
            const request : HttpRequestParser = new HttpRequestParser();
            (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
                return true;
            });
            document.cookie =
                "username=John Doe; " +
                "expires=" + Convert.TimeToGMTformat("+10min") + "; " +
                "path=/;";

            const cookies : ArrayList<string> = request.getCookies();
            assert.equal(request.getCookie("username"), "John Doe");
            // assert.equal(cookies.getItem("path"), "");
            // assert.equal(request.getCookie("expires"), "Thu, 18 Dec 2013 12:00:00 UTC");
            document.cookie = "username=; expires=" + Convert.TimeToGMTformat("-10min") + "; path=/;";
        }

        public testCookieSecond() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
            const request : HttpRequestParser = new HttpRequestParser();
            (<any>window.navigator).__defineGetter__("cookieEnabled", () : boolean => {
                return true;
            });
            const cookies : ArrayList<string> = request.getCookies();
            assert.equal(request.getCookie("name"), "");
        }

        public testgetStorageItems() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            assert.ok(this.getHttpManager().getRequest().getStorageItems().IsEmpty());
            assert.ok(request.getStorageItems().IsEmpty());

            localStorage.clear();
            assert.equal(request.getStorageItems().IsEmpty(), true);

            localStorage.setItem("testKey1", "value1");
            localStorage.setItem("testKey2", "value2");
            localStorage.setItem("testKey3", "value3");

            const data : ArrayList<string> = request.getStorageItems();
            assert.equal(data.Length(), 3);
            assert.equal(data.getItem("testKey1"), "value1");
            assert.equal(data.getItem("testKey2"), "value2");
            assert.equal(data.getItem("testKey3"), "value3");
            assert.equal(request.getStorageItems().IsEmpty(), false);
            assert.deepEqual(request.getStorageItems().getAll(), ["value1", "value2", "value3"]);
            localStorage.clear();
        }

        public testgetStorageItem() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            this.setStorageItem("testKey1", "value1");
            assert.equal(request.getStorageItem("someKey"), "");
            assert.equal(request.getStorageItem(""), "");
            assert.equal(request.getStorageItem(null), "");
            assert.equal(request.getStorageItem("testKey1"), "value1");
            assert.equal(request.getStorageItems().Length(), 1);
            localStorage.removeItem("testKey1");
            assert.equal(request.getStorageItems().Length(), 0);
            localStorage.clear();
        }

        public testIsSameOrigin() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            assert.equal(request.IsSameOrigin("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader"), true);
            assert.equal(request.IsSameOrigin("http://localhost.wuiframework.com"), false);
        }

        public testToString() : void {
            ClientUnitTestLoader.Load(<any>{
                build: {time: new Date().toTimeString()}, name: "com-wui-framework-commons", version: "1.0.0"
            });
            (<MockWindow>window).location.headers = "HTTP_IF_MODIFIED_SINCE: 12345\n";
            this.setUserAgent(
                "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; " +
                "Trident/7.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; " +
                "Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E)");
            (<any>window.navigator).__defineGetter__("platform", () : string => {
                return "win32";
            });
            const request : HttpRequestParser = new HttpRequestParser();
            this.resetCounters();
            assert.patternEqual(request.ToString(),
                "uri: http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader<br/>" +
                "url: UnitTestLoader<br/>" +
                "scheme: http://<br/>" +
                "host: http://localhost:8888/UnitTestEnvironment.js<br/>" +
                "port: <i>Object type:</i> number. <i>Return value:</i> 8888<br/>" +
                "script: UnitTestLoader<br/>" +
                "args: <i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> <span onclick=\"" +
                "document.getElementById('ContentBlock_0').style.display=" +
                "document.getElementById('ContentBlock_0').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span><br/>" +
                "relative dir: <br/>" +
                "relative root: <br/>" +
                "<br/>" +
                "server IP: 127.0.0.1<br/>" +
                "client IP: 127.0.0.1<br/>" +
                "<br/>" +
                "cookieEnabled: true<br/>" +
                "language: en-US<br/>" +
                "onServer: true<br/>" +
                "onLine: true<br/>" +
                "platform: win32<br/>" +
                "user agent: Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/7.0; SLCC2; .NET CLR 2.0.50727; " +
                ".NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; MS-RTC LM 8; .NET4.0C; .NET4.0E)<br/>" +
                "is bot: false<br/>" +
                "is mobile: false<br/>" +
                "is WUI JRE: false<br/>" +
                "is WUI Plugin: false<br/>" +
                "browser type: INTERNET_EXPLORER<br/>" +
                "browser version: <i>Object type:</i> number. <i>Return value:</i> 7<br/>" +
                "<br/>" +
                "headers: <i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> <span onclick=\"" +
                "document.getElementById('ContentBlock_1').style.display=" +
                "document.getElementById('ContentBlock_1').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
                "[ \"if-modified-since\" ]&nbsp;&nbsp;&nbsp;&nbsp;12345<br/></span><br/>" +
                "is WUI Connector: false<br/>" +
                "is IDEA Hosting: false<br/>" +
                "eTags: <i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> <span onclick=\"" +
                "document.getElementById('ContentBlock_2').style.display=" +
                "document.getElementById('ContentBlock_2').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_2\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span><br/>" +
                "last modified time: 12345<br/>" +
                "<br/>" +
                "document cookies: EMPTY<br/>" +
                "parsed cookies: <i>Com.Wui.Framework.Commons.Primitives.ArrayList object</i> <span onclick=\"" +
                "document.getElementById('ContentBlock_3').style.display=" +
                "document.getElementById('ContentBlock_3').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_3\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span>");
            this.initSendBox();
        }

        public testtoString() : void {
            const request : HttpRequestParser = new HttpRequestParser();
            this.resetCounters();
            const value : string = request.ToString();
            this.resetCounters();
            assert.equal(request.toString(), value);
        }

        protected setUp() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
        }
    }
}
