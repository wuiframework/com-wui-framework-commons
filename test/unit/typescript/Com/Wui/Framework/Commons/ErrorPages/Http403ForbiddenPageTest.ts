/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.ErrorPages {
    "use strict";

    export class Http403ForbiddenPageTest extends UnitTestRunner {

        public testgetPageBody() : void {
            assert.resolveEqual(Http403ForbiddenPage, "" +
                "<head></head>" +
                "<body>" +
                "<div id=\"Content\"><span guitype=\"HtmlAppender\"><br>" +
                "<h1>HTTP status 403:</h1>\n" +
                "<h2>Access denied</h2>" +
                "</span></div>" +
                "</body>");
        }
    }
}
