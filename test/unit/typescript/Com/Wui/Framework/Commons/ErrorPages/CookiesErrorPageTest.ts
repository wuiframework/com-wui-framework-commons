/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.ErrorPages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class CookiesErrorPageTest extends UnitTestRunner {

        public testgetPageBody() : void {
            assert.resolveEqual(CookiesErrorPage, "" +
                "<head></head>" +
                "<body><div id=\"Content\"><span guitype=\"HtmlAppender\">" +
                "<br>This library requires enabled Cookies in the browser for ability to store persistence. " +
                "See link below for more information:<br>" +
                "<a href=\"http://www.wikihow.com/Enable-Cookies-in-Your-Internet-Web-Browser\" target=\"_blank\">" +
                "How to enable Cookies?</a></span>" +
                "</div>" +
                "</body>");
        }
    }
}
