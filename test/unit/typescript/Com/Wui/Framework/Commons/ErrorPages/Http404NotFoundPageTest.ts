/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.ErrorPages {
    "use strict";
    import HttpRequestConstants = Com.Wui.Framework.Commons.Enums.HttpRequestConstants;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class Http404NotFoundPageTest extends UnitTestRunner {

        public testgetPageBody() : void {
            assert.resolveEqual(Http404NotFoundPage, "" +
                "<head></head>" +
                "<body>" +
                "<div id=\"Content\"><span guitype=\"HtmlAppender\"><br>" +
                "<h1>HTTP status 404:</h1>\n" +
                "<h2>File has not been found</h2>" +
                "</span></div>" +
                "</body>");
        }

        public testgetPageBody2() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add("newFilePath/404_Redirect", HttpRequestConstants.HTTP404_FILE_PATH);
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(
                this.getRequest().getScriptPath(), data);

            assert.equal(args.POST().getItem(HttpRequestConstants.HTTP404_FILE_PATH), "newFilePath/404_Redirect");
            assert.resolveEqual(Http404NotFoundPage, "" +
                "<head></head>" +
                "<body><div id=\"Content\"><span guitype=\"HtmlAppender\"><br>" +
                "<h1>HTTP status 404:</h1>\n" +
                "<h2>File has not been found. Required file path is:</h2>\n" +
                "<a href=\"newFilePath/404_Redirect\">newFilePath/404_Redirect</a>" +
                "</span></div>" +
                "</body>", args);
        }
    }
}
