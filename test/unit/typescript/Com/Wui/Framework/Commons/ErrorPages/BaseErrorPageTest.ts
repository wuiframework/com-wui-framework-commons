/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.ErrorPages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;

    class MockBaseErrorPage extends BaseErrorPage {
        public testEchoOutput() : string {
            return this.getEchoOutput();
        }
    }

    export class BaseErrorPageTest extends UnitTestRunner {

        public testgetEchoOutput() : void {
            const baseerror : MockBaseErrorPage = new MockBaseErrorPage();
            assert.equal(baseerror.testEchoOutput(), "Nothing has been printed by Echo yet.");
            Echo.Printf("test print");
            assert.equal(baseerror.testEchoOutput(), "<br/>test print");
            this.initSendBox();
        }

        public testgetPageBody() : void {
            assert.resolveEqual(BaseErrorPage, "" +
                "<head></head>" +
                "<body><div id=\"Content\"><span guitype=\"HtmlAppender\"><br>" +
                "<h1>Something has went wrong</h1></span>" +
                "</div>" +
                "</body>", new AsyncRequestEventArgs(""));
        }
    }
}
