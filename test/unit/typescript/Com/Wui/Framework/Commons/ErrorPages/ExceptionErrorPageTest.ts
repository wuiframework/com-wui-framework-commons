/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.ErrorPages {
    "use strict";
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpRequestConstants = Com.Wui.Framework.Commons.Enums.HttpRequestConstants;
    import ExceptionCode = Com.Wui.Framework.Commons.Enums.ExceptionCode;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;

    class MockExceptionCorrupted extends Exception {
        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            throw Error("test MockExceptionCorrupted");
        }
    }

    class MockException extends ExceptionErrorPage {
        protected getPageBody() : any {
            throw new Error("test body exception");
        }
    }

    export class ExceptionErrorPageTest extends UnitTestRunner {

        public testgetPageBody() : void {
            assert.resolveEqual(ExceptionErrorPage, "" +
                "<head></head>" +
                "<body><div id=\"Content\"><span guitype=\"HtmlAppender\">" +
                "<br>" +
                "<h1>Oops, something went wrong...</h1>" +
                "<span onclick=\"" +
                "document.getElementById('exceptionEcho').style.display=" +
                "document.getElementById('exceptionEcho').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
                "Echo output before exception</span>\n" +
                "<div id=\"exceptionEcho\" style=\"border: 0 solid black; display: none;\">\n" +
                "Nothing has been printed by Echo yet.\n" +
                "</div>" +
                "<br>" +
                "<a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; text-decoration: none;\"" +
                " href=\"#/com-wui-framework-builder/about/Cache\">Cache info</a></span></div>" +
                "</body>");
        }

        public testgetExceptionsList() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            const exceptionList : ArrayList<Exception> = new ArrayList<Exception>();
            exceptionList.Add(new Exception());
            exceptionList.Add(new MockExceptionCorrupted());
            data.Add(exceptionList, HttpRequestConstants.EXCEPTIONS_LIST);
            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(
                this.getRequest().getScriptPath(), data);

            assert.equal(args.POST().getItem(HttpRequestConstants.EXCEPTIONS_LIST), exceptionList);

            const goldenData : string =
                "<head></head>" +
                "<body>" +
                "<div id=\"Content\"><span guitype=\"HtmlAppender\">" +
                "<br>" +
                "<h1>Oops, something went wrong...</h1>" +
                "thrown by: <b></b>:<br>" +
                "<br>" +
                "<br>" +
                "thrown by: <b></b>:<br>" +
                "test MockExceptionCorrupted<br>" +
                "<span onclick=\"" +
                "document.getElementById(\'exceptionEcho\').style.display=" +
                "document.getElementById(\'exceptionEcho\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
                "Echo output before exception</span>\n" +
                "<div id=\"exceptionEcho\" style=\"border: 0 solid black; display: none;\">\n" +
                "Nothing has been printed by Echo yet.\n" +
                "</div><br>" +
                "<a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; text-decoration: none;\" " +
                "href=\"#/com-wui-framework-builder/about/Cache\">Cache info</a></span></div>" +
                "</body>";

            assert.resolveEqual(ExceptionErrorPage, goldenData, args).Process();
            assert.equal(document.documentElement.innerHTML, goldenData);
            this.initSendBox();
        }

        public testisFatalError() : void {
            const data : ArrayList<any> = new ArrayList<any>();
            data.Add(ExceptionCode.FATAL_ERROR, HttpRequestConstants.EXCEPTION_TYPE);

            const args : AsyncRequestEventArgs = new AsyncRequestEventArgs(
                this.getRequest().getScriptPath(), data);
            assert.equal(args.GET(data).getItem(HttpRequestConstants.EXCEPTION_TYPE), ExceptionCode.FATAL_ERROR);

            const goldenData : string =
                "<head></head>" +
                "<body><div id=\"Content\"><span guitype=\"HtmlAppender\">" +
                "<br><h1>FATAL Error!</h1>" +
                "<span onclick=\"" +
                "document.getElementById(\'exceptionEcho\').style.display=" +
                "document.getElementById(\'exceptionEcho\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
                "Echo output before exception</span>\n" +
                "<div id=\"exceptionEcho\" style=\"border: 0 solid black; display: none;\">\n" +
                "Nothing has been printed by Echo yet.\n" +
                "</div><br>" +
                "<a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; text-decoration: none;\" " +
                "href=\"#/com-wui-framework-builder/about/Cache\">Cache info</a></span>" +
                "</div>" +
                "</body>";

            assert.resolveEqual(ExceptionErrorPage, goldenData, args).Process();
            assert.equal(document.documentElement.innerHTML, goldenData);
            this.initSendBox();
        }

        public testSelfException() : void {
            assert.doesHandleException(() : void => {
                assert.resolveEqual(MockException, "");
            }, "Com.Wui.Framework.Commons.ErrorPages.ExceptionErrorPage self error.");
        }
    }
}
