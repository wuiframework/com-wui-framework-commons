/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons {
    "use strict";

    export class EnvironmentArgsTest extends UnitTestRunner {
        public testConstructor() : void {
            const environArgs : ClientUnitTestEnvironmentArgs = new ClientUnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620"}, name: "envir", version: "333xxx"
            }, () : void => {
                assert.equal(environArgs.getBuildTime(), "235620");
                assert.equal(environArgs.HtmlOutputAllowed(), true);
            });
        }

        public testgetProjectName() : void {
            const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620"}, name: "envir", version: "333xxx"
            }, () : void => {
                assert.equal(environArgs.getProjectName(), "envir");
            });
        }

        public testgetProjectVersion() : void {
            const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620"}, name: "envir", version: "333xxx"
            }, () : void => {
                assert.equal(environArgs.getProjectVersion(), "333xxx");
            });
        }

        public testgetProcessID() : void {
            const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620"}, name: "envir", version: "333xxx"
            }, () : void => {
                const tmpLocation : string = window.location.href;
                this.setUrl("http://localhost:8888/UnitTestEnvironment.js?AppPid=123456");
                this.getHttpResolver().CreateRequest();
                assert.equal(environArgs.getProcessID(), 123456);

                this.setUrl(tmpLocation);
                this.getHttpResolver().CreateRequest();

                this.setUrl("http://localhost:8888/UnitTestEnvironment.js?AppPid=");
                this.getHttpResolver().CreateRequest();
                assert.equal(environArgs.getProcessID(), 123456);

                this.setUrl(tmpLocation);
                this.getHttpResolver().CreateRequest();
            });
        }

        public testgetAppName() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?AppName=app");
            this.getHttpResolver().CreateRequest();
            const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620"}, name: "envir", version: "333xxx"
            }, () : void => {
                assert.equal(environArgs.getAppName(), "app");
                assert.equal(environArgs.getAppName(), "app");
            });
        }

        public testgetAppNameSecond() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js");
            this.getHttpResolver().CreateRequest();
            const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620"}, name: "envir", version: "333xxx"
            }, () : void => {
                assert.equal(environArgs.getAppName(), undefined);
            });
        }

        public testgetBuildTime() : void {
            const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620"}, name: "envir", version: "333xxx"
            }, () : void => {
                assert.equal(environArgs.getBuildTime(), "235620");
            });
        }

        public testgetReleaseName() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?ReleaseName=release");
            this.getHttpResolver().CreateRequest();
            const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620"}, name: "envir", version: "333xxx"
            }, () : void => {
                assert.equal(environArgs.getReleaseName(), "release");
                assert.equal(environArgs.getReleaseName(), "release");
            });
        }

        public testgetReleaseNameSecond() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js");
            this.getHttpResolver().CreateRequest();
            const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620"}, name: "envir", version: "333xxx"
            }, () : void => {
                assert.equal(environArgs.getReleaseName(), "null");
            });
        }

        public testgetPlatform() : void {
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js?AppName=app");
            this.getHttpResolver().CreateRequest();
            const environArgs : UnitTestEnvironmentArgs = new UnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620"}, name: "envir", version: "333xxx"
            }, () : void => {
                assert.equal(environArgs.getPlatform(), "null");
                assert.equal(environArgs.getPlatform(), "null");
            });
        }

        public testToString() : void {
            const environArgs : ClientUnitTestEnvironmentArgs = new ClientUnitTestEnvironmentArgs();
            environArgs.Load({
                build: {time: "235620", type: "prod"}, name: "envir", version: "333xxx"
            }, () : void => {
                this.resetCounters();
                assert.equal(environArgs.ToString(),
                    "[\"getProjectName\"]&nbsp;&nbsp;&nbsp;envir<br/>" +
                    "[\"getProjectVersion\"]&nbsp;&nbsp;&nbsp;333xxx<br/>" +
                    "[\"getBuildTime\"]&nbsp;&nbsp;&nbsp;235620<br/>" +
                    "[\"IsProductionMode\"]&nbsp;&nbsp;&nbsp;<i>Object type:</i> boolean. <i>Return value:</i> true<br/>" +
                    "[\"HtmlOutputAllowed\"]&nbsp;&nbsp;&nbsp;<i>Object type:</i> boolean. <i>Return value:</i> true<br/>");
                assert.equal(environArgs.ToString("", false),
                    "[\"getProjectName\"]    envir\r\n" +
                    "[\"getProjectVersion\"]    333xxx\r\n" +
                    "[\"getBuildTime\"]    235620\r\n" +
                    "[\"IsProductionMode\"]    Object type: boolean. Return value: true\r\n" +
                    "[\"HtmlOutputAllowed\"]    Object type: boolean. Return value: true\r\n");
            });
        }
    }
}
