/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Primitives {
    "use strict";

    class MockBaseArgs extends BaseArgs {
    }

    export class BaseArgsTest extends UnitTestRunner {
        public testToString() : void {
            const arg : BaseArgs = new MockBaseArgs();

            this.resetCounters();
            assert.equal(this.stripInstrumentation(arg.ToString("____", false)),
                "____[getClassName] Com.Wui.Framework.Commons.Primitives.BaseArgs\r\n" +
                "____[getNamespaceName] Com.Wui.Framework.Commons.Primitives\r\n" +
                "____[getClassNameWithoutNamespace] BaseArgs\r\n" +
                "____[IsMemberOf] Object type: boolean. Return value: false\r\n" +
                "____[Implements] Object type: boolean. Return value: false\r\n" +
                "____[getHash] Object type: number. Return value: 0\r\n" +
                "____[excludeSerializationData] Object type: array. Return values: \r\nArray object\r\n" +
                "____[ 0 ]    objectNamespace\r\n" +
                "____[ 1 ]    objectClassName\r\n\r\n" +
                "____[excludeIdentityHashData] Object type: array. Return values: \r\nArray object\r\n____Data object EMPTY\r\n",
                "return plain text with prefix");

            this.resetCounters();
            assert.equal(this.stripInstrumentation(arg.ToString()),
                "[getClassName] Com.Wui.Framework.Commons.Primitives.BaseArgs<br/>" +
                "[getNamespaceName] Com.Wui.Framework.Commons.Primitives<br/>" +
                "[getClassNameWithoutNamespace] BaseArgs<br/>" +
                "[IsMemberOf] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "[Implements] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "[getHash] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
                "[excludeSerializationData] <i>Object type:</i> array. <i>Return values:</i><br/><i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;objectNamespace<br/>" +
                "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;objectClassName<br/></span><br/>" +
                "[excludeIdentityHashData] <i>Object type:</i> array. <i>Return values:</i><br/>" +
                "<i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_1\').style.display=" +
                "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span><br/>",
                "return HTML-based output without prefix");

            this.resetCounters();
            assert.equal(this.stripInstrumentation(arg.ToString("____", true)),
                "____[getClassName] Com.Wui.Framework.Commons.Primitives.BaseArgs<br/>" +
                "____[getNamespaceName] Com.Wui.Framework.Commons.Primitives<br/>" +
                "____[getClassNameWithoutNamespace] BaseArgs<br/>" +
                "____[IsMemberOf] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "____[Implements] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "____[getHash] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
                "____[excludeSerializationData] <i>Object type:</i> array. <i>Return values:</i><br/><i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "____[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;objectNamespace<br/>" +
                "____[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;objectClassName<br/></span><br/>" +
                "____[excludeIdentityHashData] <i>Object type:</i> array. <i>Return values:</i><br/>" +
                "<i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_1\').style.display=" +
                "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">____Data object <b>EMPTY</b></span><br/>");
        }

        public testtoString() : void {
            const arg : BaseArgs = new MockBaseArgs();
            this.resetCounters();
            assert.equal(this.stripInstrumentation(arg.toString()),
                "[getClassName] Com.Wui.Framework.Commons.Primitives.BaseArgs<br/>" +
                "[getNamespaceName] Com.Wui.Framework.Commons.Primitives<br/>" +
                "[getClassNameWithoutNamespace] BaseArgs<br/>" +
                "[IsMemberOf] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "[Implements] <i>Object type:</i> boolean. <i>Return value:</i> false<br/>" +
                "[getHash] <i>Object type:</i> number. <i>Return value:</i> 0<br/>" +
                "[excludeSerializationData] <i>Object type:</i> array. <i>Return values:</i><br/>" +
                "<i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;objectNamespace<br/>" +
                "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;objectClassName<br/></span><br/>" +
                "[excludeIdentityHashData] <i>Object type:</i> array. <i>Return values:</i><br/>" +
                "<i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_1\').style.display=" +
                "document.getElementById(\'ContentBlock_1\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">Data object <b>EMPTY</b></span><br/>");
        }
    }
}
