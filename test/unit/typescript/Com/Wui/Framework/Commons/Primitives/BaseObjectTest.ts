/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Primitives {
    "use strict";
    import Interface = Com.Wui.Framework.Commons.Interfaces.Interface;
    import IBaseObject = Com.Wui.Framework.Commons.Interfaces.IBaseObject;
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import HttpRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs;

    class MockBaseObject extends BaseObject {
    }

    export class BaseObjectTest extends UnitTestRunner {
        public testClassName() : void {
            assert.equal(BaseObject.ClassName(), "Com.Wui.Framework.Commons.Primitives.BaseObject");
        }

        public testClassNameWithoutNamespace() : void {
            assert.equal(BaseObject.ClassNameWithoutNamespace(), "BaseObject");
        }

        public testNamespaceName() : void {
            assert.equal(BaseObject.NamespaceName(), "Com.Wui.Framework.Commons.Primitives");
        }

        public testUID() : void {
            const object : BaseObject = new MockBaseObject();
            assert.ok(object.getUID(), "298349f41a3db65d34bee42365dea9a115c4420e");
            assert.notEqual(object.getUID(), object.getUID());
            assert.notEqual(BaseObject.UID(), BaseObject.UID());
        }

        public testgetInstance() : void {
            const object : BaseObject = new MockBaseObject();
            const object1 : BaseObject = MockBaseObject.getInstance();
            assert.ok(new MockBaseObject(), MockBaseObject.getInstance());
        }

        public testgetClassName() : void {
            const object : BaseObject = new MockBaseObject();
            assert.equal(object.getClassName(), "Com.Wui.Framework.Commons.Primitives.BaseObject");
        }

        public testgetNamespaceName() : void {
            const object : BaseObject = new MockBaseObject();
            assert.equal(object.getNamespaceName(), "Com.Wui.Framework.Commons.Primitives");
        }

        public testgetClassNameWithoutNamespace() : void {
            const object : BaseObject = new MockBaseObject();
            assert.equal(object.getClassNameWithoutNamespace(), "BaseObject");
        }

        public testgetUID() : void {
            const object1 : BaseObject = new MockBaseObject();
            const object2 : BaseObject = new MockBaseObject();
            assert.notEqual(object1.getUID(), object2.getUID());
        }

        public testIsTypeOf() : void {
            const object : BaseObject = new MockBaseObject();
            assert.equal(object.IsTypeOf(MockBaseObject), true);
            assert.equal(object.IsTypeOf(String), false);
        }

        public testIsMemberOf() : void {
            const object : BaseObject = new MockBaseObject();
            assert.equal(object.IsMemberOf(BaseObject), true);
            assert.equal(object.IsMemberOf(String), false);
        }

        public testImplements() : void {
            const object : BaseObject = new MockBaseObject();
            assert.equal(object.Implements((<Interface>IBaseObject).ClassName()), true);
            assert.equal(object.Implements(IBaseObject), true);
            assert.equal(object.IsMemberOf(IArrayList), false);
        }

        public testSerializationData() : void {
            const object : BaseObject = new MockBaseObject();
            assert.deepEqual(object.SerializationData(), {},
                "validate object data for serialization");

            const object2 : EventArgs = new EventArgs();
            assert.deepEqual(object2.SerializationData(), {owner: object2},
                "validate object data for serialization with recursive reference");
            object2.Type("test");
            assert.deepEqual(object2.SerializationData(), {owner: object2, type: "test"},
                "validate object data for serialization");

            const nativeArgs : any = {name: "test"};
            object2.NativeEventArgs(nativeArgs);
            assert.deepEqual(object2.SerializationData(), {nativeEventArgs: nativeArgs, owner: object2, type: "test"},
                "validate object data for serialization with native args");
        }

        public testgetMethods() : void {
            const object : BaseObject = new MockBaseObject();
            assert.deepEqual(object.getMethods(), [
                "getUID",
                "getClassName",
                "getNamespaceName",
                "getClassNameWithoutNamespace",
                "getProperties",
                "getMethods",
                "IsTypeOf",
                "IsMemberOf",
                "Implements",
                "SerializationData",
                "ToString",
                "getHash",
                "excludeSerializationData",
                "excludeIdentityHashData"
            ]);
        }

        public testgetHash() : void {
            const object : BaseObject = new MockBaseObject();
            assert.equal(object.getHash(), 0);
            const object2 : HttpRequestEventArgs = new HttpRequestEventArgs("test", new ArrayList("test"));
            assert.equal(object2.getHash(), 1184522040);
            /* tslint:disable: no-empty */
            (<any>object2).test = () : void => {

            };
            /* tslint:enable */
            assert.equal(object2.getHash(), 1184522040);
        }

        public testToString() : void {
            const object : BaseObject = new MockBaseObject();
            assert.equal(object.ToString(), "object type of 'Com.Wui.Framework.Commons.Primitives.BaseObject'",
                "return HTML without attributes");
            assert.equal(object.ToString("___", true), "___object type of 'Com.Wui.Framework.Commons.Primitives.BaseObject'",
                "return HTML with prefix");
            assert.equal(object.ToString("", true), "object type of 'Com.Wui.Framework.Commons.Primitives.BaseObject'",
                "return HTML without prefix");
            assert.equal(object.ToString("___", false),
                "___object type of 'Com.Wui.Framework.Commons.Primitives.BaseObject'",
                "return PlainText");
        }
    }
}
