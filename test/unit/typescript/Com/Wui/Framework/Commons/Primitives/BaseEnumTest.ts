/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Primitives {
    "use strict";
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ExceptionCode = Com.Wui.Framework.Commons.Enums.ExceptionCode;

    export class BaseEnumTest extends UnitTestRunner {
        public testClassName() : void {
            assert.equal(BaseEnum.ClassName(), "Com.Wui.Framework.Commons.Primitives.BaseEnum");
            assert.equal(EventType.ClassName(), "Com.Wui.Framework.Commons.Enums.Events.EventType");
        }

        public testClassNameWithoutNamespace() : void {
            assert.equal(BaseEnum.ClassNameWithoutNamespace(), "BaseEnum");
        }

        public testNamespaceName() : void {
            assert.equal(BaseEnum.NamespaceName(), "Com.Wui.Framework.Commons.Primitives");
        }

        public testContains() : void {
            assert.equal(BaseEnum.Contains("onstart"), false);
            assert.equal(EventType.Contains(EventType.ON_CHANGE), true);
            assert.equal(EventType.Contains(ExceptionCode.EXIT), false);
        }

        public testgetProperties() : void {
            assert.deepEqual(EventType.getProperties(), [
                "ON_START",
                "ON_CHANGE",
                "ON_COMPLETE",
                "BEFORE_LOAD",
                "ON_LOAD",
                "BEFORE_REFRESH",
                "ON_HTTP_REQUEST",
                "ON_ASYNC_REQUEST",
                "ON_SUCCESS",
                "ON_ERROR",
                "ON_MESSAGE"
            ]);
            assert.notDeepEqual(EventType.getProperties(), [
                "ON_START",
                "ON_CHANGE",
                "ON_COMPLETE"
            ]);
        }

        public testgetKey() : void {
            assert.equal(BaseEnum.getKey("onstart"), null);
            assert.equal(EventType.getKey(EventType.ON_CHANGE), "ON_CHANGE");
            assert.equal(BaseEnum.getKey(ExceptionCode.EXIT), null);
        }
    }
}
