/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons {
    "use strict";
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class UnitTestRunnerTest extends UnitTestRunner {

        constructor() {
            super();
            // this.setMethodFilter("testSync1");
        }

        public testSync1() : void {
            LogIt.Info("sync 1");
        }

        public testAsync1() : IUnitTestRunnerPromise {
            LogIt.Info("before async 1");
            return ($done : () => void) : void => {
                LogIt.Info("on async 1");
                setTimeout(() : void => {
                    assert.ok(true);
                    LogIt.Info("after async 1");
                    $done();
                }, 300);
            };
        }

        public testSync2() : void {
            LogIt.Info("sync 2");
            ExceptionsManager.Clear();
        }

        public testAsync2() : IUnitTestRunnerPromise {
            LogIt.Info("before async 2");
            return ($done : () => void) : void => {
                LogIt.Info("on async 2");
                setTimeout(() : void => {
                    assert.ok(true);
                    LogIt.Info("after async 2");
                    $done();
                }, 300);
            };
        }

        public testSync3() : void {
            LogIt.Info("sync 3");
            assert.ok(true);
        }

        public testError() : void {
            assert.throws(() : void => {
                throw new Error("Test1");
            }, "Test1");
        }

        public testError2() : void {
            assert.doesHandleException(() : void => {
                try {
                    throw new Error("Test2");
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            }, "Test2");
        }

        public testError3() : void {
            assert.throws(() : void => {
                assert.doesHandleException(() : void => {
                    throw new Error("Test3");
                }, "Test 3");
            }, "AssertFailed: Assertion expected handling an error.");
        }

        public testMockServer() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const http : any = require("http");
                const httpRequest : any = http.request(this.getMockServerLocation() + "/com-wui-framework-commons/index",
                    ($httpResponse : any) : void => {
                        if ($httpResponse.statusCode === 200) {
                            let body : string = "";
                            $httpResponse.on("data", ($chunk : string) : void => {
                                body += $chunk;
                            });
                            $httpResponse.on("end", () : void => {
                                assert.equal(body, "hello");
                                $done();
                            });
                        }
                    });
                httpRequest.end();
            };
        }

        public testNativePromise() : IUnitTestRunnerPromise {
            LogIt.Info("before native promise");
            return ($done : () => void) : void => {
                const promise : any = new Promise(($resolve : any, $reject : any) : void => {
                    setTimeout(() : void => {
                        assert.ok(true);
                        $resolve();
                    }, 300);
                });
                promise.then(() : void => {
                    $done();
                });
            };
        }

        public __IgnoretestTimeout() : IUnitTestRunnerPromise {
            this.timeoutLimit(5000);
            return ($done : () => void) : void => {
                setTimeout(() : void => {
                    assert.ok(true);
                    $done();
                }, 10000);
            };
        }

        protected before() : IUnitTestRunnerPromise {
            this.timeoutLimit(180000);
            return ($done : () => void) : void => {
                setTimeout(() : void => {
                    LogIt.Info("before");
                    $done();
                }, 1000);
            };
        }

        protected after() : void {
            LogIt.Info("after");
        }

        protected setUp() : void {
            LogIt.Info("setUp");
        }

        protected tearDown() : void {
            LogIt.Info("tearDown");
        }
    }
}
