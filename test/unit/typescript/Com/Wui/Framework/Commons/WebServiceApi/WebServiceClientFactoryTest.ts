/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi {
    "use strict";
    import IWebServiceClient = Com.Wui.Framework.Commons.Interfaces.IWebServiceClient;
    import WebServiceClientType = Com.Wui.Framework.Commons.Enums.WebServiceClientType;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    export class WebServiceClientFactoryTest extends UnitTestRunner {

        public testgetClient() : void {
            const clientManager1 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.CEF_QUERY, "config");
            const clientManager2 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.POST_MESSAGE, "service");

            const configuration : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            const clientManager3 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.AJAX, configuration);
            const clientManager4 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.AJAX_SYNCHRONOUS, null);
            const clientManager6 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.IFRAME, "iframeservice");
            const clientManager7 : IWebServiceClient = WebServiceClientFactory.getClient(undefined);

            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
            this.getHttpManager().RefreshWithoutReload();
            this.getHttpManager().getRequest().IsSameOrigin("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
            const clientManager5 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.AJAX_SYNCHRONOUS, config);
            assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.AJAX_SYNCHRONOUS, config), clientManager5);

            assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.CEF_QUERY, "config"), clientManager1);
            assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.POST_MESSAGE, "service"), clientManager2);
            assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.AJAX, configuration), clientManager3);
            assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.AJAX_SYNCHRONOUS), clientManager4);
            assert.equal(WebServiceClientFactory.getClient(WebServiceClientType.IFRAME, "iframeservice"), clientManager6);
            assert.equal(WebServiceClientFactory.getClient(undefined), null);

            EventsManager.getInstanceSingleton().Clear(clientManager2.getId().toString());
            EventsManager.getInstanceSingleton().Clear(clientManager6.getId().toString());
        }

        public testIsSupported() : void {
            const clientManager1 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.IFRAME, "configuration");
            const clientManager2 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.HTTP, "httpservice");
            const clientManager3 : IWebServiceClient = WebServiceClientFactory.getClient(null, null);

            const service : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            const clientManager5 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.CEF_QUERY, service);
            assert.equal(WebServiceClientFactory.IsSupported(WebServiceClientType.CEF_QUERY), false);

            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            this.setUrl("http://localhost:8888/UnitTestEnvironment.js#UnitTestLoader");
            this.getHttpManager().RefreshWithoutReload();
            this.getHttpManager().getRequest().IsSameOrigin(config.getServerUrl());
            const clientManager6 : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.AJAX_SYNCHRONOUS, config);
            assert.equal(WebServiceClientFactory.IsSupported(WebServiceClientType.AJAX_SYNCHRONOUS), true);

            assert.equal(WebServiceClientFactory.IsSupported(WebServiceClientType.IFRAME), true);
            assert.equal(WebServiceClientFactory.IsSupported(WebServiceClientType.HTTP), true);
            assert.equal(WebServiceClientFactory.IsSupported(null), false);

            EventsManager.getInstanceSingleton().Clear(clientManager1.getId().toString());
            EventsManager.getInstanceSingleton().Clear(clientManager2.getId().toString());
        }

        public testExists() : void {
            const clientManager : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.POST_MESSAGE, "conf");
            assert.equal(WebServiceClientFactory.Exists(clientManager.getId()), true);
            assert.equal(WebServiceClientFactory.Exists(0), false);
            EventsManager.getInstanceSingleton().Clear(clientManager.getId().toString());
        }

        public testgetClientById() : void {
            assert.equal(WebServiceClientFactory.getClientById(null), null);
        }

        public testCloseConnection() : void {
            const configuration : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            const clientManager : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.AJAX, configuration);
            WebServiceClientFactory.CloseAllConnections();
            assert.equal(configuration.TimeoutLimit(0), 0);
        }
    }
}
