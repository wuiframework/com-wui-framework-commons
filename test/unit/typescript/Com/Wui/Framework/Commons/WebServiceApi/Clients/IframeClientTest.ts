/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi.Clients {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    export class IframeClientTest extends UnitTestRunner {

        public testConstructor() : void {
            const config : WebServiceConfiguration = new WebServiceConfiguration();
            const client : IframeClient = new IframeClient(config);
            assert.ok(StringUtils.PatternMatched("WebServiceClient(*)", client.toString()));
            client.StartCommunication();
            client.getEvents().OnTimeout(() : void => {
                client.StopCommunication();
            });
            EventsManager.getInstanceSingleton().Clear(client.getId().toString());
        }

        public testCommunicationIsRunning() : void {
            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            const client : IframeClient = new IframeClient(config);
            const iframeelement : HTMLIFrameElement = document.createElement("iframe");
            iframeelement.onload = () : void => {
                client.CommunicationIsRunning();
            };
            EventsManager.getInstanceSingleton().Clear(client.getId().toString());
        }

        public __IgnoretestLoad() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const service : WebServiceConfiguration = new WebServiceConfiguration(
                    "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
                service.Load(
                    () => {
                        const iframeclient : IframeClient = new IframeClient(service);
                        iframeclient.StartCommunication();
                        const client : HTMLIFrameElement = (<any>iframeclient).clientInstance(document.createElement("iframe"));
                        (<any>client).CommunicationIsRunning();
                        $done();
                    },
                    () => {
                        assert.equal(service.ServerAddress(), "127.0.0.1");
                        assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
                        $done();
                    });
            };
        }

        public __IgnoretestStartCommunication() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const config : WebServiceConfiguration = new WebServiceConfiguration(
                    "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
                config.ServerAddress("10.171.89.149");
                const iframeclient : IframeClient = new IframeClient(config);
                iframeclient.Send("TestingOfTheSocket", () : void => {
                    iframeclient.StartCommunication();
                    config.TimeoutLimit(2000);
                    assert.equal(config.ServerAddress(), "127.0.0.1");
                    assert.equal(iframeclient.CommunicationIsRunning(), true);
                    $done();
                });
                assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
                iframeclient.StopCommunication();
                assert.equal(iframeclient.CommunicationIsRunning(), false);
            };
        }

        public testStartCommunicationSecond() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            const client : IframeClient = new IframeClient(service);
            client.Send("hello", () : void => {
                service.ServerAddress("192.168.1.111");
            });
            client.StartCommunication();
            service.TimeoutLimit(2000);
            assert.equal(client.CommunicationIsRunning(), false);
            client.StopCommunication();
            assert.equal(client.CommunicationIsRunning(), false);
            EventsManager.getInstanceSingleton().Clear(client.getId().toString());
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
