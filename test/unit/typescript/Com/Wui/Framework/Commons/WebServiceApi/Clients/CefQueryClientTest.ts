/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi.Clients {
    "use strict";

    export class CefQueryClientTest extends UnitTestRunner {

        public testConstructor() : void {
            const cefclient : CefQueryClient = new CefQueryClient();
            assert.notEqual(cefclient.getId(), 425631);
        }

        public testStartCommunication() : void {
            const cefclient : CefQueryClient = new CefQueryClient();
            cefclient.StartCommunication();

            const cefclient2 : CefQueryClient = new CefQueryClient();
            assert.throws(() : void => {
                cefclient.StartCommunication();
                throw new Error("Not communication");
            }, /Not communication/);
        }

        public testStopCommunication() : void {
            const cefclient : CefQueryClient = new CefQueryClient();
            cefclient.StartCommunication();
            assert.equal(cefclient.CommunicationIsRunning(), true);
            cefclient.StopCommunication();
        }

        public __IgnoretestStartCommunicationSecond() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            const cefclient : CefQueryClient = new CefQueryClient();
            cefclient.Send("hello", () : void => {
                service.ServerAddress("192.168.1.111");
            });
            cefclient.StartCommunication();
            service.TimeoutLimit(2000);
            assert.equal(cefclient.CommunicationIsRunning(), true);
            cefclient.StopCommunication();
            assert.equal(cefclient.CommunicationIsRunning(), false);
        }
    }
}
