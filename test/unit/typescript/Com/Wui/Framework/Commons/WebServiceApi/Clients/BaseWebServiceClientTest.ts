/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi.Clients {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import WebServiceConfiguration = Com.Wui.Framework.Commons.WebServiceApi.WebServiceConfiguration;

    class MockBaseWebServiceClient extends BaseWebServiceClient {
    }

    export class BaseWebServiceClientTest extends UnitTestRunner {

        public testgetId() : void {
            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            const baseclient : BaseWebServiceClient = new MockBaseWebServiceClient(config);
            assert.notEqual(baseclient.getId(), 1433222251);
        }

        public testgetServerUrl() : void {
            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            const baseclient : BaseWebServiceClient = new MockBaseWebServiceClient(config);
            assert.notEqual(baseclient.getServerUrl(), "");
        }

        public testgetPath() : void {
            const config : WebServiceConfiguration = new WebServiceConfiguration();
            const baseclient : BaseWebServiceClient = new MockBaseWebServiceClient(config);
            const handler =  () : void => {
                const path : string = "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp";
            };
            baseclient.getServerPath(handler);
            assert.ok(StringUtils.PatternMatched("WebServiceClient(*)", baseclient.toString()));

            const config2 : WebServiceConfiguration = new WebServiceConfiguration();
            const baseclient2 : BaseWebServiceClient = new MockBaseWebServiceClient(config2);
            const handler2 =  () : void => {
                throw new Error("Unable to get server path from configuration: " + config2.getSource());
            };
            assert.throws(() : void => {
                baseclient2.getServerPath(handler2);
            }, /Unable to get server path from configuration: null/);
        }

        public testStartCommunication() : void {
            const config3 : WebServiceConfiguration = new WebServiceConfiguration();
            const baseclient3 : BaseWebServiceClient = new MockBaseWebServiceClient(config3);
            baseclient3.Send("hello", () : void => {
                config3.ServerAddress("192.168.1.111");
            });
            baseclient3.StartCommunication();
            config3.TimeoutLimit(2000);
            assert.equal(baseclient3.CommunicationIsRunning(), true);
            baseclient3.StopCommunication();
            assert.equal(baseclient3.CommunicationIsRunning(), false);
        }
    }
}
