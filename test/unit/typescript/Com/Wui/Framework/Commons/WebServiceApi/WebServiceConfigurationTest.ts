/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi {
    "use strict";

    export class WebServiceConfigurationTest extends UnitTestRunner {

        public testgetId() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            assert.ok(service.getId(), "1878964429");
        }

        public testgetSource() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            assert.equal(service.getSource(), "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
        }

        public testServerLocation() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            assert.equal(service.ServerLocation("www.wuiframevork.dev"), "www.wuiframevork.dev");
        }

        public testServerProtocol() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            assert.equal(service.ServerProtocol(), "http");
            assert.equal(service.ServerProtocol("https"), "https");
        }

        public testServerAddress() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            assert.equal(service.ServerAddress(), "127.0.0.1");
        }

        public testServerBase() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            assert.equal(service.ServerBase("server-base-log"), "server-base-log");
        }

        public testServerPort() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            assert.equal(service.ServerPort(), 80);
            assert.equal(service.ServerPort(100), 100);
        }

        public testgetServerUrl() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            assert.equal(service.getServerUrl(), "http://127.0.0.1/");

            service.ServerPort(100);
            service.ServerBase("home");
            service.ServerAddress("port");
            assert.equal(service.getServerUrl(), "http://port:100/home/");

            service.ServerBase("/home/");
            assert.equal(service.getServerUrl(), "http://port:100/home/");
        }

        public testResponseUrl() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            assert.equal(service.ResponseUrl("http://myObjectUrl"), "http://myObjectUrl");
        }

        public testTimeoutLimit() : void {
            const service : WebServiceConfiguration = new WebServiceConfiguration();
            assert.equal(service.TimeoutLimit(40), 40);
        }

        public __IgnoretestLoad() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const service : WebServiceConfiguration = new WebServiceConfiguration(
                    "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
                service.Load(
                    () => {
                        assert.equal(service.ServerAddress(), "192.168.1.111");
                        $done();
                    },
                    () => {
                        assert.equal(service.ServerAddress(), "127.0.0.1");
                        assert.ok(false, "Unable to load WebServiceConfiguration");
                        $done();
                    });
            };
        }
    }
}
