/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi.Clients {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import MessageEventArgs = Com.Wui.Framework.Commons.Events.Args.MessageEventArgs;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;

    export class PostMessageClientTest extends UnitTestRunner {

        public testConstructor() : void {
            const config : WebServiceConfiguration = new WebServiceConfiguration();
            const message : PostMessageClient = new PostMessageClient(config);
            assert.ok(StringUtils.PatternMatched("WebServiceClient(*)", message.toString()));

            message.Send("hello", () : void => {
                config.ServerAddress("192.168.1.111");
            });
            message.StartCommunication();
            message.getEvents().OnTimeout(() : void => {
                message.StopCommunication();
            });
            EventsManager.getInstanceSingleton().Clear(message.getId().toString());
            EventsManager.getInstanceSingleton().Clear("OnTimeout");
            EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_MESSAGE);
        }

        public testStartCommunication() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const config : WebServiceConfiguration = new WebServiceConfiguration();
                const message : PostMessageClient = new PostMessageClient(config);
                const client : HTMLIFrameElement = document.createElement("iframe");
                message.StartCommunication();

                const event : MessageEventArgs = new MessageEventArgs();
                const messageEvent : any = {origin: "test"};
                event.NativeEventArgs(messageEvent);
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_MESSAGE, messageEvent);
                (<any>PostMessageClient).onResponse = messageEvent;
                message.StopCommunication();
                EventsManager.getInstanceSingleton().Clear(message.getId().toString());
                EventsManager.getInstanceSingleton().Clear(GeneralEventOwner.WINDOW, EventType.ON_MESSAGE);
                $done();
            };
        }

        public testStartCommunicationError() : void {
            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            assert.equal(config.getSource(), "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            const message : PostMessageClient = new PostMessageClient(config);
            const client : HTMLIFrameElement = document.createElement("iframe");
            const messageEvent : any = {origin: "test"};
            assert.throws(() : void => {
                client.src = config.getServerUrl();
                (<any>PostMessageClient).isLoaded = false;
                throw new Error("Unable to load client configuration from: " + config.getSource());
            }, /Unable to load client configuration from:/);
            EventsManager.getInstanceSingleton().Clear(message.getId().toString());
        }

        public __IgnoretestStartCommunicationSecond() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const service : WebServiceConfiguration = new WebServiceConfiguration();
                const message : PostMessageClient = new PostMessageClient(service);
                const event : MessageEventArgs = new MessageEventArgs();
                const messageEvent : any = {origin: "test"};
                event.NativeEventArgs(messageEvent);
                service.ServerAddress("192.168.1.111");
                EventsManager.getInstanceSingleton().setEvent(GeneralEventOwner.WINDOW, EventType.ON_MESSAGE, messageEvent);
                message.Send("hello", () : void => {
                    assert.equal(event.NativeEventArgs(), messageEvent);
                    message.StartCommunication();
                    service.TimeoutLimit(2000);
                    assert.equal(message.CommunicationIsRunning(), true);
                    $done();
                });
                assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
                message.StopCommunication();
                assert.equal(message.CommunicationIsRunning(), false);
            };
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
