/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi.Clients {
    "use strict";

    export class WebSocketsClientTest extends UnitTestRunner {

        public testgetServerUrl() : void {
            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp"
            );
            const socket : WebSocketsClient = new WebSocketsClient(config);
            assert.equal(socket.getServerUrl(), "http://127.0.0.1/");
        }

        public testgetServerBase() : void {
            const config : WebServiceConfiguration = new WebServiceConfiguration(
                "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
            const socket : WebSocketsClient = new WebSocketsClient(config);
            assert.equal(socket.getServerBase(), "");
        }

        public __IgnoretestStartCommunication() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const config : WebServiceConfiguration = new WebServiceConfiguration(
                    "test/resource/data/Com/Wui/Framework/Commons/WebServiceConfiguration.jsonp");
                config.ServerAddress("10.171.89.149");
                const socket : WebSocketsClient = new WebSocketsClient(config);
                socket.Send("TestingOfTheSocket", () : void => {
                    socket.StartCommunication();
                    config.TimeoutLimit(2000);
                    assert.equal(config.ServerAddress(), "127.0.0.1");
                    assert.equal(socket.CommunicationIsRunning(), true);
                    $done();
                });
                assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
                socket.StopCommunication();
                assert.equal(socket.CommunicationIsRunning(), false);
            };
        }

        public __IgnoretestStartCommunicationSecond() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const config3 : WebServiceConfiguration = new WebServiceConfiguration();
                const socket3 : WebSocketsClient = new WebSocketsClient(config3);
                socket3.Send("hello", () : void => {
                    socket3.StartCommunication();
                    config3.TimeoutLimit(2000);
                    assert.equal(socket3.CommunicationIsRunning(), false);
                    socket3.StopCommunication();
                    assert.equal(socket3.CommunicationIsRunning(), false);
                    $done();
                });
                assert.ok(true, "Unable to load client configuration from: \" + this.getConfiguration().getSource()");
            };
        }
    }
}
