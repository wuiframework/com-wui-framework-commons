/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import FileSystemItemType = Com.Wui.Framework.Commons.Enums.FileSystemItemType;

    export class FileSystemFilterTest extends UnitTestRunner {

        public testCreate() : void {
            assert.equal(FileSystemFilter.Create(FileSystemItemType.MY_COMPUTER, FileSystemItemType.NETWORK), "MyComputer|Network");
            assert.equal(FileSystemFilter.Create(), "");
        }
    }
}
