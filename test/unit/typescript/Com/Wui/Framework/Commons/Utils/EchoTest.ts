/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import IOHandlerType = Com.Wui.Framework.Commons.Enums.IOHandlerType;
    import IOHandler = Com.Wui.Framework.Commons.Interfaces.IOHandler;

    export class EchoTest extends UnitTestRunner {
        public testInit() : void {
            this.registerElement("target1");
            this.registerElement("target2");
            Echo.Init();
            assert.doesNotThrow(() : void => {
                Echo.Init("target1");
            });
            assert.notEqual((<IOHandler>(<any>Echo).output).Name(), "target1");
            assert.doesNotThrow(() : void => {
                Echo.Init("target2", true);
            });
            assert.equal((<IOHandler>(<any>Echo).output).Name(), "target2");
        }

        public testgetHandlerType() : void {
            assert.equal(Echo.getHandlerType(), IOHandlerType.HTML_ELEMENT);
        }

        public testPrint() : void {
            Echo.ClearAll();
            assert.doesNotThrow(() : void => {
                Echo.Print("<span guitype=\"HtmlAppender\">this is info message</span>");
            });
            assert.equal(this.getEchoOutput(),
                "<span guitype=\"HtmlAppender\"><span guitype=\"HtmlAppender\">this is info message</span></span>");

            assert.doesNotThrow(() : void => {
                Echo.Print("this is info message");
            });

            assert.equal(this.getEchoOutput(),
                "<span guitype=\"HtmlAppender\"><span guitype=\"HtmlAppender\">this is info message</span></span>" +
                "<span guitype=\"HtmlAppender\">this is info message</span>");
            assert.equal(Echo.getStream(),
                "<span guitype=\"HtmlAppender\">this is info message</span>this is info message");

            assert.doesNotThrow(() : void => {
                Echo.Print("<span guitype=\"HtmlAppender\">this is info message3</span>", true);
            });

            assert.equal(this.getEchoOutput(),
                "<span guitype=\"HtmlAppender\"><span guitype=\"HtmlAppender\">this is info message3</span></span>");
            assert.equal(Echo.getStream(),
                "<span guitype=\"HtmlAppender\">this is info message</span>this is info message" +
                "<span guitype=\"HtmlAppender\">this is info message3</span>");
        }

        public testPrintf() : void {
            this.registerElement("target1");
            Echo.Clear();
            this.resetCounters();
            assert.doesNotThrow(() : void => {
                Echo.Printf("object to print {0} ", ["target1"]);
            });
            assert.equal(this.getEchoOutput(),
                "<span guitype=\"HtmlAppender\"><br>object to print <i>Object type:</i> array. <i>Return values:</i><br>" +
                "<i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;target1<br></span> </span>");

            Echo.Clear();
            assert.doesNotThrow(() : void => {
                Echo.Printf(true);
            });
            assert.equal(this.getEchoOutput(),
                "<span guitype=\"HtmlAppender\"><br><i>Object type:</i> boolean. <i>Return value:</i> true</span>");
        }

        public testPrintln() : void {
            Echo.Clear();
            assert.doesNotThrow(() : void => {
                Echo.Println("test");
            });
            assert.equal(this.getEchoOutput(),
                "<span guitype=\"HtmlAppender\"><br>test</span>");
        }

        public testPrintCode() : void {
            Echo.ClearAll();
            assert.doesNotThrow(() : void => {
                Echo.PrintCode("<span>test code</span>");
            });
            assert.equal(this.getEchoOutput(),
                "<span guitype=\"HtmlAppender\"><br><pre>&lt;span&gt;test code&lt;/span&gt;</pre></span>");
        }

        public testClear() : void {
            Echo.ClearAll();
            Echo.Println("test");
            Echo.Println("test2");
            assert.equal(this.getEchoOutput(),
                "<span guitype=\"HtmlAppender\"><br>test</span><span guitype=\"HtmlAppender\"><br>test2</span>");
            assert.doesNotThrow(() : void => {
                Echo.Clear();
            });
            assert.equal(this.getEchoOutput(), "");
            assert.equal(Echo.getStream(),
                "<br/>test<br/>test2");
        }

        public testClearAll() : void {
            Echo.ClearAll();
            Echo.Println("test");
            Echo.Println("test2");
            assert.equal(this.getEchoOutput(),
                "<span guitype=\"HtmlAppender\"><br>test</span><span guitype=\"HtmlAppender\"><br>test2</span>");
            assert.doesNotThrow(() : void => {
                Echo.ClearAll();
            });
            assert.equal(this.getEchoOutput(), "");
            assert.equal(Echo.getStream(), "");
        }

        public testgetStream() : void {
            Echo.Printf("before clean up screen");
            Echo.ClearAll();
            Echo.Printf("clean up screen");
            assert.equal(Echo.getStream(), "<br/>clean up screen");
        }

        public testsetOnPrint() : void {
            assert.doesNotThrow(() : void => {
                Echo.setOnPrint(($data : string) : void => {
                    assert.equal($data, "<br/>test me");
                });
                Echo.Println("test me");
            });
        }

        protected tearDown() : void {
            this.initSendBox();
        }

        private getEchoOutput() : string {
            return (<any>Echo).output.getHandler().innerHTML;
        }
    }
}
