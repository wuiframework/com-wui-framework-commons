/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";

    export class JsonUtilsTest extends UnitTestRunner {

        constructor() {
            super();
            // this.setMethodFilter("testExtend");
        }

        public testExtend() : void {
            let input : any = {
                test: true
            };
            assert.equal(input.test, true);
            JsonUtils.Extend(input, {test: false});
            assert.equal(input.test, false);

            input = {
                testArray : [1, true, "test"],
                testBool  : true,
                testInt   : 12,
                testObject: {
                    test: "test"
                }
            };
            JsonUtils.Extend(input, {
                testArray : [true, 1, "test"],
                testBool  : false,
                testObject: {
                    test2: true
                }
            });
            assert.deepEqual(input, {
                testArray : [true, 1, "test"],
                testBool  : false,
                testInt   : 12,
                testObject: {
                    test : "test",
                    test2: true
                }
            });

            input = {
                testObjectArray: [
                    {
                        test: 1
                    },
                    {
                        test: 2
                    }
                ]
            };
            JsonUtils.Extend(input, {
                testObjectArray : [
                    "string",
                    {
                        test2: 3
                    },
                    {
                        test3: 1
                    }
                ],
                testObjectArray2: []
            });
            assert.deepEqual(input, {
                testObjectArray : [
                    "string",
                    {
                        test2: 3
                    },
                    {
                        test3: 1
                    }
                ],
                testObjectArray2: []
            });
        }

        public testClone() : void {
            const reference : any = {};
            const input : any = {
                test: reference
            };
            assert.equal(input.test, reference);
            assert.notEqual(JsonUtils.Clone(input).test, reference);
        }

        public testDeepClone() : void {
            const reference : any = {
                value: "string"
            };
            const input : any = {
                test: reference
            };
            reference.parent = input;
            assert.equal(input.test, reference);
            const clone : any = JsonUtils.DeepClone(input);
            assert.notEqual(clone.test, reference);
            assert.equal(clone.test.value, "string");
            assert.throws(() : void => {
                JSON.stringify(clone);
            }, "Converting circular structure to JSON");

            const cloneWithSymbols : any = JsonUtils.DeepClone(input, false);
            assert.deepEqual(cloneWithSymbols, {
                test: {
                    parent: {
                        test: {
                            $ref: "#/test"
                        }
                    },
                    value : "string"
                }
            });
            assert.doesNotThrow(() : void => {
                JSON.stringify(cloneWithSymbols);
            });

            const cloneWithoutSymbols : any = JsonUtils.DeepClone(input, false, false);
            assert.deepEqual(cloneWithoutSymbols, {
                test: {
                    parent: {
                        test: {
                            parent: {
                                test: {}
                            },
                            value : "string"
                        }
                    },
                    value : "string"
                }
            });
            assert.doesNotThrow(() : void => {
                JSON.stringify(cloneWithoutSymbols);
            });
        }

        public testParseRefSymbols() : void {
            assert.deepEqual(JsonUtils.ParseRefSymbols({
                references: {
                    test: {
                        value: "string"
                    }
                },
                test      : {
                    $ref: "#/references/test/value"
                }
            }), {
                references: {
                    test: {
                        value: "string"
                    }
                },
                test      : "string"
            });
        }

        public testToJsonp() : void {
            assert.equal(JsonUtils.ToJsonp({
                test : true,
                test2: () : void => {
                    // test function
                }
            }), "" +
                "JsonpData({\n" +
                "    test: true,\n" +
                "    test2: function () {// test function}\n" +
                "});\n" +
                "");

            assert.equal(JsonUtils.ToJsonp({test: true}, JsonUtilsTest), "" +
                "Com.Wui.Framework.Commons.Utils.JsonUtilsTest.Data({\n" +
                "    test: true\n" +
                "});\n" +
                "");

            assert.equal(JsonUtils.ToJsonp({test: true}, JsonUtilsTest, [
                ObjectValidator, StringUtils
            ]), "" +
                "var ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;\n" +
                "var StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;\n" +
                "\n" +
                "Com.Wui.Framework.Commons.Utils.JsonUtilsTest.Data({\n" +
                "    test: true\n" +
                "});\n" +
                "");
        }
    }
}
