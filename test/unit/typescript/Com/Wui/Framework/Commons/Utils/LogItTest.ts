/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import LogLevel = Com.Wui.Framework.Commons.Enums.LogLevel;
    import IOHandlerFactory = Com.Wui.Framework.Commons.IOApi.IOHandlerFactory;
    import IOHandlerType = Com.Wui.Framework.Commons.Enums.IOHandlerType;
    import HTMLElementHandler = Com.Wui.Framework.Commons.IOApi.Handlers.HTMLElementHandler;

    class MockHandler extends HTMLElementHandler {
        public Init() : void {
            super.Init();
            this.setNewLineType(undefined);
        }
    }

    export class LogItTest extends UnitTestRunner {
        public testMockHandler() : void {
            this.reset();
            this.registerElement("MockHandlerTarget");
            LogIt.Init(LogLevel.ALL, new MockHandler("MockHandlerTarget"));
            assert.equal((<any>LogIt).level, LogLevel.ALL);
            LogIt.Info("this is info message");
            assert.patternEqual(this.getLogItOutput(),
                "<span guitype=\"HtmlAppender\">*" +
                "&nbsp;&nbsp;&nbsp;this is info message\n" +
                "</span>");
        }

        public testInit() : void {
            this.reset();
            LogIt.Init(LogLevel.DEBUG);
            assert.equal((<any>LogIt).level, LogLevel.DEBUG);
            assert.ok((<any>LogIt).output.IsTypeOf(Com.Wui.Framework.Commons.IOApi.Handlers.ConsoleHandler));
            try {
                throw new Error("error message exception");
            } catch (ex) {
                LogIt.Error("this is stack trace error message", ex);
            }

            this.reset();
            LogIt.Init();
            assert.equal((<any>LogIt).level, LogLevel.ALL);
            assert.ok((<any>LogIt).output.IsTypeOf(Com.Wui.Framework.Commons.IOApi.Handlers.ConsoleHandler));

            const output : string = "output";
            delete LogIt[output];
            LogIt.Init(LogLevel.ALL, IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT));
            assert.equal((<any>LogIt).level, LogLevel.ALL);
            assert.ok((<any>LogIt).output.IsTypeOf(Com.Wui.Framework.Commons.IOApi.Handlers.HTMLElementHandler));
        }

        public testgetHandlerType() : void {
            this.reset();
            assert.equal(LogIt.getHandlerType(), IOHandlerType.CONSOLE);
        }

        public testLogAll() : void {
            LogIt.setLevel(LogLevel.ALL);
            this.clearOutput();
            this.resetCounters();
            LogIt.Info("this is info message");
            LogIt.Warning("this is warning message");
            LogIt.Error("this is error message");
            LogIt.Debug("this is debug message");
            LogIt.Debug(1);
            LogIt.Debug(true);
            LogIt.Debug([1, 2.3, 4]);
            LogIt.Debug("object to print {0} ", ["target1"]);
            assert.patternEqual(this.getLogItOutput(),
                "<span guitype=\"HtmlAppender\">*<br>&nbsp;&nbsp;&nbsp;this is info message<br></span>" +
                "<span guitype=\"HtmlAppender\">*<br>&nbsp;&nbsp;&nbsp;this is warning message<br></span>" +
                "<span guitype=\"HtmlAppender\">*<br>&nbsp;&nbsp;&nbsp;this is error message<br></span><span guitype=\"HtmlAppender\">" +
                "*<br>&nbsp;&nbsp;&nbsp;this is debug message<br></span><span guitype=\"HtmlAppender\">*" +
                "<br>&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 1<br></span><span guitype=\"HtmlAppender\">*<br>" +
                "&nbsp;&nbsp;&nbsp;<i>Object type:</i> boolean. <i>Return value:</i> true<br></span><span guitype=\"HtmlAppender\">*<br>" +
                "&nbsp;&nbsp;&nbsp;<i>Object type:</i> array. <i>Return values:</i><br><i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById('ContentBlock_0').style.display=" +
                "document.getElementById('ContentBlock_0').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 1<br>" +
                "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 2.3<br>" +
                "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;<i>Object type:</i> number. <i>Return value:</i> 4<br></span><br></span>" +
                "<span guitype=\"HtmlAppender\">*<br>" +
                "&nbsp;&nbsp;&nbsp;object to print <i>Object type:</i> array. <i>Return values:</i>" +
                "<br><i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById('ContentBlock_1').style.display=" +
                "document.getElementById('ContentBlock_1').style.display==='block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br>" +
                "<span id=\"ContentBlock_1\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;target1<br></span> <br></span>");
        }

        public testLogWarning() : void {
            LogIt.setLevel(LogLevel.WARNING);
            this.clearOutput();
            LogIt.Info("this is info message");
            LogIt.Warning("this is warning message");
            LogIt.Error("this is error message");
            LogIt.Debug("this is debug message");
            assert.patternEqual(this.getLogItOutput(),
                "<span guitype=\"HtmlAppender\">*<br>&nbsp;&nbsp;&nbsp;this is info message<br></span>" +
                "<span guitype=\"HtmlAppender\">*<br>&nbsp;&nbsp;&nbsp;this is warning message<br></span>");
        }

        public testLogDebug() : void {
            LogIt.setLevel(LogLevel.DEBUG);
            this.clearOutput();
            LogIt.Info("this is info message");
            LogIt.Warning("this is warning message");
            LogIt.Error("this is error message");
            LogIt.Debug("this is debug message");
            assert.patternEqual(this.getLogItOutput(),
                "<span guitype=\"HtmlAppender\">*<br>&nbsp;&nbsp;&nbsp;this is debug message<br></span>");
        }

        public testLogInfo() : void {
            LogIt.setLevel(LogLevel.INFO);
            this.clearOutput();
            LogIt.Info("this is info message");
            LogIt.Warning("this is warning message");
            LogIt.Error("this is error message");
            LogIt.Debug("this is debug message");
            assert.patternEqual(this.getLogItOutput(),
                "<span guitype=\"HtmlAppender\">*<br>&nbsp;&nbsp;&nbsp;this is info message<br></span>");
        }

        public testLogError() : void {
            LogIt.setLevel(LogLevel.ERROR);
            this.clearOutput();
            LogIt.Info("this is info message");
            LogIt.Warning("this is warning message");
            LogIt.Debug("this is debug message");
            try {
                throw new Error("error message exception");
            } catch (ex) {
                LogIt.Error("this is stack trace error message", ex);
            }
            assert.patternEqual(this.getLogItOutput(),
                "<span guitype=\"HtmlAppender\">*<br>&nbsp;&nbsp;&nbsp;this is info message<br></span>" +
                "<span guitype=\"HtmlAppender\">*<br>&nbsp;&nbsp;&nbsp;this is warning message<br></span>" +
                "<span guitype=\"HtmlAppender\">*<br>&nbsp;&nbsp;&nbsp;this is stack trace error message<br>" +
                "&nbsp;&nbsp;&nbsp;error message exception<br>Stack trace: Error: error message exception<br>*");
        }

        public testsetOwner() : void {
            assert.doesNotThrow(() : void => {
                LogIt.setOwner("test");
                assert.equal((<any>LogIt).owner, "test");
                LogIt.setOwner("");
                assert.equal((<any>LogIt).owner, "test");
            });
        }

        public testsetOnPrint() : void {
            assert.doesNotThrow(() : void => {
                LogIt.setOnPrint(($data : string) : void => {
                    assert.equal($data, "test");
                });
            });
        }

        protected setUp() : void {
            this.registerElement("HtmlElement");
            this.reset();
            LogIt.Init(LogLevel.ALL, IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT));
        }

        protected tearDown() : void {
            this.initSendBox();
        }

        private reset() : void {
            delete (<any>LogIt).output;
            delete (<any>LogIt).level;
            delete (<any>LogIt).owner;
        }

        private clearOutput() : void {
            (<any>LogIt).output.Clear();
        }

        private getLogItOutput() : string {
            return (<any>LogIt).output.getHandler().innerHTML;
        }
    }
}
