/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import Primitives = Com.Wui.Framework.Commons.Primitives;

    export class ObjectValidatorTest extends UnitTestRunner {
        public testIsSet() : void {
            let undefinedProperty : any;
            assert.equal(ObjectValidator.IsSet(undefinedProperty), false);
            undefinedProperty = new Primitives.ArrayList<string>();
            assert.equal(ObjectValidator.IsSet(undefinedProperty), true);
        }

        public testIsObject() : void {
            assert.equal(ObjectValidator.IsObject(new Primitives.ArrayList<string>()), true);
            assert.equal(ObjectValidator.IsObject({test: "object"}), true);
            assert.equal(ObjectValidator.IsObject(null), false);
            assert.equal(ObjectValidator.IsObject(true), false);
            assert.equal(ObjectValidator.IsObject(false), false);
            assert.equal(ObjectValidator.IsObject(new Array<string>()), false);
            assert.equal(ObjectValidator.IsObject([]), false);
            assert.equal(ObjectValidator.IsObject(""), false);
            assert.equal(ObjectValidator.IsObject(" "), false);
            assert.equal(ObjectValidator.IsObject("asd"), false);
            assert.equal(ObjectValidator.IsObject(1234), false);
            assert.equal(ObjectValidator.IsObject(12.34), false);
            let undefinedProperty : any; // tslint:disable-line
            assert.equal(ObjectValidator.IsObject(undefinedProperty), false);
        }

        public testIsFunction() : void {
            assert.equal(ObjectValidator.IsFunction(($arg : any) : void => {
                // test function
            }), true);
            assert.equal(ObjectValidator.IsFunction(new Primitives.ArrayList<string>()), false);
            assert.equal(ObjectValidator.IsFunction({test: "object"}), false);
            assert.equal(ObjectValidator.IsFunction(null), false);
            assert.equal(ObjectValidator.IsFunction(true), false);
            assert.equal(ObjectValidator.IsFunction(false), false);
            assert.equal(ObjectValidator.IsFunction(new Array<string>()), false);
            assert.equal(ObjectValidator.IsFunction([]), false);
            assert.equal(ObjectValidator.IsFunction(""), false);
            assert.equal(ObjectValidator.IsFunction(" "), false);
            assert.equal(ObjectValidator.IsFunction("asd"), false);
            assert.equal(ObjectValidator.IsFunction(1234), false);
            assert.equal(ObjectValidator.IsFunction(12.34), false);
            let undefinedProperty : any; // tslint:disable-line
            assert.equal(ObjectValidator.IsFunction(undefinedProperty), false);
        }

        public testIsClass() : void {
            assert.equal(ObjectValidator.IsClass(new Primitives.ArrayList<string>()), true);
            assert.equal(ObjectValidator.IsClass({test: "object"}), false);
            assert.equal(ObjectValidator.IsClass(null), false);
            assert.equal(ObjectValidator.IsClass(true), false);
            assert.equal(ObjectValidator.IsClass(false), false);
            assert.equal(ObjectValidator.IsClass(new Array<string>()), false);
            assert.equal(ObjectValidator.IsClass([]), false);
            assert.equal(ObjectValidator.IsClass(""), false);
            assert.equal(ObjectValidator.IsClass(" "), false);
            assert.equal(ObjectValidator.IsClass("asd"), false);
            assert.equal(ObjectValidator.IsClass(1234), false);
            assert.equal(ObjectValidator.IsClass(12.34), false);
            let undefinedProperty : any; // tslint:disable-line
            assert.equal(ObjectValidator.IsClass(undefinedProperty), false);
        }

        public testIsEmptyOrNull() : void {
            assert.equal(ObjectValidator.IsEmptyOrNull(null), true);
            assert.equal(ObjectValidator.IsEmptyOrNull(""), true);
            assert.equal(ObjectValidator.IsEmptyOrNull("string"), false);
            assert.equal(ObjectValidator.IsEmptyOrNull(123), false);

            const array : Primitives.ArrayList<string> = new Primitives.ArrayList<string>();
            assert.equal(ObjectValidator.IsEmptyOrNull(array), true);
            array.Add("value");
            assert.equal(ObjectValidator.IsEmptyOrNull(array), false);
            assert.equal(ObjectValidator.IsEmptyOrNull([]), true);
            assert.equal(ObjectValidator.IsEmptyOrNull(["value"]), false);
            let undefinedProperty : any; // tslint:disable-line
            assert.equal(ObjectValidator.IsEmptyOrNull(undefinedProperty), true);
        }

        public testIsBoolean() : void {
            assert.equal(ObjectValidator.IsBoolean(true), true);
            assert.equal(ObjectValidator.IsBoolean(false), true);
            assert.equal(ObjectValidator.IsBoolean(null), false);
            assert.equal(ObjectValidator.IsBoolean(""), false);
            assert.equal(ObjectValidator.IsBoolean(" "), false);
            assert.equal(ObjectValidator.IsBoolean("asd"), false);
            assert.equal(ObjectValidator.IsBoolean(1), false);
            assert.equal(ObjectValidator.IsBoolean(0), false);
            assert.equal(ObjectValidator.IsBoolean(1234), false);
            assert.equal(ObjectValidator.IsBoolean(12.34), false);
            let undefinedProperty : boolean; // tslint:disable-line
            assert.equal(ObjectValidator.IsBoolean(undefinedProperty), false);
        }

        public testIsDigit() : void {
            assert.equal(ObjectValidator.IsDigit(123), true);
            assert.equal(ObjectValidator.IsDigit(12.34), true);
            assert.equal(ObjectValidator.IsDigit("123"), true);
            assert.equal(ObjectValidator.IsDigit("12.34"), true);
            assert.equal(ObjectValidator.IsDigit(""), false);
            assert.equal(ObjectValidator.IsDigit(" "), false);
            assert.equal(ObjectValidator.IsDigit(null), false);
            assert.equal(ObjectValidator.IsDigit("asd"), false);
            let undefinedProperty : number; // tslint:disable-line
            assert.equal(ObjectValidator.IsDigit(undefinedProperty), false);
            assert.equal(ObjectValidator.IsDigit("+1 sec"), false);
        }

        public testIsInteger() : void {
            assert.equal(ObjectValidator.IsInteger(123), true);
            assert.equal(ObjectValidator.IsInteger(12.34), false);
            assert.equal(ObjectValidator.IsInteger("123"), true);
            assert.equal(ObjectValidator.IsInteger("12.34"), false);
            assert.equal(ObjectValidator.IsInteger(""), false);
            assert.equal(ObjectValidator.IsInteger(" "), false);
            assert.equal(ObjectValidator.IsInteger(null), false);
            assert.equal(ObjectValidator.IsInteger("asd"), false);
            let undefinedProperty : number; // tslint:disable-line
            assert.equal(ObjectValidator.IsInteger(undefinedProperty), false);
        }

        public testIsDouble() : void {
            assert.equal(ObjectValidator.IsDouble(123), false);
            assert.equal(ObjectValidator.IsDouble(12.34), true);
            assert.equal(ObjectValidator.IsDouble("123"), false);
            assert.equal(ObjectValidator.IsDouble("12.34"), true);
            assert.equal(ObjectValidator.IsDouble(""), false);
            assert.equal(ObjectValidator.IsDouble(" "), false);
            assert.equal(ObjectValidator.IsDouble(null), false);
            assert.equal(ObjectValidator.IsDouble("asd"), false);
            let undefinedProperty : number; // tslint:disable-line
            assert.equal(ObjectValidator.IsDouble(undefinedProperty), false);
            let conversion : number;
            conversion = Com.Wui.Framework.Commons.Utils.StringUtils.ToDouble("1.0");
            assert.equal(ObjectValidator.IsDouble(conversion), false);
            conversion = Com.Wui.Framework.Commons.Utils.StringUtils.ToDouble("1.2");
            assert.equal(ObjectValidator.IsDouble(conversion), true);
        }

        public testIsString() : void {
            assert.equal(ObjectValidator.IsString(""), true);
            assert.equal(ObjectValidator.IsString(" "), true);
            assert.equal(ObjectValidator.IsString("asd"), true);
            assert.equal(ObjectValidator.IsString("123"), true);
            assert.equal(ObjectValidator.IsString("12.34"), true);
            assert.equal(ObjectValidator.IsString(123), false);
            assert.equal(ObjectValidator.IsString(12.34), false);
            assert.equal(ObjectValidator.IsString(null), false);
            let undefinedProperty : string; // tslint:disable-line
            assert.equal(ObjectValidator.IsString(undefinedProperty), false);
            assert.equal(ObjectValidator.IsString({object: "value"}), false);
            assert.equal(ObjectValidator.IsString(() : void => {
                // test function
            }), false);
            assert.equal(ObjectValidator.IsString(ObjectValidator), false);
        }

        public testIsNativeArray() : void {
            assert.equal(ObjectValidator.IsNativeArray([]), true);
            assert.equal(ObjectValidator.IsNativeArray(new Array<string>()), true);
            assert.equal(ObjectValidator.IsNativeArray(new Primitives.ArrayList<string>()), false);
            assert.equal(ObjectValidator.IsNativeArray(null), false);
            assert.equal(ObjectValidator.IsNativeArray(""), false);
            assert.equal(ObjectValidator.IsNativeArray(" "), false);
            assert.equal(ObjectValidator.IsNativeArray(123), false);
            assert.equal(ObjectValidator.IsNativeArray(12.34), false);
            let undefinedProperty : number; // tslint:disable-line
            assert.equal(ObjectValidator.IsNativeArray(undefinedProperty), false);
        }

        public testIsArray() : void {
            assert.equal(ObjectValidator.IsArray([]), true);
            assert.equal(ObjectValidator.IsArray(new Array<string>()), true);
            assert.equal(ObjectValidator.IsArray(new Primitives.ArrayList<string>()), true);
            assert.equal(ObjectValidator.IsArray(null), false);
            assert.equal(ObjectValidator.IsArray(""), false);
            assert.equal(ObjectValidator.IsArray(" "), false);
            assert.equal(ObjectValidator.IsArray(123), false);
            assert.equal(ObjectValidator.IsArray(12.34), false);
            let undefinedProperty : number; // tslint:disable-line
            assert.equal(ObjectValidator.IsArray(undefinedProperty), false);
        }

        public testIsHexadecimal() : void {
            assert.equal(ObjectValidator.IsHexadecimal("FFFFFF"), true);
            assert.equal(ObjectValidator.IsHexadecimal("E963A535"), true);
            assert.equal(ObjectValidator.IsHexadecimal("0xFFFfFF"), true);
            assert.equal(ObjectValidator.IsHexadecimal("0XFFfFFF"), true);
            assert.equal(ObjectValidator.IsHexadecimal("#FFFFFF"), true);
            assert.equal(ObjectValidator.IsHexadecimal("#FF"), true);
            assert.equal(ObjectValidator.IsHexadecimal(""), false);
            assert.equal(ObjectValidator.IsHexadecimal("0x"), false);
            assert.equal(ObjectValidator.IsHexadecimal("#"), false);
            assert.equal(ObjectValidator.IsHexadecimal("#t"), false);
            assert.equal(ObjectValidator.IsHexadecimal("0"), true);
            assert.equal(ObjectValidator.IsHexadecimal("#00"), true);
            assert.equal(ObjectValidator.IsHexadecimal("0x00"), true);
            assert.equal(ObjectValidator.IsHexadecimal(null), false);
        }
    }
}
