/* ********************************************************************************************************* *
 *
 * Copyright (c) 2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;

    export class CountersTest extends UnitTestRunner {
        public testgetNext() : void {
            assert.equal(Counters.getNext(CountersTest.ClassName()), 0);
            assert.equal(Counters.getNext(CountersTest.ClassName()), 1);
            assert.equal(Counters.getNext(CountersTest.ClassName()), 2);
            assert.equal(Counters.getNext(BaseObject.ClassName()), 0);
            assert.equal(Counters.getNext(BaseObject.ClassName()), 1);
            assert.equal(Counters.getNext(CountersTest.ClassName()), 3);
        }

        public testClearOne() : void {
            assert.equal(Counters.getNext(CountersTest.ClassName()), 0);
            assert.equal(Counters.getNext(CountersTest.ClassName()), 1);
            assert.equal(Counters.getNext(CountersTest.ClassName()), 2);
            assert.equal(Counters.getNext(BaseObject.ClassName()), 0);
            assert.equal(Counters.getNext(BaseObject.ClassName()), 1);
            Counters.Clear(CountersTest.ClassName());
            assert.equal(Counters.getNext(CountersTest.ClassName()), 0);
            assert.equal(Counters.getNext(BaseObject.ClassName()), 2);
        }

        public testClearAll() : void {
            assert.equal(Counters.getNext(CountersTest.ClassName()), 0);
            assert.equal(Counters.getNext(CountersTest.ClassName()), 1);
            assert.equal(Counters.getNext(CountersTest.ClassName()), 2);
            assert.equal(Counters.getNext(BaseObject.ClassName()), 0);
            assert.equal(Counters.getNext(BaseObject.ClassName()), 1);
            Counters.Clear();
            assert.equal(Counters.getNext(CountersTest.ClassName()), 0);
            assert.equal(Counters.getNext(BaseObject.ClassName()), 0);
        }
    }
}
