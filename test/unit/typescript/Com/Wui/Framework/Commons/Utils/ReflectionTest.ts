/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IBaseObject = Com.Wui.Framework.Commons.Interfaces.IBaseObject;
    import IReflection = Com.Wui.Framework.Commons.Interfaces.IReflection;
    import IArrayList = Com.Wui.Framework.Commons.Interfaces.IArrayList;
    import EventsManagerTest = Com.Wui.Framework.Commons.RuntimeTests.EventsManagerTest;
    import RuntimeTestRunner = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.RuntimeTestRunner;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Interface = Com.Wui.Framework.Commons.Interfaces.Interface;
    import AsyncHttpResolver = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.AsyncHttpResolver;

    class MockBaseObject extends Primitives.BaseObject {
    }

    export class ReflectionTest extends UnitTestRunner {

        public testsetInstanceNamespaces() : void {
            Reflection.setInstanceNamespaces();
            Reflection.setInstanceNamespaces("Com.Wui", "Xyz.Company", "Abc", "Declared");
            assert.deepEqual((<any>Reflection).singletonNamespaces, ["Com.Wui", "Xyz.Company", "Abc", "Declared"]);
        }

        public testgetInstance() : void {
            assert.equal(ObjectValidator.IsEmptyOrNull(Reflection.getInstance()), false);
            assert.equal(Reflection.getInstance().getClassName(new MockBaseObject()),
                "Com.Wui.Framework.Commons.Primitives.BaseObject");
        }

        public testConstructor() : void {
            (<any>window).Abc = (() : any => {
                function Abc() : void {
                    // namaspace with empty body
                }

                (<any>Abc).Test = () : void => {
                    // namaspace with empty body
                };

                return Abc;
            })();

            (<any>window).Xyz = (() : any => {
                function Xyz() : void {
                    // namaspace with empty body
                }

                return Xyz;
            })();
            (<any>window).Xyz.Company = (() : any => {
                function Company() : void {
                    // class with empty body
                }

                return Company;
            })();

            const reflection : Reflection = new Reflection();
            assert.ok(!reflection.Exists("Xyz.Company"));
            assert.ok(!reflection.Exists("Abc.Test"));
            const thisClass : AsyncHttpResolver = Reflection.getInstance().getClass(AsyncHttpResolver.ClassName());
            const getClass : string = Reflection.getInstance().getClassName();

            assert.equal(Reflection.getInstance().getNamespaceName(), "Com.Wui.Framework.Commons.Utils");
            const thisnewClass : AsyncHttpResolver = Reflection.getInstance().getClass(AsyncHttpResolver.NamespaceName());
            const getnameSpace : string = Reflection.getInstance().getNamespaceName();
            assert.equal(getnameSpace, "Com.Wui.Framework.Commons.Utils");
            const list : ArrayList<any> = ArrayList.getInstance().getNamespaceName();
            assert.equal(list, "Com.Wui.Framework.Commons.Primitives");

            const thisNamespace : AsyncHttpResolver = Reflection.getInstance().getClass(AsyncHttpResolver.ClassNameWithoutNamespace());
            const withoutNamespace : string = Reflection.getInstance().getClassNameWithoutNamespace();
            assert.equal(withoutNamespace, "Reflection");
            const array : ArrayList<any> = ArrayList.getInstance().getClassNameWithoutNamespace();
            assert.equal(array, "ArrayList");
        }

        public testClassName() : void {
            assert.equal(Reflection.ClassName(), "Com.Wui.Framework.Commons.Utils.Reflection");
        }

        public testClassNameWithoutNamespace() : void {
            assert.equal(Reflection.ClassNameWithoutNamespace(), "Reflection");
        }

        public testNamespaceName() : void {
            assert.equal(Reflection.NamespaceName(), "Com.Wui.Framework.Commons.Utils");
        }

        public testUID() : void {
            assert.notEqual(Reflection.UID(), Reflection.UID());
            assert.notEqual(Reflection.UID(BaseObject.ClassName()), Reflection.UID(BaseObject.ClassName()));
        }

        public testgetAllClasses() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.ok(!ObjectValidator.IsEmptyOrNull(reflect.getAllClasses()));
        }

        public testExists() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.equal(reflect.Exists("Com.Wui.Framework.Commons.Primitives.BaseObject"), true);
            assert.equal(reflect.Exists(BaseObject.ClassName()), true);
            assert.equal(reflect.Exists(null), false);
            assert.equal(reflect.Exists("Com.Wui.Framework.Commons.NotExisting"), false);
        }

        public testgetClass() : void {
            const reflect : Reflection = Reflection.getInstance();
            const className : any = reflect.getClass("Com.Wui.Framework.Commons.Primitives.BaseObject");
            const object : BaseObject = new className();
            assert.equal(className.ClassName(), "Com.Wui.Framework.Commons.Primitives.BaseObject");
            assert.equal(object instanceof BaseObject, true);
        }

        public testgetClassName() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.equal(reflect.getClassName(), "Com.Wui.Framework.Commons.Utils.Reflection");
            assert.equal(reflect.getClassName(new MockBaseObject()), "Com.Wui.Framework.Commons.Primitives.BaseObject");
        }

        public testgetNamespaceName() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.equal(reflect.getNamespaceName(), "Com.Wui.Framework.Commons.Utils");
            assert.equal(reflect.getNamespaceName(new MockBaseObject()), "Com.Wui.Framework.Commons.Primitives");
        }

        public testgetClassNameWithoutNamespace() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.equal(reflect.getClassNameWithoutNamespace(), "Reflection");
            assert.equal(reflect.getClassNameWithoutNamespace(new MockBaseObject()), "BaseObject");
        }

        public testgetUID() : void {
            const reflect1 : Reflection = Reflection.getInstance();
            const reflect2 : Reflection = Reflection.getInstance();
            assert.notEqual(reflect1.getUID(), reflect2.getUID());
        }

        public testIsInstanceOf() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.equal(reflect.IsInstanceOf(new MockBaseObject(), "Com.Wui.Framework.Commons.Primitives.BaseObject"), true);
            assert.equal(reflect.IsInstanceOf(new MockBaseObject(), BaseObject.ClassName()), true);
            assert.equal(reflect.IsInstanceOf(new MockBaseObject(), ArrayList.ClassName()), false);
            assert.equal(reflect.IsInstanceOf(new ArrayList(), BaseObject), false);
            assert.equal(reflect.IsInstanceOf(new ArrayList(), "Com.Wui.Framework.Commons.Primitives.ArrayList"), true);
            assert.equal(reflect.IsInstanceOf(<any>{}, BaseObject), false);
            const array : ArrayList<any> = new ArrayList<any>();
            assert.equal(reflect.IsInstanceOf(array, array.getProperties().toString()), false);
            assert.equal(reflect.IsInstanceOf(MockBaseObject.getInstance(), "Com.Wui.Framework.Commons.Primitives.BaseObject"), true);
            const thisClass : any = () : void => {
                const object : ArrayList<string> = new ArrayList<string>();
                object.Add("test0");
                object.Add("test1");
            };
            assert.equal(reflect.IsInstanceOf(new ArrayList<string>(), thisClass), false);
        }

        public testIsMemberOf() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.equal(reflect.IsMemberOf(<any>BaseObject), true);
            assert.equal(reflect.IsMemberOf(<any>Reflection), true);
            assert.equal(reflect.IsMemberOf(<any>ArrayList), false);
            assert.equal(reflect.IsMemberOf(new MockBaseObject(), BaseObject.ClassName()), true);
            assert.equal(reflect.IsMemberOf(new ArrayList<string>(), BaseObject.ClassName()), true);
            assert.equal(reflect.IsMemberOf(new MockBaseObject(), ArrayList.ClassName()), false);
            assert.equal(reflect.IsMemberOf(null, ""), false);
            assert.equal(reflect.IsMemberOf(new MockBaseObject(),
                "Com.Wui.Framework.Commons.Primitives.BaseObject"), true);
            assert.equal(reflect.IsMemberOf(new ArrayList<string>(),
                "Com.Wui.Framework.Commons.Primitives.ArrayList"), true);
            assert.equal(reflect.IsMemberOf(<any>ArrayList.ClassName()), false);
            const object : ArrayList<any> = new ArrayList<any>();
            assert.equal(reflect.IsMemberOf(object, ArrayList), true);
            assert.equal(reflect.IsMemberOf(<any>true), false);
            assert.equal(reflect.IsMemberOf(undefined, BaseObject.ClassName()), false);
        }

        public testClassHasInterface() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.equal(reflect.ClassHasInterface(BaseObject, IBaseObject), true);
            assert.equal(reflect.ClassHasInterface(BaseObject.ClassName(), IBaseObject), true);
            assert.equal(reflect.ClassHasInterface(ArrayList.ClassName(), IArrayList), true);
            assert.equal(reflect.ClassHasInterface(BaseObject, IArrayList), false);
            assert.equal(reflect.ClassHasInterface(BaseObject.ClassName(), IArrayList), false);
            assert.equal(reflect.ClassHasInterface(EventsManagerTest.ClassName(), RuntimeTestRunner.ClassName()), true);
            assert.equal(reflect.ClassHasInterface(BaseObject, ""), false);
            assert.equal(reflect.ClassHasInterface(reflect.getClassName(), IReflection), true);
            assert.equal(reflect.ClassHasInterface("Com.Wui.Framework.Commons.Primitives.ArrayList",
                "Com.Wui.Framework.Commons.Interfaces.IArrayList"), true);

            const mockClass : any = (() : any => {
                const property : string = "";

                function mockClass() : void {
                    // namaspace with empty body
                }

                mockClass.prototype[property] = false;

                return mockClass;
            })();
            assert.equal(reflect.ClassHasInterface(BaseObject, Interface.getInstance([])), false);
            assert.equal(reflect.ClassHasInterface(mockClass, IBaseObject), false);
        }

        public testImplements() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.equal(reflect.Implements(IReflection), true);
            assert.equal(reflect.Implements(IBaseObject), true);
            assert.equal(reflect.Implements(IArrayList), false);
            assert.equal(reflect.Implements(new MockBaseObject(), IBaseObject), true);
            assert.equal(reflect.Implements(new ArrayList<string>(), IArrayList), true);
            assert.equal(reflect.Implements(new MockBaseObject(), IArrayList), false);
            assert.equal(reflect.Implements("Com.Wui.Framework.Commons.Interfaces.IArrayList"), false);
            assert.equal(reflect.Implements(BaseObject.ClassName(), IBaseObject), false);
            assert.equal(reflect.Implements(ArrayList.ClassName(), IArrayList), false);
            assert.equal(reflect.Implements(new MockBaseObject(), null), false);
            assert.equal(reflect.Implements("", null), false);
            assert.equal(reflect.Implements(ArrayList.ClassName(),
                "Com.Wui.Framework.Commons.Interfaces.IArrayList"), false);
            assert.equal(reflect.Implements("Com.Wui.Framework.Commons.Primitives.ArrayList",
                "Com.Wui.Framework.Commons.Interfaces.IArrayList"), false);
            assert.equal(reflect.Implements(<any>BaseObject, IBaseObject), false);
            assert.equal(reflect.Implements(<any>true), false);
        }

        public testgetInstanceOf() : void {
            assert.deepEqual(Reflection.getInstanceOf(BaseObject.ClassName(), {test: "value"}), null);
            const instance : BaseObject = Reflection.getInstanceOf("Com.Wui.Framework.Commons.Primitives.BaseObject");
            assert.ok(instance.IsTypeOf(BaseObject));
            const object : any = "getClassName";
            assert.equal(Reflection.getInstanceOf("getClassName", object),
                null);

            const list : any = {size: 0, keys: [], data: []};
            Reflection.getInstanceOf(ArrayList.ClassName(), {size: 0, keys: [], data: []});
            Reflection.getInstanceOf("Com.Wui.Framework.Commons.Primitives.ArrayList", {size: 0, keys: [], data: []});

            const dataObject : any = {
                getClassName() {
                    return ArrayList.ClassName();
                }, size: 100
            };
            Reflection.getInstanceOf(ArrayList.ClassName(), dataObject);
            assert.equal(ArrayList.ClassName(), "Com.Wui.Framework.Commons.Primitives.ArrayList");
        }

        public testgetHash() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.equal(reflect.getHash(), StringUtils.getCrc(JSON.stringify(reflect.getAllClasses())));
        }

        public testIsTypeOf() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.equal(reflect.IsTypeOf(BaseObject), false);
            assert.equal(reflect.IsTypeOf(String), false);
            assert.equal(reflect.IsTypeOf(Reflection), true);
        }

        public testSerializationData() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.deepEqual(reflect.SerializationData(), {classesList: reflect.getAllClasses()});
        }

        public testgetProperties() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.deepEqual(reflect.getProperties(), []);
        }

        public testgetMethods() : void {
            const reflect : Reflection = Reflection.getInstance();
            assert.deepEqual(reflect.getMethods(), [
                "getUID",
                "getClassName",
                "getNamespaceName",
                "getClassNameWithoutNamespace",
                "getAllClasses",
                "Exists",
                "getClass",
                "IsInstanceOf",
                "IsMemberOf",
                "ClassHasInterface",
                "Implements",
                "getProperties",
                "getMethods",
                "getHash",
                "IsTypeOf",
                "SerializationData",
                "ToString",
                "toString",
                "setNamespace"
            ]);
        }

        public testToString() : void {
            const reflect : Reflection = Reflection.getInstance();
            this.resetCounters();
            assert.patternEqual(reflect.ToString(),
                "<i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.UnitTestRunner<br/>" +
                "[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Commons.EnvironmentArgs<br/>" +
                "[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Commons.IEnvironmentConfig<br/>" +
                "[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Commons.Index<br/>" +
                "[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Commons.Loader<br/>" +
                "*<br/>" +
                "</span>",
                "convert reflection to string");

            this.resetCounters();
            assert.patternEqual(reflect.ToString("___"),
                "<i>Array object</i> " +
                "<span onclick=\"" +
                "document.getElementById(\'ContentBlock_0\').style.display=" +
                "document.getElementById(\'ContentBlock_0\').style.display===\'block\'?\'none\':\'block\';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">Open/Close</span><br/>" +
                "<span id=\"ContentBlock_0\" style=\"border: 0 solid black; display: none;\">" +
                "___[ 0 ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.UnitTestRunner<br/>" +
                "___[ 1 ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Commons.EnvironmentArgs<br/>" +
                "___[ 2 ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Commons.IEnvironmentConfig<br/>" +
                "___[ 3 ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Commons.Index<br/>" +
                "___[ 4 ]&nbsp;&nbsp;&nbsp;&nbsp;Com.Wui.Framework.Commons.Loader<br/>" +
                "*<br/>" +
                "</span>",
                "convert reflection to string with prefix");

            this.resetCounters();
            assert.patternEqual(reflect.ToString("", false),
                "Array object\r\n" +
                "[ 0 ]    Com.Wui.Framework.UnitTestRunner\r\n" +
                "[ 1 ]    Com.Wui.Framework.Commons.EnvironmentArgs\r\n" +
                "[ 2 ]    Com.Wui.Framework.Commons.IEnvironmentConfig\r\n" +
                "[ 3 ]    Com.Wui.Framework.Commons.Index\r\n" +
                "[ 4 ]    Com.Wui.Framework.Commons.Loader\r\n" +
                "*\r\n",
                "convert reflection to plain text string");
        }

        public testtoString() : void {
            const reflect : Reflection = Reflection.getInstance();
            this.resetCounters();
            const value : string = reflect.ToString();
            this.resetCounters();
            assert.equal(reflect.toString(), value);
        }
    }
}
