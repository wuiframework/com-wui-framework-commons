/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";

    export class JSONTest extends UnitTestRunner {
        public teststringify() : void {
            assert.equal(WuiJSON.stringify(""), JSON.stringify(""));
            let value : string; // tslint:disable-line
            assert.equal(WuiJSON.stringify(value), JSON.stringify(value));

            const object : string = "value";
            assert.equal(WuiJSON.stringify(object), JSON.stringify(object));

            /* tslint:disable: no-empty */
            const object2 : () => void = () : void => {
            };
            /* tslint:enable */
            assert.equal(WuiJSON.stringify(object2), JSON.stringify(object2));

            let property : any; // tslint:disable-line
            const object3 : any = {
                /* tslint:disable: object-literal-sort-keys no-empty */
                key  : "value",
                key2 : 1,
                key3 : 12.1,
                key4 : false,
                key5 : [1, 2, property, 3],
                key6 : {key1: "test"},
                key7 : "",
                key8 : null,
                key9 : true,
                key10: property,
                key11() : void {
                },
                key12: new Date()
                /* tslint:enable */
            };
            assert.equal(WuiJSON.stringify(object3), JSON.stringify(object3));

            let property2 : any; // tslint:disable-line
            const replaceHandler : ($key : string, $value : any) => any = ($key : string, $value : any) : any => {
                if ($key === "key2") {
                    return 15;
                }
                if ($key === "key3") {
                    return property2;
                }
                return $value;
            };
            const object4 : any = {
                key : "value",
                key2: 1,
                key3: 12.1
            };

            assert.equal(WuiJSON.stringify(object4), JSON.stringify(object4));
            assert.equal(WuiJSON.stringify(object4, replaceHandler), JSON.stringify(object4, replaceHandler));
            assert.equal(WuiJSON.stringify(object4, replaceHandler, 11), JSON.stringify(object4, replaceHandler, 11));
            assert.equal(WuiJSON.stringify(object4, null), JSON.stringify(object4, null));
            assert.equal(WuiJSON.stringify(object4, null, null), JSON.stringify(object4, null, null));
            assert.equal(WuiJSON.stringify(object4, replaceHandler, 0), JSON.stringify(object4, replaceHandler, 0));
            assert.equal(WuiJSON.stringify(object4, null, "___"), JSON.stringify(object4, null, "___"));
        }

        public testparse() : void {
            assert.throws(() : void => {
                WuiJSON.parse("test");
            }, /Unexpected token in JSON/);

            const object : string = "{" +
                "\"key1\":\"value\"," +
                "\"key2\":1," +
                "\"key3\":12.3," +
                "\"key4\":true," +
                "\"key5\":false," +
                "\"key6\":[10,20,30]," +
                "\"key7\":{\"key\":\"test\"}," +
                "\"key8\":null," +
                "\"key9\":\"\"" +
                "}";
            assert.deepEqual(WuiJSON.parse(object), JSON.parse(object));
        }
    }
}
