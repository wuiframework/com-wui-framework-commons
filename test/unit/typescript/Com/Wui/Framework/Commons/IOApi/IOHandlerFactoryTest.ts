/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi {
    "use strict";
    import ConsoleHandler = Com.Wui.Framework.Commons.IOApi.Handlers.ConsoleHandler;
    import HTMLElementHandler = Com.Wui.Framework.Commons.IOApi.Handlers.HTMLElementHandler;
    import IOHandlerType = Com.Wui.Framework.Commons.Enums.IOHandlerType;
    import IOHandler = Com.Wui.Framework.Commons.Interfaces.IOHandler;
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;

    class MockBaseObject extends BaseObject {
    }

    export class IOHandlerFactoryTest extends UnitTestRunner {

        public testgetHendler() : void {
            const handler1 : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.CONSOLE, "config");
            const handler2 : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.INPUT_FILE, "input.txt");
            const handler3 : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.OUTPUT_FILE, "output.txt");
            const handler4 : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT);
            const handler5 : IOHandler = IOHandlerFactory.getHandler("test", "owner");

            assert.equal(IOHandlerFactory.getHandler(IOHandlerType.CONSOLE, "config"), handler1);
            assert.equal(IOHandlerFactory.getHandler(IOHandlerType.INPUT_FILE), handler2);
            assert.equal(IOHandlerFactory.getHandler(IOHandlerType.OUTPUT_FILE), handler3);
            assert.equal(IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT), handler4);
            assert.deepEqual(handler5, null);
        }

        public testgetHandlerType() : void {
            const console : ConsoleHandler = new ConsoleHandler("test1");
            const htmlElement : HTMLElementHandler = new HTMLElementHandler("test3");
            assert.equal(IOHandlerFactory.getHandlerType(console), IOHandlerType.CONSOLE);
            assert.equal(IOHandlerFactory.getHandlerType(htmlElement), IOHandlerType.HTML_ELEMENT);
            assert.equal(IOHandlerFactory.getHandlerType(<any>(new MockBaseObject())), null);
            assert.equal(IOHandlerFactory.getHandlerType(undefined), null);
        }

        public testDestroyAll() : void {
            this.registerElement("HtmlElement");
            this.registerElement("testHtmlTarget");
            const htmlElement : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT, "testHtmlTarget");
            htmlElement.Init();
            htmlElement.setOnPrint(($message : string) : void => {
                assert.equal($message, "testing");
            });
            htmlElement.Print("testing");
            assert.equal(document.getElementById("testHtmlTarget").innerHTML, "<span guitype=\"HtmlAppender\">testing</span>");
            IOHandlerFactory.DestroyAll();
            assert.equal(document.getElementById("testHtmlTarget").innerHTML, "");

            const member : string = "handlersList";
            delete IOHandlerFactory[member];
            IOHandlerFactory.DestroyAll();
            this.initSendBox();
        }
    }
}
