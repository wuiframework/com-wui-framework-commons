/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi.Handlers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class ConsoleHandlerTest extends UnitTestRunner {
        public testConstructor() : void {
            const console : ConsoleHandler = new ConsoleHandler("handler");
            assert.equal(console.Name(), "handler");
        }

        public testInit() : void {
            const consoleHandler : ConsoleHandler = new ConsoleHandler("haha");
            consoleHandler.Init();
        }

        public testPrint() : void {
            const consoleHandler : ConsoleHandler = new ConsoleHandler("haha");
            consoleHandler.Print("Handling Console");
            assert.deepEqual(consoleHandler.Encoding(), "UTF-8");
        }

        public testClear() : void {
            const console : ConsoleHandler = new ConsoleHandler();
            assert.doesNotThrow(() : void => {
                console.Clear();
            });
        }
    }
}
