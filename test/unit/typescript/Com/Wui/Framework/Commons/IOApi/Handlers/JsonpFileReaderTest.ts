/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi.Handlers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import HttpRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs;

    export class JsonpFileReaderTest extends UnitTestRunner {

        public __IgnoretestLoad() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const path : string = "test/resource/data/Com/Wui/Framework/Commons/ValidData.jsonp";
                JsonpFileReader.Load(
                    path,
                    ($data : any, $filePath? : string) : void => {
                        assert.deepEqual($data,
                            {
                                testKey1: "testValue1",
                                testKey2: true
                            });
                        assert.equal($filePath, path);
                        ExceptionsManager.Clear();
                        $done();
                    },
                    ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                        assert.equal($filePath, path);
                        assert.ok(false, "ValidData has not been loaded correctly.");
                        ExceptionsManager.Clear();
                        $done();
                    });
            };
        }

        public __IgnoretestLoadSecond() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.setUrl("file:///" + this.getAbsoluteRoot() + "/build/target/index.html");
                UnitTestLoader.Load(<any>{
                    build: {time: new Date().toTimeString(), type: "prod"}, name: "com-wui-framework-commons", version: "1.0.0"
                });
                const path : string = "file://test/\"?dummy\"/test.txt";
                JsonpFileReader.Load(
                    path, ($data : any, $filePath? : string) : void => {
                        assert.deepEqual($filePath, "file://test/\"?dummy\"/test.txt");
                        ExceptionsManager.Clear();
                        $done();
                    },
                    ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                        assert.ok($filePath, "file://test/\"?dummy\"/test.txt");
                        $done();
                        ExceptionsManager.Clear();
                    });
            };
        }

        public __IgnoretestLoadThirdHttps() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.setUrl("https://localhost:80/required/path/location");
                const path : string = "https://localhost:80/required/path/location";

                JsonpFileReader.Load(
                    path, ($data : any, $filePath? : string) : void => {
                        assert.deepEqual($filePath, "https://localhost:80/required/path/location");
                        ExceptionsManager.Clear();
                        $done();
                    },
                    ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                        assert.ok($filePath, "https://localhost:8888/required/path/location");
                        ExceptionsManager.Clear();
                        $done();
                    });
            };
        }

        public __IgnoretestLoadWithoutErrorHandler() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.onRedirect(
                    () : void => {
                        const path : string = "test/resource/data/Com/Wui/Framework/Commons/ValidData.jsonp";
                        JsonpFileReader.Load(
                            path, ($data : any, $filePath? : string) : void => {
                                assert.deepEqual($filePath, "test/resource/data/Com/Wui/Framework/Commons/ValidData.jsonp");
                            });
                    },
                    ($eventArgs : HttpRequestEventArgs) : void => {
                        assert.equal($eventArgs.Url(), "//ServerError/Http/NotFound");
                    },
                    () : void => {
                        this.initSendBox();
                        $done();
                    });
            };
        }

        public __IgnoretestLoadFifthHttp() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                this.setUrl("http://localhost:8888/required/path/location");
                const path : string = "http://localhost:8888/required/path/location";

                JsonpFileReader.Load(
                    path, ($data : any, $filePath? : string) : void => {
                        assert.deepEqual($filePath, "http://localhost:8888/required/path/location");
                        ExceptionsManager.Clear();
                        $done();
                    },
                    ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                        assert.ok($filePath, "http://localhost:8888/required/path/location");
                        ExceptionsManager.Clear();
                        $done();
                    });
            };
        }

        public __IgnoretestLoadException() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.throws(() : void => {
                    JsonpFileReader.Load("", null);
                    throw new Error("Not Found File Path.");
                }, /Not Found File Path./);
                $done();
            };
        }

        protected before() : void {
            this.registerElement("body");
        }

        protected setUp() : void {
            (<any>this.getRequest()).isIdeaHost = true;
        }
    }
}
