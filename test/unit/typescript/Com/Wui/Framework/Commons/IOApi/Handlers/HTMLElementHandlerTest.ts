/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi.Handlers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class HTMLElementHandlerTest extends UnitTestRunner {

        public testConstructor() : void {
            const htmlElement : HTMLElementHandler = new HTMLElementHandler("body");
            assert.equal(htmlElement.Name(), "body");
        }

        public testInit() : void {
            (<any>BaseOutputHandler).handlerIterator = 1;
            const htmlElement : HTMLElementHandler = new HTMLElementHandler();
            assert.throws(() : void => {
                htmlElement.Init();
            }, /Element "OutputHandler_2" has not been found./);
        }

        public testPrint() : void {
            const htmlElement : HTMLElementHandler = new HTMLElementHandler("body");
            htmlElement.Init();
            htmlElement.Print("printing of element");
            assert.equal(htmlElement.Encoding(), "UTF-8");
            assert.equal(htmlElement.ToString("", false),
                "object type of \'Com.Wui.Framework.Commons.IOApi.Handlers.HTMLElementHandler\'");
        }

        public testClear() : void {
            const htmlElement : HTMLElementHandler = new HTMLElementHandler("body");
            htmlElement.Clear();
            assert.equal(htmlElement.ToString("", true),
                "object type of \'Com.Wui.Framework.Commons.IOApi.Handlers.HTMLElementHandler\'");
        }

        protected setUp() : void {
            this.registerElement("body");
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
