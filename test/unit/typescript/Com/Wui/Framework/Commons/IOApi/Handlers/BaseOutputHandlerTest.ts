/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi.Handlers {
    "use strict";
    import NewLineType = Com.Wui.Framework.Commons.Enums.NewLineType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    class MockBaseOutputHandler extends BaseOutputHandler {
        public testNewLineType() : void {
            this.setNewLineType(NewLineType.WINDOWS);
        }
    }

    class MockBaseHandler extends BaseOutputHandler {
        public handlerSet() : void {
            return this.setHandler((message : string) : void => {
                const mess : string = "Testing of Handler";
            });
        }

        public handlerGet() : any {
            this.setHandler((message : string) : void => {
                const mess : string = "Testing of Handler";
            });
            this.Init();
            this.getHandler();
        }

        public newLinetype() : void {
            return this.setNewLineType(NewLineType.WINDOWS);
        }
    }

    class MockEmptyHandler extends BaseOutputHandler {
        public handlGet() : void {
            this.getHandler();
        }
    }

    export class BaseOutputHandlerTest extends UnitTestRunner {
        private baseHandler : BaseOutputHandler;

        public testConstructor() : void {
            assert.equal(this.baseHandler.Name(), "baseoutput");
        }

        public testInit() : void {
            assert.doesNotThrow(() : void => {
                this.baseHandler.Init();
            });
        }

        public testEncoding() : void {
            assert.equal(this.baseHandler.Encoding(), undefined);
            this.baseHandler.Init();
            assert.equal(this.baseHandler.Encoding(), "UTF-8");
        }

        public testNewLineType() : void {
            assert.equal(this.baseHandler.NewLineType(), undefined);
        }

        public testClear() : void {
            assert.doesNotThrow(() : void => {
                this.baseHandler.Clear();
            });
            this.baseHandler.Init();
            assert.equal(this.baseHandler.Encoding(), "UTF-8");
            this.baseHandler.setOnPrint((message : string) : void => {
                const mess : string = "message";
            });
            this.baseHandler.Clear();
        }

        public testsetOnPrint() : void {
            assert.doesNotThrow(() : void => {
                this.baseHandler.setOnPrint(($message : string) : void => {
                    assert.equal($message, "test");
                });
            });
        }

        public testsetOnPrintNullHandler() : void {
            this.baseHandler.setOnPrint(null);
        }

        public testPrint() : void {
            const exception : RegExp = new RegExp(
                "'Com.Wui.Framework.Commons.IOApi.Handlers.BaseOutputHandler' " +
                "is abstract class and does not provides fully implemented Print method.");
            assert.throws(() : void => {
                this.baseHandler.Print("test");
            }, exception);
        }

        public testtoString() : void {
            assert.equal(this.baseHandler.toString(),
                "object type of \'Com.Wui.Framework.Commons.IOApi.Handlers.BaseOutputHandler\'");
        }

        public testSetHandler() : void {
            const handler : MockBaseHandler = new MockBaseHandler();
            assert.patternEqual(StringUtils.Remove(this.stripInstrumentation(MockBaseHandler.toString()),
                "/* istanbul ignore next */ "), "" +
                StringUtils.Remove("function MockBaseHandler() {*\n*" +
                    "return _super !== null && _super.apply(this, arguments) || this;*\n*" +
                    "}", "/* istanbul ignore next */ "));
        }

        public testsetHandler() : void {
            const handler : MockBaseHandler = new MockBaseHandler();
            const testhandler : any = (message : string) : void => {
                const text : string = "Testing BaseHandler";
            };
            assert.equal(handler.handlerSet(), undefined);
        }

        public testgetHandler() : void {
            const handler : MockBaseHandler = new MockBaseHandler();
            assert.deepEqual(handler.handlerGet(), undefined);
        }

        public testnewLineType() : void {
            const handler : MockBaseHandler = new MockBaseHandler();
            handler.newLinetype();
        }

        public testgetHandlerEmpty() : void {
            const handler : MockEmptyHandler = new MockEmptyHandler();
            assert.equal(handler.handlGet(), undefined);
        }

        protected setUp() : void {
            this.baseHandler = new MockBaseOutputHandler("baseoutput");
        }
    }
}
