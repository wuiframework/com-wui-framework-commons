/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi.Handlers {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import FileHandlerEventType = Com.Wui.Framework.Commons.Enums.Events.FileHandlerEventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ProgressEventArgs = Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    export class FileHandlerTest extends UnitTestRunner {

        public testIsSupported() : void {
            assert.equal(FileHandler.IsSupported(), true);
            assert.equal(FileHandler.IsSupported(), true);
        }

        public __IgnoretestBindOnprogress() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const filehandler : FileHandler = new FileHandler(file);
                const nativeEventargs : any = {message: "onchange", lengthComputable: false};
                EventsManager.getInstanceSingleton().setEvent(filehandler.getClassName(), FileHandlerEventType.ON_CHANGE,
                    ($eventArgs : ProgressEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs(), nativeEventargs);
                        assert.equal($eventArgs.RangeStart(), 0);
                        assert.equal($eventArgs.CurrentValue(), 0);
                        assert.equal($eventArgs.RangeEnd(), 100);
                        EventsManager.getInstanceSingleton().Clear(filehandler.getClassName(), FileHandlerEventType.ON_CHANGE);
                        $done();
                    });
                (<any>filehandler).reader.onprogress(nativeEventargs);
            };
        }

        public __IgnoretestBindOnprogress2() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const filehandler : FileHandler = new FileHandler(file);
                const nativeEventargs : any = {message: "onchange", lengthComputable: true, total: 1100, loaded: 50};
                EventsManager.getInstanceSingleton().setEvent(filehandler.getClassName(), FileHandlerEventType.ON_CHANGE,
                    ($eventArgs : ProgressEventArgs) : void => {
                        assert.equal($eventArgs.NativeEventArgs(), nativeEventargs);
                        assert.equal($eventArgs.RangeStart(), 0);
                        assert.equal($eventArgs.CurrentValue(), 50);
                        assert.equal($eventArgs.RangeEnd(), 1100);
                        EventsManager.getInstanceSingleton().Clear(filehandler.getClassName(), FileHandlerEventType.ON_CHANGE);
                        $done();
                    });
                (<any>filehandler).reader.onprogress(nativeEventargs);
            };
        }

        public testConstructor() : void {
            assert.doesNotThrow(() : void => {
                const filehandler : FileHandler = new FileHandler(new NodeFile("testFile.txt"));
                assert.equal(filehandler.getName(), "testFile.txt");
            });
        }

        public testgetSource() : void {
            const file : File = new NodeFile(this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const fileHandler : FileHandler = new FileHandler(file);
            assert.deepEqual(fileHandler.getSource(), file);
        }

        public testgetName() : void {
            const file : File = new NodeFile(this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const fileHandler : FileHandler = new FileHandler(file);
            assert.deepEqual(fileHandler.getName(), "testFile.txt");
            assert.deepEqual(fileHandler.getSource(), file);
            assert.deepEqual(fileHandler.getType(), "text/plain");
        }

        public testgetEmptyName() : void {
            const file : File = new NodeFile(this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const filehandler : FileHandler = new FileHandler(file);
            assert.deepEqual(filehandler.getType(), "text/plain");
        }

        public testgetSize() : void {
            const file : File = new NodeFile(this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const filehandler : FileHandler = new FileHandler(file);
            assert.equal(filehandler.getSize(), undefined);
        }

        public testNotSettingFile() : void {
            let file : File; // tslint:disable-line
            const filehandler : FileHandler = new FileHandler(file);
            assert.equal(filehandler.getSize(), 0);
            assert.deepEqual(filehandler.getType(), "");
            assert.deepEqual(filehandler.getName(), "");
        }

        public testData() : void {
            const file : File = new NodeFile(this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const filehandler : FileHandler = new FileHandler(file);
            assert.equal(filehandler.Data(false), "");
        }

        public testtoString() : void {
            const file : File = new NodeFile(this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const filehandler : FileHandler = new FileHandler(file);
            assert.equal(filehandler.ToString("", false), "[\"name\"] testFile.txt\r\n[\"type\"]" +
                " text/plain\r\n[\"size\"] NOT DEFINED\r\n[\"lastModifiedTime\"]     Thu, 01 Jan 1970 00:00:00 GMT\r\n");
            assert.equal(filehandler.ToString(),
                "[\"name\"] testFile.txt<br/>" +
                "[\"type\"] text/plain<br/>" +
                "[\"size\"] NOT DEFINED<br/>" +
                "[\"lastModifiedTime\"] &nbsp;&nbsp;&nbsp;Thu, 01 Jan 1970 00:00:00 GMT<br/>");
        }

        public testOnnerror() : void {
            const file : File = new NodeFile(this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const filehandler : FileHandler = new FileHandler(file);
            const error : ErrorEventArgs = new ErrorEventArgs("File testFile.txt not found");
            EventsManager.getInstanceSingleton().setEvent(filehandler.getName(), EventType.ON_ERROR,
                ($eventArgs : ErrorEventArgs) : void => {
                    assert.equal($eventArgs.Message, "An error occurred while reading the file '" + name + "'.");
                });
            (<any>filehandler).reader.onerror = (event) : void => {
                console.error("An error occurred while reading the file '" + name + "'.");
            };
            EventsManager.getInstanceSingleton().Clear(filehandler.getName(), EventType.ON_ERROR);
        }

        public testErrorException() : void {
            const file : File = new NodeFile(this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const filehandler : FileHandler = new FileHandler(file);
            assert.doesHandleException(() : void => {
                (<any>filehandler).reader.onerror(null);
            }, "Cannot read property 'target' of null");
        }

        public testErrorException2() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const filehandler : FileHandler = new FileHandler(file);
                EventsManager.getInstanceSingleton().setEvent(filehandler.getClassName(), EventType.ON_ERROR,
                    ($eventArgs : ErrorEventArgs) : void => {
                        assert.equal($eventArgs.Message(),
                            "An encoding error occurred while reading the file '" + filehandler.getName() + "'");
                        EventsManager.getInstanceSingleton().Clear(filehandler.getClassName(), EventType.ON_ERROR);
                        $done();
                    });
                (<any>filehandler).reader.onerror({target: {error: {code: 0, ENCODING_ERR: 0}}});
            };
        }

        public testErrorException3() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const filehandler : FileHandler = new FileHandler(file);
                EventsManager.getInstanceSingleton().setEvent(filehandler.getClassName(), EventType.ON_ERROR,
                    ($eventArgs : ErrorEventArgs) : void => {
                        assert.equal($eventArgs.Message(), "File \'testFile.txt\' not found.");
                        EventsManager.getInstanceSingleton().Clear(filehandler.getClassName(), EventType.ON_ERROR);
                        $done();
                    });
                (<any>filehandler).reader.onerror({target: {error: {code: 0, NOT_FOUND_ERR: 0}}});
            };
        }

        public testErrorException4() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const filehandler : FileHandler = new FileHandler(file);
                EventsManager.getInstanceSingleton().setEvent(filehandler.getClassName(), EventType.ON_ERROR,
                    ($eventArgs : ErrorEventArgs) : void => {
                        assert.equal($eventArgs.Message(), "File \'testFile.txt\' is not readable.");
                        EventsManager.getInstanceSingleton().Clear(filehandler.getClassName(), EventType.ON_ERROR);
                        $done();
                    });
                (<any>filehandler).reader.onerror({target: {error: {code: 0, NOT_READABLE_ERR: 0}}});
            };
        }

        public testErrorException5() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const filehandler : FileHandler = new FileHandler(file);
                EventsManager.getInstanceSingleton().setEvent(filehandler.getClassName(), EventType.ON_ERROR,
                    ($eventArgs : ErrorEventArgs) : void => {
                        assert.equal($eventArgs.Message(), "Security issue with file \'testFile.txt\'");
                        EventsManager.getInstanceSingleton().Clear(filehandler.getClassName(), EventType.ON_ERROR);
                        $done();
                    });
                (<any>filehandler).reader.onerror({target: {error: {code: 0, SECURITY_ERR: 0}}});
            };
        }

        public testErrorException6() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const filehandler : FileHandler = new FileHandler(file);
                const error : Error = new Error("DefaultError");
                EventsManager.getInstanceSingleton().setEvent(filehandler.getClassName(), EventType.ON_ERROR,
                    ($eventArgs : ErrorEventArgs) : void => {
                        //  assert.equal($eventArgs.Owner, filehandler.getClassName());
                        //  assert.equal($eventArgs.Message(), "DefaultError");
                        EventsManager.getInstanceSingleton().Clear(filehandler.getClassName(), EventType.ON_ERROR);
                        $done();
                    });
                (<any>filehandler).reader.onerror({target: {error: {code: 0, SECURITY_ERR: -1}}});
            };
        }

        public testErrorOnAbort() : void {
            assert.doesNotThrow(() : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const filehandler : FileHandler = new FileHandler(file);
                (<any>filehandler).reader.onerror({target: {error: {code: 0, ABORT_ERR: 0}}});
            });
        }

        public testOnLoadStart() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                assert.doesHandleException(() : void => {
                    const file : File = new NodeFile(
                        this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                    const filehandler : FileHandler = new FileHandler(file);
                    EventsManager.getInstanceSingleton().setEvent(filehandler.getClassName(),
                        FileHandlerEventType.ON_START, (eventArgs : EventArgs) : void => {
                            assert.equal(eventArgs.Type(), FileHandlerEventType.ON_START);
                            EventsManager.getInstanceSingleton().Clear(filehandler.getClassName(), FileHandlerEventType.ON_START);
                            $done();
                        });
                    (<any>filehandler).reader.onloadstart(null);
                }, "");
            };
        }

        public testErrorLoad4() : void {
            assert.doesNotThrow(() : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const filehandler : FileHandler = new FileHandler(file);
                (<any>filehandler).reader.onload({target: {readyState: -1}});
            });
        }

        public testErrorLoad10() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const filehandler : FileHandler = new FileHandler(file);
                EventsManager.getInstanceSingleton().setEvent(filehandler.getClassName(), FileHandlerEventType.ON_COMPLETE,
                    ($eventArgs : ErrorEventArgs) : void => {
                        // assert.equal($eventArgs.Message(), "File \'testFile.txt\' is not readable.");
                        EventsManager.getInstanceSingleton().Clear(filehandler.getClassName(), FileHandlerEventType.ON_COMPLETE);
                        $done();
                    });
                (<any>filehandler).reader.onload({target: {readyState: (<any>FileReader).DONE, result: ""}});
            };
        }

        public __IgnoretestLoad() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const fileHandler : FileHandler = new FileHandler(file);
                EventsManager.getInstanceSingleton().setEvent(fileHandler.getClassName(), FileHandlerEventType.ON_COMPLETE, () : void => {
                    assert.equal(fileHandler.Data(), "Testing of FileHandler.ts");
                    EventsManager.getInstanceSingleton().Clear(fileHandler.getClassName(), FileHandlerEventType.ON_COMPLETE);
                    $done();
                });
                fileHandler.Load();
            };
        }

        public __IgnoretestLoad2() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const fileHandler : FileHandler = new FileHandler(file);
                const readerfile : FileReader = new FileReader();
                EventsManager.getInstanceSingleton().setEvent(fileHandler.getClassName(), FileHandlerEventType.ON_COMPLETE, () : void => {
                    assert.equal(fileHandler.Data(true), "ting of Fi");
                    EventsManager.getInstanceSingleton().Clear(fileHandler.getClassName(), FileHandlerEventType.ON_COMPLETE);
                    $done();
                });
                fileHandler.Load(3, 10);
            };
        }

        public testLoad3() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const fileHandler : FileHandler = new FileHandler(file);

                EventsManager.getInstanceSingleton().setEvent(fileHandler.getClassName(), FileHandlerEventType.ON_ABOARD, () : void => {
                    assert.ok(fileHandler.Data() === "" || fileHandler.Data() === "Testing of FileHandler.ts");
                    EventsManager.getInstanceSingleton().Clear(fileHandler.getClassName(), FileHandlerEventType.ON_ABOARD);
                    $done();
                });
                fileHandler.Load();
                setTimeout(() : void => {
                    fileHandler.Stop();
                });
            };
        }

        public testLoad4() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFileEmpty.txt");
                const filehandler : FileHandler = new FileHandler(file);
                EventsManager.getInstanceSingleton().setEvent(filehandler.getClassName(), EventType.ON_ERROR,
                    ($eventArgs : ErrorEventArgs) : void => {
                        assert.equal($eventArgs.Message(), "File source is empty.");
                        assert.equal(filehandler.Data(), "");
                        EventsManager.getInstanceSingleton().Clear(filehandler.getClassName(), EventType.ON_ERROR);
                        $done();
                    });
                (<any>filehandler).source = null;
                filehandler.Load();
            };
        }

        public __IgnoretestLoad5() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const fileHandler : FileHandler = new FileHandler(file);

                EventsManager.getInstanceSingleton().setEvent(fileHandler.getClassName(), FileHandlerEventType.ON_CHANGE,
                    ($event : ProgressEventArgs) : void => {
                        assert.equal($event.CurrentValue(), 0);
                        assert.equal($event.RangeEnd(), 100);
                        EventsManager.getInstanceSingleton().Clear(fileHandler.getClassName(), FileHandlerEventType.ON_CHANGE);
                        $done();
                    });
                fileHandler.Load();
            };
        }

        public __IgnoretestLoad6() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const file : File = new NodeFile(
                    this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
                const fileHandler : FileHandler = new FileHandler(file);
                EventsManager.getInstanceSingleton().setEvent(fileHandler.getClassName(), FileHandlerEventType.ON_COMPLETE, () : void => {
                    assert.equal(fileHandler.Data(true), "Testing of FileHandler.ts");
                    EventsManager.getInstanceSingleton().Clear(fileHandler.getClassName(), FileHandlerEventType.ON_COMPLETE);
                    $done();
                });
                fileHandler.Load();
            };
        }

        public testOnloadException() : void {
            const file : File = new NodeFile(this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const filehandler : FileHandler = new FileHandler(file);
            assert.doesHandleException(() : void => {
                (<any>filehandler).reader.onload(<any>null);
            }, "Cannot read property 'target' of null");
        }

        public testOnloadStartException() : void {
            const file : File = new NodeFile(
                this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const filehandler : FileHandler = new FileHandler(file);
            assert.doesHandleException(() : void => {
                (<any>filehandler).reader.onloadstart(<any>null);
            }, ""); /// TODO: validate missing message for handled error by WUI
        }

        public testOnProgressException() : void {
            const file : File = new NodeFile(
                this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const filehandler : FileHandler = new FileHandler(file);
            assert.doesHandleException(() : void => {
                (<any>filehandler).reader.onprogress(null);
            }, "Cannot read property 'lengthComputable' of null");
        }

        public testOnloadStart() : void {
            const file : File = new NodeFile(
                this.getAbsoluteRoot() + "\\test\\resource\\data\\Com\\Wui\\Framework\\Commons\\testFile.txt");
            const filehandler : FileHandler = new FileHandler(file);
            assert.doesHandleException(() : void => {
                (<any>filehandler).reader.onabort(null);
            }, ""); /// TODO: validate missing message for handled error by WUI
        }

        protected tearDown() : void {
            this.initSendBox();
        }
    }
}
