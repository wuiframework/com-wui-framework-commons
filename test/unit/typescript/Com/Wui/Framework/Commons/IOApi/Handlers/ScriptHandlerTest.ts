/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi.Handlers {
    "use strict";

    export class ScriptHandlerTest extends UnitTestRunner {

        public __IgnoretestLoad() : IUnitTestRunnerPromise {
            return ($done : () => void) : void => {
                const handler : ScriptHandler = new ScriptHandler();
                handler.Path("test/resource/data/Com/Wui/Framework/Commons/ValidData3.jsonp");
                handler.ErrorHandler(() : void => {
                    assert.ok(false, "ValidData has not been loaded correctly.");
                    $done();
                });
                handler.SuccessHandler(() : void => {
                    assert.equal(handler.Data(), "");
                    $done();
                });
                handler.Load();
            };
        }
    }
}
