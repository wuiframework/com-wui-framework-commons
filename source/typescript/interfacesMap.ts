/** WARNING: this file has been automatically generated from typescript interfaces, which exist in this package. */

/* tslint:disable: variable-name no-use-before-declare only-arrow-functions */
namespace Com.Wui.Framework.Commons.HttpProcessor.Resolvers {
    "use strict";
    export let IRuntimeTestEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "setOnSuiteStart",
                "setOnSuiteEnd",
                "setOnTestCaseStart",
                "setOnTestCaseEnd",
                "setOnAssert"
            ]);
        }();
}

namespace Com.Wui.Framework.Commons.HttpProcessor.Resolvers {
    "use strict";
    export let IRuntimeTestPromise : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces.Events {
    "use strict";
    export let IWebServiceClientEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnStart",
                "OnClose",
                "OnTimeout",
                "OnError"
            ]);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IBaseObject : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "getClassName",
                "getNamespaceName",
                "getClassNameWithoutNamespace",
                "getProperties",
                "getMethods",
                "IsTypeOf",
                "IsMemberOf",
                "Implements",
                "ToString",
                "toString",
                "SerializationData",
                "getHash",
                "getUID"
            ]);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IEventsHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let ITimeoutHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IWebServiceResponseHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ]);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces.Events {
    "use strict";
    export let IWuiBuilderEvents : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "OnBuildStart",
                "OnBuildComplete",
                "OnWarning",
                "OnFail",
                "OnMessage"
            ], IWebServiceClientEvents);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IArrayList : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Add",
                "getKeys",
                "getKey",
                "KeyExists",
                "Contains",
                "IndexOf",
                "getAll",
                "getItem",
                "getFirst",
                "getLast",
                "Equal",
                "Length",
                "IsEmpty",
                "Clear",
                "Copy",
                "RemoveAt",
                "RemoveLast",
                "SortByKeyUp",
                "SortByKeyDown",
                "foreach",
                "ToArray"
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let ICefQuery : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IEventArgs : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Owner",
                "Type",
                "NativeEventArgs"
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IEventsManager : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Exists",
                "getAll",
                "setEvent",
                "setEventArgs",
                "FireEvent",
                "FireAsynchronousMethod",
                "RemoveHandler",
                "Clear"
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IException : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Owner",
                "Message",
                "Line",
                "File",
                "Code",
                "Stack"
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IHttpManager : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "RegisterResolver",
                "OverrideResolver",
                "getResolverClassName",
                "getResolverParameters",
                "getResolversCollection"
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IHttpRequestResolver : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "RequestArgs",
                "Process"
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IJxbrowserBridge : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "send"
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IOHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "Init",
                "Name",
                "Encoding",
                "NewLineType",
                "Print",
                "setOnPrint",
                "Clear"
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IPersistenceHandler : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "getSessionId",
                "getSize",
                "getLoadTime",
                "getRawData",
                "LoadPersistenceAsynchronously",
                "DisableCRC",
                "ExpireTime",
                "Variable",
                "Exists",
                "Destroy",
                "Clear"
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IReflection : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "getAllClasses",
                "Exists",
                "getClass",
                "IsInstanceOf",
                "ClassHasInterface",
                "IsMemberOf",
                "Implements"
            ], IBaseObject);
        }();
}

namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    export let IWebServiceClient : Com.Wui.Framework.Commons.Interfaces.Interface =
        function () : Com.Wui.Framework.Commons.Interfaces.Interface {
            return Com.Wui.Framework.Commons.Interfaces.Interface.getInstance([
                "getId",
                "getServerUrl",
                "getServerPath",
                "getEvents",
                "StartCommunication",
                "StopCommunication",
                "CommunicationIsRunning",
                "Send",
                "setRequestFormatter",
                "setResponseFormatter"
            ], IBaseObject);
        }();
}

/* tslint:enable */
