/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.ErrorPages {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import HttpRequestConstants = Com.Wui.Framework.Commons.Enums.HttpRequestConstants;
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import ErrorPageException = Com.Wui.Framework.Commons.Exceptions.Type.ErrorPageException;
    import ExceptionCode = Com.Wui.Framework.Commons.Enums.ExceptionCode;

    export class ExceptionErrorPage extends BaseErrorPage {
        private fatalErrorExists : boolean;
        private exceptionsList : ArrayList<Exception>;

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            if (ObjectValidator.IsEmptyOrNull(this.fatalErrorExists)) {
                if ($GET.KeyExists(HttpRequestConstants.EXCEPTION_TYPE)) {
                    this.fatalErrorExists =
                        StringUtils.ToInteger($GET.getItem(HttpRequestConstants.EXCEPTION_TYPE)) === ExceptionCode.FATAL_ERROR;
                } else {
                    this.fatalErrorExists = false;
                }
            }
            if (ObjectValidator.IsEmptyOrNull(this.exceptionsList)) {
                if ($POST.KeyExists(HttpRequestConstants.EXCEPTIONS_LIST)) {
                    this.exceptionsList = $POST.getItem(HttpRequestConstants.EXCEPTIONS_LIST);
                } else {
                    this.exceptionsList = new ArrayList<Exception>();
                }
            }
        }

        protected getExceptionsList() : ArrayList<Exception> {
            return this.exceptionsList;
        }

        protected isFatalError() : boolean {
            return this.fatalErrorExists;
        }

        protected getPageBody() : string {
            let output : string = "";
            const EOL : string = StringUtils.NewLine(false);

            if (this.isFatalError()) {
                output += "<h1>FATAL Error!</h1>";
            } else {
                output += "<h1>Oops, something went wrong...</h1>";
            }

            this.getExceptionsList().foreach(($exception : Exception) : void => {
                output += "thrown by: <b>" + $exception.Owner() + "</b>:" + StringUtils.NewLine();
                try {
                    output += $exception.ToString() + StringUtils.NewLine();
                } catch (ex) {
                    output += ex.message + StringUtils.NewLine();
                }
            });

            output +=
                "<span onclick=\"" +
                "document.getElementById('exceptionEcho').style.display=" +
                "document.getElementById('exceptionEcho').style.display===" +
                "'block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\">" +
                "Echo output before exception" +
                "</span>" + EOL +
                "<div id=\"exceptionEcho\" style=\"border: 0 solid black; display: none;\">" + EOL +
                this.getEchoOutput() + EOL +
                "</div>" + StringUtils.NewLine() +
                "<a style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif; text-decoration: none;\" " +
                "href=\"#" + this.createLink("/about/Cache") + "\">Cache info</a>";

            Com.Wui.Framework.Commons.Exceptions.ExceptionsManager.Clear();

            return output;
        }

        protected resolver() : void {
            try {
                super.resolver();
            } catch (ex) {
                try {
                    Com.Wui.Framework.Commons.Exceptions.ExceptionsManager.Throw(this.getClassName(), ex);
                } catch (ex) {
                    // register error page self-error and continue with processing of general exception manager
                }
                try {
                    Com.Wui.Framework.Commons.Exceptions.ExceptionsManager.Throw(this.getClassName(),
                        new ErrorPageException(this.getClassName() + " self error."));
                } catch (ex) {
                    Com.Wui.Framework.Commons.Exceptions.ExceptionsManager.HandleException(ex);
                }
            }
        }
    }
}
