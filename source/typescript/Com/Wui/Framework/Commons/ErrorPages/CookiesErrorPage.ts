/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.ErrorPages {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    export class CookiesErrorPage extends BaseErrorPage {
        protected getPageBody() : string {
            return "This library requires enabled Cookies in the browser for ability to store persistence. " +
                "See link below for more information:" + StringUtils.NewLine() +
                "<a href=\"http://www.wikihow.com/Enable-Cookies-in-Your-Internet-Web-Browser\" " +
                "target=\"_blank\">How to enable Cookies?</a>";
        }
    }
}
