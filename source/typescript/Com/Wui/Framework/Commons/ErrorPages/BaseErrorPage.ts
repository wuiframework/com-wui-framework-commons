/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.ErrorPages {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;

    export abstract class BaseErrorPage extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.AsyncHttpResolver {

        protected getEchoOutput() : string {
            let output : string = Echo.getStream();
            if (ObjectValidator.IsEmptyOrNull(output)) {
                output = "Nothing has been printed by Echo yet.";
            }
            return output;
        }

        protected getPageBody() : string {
            return "<h1>Something has went wrong</h1>";
        }

        protected resolver() : void {
            const body : string = this.getPageBody();

            try {
                ExceptionsManager.ThrowExit();
            } catch (ex) {
                // stop all background execution, but continue in execution of current resolver
            }
            Echo.ClearAll();
            Echo.Println(body);
        }
    }
}
