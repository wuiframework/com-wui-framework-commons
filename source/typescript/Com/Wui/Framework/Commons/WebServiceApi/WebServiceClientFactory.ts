/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi {
    "use strict";
    import WebServiceClientType = Com.Wui.Framework.Commons.Enums.WebServiceClientType;
    import IWebServiceClient = Com.Wui.Framework.Commons.Interfaces.IWebServiceClient;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;

    export class WebServiceClientFactory extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        private static clientsList : ArrayList<IWebServiceClient>;

        /**
         * @param {WebServiceClientType} $clientType Create or get this type of web service client.
         * @param {string|WebServiceConfiguration} [$serverConfigurationSource] Specify source for configuration of
         * server-client communication.
         * @return {IWebServiceClient} Returns instance of web service client based on $clientType,
         * if is supported by runtime environment otherwise null.
         */
        public static getClient($clientType : WebServiceClientType,
                                $serverConfigurationSource? : string | WebServiceConfiguration) : IWebServiceClient {
            let key : string = <string>$clientType;
            if (!ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
                if (ObjectValidator.IsString($serverConfigurationSource)) {
                    key += <string>$serverConfigurationSource;
                } else {
                    key += (<WebServiceConfiguration>$serverConfigurationSource).getServerUrl();
                }
            }
            key = StringUtils.getSha1(key);
            if (!ObjectValidator.IsSet(WebServiceClientFactory.clientsList)) {
                WebServiceClientFactory.clientsList = new ArrayList<IWebServiceClient>();
            }
            if (!WebServiceClientFactory.clientsList.KeyExists(key)) {
                const client : IWebServiceClient = this.createClientInstance($clientType, $serverConfigurationSource);
                if (!ObjectValidator.IsEmptyOrNull(client)) {
                    WebServiceClientFactory.clientsList.Add(client, key);
                }
            }

            return WebServiceClientFactory.clientsList.getItem(key);
        }

        /**
         * @param {WebServiceClientType} $clientType Specify client type, which should be validated.
         * @return {boolean} Returns true, if required client type is supported by runtime environment, othewise false.
         */
        public static IsSupported($clientType : WebServiceClientType) : boolean {
            const request : HttpRequestParser = Loader.getInstance().getHttpManager().getRequest();
            switch ($clientType) {
            case WebServiceClientType.AJAX:
            case WebServiceClientType.AJAX_SYNCHRONOUS:
                return request.IsOnServer();
            case WebServiceClientType.IFRAME:
                return !!document.createElement("iframe");
            case WebServiceClientType.POST_MESSAGE:
                return "postMessage" in window;
            case WebServiceClientType.WEB_SOCKETS:
                return "WebSocket" in window;
            case WebServiceClientType.HTTP:
                return WebServiceClientFactory.IsSupported(WebServiceClientType.AJAX) ||
                    WebServiceClientFactory.IsSupported(WebServiceClientType.IFRAME) ||
                    WebServiceClientFactory.IsSupported(WebServiceClientType.POST_MESSAGE);
            case WebServiceClientType.CEF_QUERY:
                return StringUtils.Contains(request.getUserAgent(), "Chrome") && ("cefQuery" in window);
            case WebServiceClientType.JXBROWSER_BRIDGE:
                return StringUtils.Contains(request.getUserAgent(), "Chrome") && ("java" in window);
            default:
                break;
            }
            return false;
        }

        /**
         * @param {number} $clientId Specify id of the desired service client.
         * @return {boolean} Returns true if instance of web service client with desired $clientId exists, otherwise false.
         */
        public static Exists($clientId : number) : boolean {
            return !ObjectValidator.IsEmptyOrNull(WebServiceClientFactory.getClientById($clientId));
        }

        /**
         * @param {number} $clientId Specify id of the desired service client.
         * @return {IWebServiceClient} Returns instance of web service client based on $clientId,
         * if client has been found, otherwise null.
         */
        public static getClientById($clientId : number) : IWebServiceClient {
            let client : IWebServiceClient = null;
            if (!ObjectValidator.IsEmptyOrNull($clientId) && ObjectValidator.IsSet(WebServiceClientFactory.clientsList)) {
                WebServiceClientFactory.clientsList.foreach(($client : IWebServiceClient) : boolean => {
                    if ($client.getId() === $clientId) {
                        client = $client;
                        return false;
                    }
                    return true;
                });
            }

            return client;
        }

        /**
         * Close all currently running server-client communications.
         * @return {void}
         */
        public static CloseAllConnections() : void {
            if (ObjectValidator.IsSet(WebServiceClientFactory.clientsList)) {
                WebServiceClientFactory.clientsList.foreach(($client : IWebServiceClient) : void => {
                    $client.StopCommunication();
                });
            }
        }

        protected static createClientInstance($clientType : WebServiceClientType,
                                              $serverConfigurationSource? : string | WebServiceConfiguration) : IWebServiceClient {
            let configuration : WebServiceConfiguration;
            if (ObjectValidator.IsString($serverConfigurationSource) ||
                ObjectValidator.IsEmptyOrNull($serverConfigurationSource)) {
                configuration = new WebServiceConfiguration(<string>$serverConfigurationSource);
            } else {
                configuration = <WebServiceConfiguration>$serverConfigurationSource;
            }
            let client : IWebServiceClient = null;

            const request : HttpRequestParser = Loader.getInstance().getHttpManager().getRequest();

            const getRestClient : ($configuration : WebServiceConfiguration) => IWebServiceClient =
                ($configuration : WebServiceConfiguration) : IWebServiceClient => {
                    if (StringUtils.StartsWith(request.getHostUrl(), "https://")) {
                        $configuration.ServerProtocol("https");
                    } else {
                        $configuration.ServerProtocol("http");
                    }
                    if (request.IsSameOrigin($configuration.getServerUrl())) {
                        return new Clients.AjaxClient($configuration);
                    } else if (WebServiceClientFactory.IsSupported(WebServiceClientType.POST_MESSAGE)) {
                        return new Clients.PostMessageClient($configuration);
                    } else {
                        return new Clients.IframeClient($configuration);
                    }
                };

            if (WebServiceClientFactory.IsSupported($clientType)) {
                switch ($clientType) {
                case WebServiceClientType.AJAX:
                case WebServiceClientType.AJAX_SYNCHRONOUS:
                    if (request.IsSameOrigin(configuration.getServerUrl())) {
                        client = new Clients.AjaxClient(configuration, $clientType === WebServiceClientType.AJAX_SYNCHRONOUS);
                    }
                    break;
                case WebServiceClientType.IFRAME:
                    client = new Clients.IframeClient(configuration);
                    break;
                case WebServiceClientType.POST_MESSAGE:
                    client = new Clients.PostMessageClient(configuration);
                    break;
                case WebServiceClientType.WEB_SOCKETS:
                    if (StringUtils.StartsWith(request.getHostUrl(), "https://") ||
                        configuration.ServerProtocol() === "https" ||
                        configuration.ServerPort() === 443) {
                        configuration.ServerProtocol("wss");
                    } else {
                        configuration.ServerProtocol("ws");
                    }
                    client = new Clients.WebSocketsClient(configuration);
                    break;
                case WebServiceClientType.HTTP:
                    client = getRestClient(configuration);
                    break;
                case WebServiceClientType.CEF_QUERY:
                    client = new Clients.CefQueryClient();
                    break;
                case WebServiceClientType.JXBROWSER_BRIDGE:
                    client = new Clients.JxbrowserBridgeClient();
                    break;
                default:
                    break;
                }
            }

            return client;
        }
    }
}
