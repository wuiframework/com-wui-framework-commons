/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi.Clients {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    /**
     * WebSocketsClient class provides client for bidirectional cross-origin communication with web sockets services.
     */
    export class WebSocketsClient extends BaseWebServiceClient {
        private maxReconnectsCount : number;
        private communicationThread : number;

        constructor($configuration : WebServiceConfiguration) {
            super($configuration);
            this.maxReconnectsCount = -1;
            this.communicationThread = null;
        }

        public getServerUrl() : string {
            if (this.getConfiguration().ServerPort() === 443 && this.getConfiguration().ServerProtocol() !== "wss") {
                this.getConfiguration().ServerProtocol("wss");
            }
            let url : string = this.getConfiguration().ServerProtocol() + "://" + this.getConfiguration().ServerAddress();
            if (this.getConfiguration().ServerPort() !== 80) {
                url += ":" + this.getConfiguration().ServerPort();
            }
            return url + "/";
        }

        /**
         * @return {string} Returns base path used for application scope on server.
         */
        public getServerBase() : string {
            let protocol : string = this.getConfiguration().ServerBase();
            if (StringUtils.StartsWith(protocol, "/")) {
                protocol = StringUtils.Substring(protocol, 1);
            }
            if (StringUtils.EndsWith(protocol, "/")) {
                protocol = StringUtils.Substring(protocol, 0, StringUtils.Length(protocol) - 1);
            }
            return protocol;
        }

        /**
         * @param {number} $value Specify maximum count of reconnect attempts. Value -1 means infinitive reconnection.
         * @return {number} Returns count of maximum attempts for client-server reconnect.
         */
        public MaxReconnectsCount($value? : number) : number {
            if ($value === -1) {
                return this.maxReconnectsCount = $value;
            }
            return this.maxReconnectsCount = Property.PositiveInteger(this.maxReconnectsCount, $value);
        }

        public StartCommunication() : void {
            if (this.clientInstance() === null) {
                const errorHandler : ($message : string) => void = ($message : string) : void => {
                    reconnectCounter = 0;
                    this.throwError($message);
                };
                let reconnectCounter : number = 0;
                const getClient : any = ($reconnect : boolean = true) : void => {
                    if (!ObjectValidator.IsEmptyOrNull(this.communicationThread)) {
                        clearTimeout(this.communicationThread);
                        this.communicationThread = null;
                    }
                    if (this.maxReconnectsCount === -1 ||
                        this.maxReconnectsCount === 0 && reconnectCounter === 0 ||
                        reconnectCounter < this.maxReconnectsCount) {
                        this.communicationThread = <any>setTimeout(() : void => {
                            try {
                                this.getConfiguration().Load(() : void => {
                                    let client : WebSocket;
                                    if (ObjectValidator.IsEmptyOrNull(this.getServerBase())) {
                                        client = this.clientInstance(new WebSocket(this.getServerUrl()));
                                    } else {
                                        client = this.clientInstance(new WebSocket(this.getServerUrl(), this.getServerBase()));
                                    }

                                    client.onopen = () : void => {
                                        try {
                                            super.StartCommunication();
                                            reconnectCounter = 0;
                                        } catch (ex) {
                                            this.throwError(ex);
                                        }
                                    };
                                    client.onclose = () : void => {
                                        try {
                                            if (this.CommunicationIsRunning()) {
                                                super.StopCommunication();
                                                reconnectCounter = 0;
                                            } else if (client.readyState === WebSocket.CLOSED) {
                                                getClient();
                                            } else {
                                                errorHandler("Server is not running at: " + this.getServerUrl());
                                            }
                                        } catch (ex) {
                                            this.throwError(ex);
                                        }
                                    };
                                    client.onmessage = ($event : MessageEvent) : void => {
                                        this.onResponse($event);
                                    };
                                    client.onerror = () : any => {
                                        if (this.CommunicationIsRunning() &&
                                            client.readyState !== WebSocket.CONNECTING &&
                                            client.readyState !== WebSocket.OPEN &&
                                            client.readyState !== WebSocket.CLOSING &&
                                            client.readyState !== WebSocket.CLOSED) {
                                            errorHandler("Unrecognized client error: " + client.readyState);
                                        } else if (client.readyState === WebSocket.CLOSED) {
                                            getClient();
                                        } else {
                                            this.StopCommunication();
                                        }
                                    };
                                }, () : void => {
                                    if (!this.CommunicationIsRunning()) {
                                        getClient();
                                    } else {
                                        errorHandler("Unable to load client configuration from: " + this.getConfiguration().getSource());
                                    }
                                });
                            } catch (ex) {
                                this.throwError(ex);
                            }
                        }, $reconnect ? 100 : 0);
                        reconnectCounter++;
                    } else if (this.maxReconnectsCount > 0) {
                        errorHandler("Max reconnection count " + this.maxReconnectsCount + " has been reached.");
                    } else if (this.maxReconnectsCount === 0 && reconnectCounter > 0) {
                        errorHandler("Server is not running at: " + this.getServerUrl());
                    }
                };
                getClient(false);
            }
        }

        public StopCommunication() : void {
            try {
                const client : WebSocket = this.clientInstance();
                if (!ObjectValidator.IsEmptyOrNull(client)) {
                    client.close();
                }
                super.StopCommunication();
            } catch (ex) {
                this.throwError(ex);
            }
        }

        protected clientInstance($value? : WebSocket) : WebSocket {
            return <WebSocket>super.clientInstance($value);
        }

        protected sendRequest($data : string) : void {
            const client : WebSocket = this.clientInstance();
            try {
                setTimeout(() : void => {
                    try {
                        client.send($data);
                    } catch (ex) {
                        this.throwError(ex);
                    }
                });
            } catch (ex) {
                if (!ObjectValidator.IsEmptyOrNull(client)) {
                    if (client.readyState === WebSocket.CLOSED || client.readyState === WebSocket.CLOSING) {
                        this.throwError("Unable to connect to socket server: " + this.getServerUrl());
                    } else if (client.readyState === WebSocket.CONNECTING) {
                        this.throwError("Connecting to socket server: " + this.getServerUrl());
                    } else {
                        this.throwError(ex);
                    }
                } else {
                    this.throwError(ex);
                }
            }
        }

        protected onResponse($event : MessageEvent) : void {
            try {
                if ($event.data === "OK") {
                    super.onResponse($event);
                } else {
                    this.responseResolver($event.data);
                }
            } catch (ex) {
                this.throwError(ex);
            }
        }
    }
}
