/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi.Clients {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import HttpMethodType = Com.Wui.Framework.Commons.Enums.HttpMethodType;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    /**
     * AjaxClient class provides asynchronous http client for bidirectional communication with REST services on same origin.
     */
    export class AjaxClient extends BaseWebServiceClient {
        private readonly sync : boolean;

        /**
         * @param {WebServiceConfiguration} $configuration Specify client-server communication parameters
         * @param {boolean} [$synchronous=false] Specify if client-server communication should be synchronous
         */
        constructor($configuration : WebServiceConfiguration, $synchronous : boolean = false) {
            super($configuration);

            this.sync = $synchronous;
        }

        public StartCommunication() : void {
            if (this.clientInstance() === null) {
                this.getConfiguration().Load(() : void => {
                    try {
                        this.clientInstance(new XMLHttpRequest());
                    } catch (ex) {
                        try {
                            this.clientInstance(new ActiveXObject("Msxml2.XMLHTTP"));
                        } catch (ex) {
                            try {
                                this.clientInstance(new ActiveXObject("Microsoft.XMLHTTP"));
                            } catch (ex) {
                                this.throwError(ex);
                            }
                        }
                    }
                    super.StartCommunication();
                }, () : void => {
                    this.throwError("Unable to load client configuration from: " + this.getConfiguration().getSource());
                });
            }
        }

        protected clientInstance($value? : XMLHttpRequest) : XMLHttpRequest {
            return <XMLHttpRequest>super.clientInstance($value);
        }

        protected sendRequest($data : string) : void {
            const client : XMLHttpRequest = this.clientInstance();
            client.open(HttpMethodType.POST, this.getServerUrl(), this.sync);
            client.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            client.setRequestHeader("charset", "UTF-8");
            if (!this.sync) {
                client.onreadystatechange = () : void => {
                    this.onResponse();
                };
                client.send("jsonData=" + $data);
            } else {
                client.send("jsonData=" + $data);
                this.onResponse();
            }
        }

        protected onResponse() : void {
            try {
                const client : XMLHttpRequest = this.clientInstance();
                if (client.readyState === 4 || this.sync) {
                    if (client.status === 200 || client.status === 500) {
                        const responseText : string = client.responseText;
                        if (!this.sync) {
                            EventsManager.getInstanceSingleton().FireAsynchronousMethod(() : void => {
                                this.responseResolver(responseText);
                            }, false);
                        } else {
                            this.responseResolver(responseText);
                        }
                        super.onResponse();
                    } else if (client.status === 0) {
                        this.throwError("Connection has been lost.");
                    } else {
                        this.throwError("Data transfer failure with status " + client.status + ": " + client.statusText);
                    }
                }
            } catch (ex) {
                this.throwError(ex);
            }
        }
    }
}
