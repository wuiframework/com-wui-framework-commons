/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.WebServiceApi.Clients {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ICefQuery = Com.Wui.Framework.Commons.Interfaces.ICefQuery;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ICefQueryMessage = Com.Wui.Framework.Commons.Interfaces.ICefQueryMessage;

    /**
     * CefQueryClient class provides client for bidirectional cross-origin communication with web sockets services.
     */
    export class CefQueryClient extends BaseWebServiceClient {
        private requestIds : ArrayList<number>;

        constructor() {
            super(new WebServiceConfiguration());
            this.requestIds = new ArrayList<number>();
        }

        public StartCommunication() : void {
            if (this.clientInstance() === null) {
                this.clientInstance((<any>window).cefQuery);
                try {
                    super.StartCommunication();
                } catch (ex) {
                    this.throwError(ex);
                }
            }
        }

        public StopCommunication() : void {
            try {
                this.requestIds.foreach(($id : number) : void => {
                    (<any>window).cefQueryCancel($id);
                });
                super.StopCommunication();
            } catch (ex) {
                this.throwError(ex);
            }
        }

        protected clientInstance($value? : ICefQuery) : ICefQuery {
            return <ICefQuery>super.clientInstance($value);
        }

        protected sendRequest($data : string) : void {
            const client : ICefQuery = this.clientInstance();
            try {
                this.requestIds.Add(client.apply(this, [
                    <ICefQueryMessage>{
                        onFailure : ($code : number, $message? : string) : void => {
                            this.throwError($message);
                        },
                        onSuccess : ($response : string) : void => {
                            this.onResponse($response);
                        },
                        persistent: true,
                        request   : $data
                    }
                ]));
            } catch (ex) {
                Echo.Printf(ex.message);
                this.throwError(ex);
            }
        }

        protected onResponse($message : string) : void {
            try {
                if ($message === "OK") {
                    super.onResponse($message);
                } else {
                    this.responseResolver($message);
                }
            } catch (ex) {
                this.throwError(ex);
            }
        }
    }
}
