/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import LogLevel = Com.Wui.Framework.Commons.Enums.LogLevel;
    import NewLineType = Com.Wui.Framework.Commons.Enums.NewLineType;
    import IOHandler = Com.Wui.Framework.Commons.Interfaces.IOHandler;
    import IOHandlerType = Com.Wui.Framework.Commons.Enums.IOHandlerType;
    import LogSeverity = Com.Wui.Framework.Commons.Enums.LogSeverity;

    /**
     * LogIt class provides static API for logging to selected output handler.
     */
    export class LogIt extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private static output : IOHandler;
        private static level : LogLevel;
        private static ip : string;
        private static owner : string;

        /**
         * Execute initialization manually, in case of that, it has not been initialized automatically.
         * @param {LogLevel} [$level = LogLevel.ALL] Set maximal level, which will be allowed for output.
         * @param {IOHandler} [$handler] Set resources handler,
         * which will be used for print of log message.
         * @return {void}
         */
        public static Init($level : LogLevel = LogLevel.ALL, $handler? : IOHandler) : void {
            if (!ObjectValidator.IsSet(this.output)) {
                if (!ObjectValidator.IsSet($handler)) {
                    this.output = Com.Wui.Framework.Commons.IOApi.IOHandlerFactory.getHandler(IOHandlerType.CONSOLE, "LogIt");
                } else {
                    this.output = $handler;
                }
                this.output.Init();
                this.ip = "127:0:0:0";
                if (!ObjectValidator.IsSet(this.level)) {
                    this.level = $level;
                }
                if (!ObjectValidator.IsSet(this.owner)) {
                    this.owner = "unlogged";
                }
            }
        }

        /**
         * @return {IOHandlerType} Returns currently used Output handler.
         */
        public static getHandlerType() : IOHandlerType {
            this.Init();
            return Com.Wui.Framework.Commons.IOApi.IOHandlerFactory.getHandlerType(this.output);
        }

        /**
         * @param {LogLevel} $level Set maximal level, which will be allowed for output.
         * @return {void}
         */
        public static setLevel($level : LogLevel) : void {
            this.level = $level;
        }

        /**
         * @param {string} $value Set owner of the logger.
         * @return {void}
         */
        public static setOwner($value : string) : void {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.owner = $value;
            }
        }

        /**
         * Log message at Info level.
         * @param {string} $message Message which should be logged.
         * @param {LogSeverity} [$severity = LogSeverity.HIGH] Specify severity level of info message.
         * @return {void}
         */
        public static Info($message : string, $severity : LogSeverity = LogSeverity.HIGH) : void {
            this.save(LogLevel.INFO, $message, $severity);
        }

        /**
         * Log message at Warning level.
         * @param {string} $message Message which should be logged.
         * @return {void}
         */
        public static Warning($message : string) : void {
            this.save(LogLevel.WARNING, $message);
        }

        /**
         * Log message at Error level.
         * @param {string} $message Message which should be logged.
         * @param {Error} [$exception] Include exception stack trace, if is provided by environment.
         * @return {void}
         */
        public static Error($message : string, $exception? : Error) : void {
            this.Init();
            if (ObjectValidator.IsSet($exception) && ObjectValidator.IsSet((<any>$exception).stack)) {
                const htmlTagEnabled : boolean = this.getHandlerType() !== IOHandlerType.CONSOLE;
                $message += this.output.NewLineType() +
                    StringUtils.Tab(1, htmlTagEnabled) + $exception.message + StringUtils.NewLine(htmlTagEnabled) + "Stack trace: ";
                if (htmlTagEnabled) {
                    $message += StringUtils.Replace(StringUtils.Replace((<any>$exception).stack, "</", ":"), "\n", StringUtils.NewLine());
                } else {
                    $message += (<any>$exception).stack;
                }
            }
            this.save(LogLevel.ERROR, $message);
        }

        /**
         * Log message at Debug level.
         * @param {(string|any)} $messageOrObject Message, which should be logged.
         * Message, which is not type of string, is converted to the string.
         * @param {...any} $parameters Open-ended args for search in message string, if arg is not typeof string it will
         * be converted to string.
         * @return {void}
         */
        public static Debug($messageOrObject : string | any, ...$parameters : any[]) : void {
            if (ObjectValidator.IsSet($parameters) && ObjectValidator.IsString($messageOrObject)) {
                const args : any[] = [$messageOrObject];
                let index : number;
                for (index = 0; index < $parameters.length; index++) {
                    args[index + 1] = Convert.ObjectToString($parameters[index], "", this.getHandlerType() !== IOHandlerType.CONSOLE);
                }
                this.save(LogLevel.DEBUG, StringUtils.Format.apply(String, args));
            } else {
                this.save(LogLevel.DEBUG, Convert.ObjectToString($messageOrObject, "", this.getHandlerType() !== IOHandlerType.CONSOLE));
            }
        }

        /**
         * @param {Function} $handler Callback, which should be called in time on message print.
         * @return {void}
         */
        public static setOnPrint($handler : ($message : string) => void) : void {
            this.Init();
            this.output.setOnPrint($handler);
        }

        private static formatter($type : LogLevel, $message : string,
                                 $newLineType : NewLineType = NewLineType.WINDOWS, $severity? : LogSeverity) : string {
            return StringUtils.Format("{0}: {1} {2}({3}){4}{5}{6}{4}",
                LogLevel[$type], Convert.TimeToGMTformat(new Date()), this.ip, this.owner, $newLineType,
                StringUtils.Tab(1, this.getHandlerType() !== IOHandlerType.CONSOLE), $message);
        }

        private static save($level : LogLevel, $message : string, $severity? : LogSeverity) : void {
            this.Init();
            $message = this.formatter($level, $message, this.output.NewLineType(), $severity);

            if (this.level === LogLevel.ALL ||
                this.level === LogLevel.DEBUG && $level === LogLevel.DEBUG ||
                this.level === LogLevel.INFO && $level === LogLevel.INFO ||
                this.level === LogLevel.WARNING && ($level === LogLevel.INFO || $level === LogLevel.WARNING) ||
                this.level === LogLevel.ERROR && ($level === LogLevel.INFO || $level === LogLevel.WARNING || $level === LogLevel.ERROR)) {
                this.output.Print($message);
            }
        }
    }
}
