/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import IBaseObject = Com.Wui.Framework.Commons.Interfaces.IBaseObject;
    import IReflection = Com.Wui.Framework.Commons.Interfaces.IReflection;
    import Constants = Com.Wui.Framework.Commons.Enums.SyntaxConstants;
    import Interface = Com.Wui.Framework.Commons.Interfaces.Interface;

    /**
     * Reflection class provides OOP metadata for classes implemented at Com.Wui namespace.
     */
    export class Reflection implements IReflection {

        private static singletonNamespaces : string[] = ["Com.Wui"];
        private static singleton : Reflection;
        private static classNamespace : string = "";
        private static className : string = "";
        private classesList : string[] = [];
        private classesSize : number;

        /**
         * @return {string} Returns class name with namespace in string format.
         */
        public static ClassName() : string {
            return this.classNamespace + "." + this.className;
        }

        /**
         * @return {string} Returns namespace of the class in string format.
         */
        public static NamespaceName() : string {
            return this.classNamespace;
        }

        /**
         * @return {string} Returns only the class name without namespace in string format.
         */
        public static ClassNameWithoutNamespace() : string {
            return this.className;
        }

        /**
         * @param {...string[]} $values Specify namespaces, which should be reflected by singleton instance.
         * @return {void}
         */
        public static setInstanceNamespaces(...$values : string[]) : void {
            if (!ObjectValidator.IsEmptyOrNull($values)) {
                this.singletonNamespaces = $values;
            }
        }

        /**
         * @return {Reflection} Returns class instance singleton.
         */
        public static getInstance() : Reflection {
            if (!ObjectValidator.IsSet(this.singleton)) {
                this.singleton = new Reflection();
            }
            return this.singleton;
        }

        /**
         * @param {string} $className Desired instance's class name, which should be created.
         * @param {object} [$dataObject] Data, which should be passed to the created instance.
         * @return {any} Returns instance of desired class with defined data.
         */
        public static getInstanceOf($className : string, $dataObject? : object) : any {
            if (!ObjectValidator.IsSet($dataObject) ||
                ObjectValidator.IsFunction($dataObject[Constants.CLASS_NAME]) &&
                $dataObject[Constants.CLASS_NAME]() === $className) {
                /* tslint:disable: only-arrow-functions */
                const baseClass : any = function () : void {
                    // default constructor with empty body
                };
                const thisClass : any = function () : void {
                    // default constructor with empty body
                };
                /* tslint:enable */

                baseClass.prototype = Reflection.getInstance().getClass($className).prototype;
                let propertyName : string;
                for (propertyName in baseClass.prototype) {
                    /* istanbul ignore else : bulletproof condition */
                    if (ObjectValidator.IsSet(baseClass.prototype[propertyName])) {
                        thisClass.prototype[propertyName] = baseClass.prototype[propertyName];
                    }
                }
                thisClass.prototype.constructor = thisClass;
                thisClass.prototype = new baseClass();

                const output : any = new thisClass();
                if (ObjectValidator.IsSet($dataObject)) {
                    for (propertyName in $dataObject) {
                        /* istanbul ignore else : bulletproof condition */
                        if (ObjectValidator.IsSet($dataObject[propertyName])) {
                            output[propertyName] = $dataObject[propertyName];
                        }
                    }
                }
                return output;
            }
            return null;
        }

        /**
         * @param {string} [$className] Specify class name for which should be UID generated.
         * @return {string} Returns unique identifier, based on class name and timestamp.
         */
        public static UID($className? : string) : string {
            if (ObjectValidator.IsEmptyOrNull($className)) {
                $className = this.ClassName();
            }
            return Com.Wui.Framework.Commons.Utils.StringUtils.getSha1(
                $className + new Date().getTime() + Math.floor(Math.random() * 10000).toString());
        }

        constructor() {
            this.classesSize = 0;
            let index : number;
            for (index = 0; index < Reflection.singletonNamespaces.length; index++) {
                let parentNamespace : string = Reflection.singletonNamespaces[index];
                let subNamespace : string = "";
                if ((<any>parentNamespace).indexOf(".") !== -1) {
                    const namespaces : string[] = (<any>parentNamespace).split(".");
                    parentNamespace = namespaces[0];
                    subNamespace = namespaces[1];
                }
                if (ObjectValidator.IsSet(window[parentNamespace])) {
                    this.setNamespace(window[parentNamespace], parentNamespace, subNamespace);
                }
            }
        }

        /**
         * @return {string} Returns unique identifier, based on class name and timestamp.
         */
        public getUID() : string {
            return Reflection.UID(this.getClassName());
        }

        /**
         * @param {IBaseObject} [$instance] Object instance for, which should be looked up class name.
         * @return {string} Returns class name with namespace in string format for the this class instance
         * or for instance of Object, which has been passed as method argument.
         * If class has not been found in reflected namespace, it will returns null.
         */
        public getClassName($instance? : IBaseObject) : string {
            if (ObjectValidator.IsSet($instance)) {
                let output : string = null;
                let index : number;
                for (index = 1; index < this.classesSize; index++) {
                    if ($instance instanceof this.getClass(this.classesList[index])) {
                        output = this.classesList[index];
                    }
                }

                return output;
            } else {
                return this.getClassName(<any>this);
            }
        }

        /**
         * @param {IBaseObject} [$instance] Object instance for, which should be looked up class name.
         * @return {string} Returns namespace of the class in string format for the this class instance
         * or for instance of Object, which has been passed as method argument.
         * If class has not been found in reflected namespace, it will returns null.
         */
        public getNamespaceName($instance? : IBaseObject) : string {
            if (!ObjectValidator.IsSet($instance)) {
                $instance = <any>this;
            }
            const fullName : any = this.getClassName($instance);
            return fullName.substring(0, fullName.lastIndexOf("."));
        }

        /**
         * @param {IBaseObject} [$instance] Object instance for, which should be looked up class name.
         * @return {string} Returns only the class name without namespace in string format for the this class instance
         * or for instance of Object, which has been passed as method argument.
         * If class has not been found in reflected namespace, it will returns null.
         */
        public getClassNameWithoutNamespace($instance? : IBaseObject) : string {
            if (!ObjectValidator.IsSet($instance)) {
                $instance = <any>this;
            }
            const fullName : any = this.getClassName($instance);
            return fullName.substr((this.getNamespaceName($instance) + "").length + 1);
        }

        /**
         * @return {string[]} Returns list of all mapped classes in looked up namespace.
         */
        public getAllClasses() : string[] {
            return this.classesList;
        }

        /**
         * @param {string} $className Class name in string format, which should be validated.
         * @return {boolean} Returns true, if class has been mapped, otherwise false.
         */
        public Exists($className : string) : boolean {
            return this.classesList.indexOf($className) !== -1;
        }

        /**
         * @param {string} $className Class name in string format, which should be reflected.
         * @return {any} Returns class object, if class name has been found in namespace, otherwise null.
         */
        public getClass($className : string) : any {
            const namespaces : string[] = (<any>$className).split(".");
            let object : any = null;
            if (namespaces.length > 1 && ObjectValidator.IsSet(window[namespaces[0]])) {
                object = window[namespaces[0]];
                let index : number;
                for (index = 1; index < namespaces.length; index++) {
                    object = object[namespaces[index]];
                }
            }
            return object;
        }

        /**
         * @param {IBaseObject} $instance Object, which should be validated.
         * @param {string|ClassName} $className Class name in string format or as Object, which should be validated as instance type.
         * @return {boolean} Returns true, if instance is type of $className object.
         */
        public IsInstanceOf($instance : IBaseObject, $className : string | ClassName) : boolean {
            if (!ObjectValidator.IsString($className) && ObjectValidator.IsSet((<any>$className).ClassName)) {
                $className = (<any>$className).ClassName();
            }
            if (ObjectValidator.IsSet($instance.getClassName)) {
                return $className === $instance.getClassName();
            }
            return false;
        }

        /**
         * @param {IBaseObject} $instance Object, which should be validated.
         * @param {string|ClassName} $className Class name in string format or as Object, which should be validated as instance type.
         * @return {boolean} Returns true, if instance is type of $className object or it's child.
         */
        public IsMemberOf($instance : IBaseObject, $className? : string | ClassName) : boolean {
            let requiredClass : any = null;
            if (!ObjectValidator.IsSet($className)) {
                if (ObjectValidator.IsSet((<any>$instance).ClassName)) {
                    $className = (<any>$instance).ClassName();
                } else if (ObjectValidator.IsString($instance)) {
                    $className = $instance;
                } else {
                    $className = null;
                }
                if ($className === "Com.Wui.Framework.Commons.Primitives.BaseObject") {
                    return true;
                }
                return this.IsMemberOf(<any>this, $className);
            } else if (ObjectValidator.IsString($className)) {
                requiredClass = this.getClass(<string>$className);
            } else {
                requiredClass = $className;
            }

            if (requiredClass === null) {
                return false;
            }
            return $instance instanceof requiredClass;
        }

        /**
         * @param {string|ClassName} $className Class, which should be validated.
         * @param {string|Interface} $interface Interface, which should be reflected by the class.
         * @return {boolean} Returns true, if class has required interface, otherwise false.
         */
        public ClassHasInterface($className : string | ClassName, $interface : string | Interface) : boolean {
            if (ObjectValidator.IsString($interface)) {
                $interface = this.getClass(<string>$interface);
            }
            if (!ObjectValidator.IsEmptyOrNull($interface) &&
                ObjectValidator.IsSet((<Interface>$interface).ClassName) &&
                ObjectValidator.IsSet((<Interface>$interface).NamespaceName) &&
                ObjectValidator.IsSet((<Interface>$interface).ClassNameWithoutNamespace)) {
                if (!<any>$interface.hasOwnProperty("getClassName")) {
                    $interface = <Interface>(<any>$interface).prototype;
                }
                if (ObjectValidator.IsSet((<any>$interface).getClassName)) {
                    let classDeclaration : any = $className;
                    if (ObjectValidator.IsString($className)) {
                        classDeclaration = this.getClass(<string>$className);
                    }
                    const classProperties : string[] = [];
                    let propertyIndex : number = 0;
                    let classProperty : string;
                    for (classProperty in classDeclaration.prototype) {
                        if (classProperty !== "") {
                            classProperties[propertyIndex] = classProperty;
                            propertyIndex++;
                        }
                    }
                    let interfaceProperty : any;
                    for (interfaceProperty in (<any>$interface)) {
                        if (interfaceProperty !== "classNamespace" &&
                            interfaceProperty !== "className" &&
                            interfaceProperty !== "ClassName" &&
                            interfaceProperty !== "NamespaceName" &&
                            interfaceProperty !== "ClassNameWithoutNamespace" &&
                            interfaceProperty !== "toString") {
                            if (classProperties.indexOf(interfaceProperty) === -1) {
                                return false;
                            }
                        }
                    }
                    return true;
                }
            }
            return false;
        }

        /**
         * @param {IBaseObject|Interface|string} $instance Object, which should be validated.
         * @param {(Interface|string)} $className Interface class name in string format or as Object, which should be validated.
         * @return {boolean} Returns true if the class instance implements $className interface, otherwise false.
         */
        public Implements($instance : IBaseObject | Interface | string, $className? : Interface | string) : boolean {
            if (!ObjectValidator.IsSet($className)) {
                if (ObjectValidator.IsString($instance)) {
                    $className = this.getClass(<any>$instance);
                } else if (ObjectValidator.IsSet((<any>$instance).ClassName)) {
                    $className = <any>$instance;
                }
                $instance = <any>this;
            } else if (ObjectValidator.IsString($className)) {
                $className = this.getClass(<string>$className);
            }

            if (!ObjectValidator.IsEmptyOrNull($className) &&
                ObjectValidator.IsSet((<Interface>$className).ClassName) &&
                ObjectValidator.IsSet((<Interface>$className).NamespaceName) &&
                ObjectValidator.IsSet((<Interface>$className).ClassNameWithoutNamespace)) {
                let property : any;
                for (property in (<Interface>$className)) {
                    if (property !== "classNamespace" &&
                        property !== "className" &&
                        property !== "ClassName" &&
                        property !== "NamespaceName" &&
                        property !== "ClassNameWithoutNamespace") {
                        if (!ObjectValidator.IsSet($instance[property])) {
                            return false;
                        }
                    }
                }
                return true;
            }
            return false;
        }

        /**
         * @return {string[]} Returns string array of all properties in the class instance.
         */
        public getProperties() : string[] {
            return [];
        }

        /**
         * @return {string[]} Returns string array of all methods in the class instance.
         */
        public getMethods() : string[] {
            const output : string[] = [];
            let parameterIndex : number = 0;
            let parameter : any;
            for (parameter in this) {
                if (ObjectValidator.IsFunction(this[parameter])) {
                    output[parameterIndex] = parameter;
                    parameterIndex++;
                }
            }
            return output;
        }

        /**
         * @return {number} Returns CRC calculated from data, which represents current object.
         */
        public getHash() : number {
            return Com.Wui.Framework.Commons.Utils.StringUtils.getCrc(JSON.stringify(this.classesList));
        }

        /**
         * @param {string|ClassName} $className Class name for validation.
         * @return {boolean} Returns true if the class instance is type of $className, otherwise false.
         */
        public IsTypeOf($className : string | ClassName) : boolean {
            return this.IsInstanceOf(<any>this, $className);
        }

        /**
         * @return {object} Returns data suitable for object serialization.
         */
        public SerializationData() : object {
            return {classesList: this.classesList};
        }

        public ToString($prefix? : string, $htmlTag : boolean = true) : string {
            return Convert.ArrayToString(this.classesList, $prefix, $htmlTag);
        }

        public toString() : string {
            return this.ToString();
        }

        private setNamespace($objectHolder : any, $parentNamespace : string, $name : string) : void {
            let currentNamespace : string = $parentNamespace;
            if ($name !== null && $name !== "") {
                $objectHolder = $objectHolder[$name];
                currentNamespace = $parentNamespace + "." + $name;
            }
            let parameter : any;
            for (parameter in $objectHolder) {
                if (typeof $objectHolder[parameter] === Constants.OBJECT) {
                    let child : any;
                    let isNamespace : boolean = false;
                    for (child in $objectHolder[parameter]) {
                        /* istanbul ignore else : bulletproof condition */
                        if (ObjectValidator.IsSet($objectHolder[parameter][child])) {
                            if (ObjectValidator.IsFunction($objectHolder[parameter][child])) {
                                const classNameWithNamespace : string =
                                    currentNamespace + "." + parameter.toString() + "." + child.toString();
                                /* istanbul ignore else : other than singleton instance can not be created */
                                if (this.classesList.indexOf(classNameWithNamespace) === -1) {
                                    this.classesList[this.classesSize] = classNameWithNamespace;
                                    this.classesSize++;
                                }

                                if (ObjectValidator.IsSet($objectHolder[parameter][child].ClassName)) {
                                    $objectHolder[parameter][child].classNamespace = currentNamespace + "." + parameter.toString();
                                    $objectHolder[parameter][child].className = child.toString();
                                    if (currentNamespace + "." + parameter.toString() + "." + child.toString() !==
                                        "Com.Wui.Framework.Commons.Utils.Reflection") {
                                        $objectHolder[parameter][child].ClassName = function () : string {
                                            return this.classNamespace + "." + this.className;
                                        };
                                        $objectHolder[parameter][child].NamespaceName = function () : string {
                                            return this.classNamespace;
                                        };
                                        $objectHolder[parameter][child].ClassNameWithoutNamespace = function () : string {
                                            return this.className;
                                        };
                                    }
                                }
                                if (ObjectValidator.IsSet($objectHolder[parameter][child].prototype.getClassName) &&
                                    currentNamespace + "." + parameter.toString() !==
                                    "Com.Wui.Framework.Commons.Environment") {
                                    $objectHolder[parameter][child].prototype.objectNamespace =
                                        currentNamespace + "." + parameter.toString();
                                    $objectHolder[parameter][child].prototype.objectClassName = child.toString();
                                    if (currentNamespace + "." + parameter.toString() + "." + child.toString() !==
                                        "Com.Wui.Framework.Commons.Utils.Reflection") {
                                        $objectHolder[parameter][child].prototype.getClassName = function () : string {
                                            return this.objectNamespace + "." + this.objectClassName;
                                        };
                                        $objectHolder[parameter][child].prototype.getNamespaceName = function () : string {
                                            return this.objectNamespace;
                                        };
                                        $objectHolder[parameter][child].prototype.getClassNameWithoutNamespace = function () : string {
                                            return this.objectClassName;
                                        };
                                    }
                                }
                                isNamespace = true;
                            } else if (ObjectValidator.IsObject($objectHolder[parameter][child])) {
                                isNamespace = true;
                            }
                        }
                    }

                    if (isNamespace) {
                        this.setNamespace($objectHolder, currentNamespace, parameter);
                    }
                } else if (ObjectValidator.IsFunction($objectHolder[parameter])) {
                    if (this.classesList.indexOf(currentNamespace + "." + parameter.toString()) === -1) {
                        this.classesList[this.classesSize] = currentNamespace + "." + parameter.toString();
                        this.classesSize++;
                    }
                }
            }
        }
    }
}
