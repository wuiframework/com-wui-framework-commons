/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import NewLineType = Com.Wui.Framework.Commons.Enums.NewLineType;
    import IOHandler = Com.Wui.Framework.Commons.Interfaces.IOHandler;
    import IOHandlerType = Com.Wui.Framework.Commons.Enums.IOHandlerType;
    import IOHandlerFactory = Com.Wui.Framework.Commons.IOApi.IOHandlerFactory;

    /**
     * Echo class provides static API for handling of browser content.
     */
    export class Echo extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private static stream : string;
        private static output : IOHandler;
        private static htmlEnabled : boolean;

        /**
         * Execute initialization manually, in case of that, it has not been initialized automatically.
         * @param {string|IOHandler} $targetIdOrHandler Set HTML element ID subscribed to echo or
         * specify IOHandler, which should be used for echo.
         * @param {boolean} $forceInit Reload output handler, if needed.
         * @return {void}
         */
        public static Init($targetIdOrHandler? : string | IOHandler, $forceInit : boolean = false) : void {
            if (!ObjectValidator.IsSet(this.output) || $forceInit) {
                if (!ObjectValidator.IsSet($targetIdOrHandler)) {
                    this.output = IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT, "Content");
                } else if (ObjectValidator.IsString($targetIdOrHandler)) {
                    this.output = IOHandlerFactory.getHandler(IOHandlerType.HTML_ELEMENT, <string>$targetIdOrHandler);
                } else {
                    this.output = <IOHandler>$targetIdOrHandler;
                }
                this.output.Init();
                this.htmlEnabled = this.getHandlerType() === IOHandlerType.HTML_ELEMENT;
            }
            if (!ObjectValidator.IsSet(this.stream) || $forceInit) {
                this.stream = "";
            }
        }

        /**
         * @return {IOHandlerType} Returns currently used Output handler.
         */
        public static getHandlerType() : IOHandlerType {
            return Com.Wui.Framework.Commons.IOApi.IOHandlerFactory.getHandlerType(this.output);
        }

        /**
         * @param {string} $message Print message text to the screen without any modifications.
         * @param {boolean} [$clearBeforePrint=false] Clean up screen content before print if true,
         * otherwise append message to current content.
         * @return {void}
         */
        public static Print($message : string, $clearBeforePrint : boolean = false) : void {
            if ($clearBeforePrint) {
                this.Clear();
            }
            this.save($message);
        }

        /**
         * @param {string|any} $messageOrObject Print text or any type of object to the screen.
         * String message has support of string formatting.
         * @param {...any} $parameters Open-ended args for search in message string, if arg is not typeof string it will
         * be converted to string.
         * @return {void}
         */
        public static Printf($messageOrObject : string | any, ...$parameters : any[]) : void {
            if (ObjectValidator.IsSet($parameters) && ObjectValidator.IsString($messageOrObject)) {
                const args : any[] = [$messageOrObject];
                let index : number;
                for (index = 0; index < $parameters.length; index++) {
                    args[index + 1] = Convert.ObjectToString($parameters[index], "", this.htmlEnabled);
                }
                this.Println(StringUtils.Format.apply(String, args));
            } else {
                this.Println(Convert.ObjectToString($messageOrObject, "", this.htmlEnabled));
            }
        }

        /**
         * @param {string} $message Print message to the screen at new line.
         * @return {void}
         */
        public static Println($message : string) : void {
            this.Print((this.htmlEnabled ? NewLineType.HTML : NewLineType.LINUX) + $message);
        }

        /**
         * @param {string} $message Print message with escaped html to the screen at new line.
         * @return {void}
         */
        public static PrintCode($message : string) : void {
            $message = StringUtils.Replace($message, "<", "&lt;");
            $message = StringUtils.Replace($message, ">", "&gt;");
            this.Println("<pre>" + $message + "</pre>");
        }

        /**
         * Clean up screen content.
         * @return {void}
         */
        public static Clear() : void {
            this.Init();
            this.output.Clear();
        }

        /**
         * Clean up screen content and also content stream.
         * @return {void}
         */
        public static ClearAll() : void {
            this.Clear();
            this.stream = "";
        }

        /**
         * @return {string} Returns whole content stream printed by Echo, from last clean up.
         */
        public static getStream() : string {
            return this.stream;
        }

        /**
         * @param {Function} $handler Callback, which should be called in time on message print.
         * @return {void}
         */
        public static setOnPrint($handler : ($message : string) => void) : void {
            this.Init();
            this.output.setOnPrint($handler);
        }

        private static save($message : string) : void {
            this.Init();
            this.stream += $message;
            this.output.Print($message);
        }
    }
}
