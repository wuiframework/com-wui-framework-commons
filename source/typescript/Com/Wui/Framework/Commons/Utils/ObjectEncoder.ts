/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import BaseObject = Com.Wui.Framework.Commons.Primitives.BaseObject;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import TimeoutManager = Com.Wui.Framework.Commons.Events.TimeoutManager;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    /**
     * ObjectEncoder class provides static methods focused on object or string encoding.
     */
    export class ObjectEncoder extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        /**
         * @param {any} $input Object or primitive value, which should be serialized to the string.
         * @param {Function} [$asyncHandler] Specify handler function, which will be asynchronously called,
         * when the serialization is finished.
         * @return {string} Returns string representation of input value.
         * @return {any} Returns string representation of input value, if $asyncHandler has not been specified,
         * otherwise null and full string representation of input value will be returned as $asyncHandler input argument.
         */
        public static Serialize($input : any, $asyncHandler? : ($data : string) => void) : any {
            const classMap : string[] = [];
            let classMapSize : number = 0;
            const asyncManager : TimeoutManager = new TimeoutManager();

            const getArrayBlock : ($values : string) => string = ($values : string) : string => {
                return "a:" + StringUtils.Length($values) + ":" + $values;
            };
            const getObjectBlock : ($values : string) => string = ($values : string) : string => {
                return "o:" + StringUtils.Length($values) + ":" + $values;
            };
            const getClassBlock : ($classIndex : number, $values : string) => string =
                ($classIndex : number, $values : string) : string => {
                    return "c:" + $classIndex + ":" + StringUtils.Length($values) + ":" + $values;
                };

            let serializeAsync : ($input : any, $handler : ($data : string, $index? : number) => void,
                                  $handlerIndex? : number) => void;
            const serialize : ($input : any, $handler? : ($data : string, $index? : number) => void, $handlerIndex? : number) => any =
                ($input : any, $handler? : ($data : string, $index? : number) => void, $handlerIndex? : number) : any => {
                    const async : boolean = ObjectValidator.IsSet($handler);
                    let key : any;
                    let value : string;
                    const keys : string[] = [];
                    const values : string[] = [];
                    let items : string = "";
                    let itemsLength : number;
                    let itemIndex : number = 0;
                    let parserIndex : number = 0;
                    /* istanbul ignore else : all required cases are covered */
                    if (!ObjectValidator.IsSet($input)) {
                        value = "u";
                        if (async) {
                            $handler(value, $handlerIndex);
                        }
                    } else if ($input === null) {
                        value = "n";
                        if (async) {
                            $handler(value, $handlerIndex);
                        }
                    } else if (ObjectValidator.IsBoolean($input)) {
                        value = "b:" + ($input ? "1" : "0");
                        if (async) {
                            $handler(value, $handlerIndex);
                        }
                    } else if (!ObjectValidator.IsEmptyOrNull($input) && ObjectValidator.IsString($input)) {
                        value = "s:" + StringUtils.Length($input) + ":" + $input;
                        if (async) {
                            $handler(value, $handlerIndex);
                        }
                    } else if (ObjectValidator.IsInteger($input)) {
                        value = "i:" + $input + ";";
                        if (async) {
                            $handler(value, $handlerIndex);
                        }
                    } else if (ObjectValidator.IsDouble($input)) {
                        value = "d:" + $input + ";";
                        if (async) {
                            $handler(value, $handlerIndex);
                        }
                    } else if (ObjectValidator.IsNativeArray($input)) {
                        itemsLength = $input.length;
                        if (itemsLength > 0) {
                            for (key in $input) {
                                /* istanbul ignore else : bulletproof condition */
                                if ($input.hasOwnProperty(key)) {
                                    if (async) {
                                        serializeAsync(Convert.StringToInteger(key), ($data : string, $index? : number) : void => {
                                            keys[$index] = $data;
                                        }, parserIndex);
                                        serializeAsync($input[key], ($data : string, $index? : number) : void => {
                                            values[$index] = $data;
                                            itemIndex++;
                                            if (itemIndex === itemsLength) {
                                                asyncManager.Add(() : void => {
                                                    for (itemIndex = 0; itemIndex < itemsLength; itemIndex++) {
                                                        items += keys[itemIndex] + values[itemIndex];
                                                    }
                                                    value = getArrayBlock(items);
                                                    items = null;
                                                    $handler(value, $handlerIndex);
                                                });
                                            }
                                        }, parserIndex);
                                        parserIndex++;
                                    } else {
                                        items += serialize(Convert.StringToInteger(key)) + serialize($input[key]);
                                    }
                                }
                            }
                        } else {
                            value = getArrayBlock("");
                            if (async) {
                                $handler(value, $handlerIndex);
                            }
                        }
                        if (!async) {
                            value = getArrayBlock(items);
                        }
                    } else if (ObjectValidator.IsObject($input)) {
                        if (Reflection.getInstance().IsMemberOf($input, BaseObject)) {
                            const className : string = $input.getClassName();
                            if (classMap.indexOf(className) === -1) {
                                classMap[classMapSize] = className;
                                classMapSize++;
                            }
                            const classIndex : number = classMap.indexOf(className);
                            if (async) {
                                serializeAsync($input.SerializationData(), ($data : string) : void => {
                                    value = getClassBlock(classIndex, $data);
                                    $handler(value, $handlerIndex);
                                });
                            } else {
                                value = serialize($input.SerializationData());
                                value = getClassBlock(classIndex, value);
                            }
                        } else {
                            itemsLength = 0;
                            for (key in $input) {
                                /* istanbul ignore else : bulletproof condition */
                                if ($input.hasOwnProperty(key)) {
                                    itemsLength++;
                                }
                            }
                            if (itemsLength > 0) {
                                for (key in $input) {
                                    /* istanbul ignore else : bulletproof condition */
                                    if ($input.hasOwnProperty(key)) {
                                        if (async) {
                                            serializeAsync(key, ($data : string, $index? : number) : void => {
                                                keys[$index] = $data;
                                            }, parserIndex);
                                            serializeAsync($input[key], ($data : string, $index? : number) : void => {
                                                values[$index] = $data;
                                                itemIndex++;
                                                if (itemIndex === itemsLength) {
                                                    asyncManager.Add(() : void => {
                                                        for (itemIndex = 0; itemIndex < itemsLength; itemIndex++) {
                                                            items += keys[itemIndex] + values[itemIndex];
                                                        }
                                                        value = getObjectBlock(items);
                                                        items = null;
                                                        $handler(value, $handlerIndex);
                                                    });
                                                }
                                            }, parserIndex);
                                            parserIndex++;
                                        } else {
                                            items += serialize(key) + serialize($input[key]);
                                        }
                                    }
                                }
                            } else {
                                value = getObjectBlock("");
                                if (async) {
                                    $handler(value, $handlerIndex);
                                }
                            }
                            if (!async) {
                                value = getObjectBlock(items);
                            }
                        }
                    } else if (ObjectValidator.IsFunction($input)) {
                        value = Convert.FunctionToString($input);
                        value = StringUtils.Substring(value, StringUtils.IndexOf(value, "(") + 1, StringUtils.Length(value) - 1);
                        value = "f:" + StringUtils.Length(value) + ":" + value;
                        if (async) {
                            $handler(value, $handlerIndex);
                        }
                    } else if (ObjectValidator.IsEmptyOrNull($input)) {
                        value = "e";
                        if (async) {
                            $handler(value, $handlerIndex);
                        }
                    }

                    if (!async) {
                        return value;
                    }
                };
            serializeAsync = ($input : any, $handler : ($data : string, $index? : number) => void, $handlerIndex? : number) : void => {
                asyncManager.Add(() : void => {
                    serialize($input, $handler, $handlerIndex);
                });
            };

            if (ObjectValidator.IsSet($asyncHandler)) {
                let output : any;
                serializeAsync($input, ($data : string) : void => {
                    output = $data;
                });
                EventsManager.getInstanceSingleton().setEvent(asyncManager.getId(), EventType.ON_COMPLETE, () : void => {
                    if (classMapSize > 0) {
                        const serializedMap : string = serialize(classMap);
                        output = "m:" + StringUtils.Length(serializedMap) + ":" + serializedMap + output;
                    }
                    $asyncHandler(output);
                });
                asyncManager.Execute();
                return null;
            } else {
                const serialized : string = serialize($input);
                if (classMapSize > 0) {
                    const serializedMap : string = serialize(classMap);
                    return "m:" + StringUtils.Length(serializedMap) + ":" + serializedMap + serialized;
                } else {
                    return serialized;
                }
            }
        }

        /**
         * @param {string} $input String value, which should be encoded.
         * @return {string} Returns escaped string, which can be used as http parameter or cookie value.
         */
        public static Url($input : string) : string {
            return encodeURIComponent($input);
        }

        /**
         * @param {string} $input String value, which should be encoded.
         * @return {string} Returns string encode at Utf8 format.
         */
        public static Utf8($input : string) : string {
            if (ObjectValidator.IsEmptyOrNull($input)) {
                return "";
            }

            $input += "";
            let buffer : string = "";
            let start : number = 0;
            let end : number = 0;
            const inputLength : number = StringUtils.Length($input);
            let index : number;
            for (index = 0; index < inputLength; index++) {
                let code : number = StringUtils.getCodeAt($input, index);
                let encoded : string = "";

                /* tslint:disable: no-bitwise */
                if (code < 128) {
                    end++;
                } else if (code > 127 && code < 2048) {
                    encoded = Convert.UnicodeToString((code >> 6) | 192, (code & 63) | 128);
                } else if ((code & 0xF800) !== 0xD800) {
                    encoded = Convert.UnicodeToString((code >> 12) | 224, ((code >> 6) & 63) | 128, (code & 63) | 128);
                } else {
                    if ((code & 0xFC00) !== 0xD800) {
                        ExceptionsManager.Throw("Uft8 encoder", "Unmatched trail surrogate at " + index);
                    }
                    const surrogateCode : number = StringUtils.getCodeAt($input, ++index);
                    if ((surrogateCode & 0xFC00) !== 0xDC00) {
                        ExceptionsManager.Throw("Uft8 encoder", "Unmatched lead surrogate at " + (index - 1));
                    }
                    code = ((code & 0x3FF) << 10) + (surrogateCode & 0x3FF) + 0x10000;
                    encoded = Convert.UnicodeToString(
                        (code >> 18) | 240, ((code >> 12) & 63) | 128, ((code >> 6) & 63) | 128, (code & 63) | 128);
                }
                /* tslint:enable */
                if (!ObjectValidator.IsEmptyOrNull(encoded)) {
                    if (end > start) {
                        buffer += StringUtils.Substring($input, start, end);
                    }
                    buffer += encoded;
                    start = end = index + 1;
                }
            }

            if (end > start) {
                buffer += StringUtils.Substring($input, start, inputLength);
            }

            return buffer;
        }

        /**
         * @param {string} $input String value, which should be encoded.
         * @param {boolean} [$urlSafe=false] Switch output format.
         * @return {string} Returns string encoded in base64 format, which can be used as http parameter or
         * cookie value in case of, that $urlSafe is true.
         */
        public static Base64($input : string, $urlSafe : boolean = false) : string {
            if (ObjectValidator.IsEmptyOrNull($input)) {
                return "";
            }

            const b64 : string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
            let o1 : number;
            let o2 : number;
            let o3 : number;
            let h1 : number;
            let h2 : number;
            let h3 : number;
            let h4 : number;
            let bits : number;
            let index : number = 0;
            let ac : number = 0;
            const tmp : string[] = [];

            $input = ObjectDecoder.Url(this.Url(this.Utf8($input)));
            const length : number = StringUtils.Length($input);

            do {
                o1 = StringUtils.getCodeAt($input, index++);
                o2 = StringUtils.getCodeAt($input, index++);
                o3 = StringUtils.getCodeAt($input, index++);

                /* tslint:disable: no-bitwise */
                bits = o1 << 16 | o2 << 8 | o3;

                h1 = bits >> 18 & 0x3f;
                h2 = bits >> 12 & 0x3f;
                h3 = bits >> 6 & 0x3f;
                h4 = bits & 0x3f;
                /* tslint:enable */

                tmp[ac++] =
                    StringUtils.getCharacterAt(b64, h1) +
                    StringUtils.getCharacterAt(b64, h2) +
                    StringUtils.getCharacterAt(b64, h3) +
                    StringUtils.getCharacterAt(b64, h4);
            } while (index < length);

            let encoded : string = tmp.join("");

            const r : number = length % 3;
            encoded = (r ? StringUtils.Substring(encoded, 0, StringUtils.Length(encoded) + r - 3) : encoded) +
                StringUtils.Substring("===", r || 3, 3);

            if ($urlSafe) {
                encoded = StringUtils.Replace(encoded, "+", "-");
                encoded = StringUtils.Replace(encoded, "/", "_");
                encoded = StringUtils.Replace(encoded, "=", ".");
            }
            return encoded;
        }
    }
}
