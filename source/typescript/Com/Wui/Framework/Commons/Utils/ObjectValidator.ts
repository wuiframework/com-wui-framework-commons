/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Utils {
    "use strict";
    import Constants = Com.Wui.Framework.Commons.Enums.SyntaxConstants;

    /**
     * ObjectValidator class provides static methods focused on object type validations.
     */
    export class ObjectValidator extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        /**
         * @param {any} $input Validate this object or property.
         * @return {boolean} Returns true, if object or property is defined, otherwise false.
         */
        public static IsSet($input : any) : boolean {
            return Constants.UNDEFINED.indexOf($input) === -1;
        }

        /**
         * @param {any} $input Validate this type of object.
         * @return {boolean} Returns true, if object is empty or null, otherwise false.
         */
        public static IsEmptyOrNull($input : any) : boolean {
            if (!this.IsSet($input)) {
                return true;
            }
            if ($input === null || $input === "") {
                return true;
            }
            if (this.IsNativeArray($input)) {
                return $input.length === 0;
            }
            if (typeof $input[Constants.IS_EMPTY] === Constants.FUNCTION && typeof $input[Constants.LENGTH] === Constants.FUNCTION) {
                return $input.IsEmpty();
            }
            return false;
        }

        /**
         * @param {any} $input Validate this type of object.
         * @return {boolean} Returns true, if $input is object, otherwise false.
         */
        public static IsObject($input : any) : boolean {
            return $input !== null && typeof $input === Constants.OBJECT &&
                Constants.ARRAYS.indexOf(Object.prototype.toString.call($input)) === -1;
        }

        /**
         * @param {any} $input Validate this type of object.
         * @return {boolean} Returns true, if $input is function, otherwise false.
         */
        public static IsFunction($input : any) : boolean {
            return this.IsSet($input) && typeof $input === Constants.FUNCTION;
        }

        /**
         * @param {any} $input Validate this type of class instance.
         * @return {boolean} Returns true, if $input is class instance, otherwise false.
         */
        public static IsClass($input : any) : boolean {
            return this.IsObject($input) && typeof $input[Constants.CLASS_NAME] === Constants.FUNCTION;
        }

        /**
         * @param {any} $input Validate this type of object.
         * @return {boolean} Returns true, if object is type of boolean, otherwise false.
         */
        public static IsBoolean($input : any) : boolean {
            return typeof $input === Constants.BOOLEAN;
        }

        /**
         * @param {any} $input Validate this type of object.
         * @return {boolean} Returns true, if object is number, otherwise false.
         */
        public static IsDigit($input : any) : boolean {
            if (isNaN($input)) {
                return false;
            }

            if (typeof $input === Constants.NUMBER) {
                return true;
            }

            if (!this.IsEmptyOrNull($input)) {
                if (this.IsString($input)) {
                    return $input.match(/[a-z]/i) === null && !isNaN(parseInt($input, 10));
                }
            } else {
                return false;
            }
        }

        /**
         * @param {any} $input Validate this type of object.
         * @return {boolean} Returns true, if object is integer, otherwise false.
         */
        public static IsInteger($input : any) : boolean {
            return this.IsDigit($input) && parseFloat($input) === parseInt($input, 10) && !isNaN($input);
        }

        /**
         * @param {any} $input Validate this type of object.
         * @return {boolean} Returns true, if object has floating point, otherwise false.
         */
        public static IsDouble($input : any) : boolean {
            return this.IsDigit($input) && !this.IsInteger($input);
        }

        /**
         * @param {any} $input Validate this type of object.
         * @return {boolean} Returns true, if object is type of string, otherwise false.
         */
        public static IsString($input : any) : boolean {
            return typeof $input === Constants.STRING || $input instanceof Com.Wui.Framework.Commons.Utils.StringUtils;
        }

        /**
         * @param {any} $input Validate this type of object.
         * @return {boolean} Returns true, if object is type of array, otherwise false.
         */
        public static IsNativeArray($input : any) : boolean {
            return Constants.ARRAYS.indexOf(Object.prototype.toString.call($input)) !== -1;
        }

        /**
         * @param {any} $input Validate this type of object.
         * @return {boolean} Returns true, if object is type of native array or instance of ArrayList, otherwise false.
         */
        public static IsArray($input : any) : boolean {
            return this.IsNativeArray($input) || $input instanceof Com.Wui.Framework.Commons.Primitives.ArrayList;
        }

        /**
         * @param {string} $input Validate if string is hex format.
         * @return {boolean} Returns true, if string represents hexadecimal number, otherwise false.
         */
        public static IsHexadecimal($input : string) : boolean {
            if (!this.IsEmptyOrNull($input)) {
                if (($input + "").toLowerCase().indexOf("0x") === 0) {
                    $input = ($input + "").slice(2);
                }
                if (($input + "").toLowerCase().indexOf("#") === 0) {
                    $input = ($input + "").slice(1);
                }
            }
            return !this.IsEmptyOrNull($input) && !isNaN(parseInt($input, 16));
        }
    }
}
