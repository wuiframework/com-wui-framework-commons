/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Primitives {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;

    /**
     * ArrayList class provides generic manipulation with primitive values or objects.
     */
    export class ArrayList<T> extends BaseObject implements Com.Wui.Framework.Commons.Interfaces.IArrayList<T> {

        private size : number;
        private keys : Array<string | number>;
        private data : T[];
        private lastIndex : number;

        /**
         * @param {any} $input Object for conversion to ArrayList.
         * @return {ArrayList<any>} Returns ArrayList converted from $input object type of any.
         */
        public static ToArrayList($input : any) : ArrayList<any> {
            const output : ArrayList<any> = new ArrayList<any>();

            if (ObjectValidator.IsArray($input)) {
                if (!($input instanceof ArrayList)) {
                    let index : number;
                    let value : any;
                    for (index = 0; index < (<any[]>$input).length; index++) {
                        value = $input[index];
                        if (ObjectValidator.IsArray(value)) {
                            output.Add(this.ToArrayList(value), index);
                        } else {
                            output.Add(value, index);
                        }
                    }
                } else {
                    $input.foreach(($value : any, $key? : any) : void => {
                        if (ObjectValidator.IsArray($value)) {
                            output.Add(ArrayList.ToArrayList($value), $key);
                        } else {
                            output.Add($value, $key);
                        }
                    });
                }
            } else {
                output.Add($input);
            }

            return output;
        }

        /**
         * @param {T} [$value] Set initial array item value.
         * @param {string|number} [$key] Set initial array item key.
         */
        constructor($value? : T, $key? : any) {
            super();
            this.size = 0;
            this.keys = [];
            this.data = [];

            if (!ObjectValidator.IsEmptyOrNull($value)) {
                this.Add($value, $key);
            }
        }

        /**
         * @param {T} $value Value which should be added/overridden to the array.
         * @param {(string|number)} [$key] Set value key, if not specified key will be generated.
         * @return {void}
         */
        public Add($value : T, $key? : string | number) : void {
            if (ObjectValidator.IsSet($key)) {
                if (!this.KeyExists($key)) {
                    this.keys[this.size] = $key;
                    this.data[this.size] = $value;
                    this.size++;
                    if (ObjectValidator.IsDigit($key) && $key > this.lastIndex) {
                        this.lastIndex = <number>$key + 1;
                    }
                } else {
                    this.data[this.keys.indexOf($key)] = $value;
                }
            } else {
                if (!ObjectValidator.IsSet(this.lastIndex)) {
                    let index : number = this.size - 1;
                    while (index >= 0 && !ObjectValidator.IsDigit(this.keys[index])) {
                        index--;
                    }
                    if (index >= 0) {
                        this.lastIndex = <number>this.keys[index] + 1;
                    } else {
                        this.lastIndex = 0;
                    }
                }
                this.data[this.size] = $value;
                this.keys[this.size] = this.lastIndex;
                this.size++;
                this.lastIndex++;
            }
        }

        /**
         * @return {(Array<string|number>)} Returns native array of ArrayList keys.
         */
        public getKeys() : Array<string | number> {
            return this.keys;
        }

        /**
         * @param {T} $value Search for this value in the array.
         * @return {(string|number)} Returns key associated to searched value, if value has been found, otherwise null.
         */
        public getKey($value : T) : string | number {
            let index : number;
            for (index = 0; index < this.size; index++) {
                if (this.data[index] === $value) {
                    return this.keys[index];
                }
            }
            return null;
        }

        /**
         * @param {(string|number)} $value Search for this key in the array.
         * @return {boolean} Returns true, if key has been found, otherwise false.
         */
        public KeyExists($value : string | number) : boolean {
            if (ObjectValidator.IsEmptyOrNull($value)) {
                return false;
            }
            if (!ObjectValidator.IsDigit($value) && !ObjectValidator.IsString($value)) {
                throw new Error("Only string or number type of key is allowed for '" + this.getClassName() + "'" +
                    ", passed typeof: " + Convert.ObjectToString($value));
            } else {
                return this.keys.indexOf($value) !== -1;
            }
        }

        /**
         * @param {T} $value Search for this value in the array.
         * @return {boolean} Returns true, if the value has been found, otherwise false.
         */
        public Contains($value : T) : boolean {
            let index : number;
            for (index = 0; index < this.size; index++) {
                if (this.data[index] === $value) {
                    return true;
                }
            }
            return false;
        }

        /**
         * @param {T} $value Search for this value in the array.
         * @return {number} Returns index of value in the array, if the value has been found, otherwise -1.
         */
        public IndexOf($value : T) : number {
            let index : number;
            for (index = 0; index < this.size; index++) {
                if (this.data[index] === $value) {
                    return index;
                }
            }
            return -1;
        }

        /**
         * Fix items indexes based on real item position in the array.
         * @return {void}
         */
        public Reindex() : void {
            if (this.size > 0) {
                this.lastIndex = 0;
                let index : number;
                for (index = 0; index < this.size; index++) {
                    if (!ObjectValidator.IsString(this.keys[index])) {
                        this.keys[index] = this.lastIndex;
                        this.lastIndex++;
                    }
                }
            }
        }

        /**
         * @param {(string|number)} $key Search for this key in the array.
         * @return {T} Returns value associated with searched key, if the key has been found, otherwise null.
         */
        public getItem($key : string | number) : T {
            if (this.KeyExists($key)) {
                return <T> this.data[this.keys.indexOf($key)];
            } else {
                return null;
            }
        }

        /**
         * @return {T} Returns first value in the array, if the array is not empty, otherwise null.
         */
        public getFirst() : T {
            if (!this.IsEmpty()) {
                return <T> this.data[0];
            } else {
                return null;
            }
        }

        /**
         * @return {T} Returns last value in the array, if the array is not empty, otherwise null.
         */
        public getLast() : T {
            if (!this.IsEmpty()) {
                return <T> this.data[this.size - 1];
            } else {
                return null;
            }
        }

        /**
         * @return {Array<T>} Returns native array of values without keys. Array is not sorted by the default.
         */
        public getAll() : T[] {
            return this.data;
        }

        /**
         * @param {ArrayList<any>} $compareWith Data source for compare with the array.
         * @return {boolean} Returns true if arrays are equal, otherwise false.
         */
        public Equal($compareWith : ArrayList<any>) : boolean {
            const keys : Array<string | number> = $compareWith.getKeys();
            let index : number;
            for (index = 0; index < keys.length; index++) {
                if (!this.KeyExists(keys[index])) {
                    return false;
                } else if (this.getItem(keys[index]) !== $compareWith.getItem(keys[index])) {
                    return false;
                }
            }
            return true;
        }

        /**
         * @return {number} Returns length of the array.
         */
        public Length() : number {
            return this.size;
        }

        /**
         * @return {boolean} Returns true if length of the array is equal to 0, otherwise false.
         */
        public IsEmpty() : boolean {
            return this.size === 0;
        }

        public Clear() : void {
            this.data = [];
            this.keys = [];
            this.size = 0;
            this.lastIndex = 0;
        }

        /**
         * @param {ArrayList<T>} $copyFrom Data source for copy of values to the array.
         * @param {number} [$count] Count of values witch will be copied.
         * @param {boolean} [$clearBefore] If true the array will be cleaned up before copy.
         * @return {void}
         */
        public Copy($copyFrom : ArrayList<T>, $count? : number, $clearBefore? : boolean) : void {
            let count : number = 0;
            if ($count && $count > 0) {
                count = $count;
            } else {
                count = $copyFrom.Length();
            }

            if ($clearBefore) {
                this.Clear();
            }

            let addIterator : number = 0;
            const keys : Array<string | number> = $copyFrom.getKeys();
            let index : number;
            for (index = 0; index < $copyFrom.Length(); index++) {
                this.Add($copyFrom.getItem(keys[index]), keys[index]);
                addIterator++;
                if (addIterator >= count) {
                    break;
                }
            }
        }

        /**
         * @param {number} $index Index of value in the array which should be removed.
         * @return {void}
         */
        public RemoveAt($index : number) : void {
            if ($index < this.size) {
                let index : number;
                let newIndex : number = 0;
                const data : T[] = [];
                const keys : any[] = [];
                for (index = 0; index < this.size; index++) {
                    if (index !== $index) {
                        data[newIndex] = this.data[index];
                        keys[newIndex] = this.keys[index];
                        newIndex++;
                    }
                }
                this.data = data;
                this.keys = keys;
                this.size = newIndex;
            }
        }

        /**
         * Removes last item in the array.
         * @return {void}
         */
        public RemoveLast() : void {
            this.RemoveAt(this.size - 1);
        }

        /**
         * Sort items by key up.
         * @return {void}
         */
        public SortByKeyUp() : void {
            let current : any;
            let next : any;
            let tmp : T;
            const length : number = this.size - 1;
            let index : number;
            for (index = 0; index < length; index++) {
                current = this.keys[index];
                next = this.keys[index + 1];
                if (current.toString() > next.toString()) {
                    tmp = this.data[index];
                    this.keys[index] = next;
                    this.data[index] = this.data[index + 1];
                    this.keys[index + 1] = current;
                    this.data[index + 1] = tmp;
                    index = -1;
                }
            }
        }

        /**
         * Sort items by key down.
         * @return {void}
         */
        public SortByKeyDown() : void {
            let current : any;
            let next : any;
            let tmp : T;
            const length : number = this.size - 1;
            let index : number;
            for (index = 0; index < length; index++) {
                current = this.keys[index];
                next = this.keys[index + 1];
                if (current.toString() < next.toString()) {
                    tmp = this.data[index];
                    this.keys[index] = next;
                    this.data[index] = this.data[index + 1];
                    this.keys[index + 1] = current;
                    this.data[index + 1] = tmp;
                    index = -1;
                }
            }
        }

        /**
         * Go through all items and process callback function with parameters {value: T, key?: any} on each of them.
         * @param {function} $handler Callback function for handling of array values.
         * @return {void}
         */
        public foreach($handler : ($value : T, $key? : string | number) => void | boolean) : void {
            let index : number;
            for (index = 0; index < this.size; index++) {
                const result : any = $handler(this.data[index], this.keys[index]);
                if (ObjectValidator.IsBoolean(result) && !result) {
                    break;
                }
            }
        }

        /**
         * @return {Array<T>} Returns native array of values without keys. Method is equal to method getAll.
         */
        public ToArray() : T[] {
            return this.data;
        }

        /**
         * @return {object} Return validate object data for serialization.
         */
        public SerializationData() : object {
            return {size: this.size, keys: this.keys, data: this.data};
        }

        /**
         * @param {string} $prefix value converted to string.
         * @param {boolean} $htmlTag Specify, if output format should be HTML or plain text.
         * @return String of prefix value and HTML tag.
         */
        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            let output : string = "";
            let key : any;
            let value : any;
            let index : number;
            for (index = 0; index < this.size; index++) {
                key = this.keys[index];
                value = this.data[index];

                output += $prefix;

                if (ObjectValidator.IsString(key)) {
                    key = "\"" + key + "\"";
                }

                if (value !== null) {
                    if (value instanceof ArrayList) {
                        value = value.ToString($prefix + StringUtils.Tab(2, $htmlTag), $htmlTag);
                    } else if (ObjectValidator.IsArray(value)) {
                        value = Convert.ArrayToString(value, $prefix + StringUtils.Tab(2, $htmlTag), $htmlTag);
                    } else if (ObjectValidator.IsBoolean(value)) {
                        value = Convert.BooleanToString(<boolean>value) + StringUtils.NewLine($htmlTag);
                    } else if (!ObjectValidator.IsString(value)) {
                        if (ObjectValidator.IsClass(value) &&
                            value instanceof Com.Wui.Framework.Commons.HttpProcessor.HttpResolversCollection) {
                            value = value.ToString($prefix, $htmlTag);
                        } else {
                            value = Convert.ObjectToString(value, "", $htmlTag) + StringUtils.NewLine($htmlTag);
                        }
                    } else if (<string>value === "") {
                        if ($htmlTag) {
                            value += "<b>EMPTY</b>";
                        } else {
                            value += "EMPTY";
                        }
                        value += StringUtils.NewLine($htmlTag);
                    } else {
                        value += StringUtils.NewLine($htmlTag);
                    }
                } else {
                    value = "NULL" + StringUtils.NewLine($htmlTag);
                }

                output += "[ " + key + " ]" + StringUtils.Space(4, $htmlTag) + value;
            }

            if (StringUtils.IsEmpty(output)) {
                output = $prefix + "Data object ";
                if ($htmlTag) {
                    output += "<b>EMPTY</b>";
                } else {
                    output += "EMPTY";
                }
            }

            if ($htmlTag) {
                output = Convert.ToHtmlContentBlock("<i>" + this.getClassName() + " object</i> ", output);
            } else {
                output = this.getClassName() + " object" + StringUtils.NewLine(false) + output;
            }

            return output;
        }

        public toString() : string {
            return this.ToString();
        }
    }
}
