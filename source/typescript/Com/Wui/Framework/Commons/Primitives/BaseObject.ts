/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Primitives {
    "use strict";
    import Constants = Com.Wui.Framework.Commons.Enums.SyntaxConstants;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import Interface = Com.Wui.Framework.Commons.Interfaces.Interface;

    /**
     * BaseObject class provides helper methods focused on validations and handling of class instance.
     */
    export abstract class BaseObject implements Com.Wui.Framework.Commons.Interfaces.IBaseObject {

        private static classNamespace : string = "";
        private static className : string = "";
        private objectNamespace : string;
        private objectClassName : string;

        /**
         * @return {string} Returns class name with namespace in string format.
         */

        /* istanbul ignore next: function is called by ClassName and running in Reflection Class */
        public static ClassName() : string {
            Reflection.getInstance();
            return this.classNamespace + "." + this.className;
        }

        /**
         * @return {string} Returns namespace of the class in string format.
         */

        /* istanbul ignore next: function is running in Reflection Class */
        public static NamespaceName() : string {
            Reflection.getInstance();
            return this.classNamespace;
        }

        /**
         * @return {string} Returns only the class name without namespace in string format.
         */

        /* istanbul ignore next: function is running in Reflection Class */
        public static ClassNameWithoutNamespace() : string {
            Reflection.getInstance();
            return this.className;
        }

        /**
         * @return {string} Returns unique identifier, based on class name and timestamp.
         */
        public static UID() : string {
            return Reflection.UID(this.className);
        }

        /**
         * @param {any} [$dataObject] Data object suitable for data reconstruction.
         * @return {any} Returns class instance in case of, that data object is suitable for object reconstruction,
         * otherwise null. This method is mainly focused to unserialization purposes, but it can also returns class singleton.
         */
        public static getInstance($dataObject? : any) : any {
            return Reflection.getInstanceOf(this.ClassName(), $dataObject);
        }

        private static hashProcessor($input : any, $cache : any[], $cacheIndex : number) : string {
            if ($cache.indexOf($input) !== -1) {
                return "";
            }
            $cache[$cacheIndex] = $input;
            $cacheIndex++;

            let exclude : string[] = [];
            if ($input instanceof BaseObject) {
                exclude = (<BaseObject>$input).excludeIdentityHashData();
            }

            let parameterIndex : number = 0;
            let parameter : any;
            let output : string = "";
            for (parameter in $input) {
                if ($input.hasOwnProperty(parameter) && $input[parameter] !== null && exclude.indexOf(parameter) === -1) {
                    const type : string = typeof $input[parameter];
                    if (type !== Constants.FUNCTION) {
                        output += parameter + ":";
                        if (type === Constants.OBJECT) {
                            output += BaseObject.hashProcessor($input[parameter], $cache, $cacheIndex);
                        } else {
                            output += $input[parameter];
                        }
                        output += ",";
                        parameterIndex++;
                    }
                }
            }
            return output;
        }

        /**
         * @return {string} Returns unique identifier, based on class name and timestamp.
         */
        public getUID() : string {
            return Reflection.UID(this.getClassName());
        }

        /**
         * @return {string} Returns class name with namespace for the class instance in string format.
         */

        /* istanbul ignore next: function is called by ClassName and running in Reflection Class */
        public getClassName() : string {
            if (Constants.UNDEFINED.indexOf(this.objectClassName) > -1) {
                this.objectClassName = Reflection.getInstance().getClassName(this);
            }
            if (this.objectClassName === "") {
                this.objectClassName = this[Constants.CONSTRUCTOR].toString().match(/function\s(\w*)/)[1];
                this.objectClassName = Constants.ALIASES.indexOf(this.objectClassName) > -1 ? "Function" : this.objectClassName;
            }
            return this.objectClassName;
        }

        /**
         * @return {string} Returns namespace of the class in string format.
         */

        /* istanbul ignore next: function is running in Reflection Class */
        public getNamespaceName() : string {
            const fullName : string = this.getClassName();
            return (fullName + "").substring(0, (fullName + "").lastIndexOf("."));
        }

        /**
         * @return {string} Returns only the class name without namespace in string format.
         */

        /* istanbul ignore next: function is running in Reflection Class */
        public getClassNameWithoutNamespace() : string {
            const fullName : string = this.getClassName();
            return (fullName + "").substr((this.getNamespaceName() + "").length + 1);
        }

        /**
         * @return {string[]} Returns string array of all properties in the class instance.
         */
        public getProperties() : string[] {
            const output : string[] = [];
            let parameterIndex : number = 0;
            let parameter : any;
            for (parameter in this) {
                if (typeof this[parameter] !== Constants.FUNCTION) {
                    output[parameterIndex] = parameter;
                    parameterIndex++;
                }
            }
            return output;
        }

        /**
         * @return {string[]} Returns string array of all methods in the class instance.
         */
        public getMethods() : string[] {
            const output : string[] = [];
            let parameterIndex : number = 0;
            let parameter : any;
            for (parameter in this) {
                if (typeof this[parameter] === Constants.FUNCTION && parameter !== Constants.CONSTRUCTOR) {
                    output[parameterIndex] = parameter;
                    parameterIndex++;
                }
            }
            return output;
        }

        /**
         * @param {ClassName|string} $className Class name for validation.
         * @return {boolean} Returns true if the class instance is type of $className, otherwise false.
         */
        public IsTypeOf($className : ClassName | string) : boolean {
            return Reflection.getInstance().IsInstanceOf(this, $className);
        }

        /**
         * @param {ClassName|string} $className Class name for validation.
         * @return {boolean} Returns true if the class instance is type of $className object or it's child, otherwise false.
         */
        public IsMemberOf($className : ClassName | string) : boolean {
            return Reflection.getInstance().IsMemberOf(this, $className);
        }

        /**
         * @param {(Interface|string)} $className Interface class name in string format or as Object, which should be validated.
         * @return {boolean} Returns true if the class instance implements $className interface, otherwise false.
         */
        public Implements($className : Interface | string) : boolean {
            return Reflection.getInstance().Implements(this, $className);
        }

        /**
         * @return {object} Returns data suitable for object serialization.
         */
        public SerializationData() : object {
            const properties : string[] = this.getProperties();
            const output : object = {};
            const exclude : string[] = this.excludeSerializationData();
            let index : number;
            const length : number = properties.length;
            for (index = 0; index < length; index++) {
                if (exclude.indexOf(properties[index]) === -1) {
                    output[properties[index]] = this[properties[index]];
                }
            }
            return output;
        }

        /**
         * @param  {string} $prefix value converted to String.
         * @param {boolean} $htmlTag Specify, if output format should be HTML or plain text.
         * @return String of prefix value and HTML tag.
         */
        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            return $prefix + "object type of '" + this.getClassName() + "'";
        }

        /**
         * @return {number} Returns CRC calculated from data, which represents current object.
         */
        public getHash() : number {
            let cacheMap : any[] = [];
            const cacheMapIndex : number = 0;
            const output : number =
                Com.Wui.Framework.Commons.Utils.StringUtils.getCrc(BaseObject.hashProcessor(this, cacheMap, cacheMapIndex));
            cacheMap = null;
            return output;
        }

        protected excludeSerializationData() : string[] {
            return ["objectNamespace", "objectClassName"];
        }

        protected excludeIdentityHashData() : string[] {
            return [];
        }
    }
}
