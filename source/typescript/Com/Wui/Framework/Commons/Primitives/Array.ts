/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
/* tslint:disable: variable-name */
let __istanbulGlobalIgnorePlaceholder : any;
/* tslint:enable */
/* dev:end */

/* istanbul ignore if */
if (!Array.prototype.indexOf) {
    /* istanbul ignore next : cross-browser compatibility bug fix can be covered only by selenium run */
    Array.prototype.indexOf = function (searchElement : any, fromIndex? : number) : number {
        let index : number;
        const length : number = this.length;
        for (index = (fromIndex || 0); index < length; index++) {
            if (this[index] === searchElement) {
                return index;
            }
        }
        return -1;
    };
}

/* istanbul ignore if */
if (!Array.prototype.forEach) {
    /* istanbul ignore next : cross-browser compatibility bug fix can be covered only by selenium run */
    Array.prototype.forEach = function (callbackfn : (value : any, index? : number, array? : any[]) => void, thisArg? : any) : void {
        let index : number;
        const length : number = this.length;
        for (index = 0; index < length; index++) {
            callbackfn(this[index], index, thisArg || this);
        }
    };
}

interface Array<T> { // tslint:disable-line
    Contains : (...$searchElements : string[]) => boolean;
}

Array.prototype.Contains = function (...$searchElements : string[]) : boolean {
    let index : number;
    const length : number = $searchElements.length;
    for (index = 0; index < length; index++) {
        if (this.indexOf($searchElements[index]) === -1) {
            return false;
        }
    }
    return true;
};
