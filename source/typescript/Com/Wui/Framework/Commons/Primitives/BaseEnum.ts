/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Primitives {
    "use strict";
    import Constants = Com.Wui.Framework.Commons.Enums.SyntaxConstants;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;

    /**
     * BaseEnum class is abstract class providing base methods for reflection of extended enums.
     */
    export abstract class BaseEnum {

        private static classNamespace : string = "";
        private static className : string = "";

        /**
         * @return {string} Returns class name with namespace in string format.
         */

        /* istanbul ignore next: function is running in Reflection Class */
        public static ClassName() : string {
            Reflection.getInstance();
            return this.classNamespace + "." + this.className;
        }

        /**
         * @return {string} Returns namespace of the class in string format.
         */

        /* istanbul ignore next: function is running in Reflection Class */
        public static NamespaceName() : string {
            Reflection.getInstance();
            return this.classNamespace;
        }

        /**
         * @return {string} Returns only the class name without namespace in string format.
         */

        /* istanbul ignore next: function is running in Reflection Class */
        public static ClassNameWithoutNamespace() : string {
            Reflection.getInstance();
            return this.className;
        }

        /**
         * @param {any} $value Search this value in the enum
         * @return {boolean} Returns true if searched value has been found in the enum, otherwise false.
         */
        public static Contains($value : any) : boolean {
            const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
            let parameter : any;
            for (parameter in thisClass) {
                if (typeof thisClass[parameter] !== Constants.FUNCTION && thisClass[parameter] === $value) {
                    return true;
                }
            }
            return false;
        }

        /**
         * @return {string[]} Returns string array of all properties defined by the enum.
         */
        public static getProperties() : string[] {
            const output : string[] = [];
            const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
            let parameter : any;
            let index : number = 0;
            for (parameter in thisClass) {
                if (parameter !== "classNamespace" &&
                    parameter !== "className" &&
                    thisClass.hasOwnProperty(parameter) &&
                    typeof thisClass[parameter] !== Constants.FUNCTION) {
                    output[index] = parameter;
                    index++;
                }
            }
            return output;
        }

        /**
         * @param {any} $value Search this value in the enum
         * @return {string} Returns property name of first occurrence of desired value in the enum,
         * if value has been defined otherwise null.
         */
        public static getKey($value : any) : string {
            const thisClass : any = Reflection.getInstance().getClass(this.ClassName());
            let parameter : any;
            for (parameter in thisClass) {
                if (typeof thisClass[parameter] !== Constants.FUNCTION && thisClass[parameter] === $value) {
                    return parameter;
                }
            }
            return null;
        }
    }
}
