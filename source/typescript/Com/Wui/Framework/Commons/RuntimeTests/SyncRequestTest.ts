/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Commons.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class SyncRequestTest extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            Echo.Printf(this.getEnvironmentArgs());
            Echo.Printf(this.getRequest());
            Echo.Printf(this.RequestArgs());
        }

    }
}
/* dev:end */
