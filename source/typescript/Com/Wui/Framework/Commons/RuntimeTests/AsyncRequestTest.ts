/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Commons.RuntimeTests {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;

    export class AsyncRequestTest extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.AsyncHttpResolver {

        protected resolver() : void {
            let output : string = "";
            output += Convert.ObjectToString(this.getEnvironmentArgs()) + StringUtils.NewLine();
            output += Convert.ObjectToString(this.getRequest()) + StringUtils.NewLine();
            output += Convert.ObjectToString(this.RequestArgs()) + StringUtils.NewLine();
            this.RequestArgs().Result(output);
            this.success();
        }

    }
}
/* dev:end */
