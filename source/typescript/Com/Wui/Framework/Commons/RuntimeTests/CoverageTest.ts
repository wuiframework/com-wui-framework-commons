/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Commons.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import RuntimeTestRunner = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.RuntimeTestRunner;

    export class CoverageTest extends RuntimeTestRunner {
        public testgetData() : void {
            this.addButton("generate", () : void => {
                RuntimeTestRunner.GenerateCoverage();
            });
        }
    }
}
/* dev:end */
