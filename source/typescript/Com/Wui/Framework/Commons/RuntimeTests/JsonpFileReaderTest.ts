/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Commons.RuntimeTests {
    "use strict";
    import RuntimeTestRunner = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.RuntimeTestRunner;
    import JsonpFileReader = Com.Wui.Framework.Commons.IOApi.Handlers.JsonpFileReader;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IRuntimeTestPromise = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.IRuntimeTestPromise;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    export class JsonpFileReaderTest extends RuntimeTestRunner {

        constructor() {
            super();
            // this.setMethodFilter("testReadFromString");
        }

        public testSuccess() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                JsonpFileReader.Load(
                    "https://hub.wuiframework.com/connector.config.jsonp",
                    ($data : any, $filePath? : string) : void => {
                        Echo.Printf("success from {1}: {0}", $data, $filePath);
                        this.assertEquals(!ObjectValidator.IsEmptyOrNull($data), true);
                        $done();
                    },
                    ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                        Echo.Printf("error from {1}: {0}", $eventArgs, $filePath);
                        this.assertEquals(false, true);
                        $done();
                    });
            };
        }

        public testError() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                JsonpFileReader.Load(
                    "test/resource/data/Com/Wui/Framework/Commons/InvalidData.jsonp",
                    ($data : any, $filePath? : string) : void => {
                        Echo.Printf("success from {1}: {0}", $data, $filePath);
                        this.assertEquals(false, true);
                        $done();
                    },
                    ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                        Echo.Printf("error from {1}: {0}", $eventArgs, $filePath);
                        this.assertEquals(true, true);
                        $done();
                    });
            };
        }

        public testTimeout() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                JsonpFileReader.Load(
                    "https://localhost2.wuiframework.com/com-wui-framework-rest-services/Configuration/" +
                    "com-wui-framework-commons/ValidData2",
                    ($data : any, $filePath? : string) : void => {
                        Echo.Printf("success from {1}: {0}", $data, $filePath);
                        this.assertEquals(false, true);
                        $done();
                    },
                    ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                        Echo.Printf("error from {1}: {0}", $eventArgs, $filePath);
                        this.assertEquals(true, true);
                        $done();
                    });
            };
        }

        public testDummy() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                JsonpFileReader.Load("test/resource/data/Com/Wui/Framework/Commons/ValidData2.jsonp",
                    ($data : any, $filePath? : string) : void => {
                        Echo.Printf("success from {1}: {0}", $data, $filePath);
                        this.assertEquals(!ObjectValidator.IsEmptyOrNull($data), true);
                        $done();
                    },
                    ($eventArgs : ErrorEvent, $filePath? : string) : void => {
                        Echo.Printf("error from {1}: {0}", $eventArgs, $filePath);
                        this.assertEquals(false, true);
                        $done();
                    });
            };
        }

        public testReadFromString() : IRuntimeTestPromise {
            return ($done : () => void) : void => {
                JsonpFileReader.LoadString("" +
                    "JsonpData(\n" +
                    "    {\n" +
                    "        \"$interface\": \"IBasePageConfiguration\",\n" +
                    "        \"title\": \"WUI Services v{0}\",\n" +
                    "        \"loading\": \"Loading, please wait ...\",\n" +
                    "        \"loadingProgress\": \"Progress: {0}%\"\n" +
                    "    }\n" +
                    ");",
                    ($data : any) : void => {
                        Echo.Printf("success: {0}", $data);
                        this.assertEquals(!ObjectValidator.IsEmptyOrNull($data), true);
                        $done();
                    },
                    ($eventArgs : ErrorEvent) : void => {
                        Echo.Printf("error from: {0}", $eventArgs);
                        this.assertEquals(false, true);
                        $done();
                    });
            };
        }
    }
}
/* dev:end */
