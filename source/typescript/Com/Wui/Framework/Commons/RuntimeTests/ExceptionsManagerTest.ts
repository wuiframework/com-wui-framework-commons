/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Commons.RuntimeTests {
    "use strict";
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;

    export class ExceptionsManagerTest extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testSetEvent() : void {
            ExceptionsManager.Throw("test exception", new Error("test exception message"));
        }

    }
}
/* dev:end */
