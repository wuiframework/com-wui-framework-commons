/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Commons.RuntimeTests {
    "use strict";
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import WebServiceClientFactory = Com.Wui.Framework.Commons.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Commons.Enums.WebServiceClientType;
    import IWebServiceClient = Com.Wui.Framework.Commons.Interfaces.IWebServiceClient;

    export class JxbrowserBridgeClientTest extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testConnect() : void {
            Echo.Println("Running test jxbrowser bridge.");
            const client : IWebServiceClient = WebServiceClientFactory.getClient(WebServiceClientType.JXBROWSER_BRIDGE);
            client.Send({test: "test"}, ($response : any) : void => {
                Echo.Printf($response);
            });
            client.Send({test: "test1"});
            client.Send({test: "test2"});
        }
    }
}
/* dev:end */
