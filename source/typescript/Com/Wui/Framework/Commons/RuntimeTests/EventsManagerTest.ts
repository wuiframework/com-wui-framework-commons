/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
/* dev:start */
namespace Com.Wui.Framework.Commons.RuntimeTests {
    "use strict";
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import ThreadPool = Com.Wui.Framework.Commons.Events.ThreadPool;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    export class EventsManagerTest extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.RuntimeTestRunner {

        public testSetEvent() : void {
            this.getEventsManager().setEvent("test", EventType.ON_COMPLETE, ($eventArgs? : EventArgs) : void => {
                Echo.Print($eventArgs.Owner() + " " + $eventArgs.Type());
            });
            this.getEventsManager().FireEvent("test", EventType.ON_START, new EventArgs());
            this.getEventsManager().FireEvent("test", EventType.ON_COMPLETE, new EventArgs());
        }

        public testThreadPool() : void {
            let index : number = 1;
            ThreadPool.AddThread("test", "incrementer", ($eventArgs? : EventArgs) : void => {
                Echo.Println(index + ". " + $eventArgs.ToString());
                index++;
                if (index === 15) {
                    ThreadPool.RemoveThread("test", "incrementer");
                }
            });
            ThreadPool.Execute();
        }

    }
}
/* dev:end */
