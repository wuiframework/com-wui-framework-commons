/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Exceptions {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import IOHandlerType = Com.Wui.Framework.Commons.Enums.IOHandlerType;
    import ExceptionCode = Com.Wui.Framework.Commons.Enums.ExceptionCode;
    import HttpRequestConstants = Com.Wui.Framework.Commons.Enums.HttpRequestConstants;

    /**
     * ExceptionsManager class provides handling of exceptions.
     */
    export class ExceptionsManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        private static exceptionList : ArrayList<Type.Exception>;
        private static fatalErrorExists : boolean = false;
        private static selfErrorExists : boolean = false;

        /**
         * @param {string} $owner Specify who has been exception throwing.
         * @param {(string|Error|Exception)} $message Specify exception message or detailed content.
         * @param {number} [$line] Specify line number where exception has been thrown.
         * @param {string} [$file] Specify file name where has been exception throwing.
         * @param {ExceptionCode} [$code] Specify exception code for better identification.
         * @return {void}
         */
        public static Throw($owner : string, $message : string | Error | Type.Exception, $line? : number, $file? : string,
                            $code? : ExceptionCode) : void {
            let exception : Type.Exception;
            let isExit : boolean = false;
            if (ObjectValidator.IsObject($message) && ObjectValidator.IsSet((<Type.Exception>$message).IsMemberOf) &&
                (<Type.Exception>$message).IsMemberOf(Type.Exception)) {
                if (!(<Type.Exception>$message).IsTypeOf(Type.Exit)) {
                    exception = <Type.Exception>$message;
                } else {
                    isExit = true;
                }
                if ((<Type.Exception>$message).IsTypeOf(Type.ResolverFatalException)) {
                    ExceptionsManager.selfErrorExists = true;
                    ExceptionsManager.fatalErrorExists = true;
                } else if ((<Type.Exception>$message).IsTypeOf(Type.ErrorPageException)) {
                    ExceptionsManager.selfErrorExists = true;
                } else if ((<Type.Exception>$message).IsTypeOf(Type.FatalError)) {
                    ExceptionsManager.fatalErrorExists = true;
                }
            } else {
                let stack : string = "";
                if (ObjectValidator.IsSet((<any>$message).stack)) {
                    stack = (<any>$message).stack;
                }
                if (ObjectValidator.IsEmptyOrNull($line)) {
                    $line = -1;
                }
                if (ObjectValidator.IsEmptyOrNull($file)) {
                    $file = "";
                }
                if (ObjectValidator.IsEmptyOrNull($code)) {
                    $code = ExceptionCode.GENERAL;
                }
                if (ObjectValidator.IsSet((<any>$message).message)) {
                    $message = (<any>$message).message;
                }
                exception = new Type.Exception();
                exception.Owner($owner);
                exception.Message(<string>$message);
                exception.Line($line);
                exception.File($file);
                exception.Code($code);
                exception.Stack(stack);
            }

            if (ObjectValidator.IsSet(exception) && !isExit) {
                if (!ObjectValidator.IsSet(ExceptionsManager.exceptionList)) {
                    ExceptionsManager.exceptionList = new ArrayList<Type.Exception>();
                }
                if (ObjectValidator.IsEmptyOrNull(exception.Owner())) {
                    if (!ObjectValidator.IsEmptyOrNull($owner)) {
                        exception.Owner($owner);
                    } else {
                        exception.Owner(ExceptionsManager.ClassName());
                    }
                }
                if (ObjectValidator.IsEmptyOrNull(exception.Message())) {
                    exception.Message(exception.getClassNameWithoutNamespace() +
                        " - " + ExceptionCode[exception.Code()] + " (" + exception.Code() + ")");
                }
                ExceptionsManager.exceptionList.Add(exception);
            }

            if (ObjectValidator.IsSet(setTimeout)) {
                let lastTimeoutId : any = setTimeout(() : void => {
                    // force invoke creation of last timeout id
                }, 0);
                do {
                    clearTimeout(lastTimeoutId);
                }
                while (lastTimeoutId--);
            }
            if (ObjectValidator.IsSet(setInterval)) {
                let lastIntervalId : any = setInterval(() : void => {
                    // force invoke creation of last interval id
                }, 0);
                do {
                    clearInterval(lastIntervalId);
                }
                while (lastIntervalId--);
            }

            if (!isExit) {
                throw new Error(ExceptionsManager.ClassName() + ".ExRethrow");
            } else {
                throw new Error(ExceptionsManager.ClassName() + ".Exit");
            }
        }

        /**
         * Stop execution of current thread without thrown of exception.
         * @return {void}
         */
        public static ThrowExit() : void {
            ExceptionsManager.Throw("", new Type.Exit());
        }

        /**
         * @return {Exception} Returns the last thrown exception especially in case of, that exception
         * chain has been processed.
         */
        public static getLast() : Type.Exception {
            if (!ObjectValidator.IsSet(ExceptionsManager.exceptionList)) {
                ExceptionsManager.exceptionList = new ArrayList<Type.Exception>();
            }
            return ExceptionsManager.exceptionList.getLast();
        }

        /**
         * @return {ArrayList<Exception>} Returns list of all processed exceptions.
         */
        public static getAll() : ArrayList<Type.Exception> {
            if (!ObjectValidator.IsSet(ExceptionsManager.exceptionList)) {
                ExceptionsManager.exceptionList = new ArrayList<Type.Exception>();
            }
            return ExceptionsManager.exceptionList;
        }

        /**
         * Clear list of processed exceptions
         * @return {void}
         */
        public static Clear() : void {
            if (ObjectValidator.IsSet(ExceptionsManager.exceptionList)) {
                ExceptionsManager.exceptionList.Clear();
                ExceptionsManager.selfErrorExists = false;
                ExceptionsManager.fatalErrorExists = false;
            }
        }

        public static ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            let output : string = "";
            if (!ObjectValidator.IsEmptyOrNull(ExceptionsManager.exceptionList)) {
                if (ExceptionsManager.fatalErrorExists) {
                    output = "FATAL Error!";
                } else {
                    output = "Oops, something went wrong...";
                }

                if ($htmlTag) {
                    output = "<h1>" + output + "</h1>";
                } else {
                    output += StringUtils.NewLine(false);
                }

                let exceptionIndex : number = 0;
                const maxExceptionIndex : number = 15;
                try {
                    ExceptionsManager.exceptionList.foreach(($exception : Type.Exception) : boolean => {
                        if (exceptionIndex < maxExceptionIndex) {
                            output += $prefix + "thrown by: ";
                            if (ObjectValidator.IsEmptyOrNull($exception.Owner())) {
                                $exception.Owner("undefined");
                            }
                            if ($htmlTag) {
                                output += "<b>" + $exception.Owner() + "</b>: ";
                            } else {
                                output += $exception.Owner();
                            }
                            output += StringUtils.NewLine($htmlTag);
                            output += $exception.ToString($prefix, $htmlTag) + StringUtils.NewLine($htmlTag);
                            exceptionIndex++;
                        }
                        return exceptionIndex < maxExceptionIndex;
                    });
                } catch (ex) {
                    output += $prefix + "thrown by: ";
                    if ($htmlTag) {
                        output += "<b>" + ExceptionsManager.ClassName() + "</b>: ";
                    } else {
                        output += ExceptionsManager.ClassName();
                    }
                    output += StringUtils.NewLine($htmlTag);
                    output += ex.message + StringUtils.NewLine($htmlTag);
                }
                if (exceptionIndex >= maxExceptionIndex) {
                    output += StringUtils.NewLine($htmlTag);
                    output += "... and more " + (ExceptionsManager.exceptionList.Length() - maxExceptionIndex) +
                        " exceptions not printed";
                }

                if (Echo.getHandlerType() !== IOHandlerType.CONSOLE) {
                    let message : string = output;
                    if (LogIt.getHandlerType() === IOHandlerType.CONSOLE ||
                        LogIt.getHandlerType() === IOHandlerType.OUTPUT_FILE) {
                        message = StringUtils.Replace(message, StringUtils.NewLine(), StringUtils.NewLine(false));
                        message = StringUtils.StripTags(message);
                    }
                    LogIt.Debug(message);
                }
            } else if (Echo.getHandlerType() === IOHandlerType.CONSOLE) {
                output = "Exception list is <b>EMPTY</b>";
            }

            return output;
        }

        /**
         * @param {Error|string} $ex Exception instance, which should be validated.
         * @return {boolean} Returns true, if instance has been detected as native javascript error, otherwise false.
         */
        public static IsNativeException($ex : Error | string) : boolean {
            const message : string = ObjectValidator.IsString($ex) ? <string>$ex : (<Error>$ex).message;
            return !StringUtils.Contains(message, ExceptionsManager.ClassName() + ".ExRethrow", ExceptionsManager.ClassName() + ".Exit");
        }

        /**
         * @param {Error} $ex Handle catched exception and show or log it in correct format.
         * @return {void}
         */
        public static HandleException($ex : Error) : void {
            const selfErrorHandler : () => void = () : void => {
                if (!Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
                    LogIt.Error(ExceptionsManager.ToString("", false));
                } else {
                    const output : string = ExceptionsManager.ToString();
                    if (!ObjectValidator.IsEmptyOrNull(output)) {
                        try {
                            document.body.style.cursor = "default";
                            Echo.Print(output, true);
                        } catch (ex) {
                            const content : HTMLElement = document.createElement("span");
                            content.id = "Content";
                            document.body.appendChild(content);
                            Echo.Init("Content", true);
                            Echo.Print(output);
                        }
                    }
                }
            };

            try {
                try {
                    if (ExceptionsManager.IsNativeException($ex)) {
                        ExceptionsManager.Throw("", $ex);
                    }
                } catch (ex) {
                    // do not catch rethrow of native error
                }

                if (!ExceptionsManager.getAll().IsEmpty()) {
                    if (ExceptionsManager.selfErrorExists || !Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed()) {
                        selfErrorHandler();
                    } else {
                        const data : ArrayList<any> = new ArrayList<any>();
                        const fatal : boolean = ExceptionsManager.fatalErrorExists;
                        const exceptions : ArrayList<Type.Exception> = new ArrayList<Type.Exception>();
                        exceptions.Copy(ExceptionsManager.getAll());
                        data.Add(exceptions, HttpRequestConstants.EXCEPTIONS_LIST);
                        let resolverLink : string = "/ServerError/Exception/" + ExceptionCode.GENERAL;
                        if (fatal) {
                            resolverLink = "/ServerError/Exception/" + ExceptionCode.FATAL_ERROR;
                        }
                        if (!ObjectValidator.IsEmptyOrNull(Loader.getInstance().getHttpManager())) {
                            Loader.getInstance().getHttpManager().ReloadTo(resolverLink, data, true);
                        } else {
                            selfErrorHandler();
                        }
                    }
                }
            } catch (ex) {
                try {
                    ExceptionsManager.Throw("", ex);
                } catch (ex) {
                    // do not catch rethrow of handler self-error
                }
                selfErrorHandler();
            }
        }
    }
}
