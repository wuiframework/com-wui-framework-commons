/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Exceptions.Type {
    "use strict";
    import IException = Com.Wui.Framework.Commons.Interfaces.IException;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ExceptionCode = Com.Wui.Framework.Commons.Enums.ExceptionCode;
    import IBaseObject = Com.Wui.Framework.Commons.Interfaces.IBaseObject;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;

    /**
     * Exception class should be used as structure object describing particular type of exception.
     */
    export class Exception extends Com.Wui.Framework.Commons.Primitives.BaseArgs implements IException {

        private owner : string;
        private message : string;
        private line : number;
        private file : string;
        private code : ExceptionCode;
        private stack : string;

        /**
         * @param {string} [$message] Set exception message by constructor argument.
         */
        constructor($message? : string) {
            super();
            this.owner = "";
            if (ObjectValidator.IsSet($message)) {
                this.message = $message;
            } else {
                this.message = "";
            }
            this.code = ExceptionCode.GENERAL;
            this.stack = "";
        }

        /**
         * @param {(string|IBaseObject)} [$value] Specify who has been exception throwing.
         * @return {string} Returns exception owner.
         */
        public Owner($value? : string | IBaseObject) : string {
            if (!ObjectValidator.IsEmptyOrNull($value)) {
                ObjectValidator.IsObject($value) ? this.owner = (<any>$value).getClassName() : this.owner = <string>$value;
            }
            return this.owner;
        }

        /**
         * @param {string} [$value] Specify exception's message.
         * @return {string} Returns exception's message.
         */
        public Message($value? : string) : string {
            this.message = Property.String(this.message, $value);
            return this.message;
        }

        /**
         * @param {number} [$value] Specify line number where exception has been thrown.
         * @return {number} Returns line number where exception has been thrown.
         */
        public Line($value? : number) : number {
            this.line = Property.PositiveInteger(this.line, $value);
            return this.line;
        }

        /**
         * @param {string} [$value] Specify file name where has been exception throwing.
         * @return {string} Returns exception's file name.
         */
        public File($value? : string) : string {
            this.file = Property.String(this.file, $value);
            return this.file;
        }

        /**
         * @param {ExceptionCode} [$value] Specify exception code for better identification.
         * @return {ExceptionCode} Returns exception's code.
         */
        public Code($value? : ExceptionCode) : ExceptionCode {
            if (ObjectValidator.IsSet($value)) {
                this.code = $value;
            }
            return this.code;
        }

        /**
         * @param {string} [$value] Specify additional information about exception.
         * @return {string} Returns more info about exception.
         */
        public Stack($value? : string) : string {
            this.stack = Property.String(this.stack, $value);
            return this.stack;
        }

        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            let output : string = this.message;
            if (!$htmlTag) {
                output = StringUtils.StripTags(StringUtils.Replace(output, StringUtils.NewLine(), StringUtils.NewLine(false)));
            }

            if (!ObjectValidator.IsEmptyOrNull(this.stack)) {
                this.stack = StringUtils.Replace(StringUtils.Replace(this.stack, "</", ":"), "\n", StringUtils.NewLine($htmlTag));
            }

            if ($htmlTag) {
                output += StringUtils.NewLine();
                if (!ObjectValidator.IsEmptyOrNull(this.file)) {
                    output += $prefix + "file: " + this.file + StringUtils.NewLine();
                }
                if (!ObjectValidator.IsEmptyOrNull(this.line)) {
                    output += $prefix + "at line: " + this.line + StringUtils.NewLine();
                }
                if (!ObjectValidator.IsEmptyOrNull(this.stack)) {
                    output += Convert.ToHtmlContentBlock("", this.stack, "Stack trace");
                }
            } else {
                if (!ObjectValidator.IsEmptyOrNull(this.file)) {
                    output += ", file: " + this.file;
                }
                if (!ObjectValidator.IsEmptyOrNull(this.line)) {
                    output += ", at line: " + this.line;
                }
                if (!ObjectValidator.IsEmptyOrNull(this.stack)) {
                    output += ", stack: " + StringUtils.NewLine(false) +
                        StringUtils.StripTags(StringUtils.Replace(this.stack, StringUtils.NewLine(), StringUtils.NewLine(false)));
                }
            }

            return $prefix + output;
        }

        public toString() : string {
            return this.ToString();
        }
    }
}
