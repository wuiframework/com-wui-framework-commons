/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Exceptions.Type {
    "use strict";
    import Codes = Com.Wui.Framework.Commons.Enums.ExceptionCode;

    /**
     * ResolverFatalException class should be used in case of, that resolver processing has failed
     * in phase of instantiation or preparation.
     */
    export class ResolverFatalException extends Exception {

        constructor($message? : string) {
            super($message);
            this.Code(Codes.RESOLVER_EXCEPTION);
        }
    }
}
