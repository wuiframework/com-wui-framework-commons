/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Exceptions.Type {
    "use strict";
    import Codes = Com.Wui.Framework.Commons.Enums.ExceptionCode;

    /**
     * NullPointerException class should be used in case of accessing null object or property.
     */
    export class NullPointerException extends Exception {

        constructor($message? : string) {
            super($message);
            this.Code(Codes.NULL_POINTER);
        }
    }
}
