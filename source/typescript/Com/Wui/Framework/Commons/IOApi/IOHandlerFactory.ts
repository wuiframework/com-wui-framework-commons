/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import IOHandlerType = Com.Wui.Framework.Commons.Enums.IOHandlerType;
    import IOHandler = Com.Wui.Framework.Commons.Interfaces.IOHandler;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * IOHandlerFactory class provides factory for input and output resources handlers.
     */
    export class IOHandlerFactory extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        private static handlersList : ArrayList<IOHandler>;

        /**
         * @param {IOHandlerType} $handlerType Create or get this type of IO handler.
         * @param {string} [$owner] Set handler owner.
         * @return {IOHandler} Returns instance of IO handler based on $handlerType.
         */
        public static getHandler($handlerType : IOHandlerType, $owner? : string) : IOHandler {
            const key : string = $handlerType + $owner;

            if (!ObjectValidator.IsSet(this.handlersList)) {
                this.handlersList = new ArrayList<IOHandler>();
            }
            if (!this.handlersList.KeyExists(key)) {
                const handlerName : string = this.handlerIdGenerator($handlerType, $owner);
                switch ($handlerType) {
                case IOHandlerType.CONSOLE:
                    this.handlersList.Add(new Handlers.ConsoleHandler(handlerName), key);
                    break;
                case IOHandlerType.HTML_ELEMENT:
                    this.handlersList.Add(new Handlers.HTMLElementHandler(handlerName), key);
                    break;
                default:
                    break;
                }
            }

            return this.handlersList.getItem(key);
        }

        /**
         * @param {IOHandler} $handler Validate this type of IO handler.
         * @return {IOHandlerType} Returns type of handler instance as value of IOHandlerType enum.
         */
        public static getHandlerType($handler : IOHandler) : IOHandlerType {
            if (!ObjectValidator.IsSet($handler)) {
                return null;
            }

            if ($handler.IsTypeOf(Handlers.ConsoleHandler)) {
                return IOHandlerType.CONSOLE;
            }

            if ($handler.IsTypeOf(Handlers.HTMLElementHandler)) {
                return IOHandlerType.HTML_ELEMENT;
            }
            return null;
        }

        /**
         * Clean up content of all registered handlers.
         * @return {void}
         */
        public static DestroyAll() : void {
            if (ObjectValidator.IsSet(this.handlersList)) {
                this.handlersList.foreach(($handler : IOHandler) : void => {
                    $handler.Clear();
                });
            }
        }

        private static handlerIdGenerator($handlerType : any, $owner : string) : string {
            let handlerName : string = "";
            switch ($handlerType) {
            case IOHandlerType.CONSOLE:
            case IOHandlerType.HTML_ELEMENT:
                if (!ObjectValidator.IsEmptyOrNull($owner)) {
                    handlerName = $owner;
                } else {
                    handlerName = $handlerType;
                }
                break;
            case IOHandlerType.OUTPUT_FILE:
                handlerName = "handlers\\output.txt";
                if (!ObjectValidator.IsEmptyOrNull($owner)) {
                    handlerName = $owner + "\\" + handlerName;
                }
                break;
            case IOHandlerType.INPUT_FILE:
                handlerName = "inputhandler";
                if (!ObjectValidator.IsEmptyOrNull($owner)) {
                    handlerName = $owner + "\\" + handlerName;
                }
                break;
            default:
                break;
            }
            return handlerName;
        }
    }
}
