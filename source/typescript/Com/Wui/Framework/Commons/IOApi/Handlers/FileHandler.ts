/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi.Handlers {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import ProgressEventArgs = Com.Wui.Framework.Commons.Events.Args.ProgressEventArgs;
    import FileHandlerEventType = Com.Wui.Framework.Commons.Enums.Events.FileHandlerEventType;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    /**
     * FileHandler class provides handling of file data.
     */
    export class FileHandler extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        private readonly source : File;
        private reader : FileReader;
        private data : string;

        /**
         * @return {boolean} Returns true, if File API is supported, otherwise false.
         */
        public static IsSupported() : boolean {
            const apiSupported : boolean =
                ObjectValidator.IsSet((<any>window).File) &&
                ObjectValidator.IsSet((<any>window).FileReader) &&
                ObjectValidator.IsSet((<any>window).FileList) &&
                ObjectValidator.IsSet((<any>window).Blob);
            FileHandler.IsSupported = () : boolean => {
                return apiSupported;
            };
            return apiSupported;
        }

        /**
         * @param {File} $value Specify File instance, which should be handled.
         */
        constructor($value : File) {
            super();
            this.source = $value;
            this.data = "";

            this.reader = new FileReader();
            let progress : number = 0;
            this.reader.onerror = ($event : Event) : void => {
                try {
                    const name : string = this.source.name;
                    let message : string = "";
                    const error : any = (<any>$event.target).error;
                    switch (error.code) {
                    case error.ENCODING_ERR:
                        message = "An encoding error occurred while reading the file '" + name + "'";
                        break;
                    case error.NOT_FOUND_ERR:
                        message = "File '" + name + "' not found.";
                        break;
                    case error.NOT_READABLE_ERR:
                        message = "File '" + name + "' is not readable.";
                        break;
                    case error.SECURITY_ERR:
                        message = "Security issue with file '" + name + "'";
                        break;
                    case error.ABORT_ERR:
                        break;
                    default:
                        message = "An error occurred while reading the file '" + name + "'.";
                        break;
                    }
                    if (!ObjectValidator.IsEmptyOrNull(message)) {
                        const eventArgs : ErrorEventArgs = new ErrorEventArgs(message);
                        eventArgs.Owner(this);
                        eventArgs.NativeEventArgs($event);
                        EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), EventType.ON_ERROR, eventArgs);
                    }
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
            this.reader.onprogress = ($event : ProgressEvent) : void => {
                try {
                    const eventArgs : ProgressEventArgs = new ProgressEventArgs();
                    eventArgs.Owner(this);
                    eventArgs.NativeEventArgs($event);
                    if ($event.lengthComputable) {
                        eventArgs.RangeEnd($event.total);
                        eventArgs.CurrentValue($event.loaded);
                    } else {
                        eventArgs.CurrentValue(progress++);
                    }
                    EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), FileHandlerEventType.ON_CHANGE, eventArgs);
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
            this.reader.onabort = ($event : Event) : void => {
                try {
                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.NativeEventArgs($event);
                    eventArgs.Owner(this);
                    EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), FileHandlerEventType.ON_ABOARD, eventArgs);
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
            this.reader.onloadstart = ($event : Event) : void => {
                try {
                    const eventArgs : EventArgs = new EventArgs();
                    eventArgs.NativeEventArgs($event);
                    eventArgs.Owner(this);
                    EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), FileHandlerEventType.ON_START, eventArgs);
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
            this.reader.onload = ($event : Event) : void => {
                try {
                    this.data += (<any>$event.target).result;
                    if ((<any>$event.target).readyState === (<any>FileReader).DONE) {
                        if (!ObjectValidator.IsEmptyOrNull(this.data)) {
                            this.data = StringUtils.Split(this.data, ",")[1];
                        }
                        const eventArgs : EventArgs = new EventArgs();
                        eventArgs.Owner(this);
                        eventArgs.NativeEventArgs($event);
                        EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), FileHandlerEventType.ON_COMPLETE, eventArgs);
                    }
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            };
        }

        /**
         * @return {File} Returns source, which is handled by the FileHandler instance.
         */
        public getSource() : File {
            return this.source;
        }

        /**
         * @return {string} Returns file name, which is handled by the FileHandler instance.
         */
        public getName() : string {
            if (!ObjectValidator.IsEmptyOrNull(this.source)) {
                return this.source.name;
            }
            return "";
        }

        /**
         * @return {number} Returns file size, which is handled by the FileHandler instance
         */
        public getSize() : number {
            if (!ObjectValidator.IsEmptyOrNull(this.source)) {
                return this.source.size;
            }
            return 0;
        }

        /**
         * @return {string} Returns file type, which is handled by the FileHandler instance.
         */
        public getType() : string {
            if (!ObjectValidator.IsEmptyOrNull(this.source)) {
                return this.source.type;
            }
            return "";
        }

        /**
         * @param {boolean} [$encoded=false] Specify, if data should be in base64 format.
         * @param {boolean} [$urlSafe=false] Specify, if base64 format should respect url formatting.
         * @return {string} Returns data loaded from the file.
         */
        public Data($encoded : boolean = false, $urlSafe : boolean = false) : string {
            if (ObjectValidator.IsEmptyOrNull(this.data)) {
                return "";
            }
            if ($encoded) {
                if ($urlSafe) {
                    this.data = StringUtils.Replace(this.data, "+", "-");
                    this.data = StringUtils.Replace(this.data, "/", "_");
                    this.data = StringUtils.Replace(this.data, "=", ".");
                }
                return this.data;
            }
            return ObjectDecoder.Base64(this.data);
        }

        /**
         * @param {number} [$start] Specify start of file read.
         * @param {number} [$end] Specify end of file read.
         * @return {void}
         */
        public Load($start? : number, $end? : number) : void {
            this.data = "";
            if (!ObjectValidator.IsEmptyOrNull(this.source)) {
                if (ObjectValidator.IsEmptyOrNull($start)) {
                    this.reader.readAsDataURL(this.source);
                } else {
                    if (ObjectValidator.IsEmptyOrNull($end) || $end > this.getSize()) {
                        $end = this.getSize();
                    }
                    this.reader.readAsDataURL(this.source.slice($start, $end));
                }
            } else {
                const eventArgs : ErrorEventArgs = new ErrorEventArgs("File source is empty.");
                eventArgs.Owner(this);
                EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), EventType.ON_ERROR, eventArgs);
            }
        }

        /**
         * Stop file loading
         * @return {void}
         */
        public Stop() : void {
            this.reader.abort();
        }

        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            let output : string = "";
            output += $prefix +
                "[\"name\"] " + Convert.ObjectToString(this.getName(), StringUtils.Tab(1, $htmlTag), $htmlTag) +
                StringUtils.NewLine($htmlTag);
            output += $prefix +
                "[\"type\"] " + Convert.ObjectToString(this.getType(), StringUtils.Tab(1, $htmlTag), $htmlTag) +
                StringUtils.NewLine($htmlTag);
            output += $prefix +
                "[\"size\"] " + Convert.ObjectToString(this.getSize(), StringUtils.Tab(1, $htmlTag), $htmlTag) +
                StringUtils.NewLine($htmlTag);
            output += $prefix +
                "[\"lastModifiedTime\"] " + StringUtils.Tab(1, $htmlTag) + Convert.TimeToGMTformat((<any>this.source).lastModified) +
                StringUtils.NewLine($htmlTag);
            return output;
        }
    }
}
