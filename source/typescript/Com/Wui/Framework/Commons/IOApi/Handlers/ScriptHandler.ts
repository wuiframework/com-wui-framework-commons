/* ********************************************************************************************************* *
 *
 * Copyright (c) 2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi.Handlers {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import IOHandler = Com.Wui.Framework.Commons.Interfaces.IOHandler;
    import IOHandlerType = Com.Wui.Framework.Commons.Enums.IOHandlerType;

    /**
     * ScriptHandler class provides basic mechanism for reading of external data over script tag.
     */
    export class ScriptHandler extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        public static Data : ($value : any) => void;
        protected successHandler : any;
        protected errorHandler : any;
        private mimeType : string;
        private data : any;
        private path : string;
        private timeout : number;
        private timeoutId : number;

        constructor() {
            super();
            this.path = null;
            this.mimeType = "text/javascript";
            this.timeout = 100;
            this.successHandler = () : void => {
                // default success handler
            };
            this.errorHandler = ($error : Error) : void => {
                try {
                    const console : IOHandler = IOHandlerFactory.getHandler(IOHandlerType.CONSOLE);
                    if (!ObjectValidator.IsEmptyOrNull($error) && !ObjectValidator.IsEmptyOrNull($error.message)) {
                        console.Print($error.message);
                    } else if (!ObjectValidator.IsEmptyOrNull(this.path)) {
                        console.Print("Failed to load resource at: " + this.path);
                    } else {
                        console.Print("Failed to load resource");
                    }
                } catch (ex) {
                    console.log(ex.stack); // tslint:disable-line
                }
            };
        }

        /**
         * @param {number} [$value] Specify maximal time in milliseconds for script load.
         * @return {number} Returns maximal time allowed for script load.
         */
        public Timeout($value? : number) : number {
            return this.timeout = Property.PositiveInteger(this.timeout, $value);
        }

        /**
         * @param {string} [$value] Specify mime type of data which should be loaded.
         * Value "json" can be used as shorthand for JSON data type.
         * @return {string} Returns mime type used by current script loader instance.
         */
        public MimeType($value? : string) : string {
            if (ObjectValidator.IsEmptyOrNull($value) && StringUtils.ToLowerCase($value) === "json") {
                $value = "application/json";
            }
            return this.mimeType = Property.String(this.mimeType, $value);
        }

        /**
         * @param {string} [$value] Specify path to external data, which should be loaded.
         * @return {string} Returns path to external data if it has been specified otherwise null.
         */
        public Path($value? : string) : string {
            return this.path = Property.String(this.path, $value);
        }

        /**
         * @param {Object|string} [$value] Specify data which should be converted from string to JSON object.
         * @return {Object} Returns data in JSON format which has been loaded or converted.
         */
        public Data($value? : any) : any {
            if (ObjectValidator.IsSet($value)) {
                try {
                    this.data = $value;
                } catch (ex) {
                    this.errorHandler(ex);
                }
            }
            return this.data;
        }

        /**
         * @param {ClassName} $class Specify class which should be in role of JSONP data namespace.
         * @param {string} $method Specify method which will be used for consumption of JSONP data.
         * @return {void}
         */
        public DataHandler($class : ClassName, $method : string) : void {
            if (!ObjectValidator.IsEmptyOrNull($class)) {
                $class[$method] = ($data : any) : void => {
                    this.data = $data;
                };
            }
        }

        /**
         * @param {Function} $handler Specify handler which should be invoked on successful data load.
         * @return {void}
         */
        public SuccessHandler($handler : () => void) : void {
            if (!ObjectValidator.IsEmptyOrNull($handler)) {
                this.successHandler = $handler;
            }
        }

        /**
         * @param {Function} $handler Specify method which should be invoked in case of load errors.
         * @return {void}
         */
        public ErrorHandler($handler : ($error : Error | ErrorEvent) => void) : void {
            if (!ObjectValidator.IsEmptyOrNull($handler)) {
                this.errorHandler = $handler;
            }
        }

        /**
         * Init processing of external data based on specified configuration
         * @return {void}
         */
        public Load() : void {
            const loader : HTMLScriptElement = document.createElement("script");
            if (!ObjectValidator.IsEmptyOrNull(this.path)) {
                loader.src = this.path;
            } else if (!ObjectValidator.IsEmptyOrNull(this.data) && ObjectValidator.IsString(this.data)) {
                loader.text = this.data;
                this.data = undefined;
            } else {
                this.errorHandler(new Error("Path or body for resource must be defined before load."));
            }
            loader.type = this.mimeType;
            const cleanUp : any = () : boolean => {
                let passed : boolean = true;
                try {
                    if (!ObjectValidator.IsEmptyOrNull(this.timeoutId)) {
                        clearTimeout(this.timeoutId);
                    }
                    if (!ObjectValidator.IsEmptyOrNull(loader.parentNode)) {
                        loader.parentNode.removeChild(loader);
                    }
                } catch (ex) {
                    passed = false;
                    this.errorHandler(new Error("Failed to clean up ScriptHandler."));
                }
                return passed;
            };

            const errorBackup : any = window.onerror;
            window.onerror = () : boolean => {
                return true;
            };
            loader.onerror = ($error : ErrorEvent) : boolean => {
                window.onerror = errorBackup;
                if (cleanUp()) {
                    if (ObjectValidator.IsEmptyOrNull($error.message)) {
                        this.errorHandler(new Error("Failed to load resource data."));
                    } else {
                        this.errorHandler($error);
                    }
                }
                return true;
            };

            let loaded : boolean = false;
            const onloadHandler : () => any = () : void => {
                window.onerror = errorBackup;
                if (cleanUp()) {
                    try {
                        if (!loaded) {
                            loaded = true;
                            if (this.data !== undefined) {
                                this.successHandler();
                            } else {
                                this.errorHandler(new Error("Failed to load resource data."));
                            }
                        }
                    } catch (ex) {
                        this.errorHandler(ex);
                    }
                }
            };
            if (ObjectValidator.IsEmptyOrNull(loader.text)) {
                loader.onload = onloadHandler;
                (<any>loader).onreadystatechange = () : void => {
                    if ((<any>loader).readyState === "complete" || (<any>loader).readyState === "loaded") {
                        onloadHandler();
                    }
                };
            }

            cleanUp();
            this.timeoutId = <any>setTimeout(($error : Error) : void => {
                if (cleanUp()) {
                    this.errorHandler($error);
                }
            }, this.timeout, new Error("Timeout reached"));
            document.body.appendChild(loader);
            if (!ObjectValidator.IsEmptyOrNull(loader.text)) {
                onloadHandler();
            }
        }
    }
}
