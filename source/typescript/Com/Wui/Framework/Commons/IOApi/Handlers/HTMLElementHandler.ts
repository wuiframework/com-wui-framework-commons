/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi.Handlers {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import NewLineType = Com.Wui.Framework.Commons.Enums.NewLineType;

    /**
     * HTMLElementHandler class provides handling of HTML element.
     */
    export class HTMLElementHandler extends BaseOutputHandler {

        /**
         * @param {string} [$handlerName] Set handler name for future identification and internal use.
         */
        constructor($handlerName? : string) {
            super($handlerName);
        }

        /**
         * Provides initialization, which is specific for HTMLElement type of output handler.
         * @return {void}
         */
        public Init() : void {
            super.Init();
            const target : HTMLElement = document.getElementById(this.Name());
            if (ObjectValidator.IsEmptyOrNull(target)) {
                throw new Error("Element \"" + this.Name() + "\" has not been found.");
            }
            this.setHandler(target);
            this.setNewLineType(NewLineType.HTML);
        }

        /**
         * @param {string} $message Value which should be printed to resource handled by handler.
         * @return {void}
         */
        public Print($message : string) : void {
            this.onPrintHandler($message);
            const newContent : HTMLElement = document.createElement("span");
            newContent.setAttribute("guiType", "HtmlAppender");
            newContent.innerHTML = $message;
            this.getHandler().appendChild(newContent);
        }

        /**
         * Clean up resource handled by handler.
         * @return {void}
         */
        public Clear() : void {
            this.getHandler().innerHTML = "";
        }
    }
}
