/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.IOApi.Handlers {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    /**
     * JsonpFileReader class provides reading data in JSONP format.
     */
    export class JsonpFileReader extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        /**
         * @param {Object} $value Specify JSON data object, which should be read from the file.
         * @return {void}
         */
        public static Data : ($value : any) => void;
        private static serviceTimeout : number = 60000;

        /**
         * @param {string} $filePath Specify file path from, which should be data loaded.
         * @param {Function} $successHandler Specify asynchronous handler, which should be called,
         * when the data has been successfully loaded.
         * @param {Function} [$errorHandler] Specify error handler, which should be used instead of general error handler.
         * @return {void}
         */
        public static Load($filePath : string, $successHandler : ($data : any, $filePath? : string) => void,
                           $errorHandler? : ($eventArgs? : ErrorEvent, $filePath? : string) => void) : void {
            const application : Loader = Loader.getInstance();
            const fileReader : ScriptHandler = new ScriptHandler();
            fileReader.Timeout(JsonpFileReader.serviceTimeout);

            let filePath : string = StringUtils.Replace(StringUtils.Remove($filePath, "file://", "http://", "https://"), "//", "/");
            if (StringUtils.StartsWith($filePath, "file://")) {
                filePath = "file://" + filePath;
            } else if (StringUtils.StartsWith($filePath, "http://")) {
                filePath = "http://" + filePath;
            } else if (StringUtils.StartsWith($filePath, "https://")) {
                filePath = "https://" + filePath;
            }
            if (!application.getEnvironmentArgs().IsProductionMode() && !StringUtils.Contains(filePath, "?dummy")) {
                filePath += "?dummy=" + (new Date().getTime());
            }

            if (application.getHttpManager().getRequest().IsIdeaHost() && StringUtils.Contains(filePath, ".jsonp") && (
                !StringUtils.Contains(filePath, "http://", "https://") || StringUtils.Contains(filePath, "file://", "localhost:"))) {
                filePath = StringUtils.Replace(filePath, ".jsonp", ".js");
            }

            filePath = this.filterPath(filePath);
            fileReader.Path(filePath);

            fileReader.ErrorHandler(($error : ErrorEvent) : void => {
                try {
                    const eventArgs : EventArgs = new EventArgs();
                    if (ObjectValidator.IsSet($error)) {
                        eventArgs.NativeEventArgs($error);
                    } else {
                        eventArgs.NativeEventArgs(new ErrorEvent(""));
                    }
                    if (ObjectValidator.IsEmptyOrNull((<ErrorEvent>eventArgs.NativeEventArgs()).message) &&
                        StringUtils.Contains(filePath, ".jsonp") &&
                        (!StringUtils.Contains(filePath, "http://", "https://") ||
                            StringUtils.Contains(filePath, "file://", "localhost:"))) {
                        JsonpFileReader.Load(StringUtils.Replace(filePath, ".jsonp", ".js"), $successHandler, $errorHandler);
                    } else {
                        application.getHttpResolver().getEvents()
                            .FireEvent(JsonpFileReader.ClassName(), EventType.ON_ERROR, eventArgs, false);
                        if (!ObjectValidator.IsEmptyOrNull($errorHandler)) {
                            $errorHandler($error, $filePath);
                        } else {
                            if (!ObjectValidator.IsEmptyOrNull((<ErrorEvent>eventArgs.NativeEventArgs()))
                                && !ObjectValidator.IsEmptyOrNull((<ErrorEvent>eventArgs.NativeEventArgs()).message)) {
                                ExceptionsManager.Throw(
                                    JsonpFileReader.ClassName(), (<ErrorEvent>eventArgs.NativeEventArgs()).message);
                            } else {
                                let notFoundFilePath : string = "";
                                if (StringUtils.StartsWith(filePath, "file://") ||
                                    StringUtils.StartsWith(filePath, "http://") ||
                                    StringUtils.StartsWith(filePath, "https://")) {
                                    notFoundFilePath = filePath;
                                } else {
                                    let rootPath : string = application.getHttpManager().getRequest().getHostUrl();
                                    if (StringUtils.Contains(rootPath, ".html")) {
                                        rootPath = StringUtils.Substring(rootPath, 0, StringUtils.IndexOf(rootPath, "/", false));
                                    }
                                    notFoundFilePath = rootPath + "/" + filePath;
                                    if (StringUtils.Contains(notFoundFilePath, "?dummy=")) {
                                        notFoundFilePath =
                                            StringUtils.Substring(notFoundFilePath, 0, StringUtils.IndexOf(notFoundFilePath, "?"));
                                    }
                                }
                                application.getHttpManager().Return404NotFound(notFoundFilePath);
                            }
                        }
                    }
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            });
            application.getHttpResolver().getEvents().FireAsynchronousMethod(() : void => {
                fileReader.DataHandler(JsonpFileReader, "Data");
                fileReader.SuccessHandler(() : void => {
                    try {
                        application.getHttpResolver().getEvents().FireEvent(JsonpFileReader.ClassName(), EventType.ON_LOAD, false);
                        $successHandler(fileReader.Data(), $filePath);
                    } catch (ex) {
                        ExceptionsManager.HandleException(ex);
                    }
                });
                fileReader.Load();
            }, false);
        }

        /**
         * @param {string} $input Specify string, from which data should be loaded.
         * @param {Function} $successHandler Specify asynchronous handler, which should be called,
         * when the data has been successfully loaded.
         * @param {Function} [$errorHandler] Specify error handler, which should be used instead of general error handler.
         * @return {void}
         */
        public static LoadString($input : string, $successHandler : ($data : any) => void,
                                 $errorHandler? : ($eventArgs? : ErrorEvent) => void) : void {
            const application : Loader = Loader.getInstance();
            const scriptLoader : ScriptHandler = new ScriptHandler();
            scriptLoader.Timeout(JsonpFileReader.serviceTimeout);
            scriptLoader.ErrorHandler(($error : ErrorEvent) : void => {
                try {
                    const eventArgs : EventArgs = new EventArgs();
                    if (ObjectValidator.IsSet($error)) {
                        eventArgs.NativeEventArgs($error);
                    } else {
                        eventArgs.NativeEventArgs(new ErrorEvent(""));
                    }

                    application.getHttpResolver().getEvents()
                        .FireEvent(JsonpFileReader.ClassName(), EventType.ON_ERROR, eventArgs, false);
                    if (!ObjectValidator.IsEmptyOrNull($errorHandler)) {
                        $errorHandler($error);
                    } else {
                        if (!ObjectValidator.IsEmptyOrNull((<ErrorEvent>eventArgs.NativeEventArgs()))
                            && !ObjectValidator.IsEmptyOrNull((<ErrorEvent>eventArgs.NativeEventArgs()).message)) {
                            ExceptionsManager.Throw(JsonpFileReader.ClassName(), (<ErrorEvent>eventArgs.NativeEventArgs()).message);
                        } else {
                            ExceptionsManager.Throw(JsonpFileReader.ClassName(), "Unexpected read failure.");
                        }
                    }
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            });
            application.getHttpResolver().getEvents().FireAsynchronousMethod(() => {
                scriptLoader.DataHandler(JsonpFileReader, "Data");
                scriptLoader.Data($input);
                scriptLoader.SuccessHandler(() : void => {
                    try {
                        application.getHttpResolver().getEvents().FireEvent(JsonpFileReader.ClassName(), EventType.ON_LOAD, false);
                        $successHandler(scriptLoader.Data());
                    } catch (ex) {
                        ExceptionsManager.HandleException(ex);
                    }
                });
                scriptLoader.Load();
            }, false);
        }

        protected static filterPath($value : string) : string {
            return $value;
        }
    }
}

/**
 * Global function suitable for loading of data in JSON format.
 * @param {Object} $value Specify data in JSON format, which should be loaded.
 * @return {void}
 */
function JsonpData($value : any) : void {
    "use strict";
    Com.Wui.Framework.Commons.IOApi.Handlers.JsonpFileReader.Data($value);
}
