/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.PersistenceApi.Handlers {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import Exceptions = Com.Wui.Framework.Commons.Exceptions.Type;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import JsonpFileReader = Com.Wui.Framework.Commons.IOApi.Handlers.JsonpFileReader;

    /**
     * StorageHandler class provides API for handling of local storage.
     */
    export class StorageHandler extends BasePersistenceHandler {
        private session : ArrayList<any>;
        private size : number;
        private loadTime : number;

        /**
         * @return {boolean} Returns true, if LocalStorage API is supported, otherwise false.
         */
        public static IsSupported() : boolean {
            try {
                return !ObjectValidator.IsEmptyOrNull(localStorage);
            } catch (ex) {
                return false;
            }
        }

        /**
         * @param {string} [$sessionId] If not defined, session id will be generated.
         */
        constructor($sessionId? : string) {
            super($sessionId);
            this.size = 0;
            this.loadTime = 0;
            if (ObjectValidator.IsEmptyOrNull(this.sessionId)) {
                this.sessionId = this.getUID();
            }
        }

        /**
         * @return {number} Returns original or modified value as integer.
         */
        public getSize() : number {
            return this.size;
        }

        /**
         * @return {number} Returns time needed for load of the Storage.
         */
        public getLoadTime() : number {
            return this.loadTime;
        }

        /**
         * @param {Function} $asyncHandler Callback, which should be executed when persistence has been loaded.
         * @param {string} [$sourceFilePath] Specify path to data in expected format, which should be loaded as persistence resource.
         * @return {void}
         */
        public LoadPersistenceAsynchronously($asyncHandler : () => void, $sourceFilePath? : string) : void {
            if (ObjectValidator.IsEmptyOrNull(this.session)) {
                if (!ObjectValidator.IsEmptyOrNull($sourceFilePath)) {
                    JsonpFileReader.Load($sourceFilePath, ($data : string) : void => {
                        if (!ObjectValidator.IsEmptyOrNull($data)) {
                            ObjectDecoder.Unserialize(ObjectDecoder.Base64($data), ($object : any) : void => {
                                this.session = $object;
                                $asyncHandler();
                            });
                        } else {
                            this.initSession($asyncHandler);
                        }
                    }, () : void => {
                        this.initSession($asyncHandler);
                    });
                } else {
                    this.initSession($asyncHandler);
                }
            } else {
                $asyncHandler();
            }
        }

        /**
         * @param {string|ArrayList<any>} $name Specify variable name, which should be managed.
         * List of variable/values pairs is allowed for ability to store multiple values at once.
         * @param {any} [$value] Specify value, which should be stored.
         * @param {Function} [$asyncHandler] Enable asynchronous load of variable.
         * @return {any} Returns value associated with variable name, if persistence has been loaded and variable name exists,
         * otherwise null.
         */
        public Variable($name : string | ArrayList<any>, $value? : any, $asyncHandler? : () => void) : any {
            const async : boolean = ObjectValidator.IsSet($value) && ObjectValidator.IsSet($asyncHandler);

            this.initSession();
            let updateSession : boolean = false;
            if (!ObjectValidator.IsSet($value)) {
                if (ObjectValidator.IsArray($name)) {
                    const keys : string[] = <string[]>(<ArrayList<any>>$name).getKeys();
                    const data : string[] = (<ArrayList<any>>$name).getAll();
                    let index : number;
                    for (index = 0; index < (<ArrayList<any>>$name).Length(); index++) {
                        this.addVariable(keys[index], data[index]);
                        updateSession = true;
                    }
                }
            } else if (ObjectValidator.IsString($name) || ObjectValidator.IsInteger($name)) {
                this.addVariable(<string>$name, $value);
                updateSession = true;
            } else {
                ExceptionsManager.Throw("StorageHandler",
                    new Exceptions.IllegalArgumentException("Persistence name is supposed to be type of string or integer"));
            }

            if (updateSession) {
                this.session.Add(this.variables, "variables");
                if (this.crcEnabled()) {
                    if (async) {
                        ObjectEncoder.Serialize(this.variables, ($data : string) : void => {
                            this.session.Add(StringUtils.getCrc($data), "crc");
                            this.save($asyncHandler);
                        });
                    } else {
                        this.session.Add(StringUtils.getCrc(ObjectEncoder.Serialize(this.variables)), "crc");
                        this.save();
                    }
                } else {
                    this.save($asyncHandler);
                }
            }

            if ((ObjectValidator.IsString($name) || ObjectValidator.IsInteger($name)) &&
                this.variables.KeyExists(<string>$name)) {
                return this.variables.getItem(<string>$name);
            } else {
                return null;
            }
        }

        /**
         * @param {string} $name Key of exists handler in string format, which should be validated.
         * @return {boolean} Returns true, if key has been mapped, otherwise false.
         */
        public Exists($name : string) : boolean {
            this.initSession();
            return super.Exists($name);
        }

        /**
         * Clean up content of  registered handler.
         * @param {string|ArrayList<string>} [$name] Specify variable name or list of variables, which should be destroyed.
         * @return {void}
         */
        public Destroy($name : string | ArrayList<string>) : void {
            if (!ObjectValidator.IsEmptyOrNull($name)) {
                this.initSession();
                let updateSession : boolean = false;
                if (ObjectValidator.IsArray($name)) {
                    const removeNames : string[] = (<ArrayList<string>>$name).getAll();
                    let index : number;
                    for (index = 0; index < removeNames.length; index++) {
                        this.removeVariable(removeNames[index]);
                        updateSession = true;
                    }
                } else {
                    this.removeVariable(<string>$name);
                    updateSession = true;
                }

                if (updateSession) {
                    this.session.Add(this.variables, "variables");
                    if (this.crcEnabled()) {
                        this.session.Add(StringUtils.getCrc(ObjectEncoder.Serialize(this.variables)), "crc");
                    }
                    this.save();
                }
            }
        }

        /**
         * Clean up resource handled by handler.
         * @return {void}
         */
        public Clear() : void {
            this.remove();
            this.session = null;
            this.variables = null;
            this.initSession();
        }

        public toString() : string {
            this.initSession();
            return this.ToString();
        }

        /**
         * @param {Function} [$asyncHandler] Callback, which should be executed in case of asynchronous data load.
         * @return {string} Returns raw persistence data synchronously in case of that $asyncHandler has not been specified,
         * otherwise empty string.
         */
        public getRawData($asyncHandler? : ($data : string) => void) : string {
            if (!ObjectValidator.IsSet($asyncHandler)) {
                return ObjectEncoder.Serialize(this.session);
            } else {
                ObjectEncoder.Serialize(this.session, $asyncHandler);
                return "";
            }
        }

        private load() : string {
            const data : string = localStorage.getItem(this.sessionId);
            this.size = StringUtils.Length(data);

            return data;
        }

        private save($asyncHandler? : () => void) : void {
            const sessionId : string = this.sessionId;
            if (ObjectValidator.IsSet($asyncHandler)) {
                this.encode(this.session, ($data : string) : void => {
                    localStorage.setItem(sessionId, $data);
                    $asyncHandler();
                });
            } else {
                localStorage.setItem(sessionId, this.encode(this.session));
            }
        }

        private remove() : void {
            localStorage.removeItem(this.sessionId);
        }

        private backup() : void {
            localStorage.setItem("back" + this.sessionId, localStorage.getItem(this.sessionId));
            this.remove();
        }

        private initSession($asyncHandler? : () => void) : void {
            if (!StorageHandler.IsSupported()) {
                Loader.getInstance().getHttpManager().ReloadTo("/ServerError/Browser", null, true);
                ExceptionsManager.ThrowExit();
            }

            const setVariables : () => void = () : void => {
                if (ObjectValidator.IsEmptyOrNull(this.variables)) {
                    this.variables = this.session.getItem("variables");
                }

                if (!ObjectValidator.IsEmptyOrNull(this.getExpireTime())) {
                    if (this.session.KeyExists("ttl") && this.session.getItem("ttl") < new Date().getTime()) {
                        this.remove();
                        this.session.Add(this.getExpireTime(), "ttl");
                        this.session.Add(null, "crc");
                        this.session.Add(new ArrayList<any>(), "variables");
                        this.variables = null;
                        this.initSession($asyncHandler);
                    } else {
                        this.session.Add(this.getExpireTime(), "ttl");
                    }
                }
            };
            if (ObjectValidator.IsEmptyOrNull(this.session)) {
                const startTime : number = new Date().getTime();
                const session : string = this.load();
                if (!ObjectValidator.IsEmptyOrNull(session)) {
                    const validateCrc : ($values : string) => void = ($values : string) : void => {
                        if (this.session.getItem("crc") !== StringUtils.getCrc($values)) {
                            this.backup();
                            ExceptionsManager.Throw("StorageHandler",
                                new Exceptions.FatalError("Data has been externally changed. " +
                                    "We can not trust them, so they has been removed!"));
                        }
                    };
                    const initData : ($asyncHandler? : () => void) => void = ($asyncHandler? : () => void) : void => {
                        if (this.crcEnabled()) {
                            if (ObjectValidator.IsSet($asyncHandler)) {
                                ObjectEncoder.Serialize(this.session.getItem("variables"),
                                    ($data : string) : void => {
                                        validateCrc($data);
                                        this.loadTime = new Date().getTime() - startTime;
                                        setVariables();
                                        $asyncHandler();
                                    });
                            } else {
                                validateCrc(ObjectEncoder.Serialize(this.session.getItem("variables")));
                                this.loadTime = new Date().getTime() - startTime;
                                setVariables();
                            }
                        } else {
                            this.loadTime = new Date().getTime() - startTime;
                            setVariables();
                            if (ObjectValidator.IsSet($asyncHandler)) {
                                $asyncHandler();
                            }
                        }
                    };
                    if (ObjectValidator.IsSet($asyncHandler)) {
                        this.decode(session, ($decoded : any) : void => {
                            this.session = $decoded;
                            initData($asyncHandler);
                        });
                    } else {
                        this.session = this.decode(session);
                        initData();
                    }
                } else {
                    this.session = new ArrayList<any>();
                    this.session.Add(new ArrayList<any>(), "variables");
                    this.loadTime = new Date().getTime() - startTime;
                }
            }
            if (!ObjectValidator.IsSet($asyncHandler)) {
                setVariables();
            } else if (!ObjectValidator.IsEmptyOrNull(this.session)) {
                $asyncHandler();
            }
        }
    }
}
