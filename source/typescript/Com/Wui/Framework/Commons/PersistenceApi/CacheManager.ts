/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.PersistenceApi {
    "use strict";
    import IHttpRequestResolver = Com.Wui.Framework.Commons.Interfaces.IHttpRequestResolver;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import StorageHandler = Com.Wui.Framework.Commons.PersistenceApi.Handlers.StorageHandler;

    /**
     * CacheManager class is providing master management for all storage types.
     */
    export class CacheManager extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver implements IHttpRequestResolver {

        private static clearCookies() : void {
            if (Loader.getInstance().getHttpManager().getRequest().IsCookieEnabled()) {
                const cookies : ArrayList<string> = Loader.getInstance().getHttpManager().getRequest().getCookies();
                cookies.foreach(($value : any, $key? : any) : void => {
                    document.cookie = $key + "=; expires=" + Convert.TimeToGMTformat(Property.Time(null, "-1 year"));
                });
            }
            Loader.getInstance().getHttpManager().Refresh();
        }

        private static clearStorage() : void {
            if (StorageHandler.IsSupported()) {
                const items : ArrayList<string> = Loader.getInstance().getHttpManager().getRequest().getStorageItems();
                items.foreach(($value : any, $key? : any) : void => {
                    localStorage.removeItem($key);
                });
            }
            Loader.getInstance().getHttpManager().Refresh();
        }

        protected resolver() : void {
            Echo.Println("<h1>Local cache manager</h1>");

            Echo.Println("<h2>Domain cookies</h2>");
            Echo.Printf(this.getRequest().getCookies());
            Echo.Println("<span style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\" " +
                "onclick=\"" + this.getClassName() + ".clearCookies();\">Clear cookies</span>");

            Echo.Println("<h2>Local storage items</h2>");
            Echo.Printf(this.getRequest().getStorageItems());
            Echo.Println("<span style=\"CURSOR: pointer; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana, sans-serif;\" " +
                "onclick=\"" + this.getClassName() + ".clearStorage();\">Clear storage</span>");
        }
    }
}
