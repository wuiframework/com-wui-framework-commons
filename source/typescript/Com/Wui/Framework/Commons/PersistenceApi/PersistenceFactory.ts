/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.PersistenceApi {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import PersistenceType = Com.Wui.Framework.Commons.Enums.PersistenceType;
    import PersistenceHandlerType = Com.Wui.Framework.Commons.Enums.PersistenceHandlerType;
    import IPersistenceHandler = Com.Wui.Framework.Commons.Interfaces.IPersistenceHandler;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import StorageHandler = Com.Wui.Framework.Commons.PersistenceApi.Handlers.StorageHandler;
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;

    /**
     * PersistenceFactory class provides factory for persistence handlers.
     */
    export class PersistenceFactory extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        private static sessionIds : ArrayList<string>;
        private static persistencesList : ArrayList<IPersistenceHandler>;
        private static persistenceHandlerType : PersistenceHandlerType;
        private static httpSessionId : string;

        /**
         * @param {(PersistenceType|string)} $persistenceType Create or get this type of persistence.
         * @param {string} [$owner] Set persistence owner.
         * @return {IPersistenceHandler} Returns instance of Persistence handler based on $persistenceType.
         */
        public static getPersistence($persistenceType : string | PersistenceType, $owner? : string) : IPersistenceHandler {
            const key : string = StringUtils.getSha1($persistenceType + $owner);
            let sessionId : string = "";

            if (!ObjectValidator.IsSet(PersistenceFactory.sessionIds)) {
                PersistenceFactory.sessionIds = new ArrayList<string>();
            }
            if (!ObjectValidator.IsSet(PersistenceFactory.persistencesList)) {
                PersistenceFactory.persistencesList = new ArrayList<IPersistenceHandler>();
            }
            if (!PersistenceFactory.sessionIds.KeyExists(key)) {
                sessionId = PersistenceFactory.sessionIdGenerator($persistenceType, $owner);
                if (!ObjectValidator.IsSet(PersistenceFactory.persistenceHandlerType)) {
                    if (StorageHandler.IsSupported()) {
                        PersistenceFactory.persistenceHandlerType = PersistenceHandlerType.STORAGE;
                    } else {
                        PersistenceFactory.persistenceHandlerType = PersistenceHandlerType.COOKIES;
                    }
                }
                PersistenceFactory.sessionIds.Add(sessionId, key);
            }
            sessionId = PersistenceFactory.sessionIds.getItem(key);

            return PersistenceFactory.getPersistenceById(sessionId);
        }

        /**
         * @param {string} $sessionId Create or get this type of persistence.
         * @return {IPersistenceHandler} Returns instance of Persistence handler based on $sessionId,
         * if handler has been found, otherwise null.
         */
        public static getPersistenceById($sessionId : string) : IPersistenceHandler {
            if (!ObjectValidator.IsEmptyOrNull($sessionId) && !PersistenceFactory.persistencesList.KeyExists($sessionId)) {
                /* tslint:disable: indent */
                switch (PersistenceFactory.persistenceHandlerType) {
                case PersistenceHandlerType.COOKIES:
                    PersistenceFactory.persistencesList.Add(new Handlers.CookiesHandler($sessionId), $sessionId);
                    break;
                case PersistenceHandlerType.STORAGE:
                    PersistenceFactory.persistencesList.Add(new Handlers.StorageHandler($sessionId), $sessionId);
                    break;
                default:
                    break;
                }
                /* tslint:enable */
            }

            if (PersistenceFactory.persistencesList.KeyExists($sessionId)) {
                return PersistenceFactory.persistencesList.getItem($sessionId);
            }

            return null;
        }

        /**
         * @param {IPersistenceHandler} $handler Validate this type of Persistence handler.
         * @return {PersistenceHandlerType} Returns type of handler instance as value of PersistenceHandlerType enum.
         */
        public static getPersistenceType($handler : IPersistenceHandler) : PersistenceHandlerType {
            if ($handler.IsTypeOf(Handlers.CookiesHandler)) {
                return PersistenceHandlerType.COOKIES;
            } else if ($handler.IsTypeOf(Handlers.StorageHandler)) {
                return PersistenceHandlerType.STORAGE;
            }

            return null;
        }

        /**
         * @param {PersistenceHandlerType} $persistenceHandlerType Force type of handler, which should be generated
         * by the factory.
         * @return {void}
         */
        public static setPersistenceHandler($persistenceHandlerType : PersistenceHandlerType) : void {
            PersistenceFactory.persistenceHandlerType = $persistenceHandlerType;
        }

        /**
         * @return {string} Returns session id, which should be used as part of http argument in case of,
         * that session id can not be provided by cookies.
         */
        public static getHttpSessionId() : string {
            let items : ArrayList<string>;
            const request : HttpRequestParser = Loader.getInstance().getHttpManager().getRequest();
            if (!request.IsWuiJre() && StorageHandler.IsSupported()) {
                items = request.getStorageItems();
            } else {
                items = request.getCookies();
            }
            if (!items.KeyExists("sessionid")) {
                const urlArgs : ArrayList<string> = request.getUrlArgs();
                if (urlArgs.KeyExists("sessionid") && !ObjectValidator.IsEmptyOrNull(urlArgs.getItem("sessionid"))) {
                    PersistenceFactory.httpSessionId = urlArgs.getItem("sessionid");
                } else {
                    PersistenceFactory.httpSessionId = PersistenceFactory.UID();
                }
            } else {
                PersistenceFactory.httpSessionId = items.getItem("sessionid");
            }
            return PersistenceFactory.httpSessionId;
        }

        /**
         * Clean up content of all registered persistence resources.
         * @return {void}
         */
        public static DestroyAll() : void {
            if (ObjectValidator.IsSet(PersistenceFactory.persistencesList)) {
                PersistenceFactory.persistencesList.foreach(($handler : IPersistenceHandler) : void => {
                    $handler.Clear();
                });
            }
        }

        protected static sessionIdGenerator($persistenceType : any, $owner : string) : string {
            let sessionId : string = "";
            const request : HttpRequestParser = Loader.getInstance().getHttpManager().getRequest();
            if ($persistenceType === PersistenceType.CLIENT_IP) {
                sessionId = request.getClientIP();
            } else if ($persistenceType === PersistenceType.BROWSER) {
                let items : ArrayList<string>;
                if (!request.IsWuiJre() && StorageHandler.IsSupported()) {
                    items = request.getStorageItems();
                } else {
                    items = request.getCookies();
                }
                if (!items.KeyExists("sessionid")) {
                    sessionId = this.getHttpSessionId();
                    if (!request.IsWuiJre() && StorageHandler.IsSupported()) {
                        localStorage.setItem("sessionid", sessionId);
                    } else {
                        document.cookie = "sessionid=" + sessionId + ";";
                    }
                } else {
                    sessionId = items.getItem("sessionid");
                }
            } else {
                sessionId = $persistenceType;
            }

            if (!ObjectValidator.IsEmptyOrNull($owner)) {
                sessionId = $owner + "\\" + sessionId;
            }
            return StringUtils.getSha1(sessionId);
        }
    }
}
