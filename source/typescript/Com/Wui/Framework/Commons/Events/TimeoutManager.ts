/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ITimeoutHandler = Com.Wui.Framework.Commons.Interfaces.ITimeoutHandler;
    import IEventsManager = Com.Wui.Framework.Commons.Interfaces.IEventsManager;

    /**
     * TimeoutManager class provides handling of setTimeout methods without window timeout errors.
     */
    export class TimeoutManager extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private register : ITimeoutHandler[];
        private index : number;
        private size : number;
        private readonly id : string;

        constructor() {
            super();
            this.register = [];
            this.index = -1;
            this.size = 0;
            this.id = this.getUID();
        }

        /**
         * @return {string} Returns manager's thread identification hash suitable for hook at manager's events.
         */
        public getId() : string {
            return this.id;
        }

        /**
         * @return {number} Returns count of the registered handlers.
         */
        public Length() : number {
            return this.size;
        }

        /**
         * @param {ITimeoutHandler} $handler Register timeout handler.
         * @return {void}
         */
        public Add($handler : ITimeoutHandler) : void {
            this.register[this.size] = $handler;
            this.size++;
        }

        /**
         * @return {string} Returns manager's thread identification hash suitable for hook at manager's events.
         */
        public Execute() : string {
            EventsManager.getInstanceSingleton().FireEvent(this.getClassName(), EventType.ON_START);
            EventsManager.getInstanceSingleton().FireEvent(this.id, EventType.ON_START);
            this.getNextFunction();
            return this.id;
        }

        private getNextFunction() : void {
            const events : IEventsManager = EventsManager.getInstanceSingleton();
            setTimeout(() : void => {
                try {
                    const nextIndex : number = this.index++;
                    if (nextIndex < this.size) {
                        this.getNextFunction();
                        if (ObjectValidator.IsSet(this.register[nextIndex])) {
                            this.register[nextIndex](nextIndex);
                        }
                    } else {
                        this.register = [];
                        events.FireEvent(this.getClassName(), EventType.ON_COMPLETE);
                        events.FireEvent(this.getId(), EventType.ON_COMPLETE);
                    }
                } catch (ex) {
                    ExceptionsManager.HandleException(ex);
                }
            }, 0);
        }
    }
}
