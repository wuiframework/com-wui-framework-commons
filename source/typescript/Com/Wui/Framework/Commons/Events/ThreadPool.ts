/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events {
    "use strict";
    import IEventsHandler = Com.Wui.Framework.Commons.Interfaces.IEventsHandler;
    import IEventArgs = Com.Wui.Framework.Commons.Interfaces.IEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * ThreadPool class provides asynchronous handling of custom events group.
     */
    export class ThreadPool extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private static events : EventsManager = new EventsManager();
        private static threadId : number;

        /**
         * @param {string} $owner Specify thread owner.
         * @param {string} $type Specify type of thread.
         * @return {boolean} Returns true, if specified thread is running, otherwise false.
         */
        public static IsRunning($owner : string, $type : string) : boolean {
            return ThreadPool.events.Exists($owner, $type);
        }

        /**
         * @param {string} $owner Specify thread owner.
         * @param {string} $type Specify type of thread.
         * @param {IEventsHandler} [$handler] Function suitable for handling of the thread.
         * @param {IEventArgs} [$args] Specify initial thread args.
         * @return {void}
         */
        public static AddThread($owner : string, $type : string, $handler? : IEventsHandler,
                                $args? : IEventArgs) : void {
            ThreadPool.events.setEvent($owner, $type, $handler, $args);
        }

        /**
         * @param {string} $owner Specify thread owner.
         * @param {string} $type Specify type of thread.
         * @param {IEventArgs} $args Specify current thread args.
         * @return {void}
         */
        public static setThreadArgs($owner : string, $type : string, $args : IEventArgs) : void {
            ThreadPool.events.setEventArgs($owner, $type, $args);
        }

        /**
         * @param {string} $owner Specify thread owner.
         * @param {string} $type Specify type of thread, which should be removed.
         * @return {void}
         */
        public static RemoveThread($owner : string, $type : string) : void {
            ThreadPool.events.Clear($owner, $type);
        }

        /**
         * Fire execution of all registered threads.
         * @return {void}
         */
        public static Execute() : void {
            if (!ThreadPool.events.getAll().IsEmpty()) {
                ThreadPool.threadId = ThreadPool.events.FireAsynchronousMethod(() : void => {
                    const owners : any[] = ThreadPool.events.getAll().getKeys();
                    let ownerIndex : number;
                    let typeIndex : number;
                    for (ownerIndex = 0; ownerIndex < owners.length; ownerIndex++) {
                        const types : any[] = ThreadPool.events.getAll().getItem(owners[ownerIndex]).getKeys();
                        for (typeIndex = 0; typeIndex < types.length; typeIndex++) {
                            ThreadPool.events.FireEvent(owners[ownerIndex], types[typeIndex]);
                        }
                    }
                    ThreadPool.Execute();
                }, 5);
            }
        }

        /**
         * Stop execution of all registered threads.
         * @return {void}
         */
        public static Clear() : void {
            if (!ObjectValidator.IsEmptyOrNull(ThreadPool.threadId)) {
                clearTimeout(ThreadPool.threadId);
            }
            ThreadPool.events.Clear();
        }
    }
}
