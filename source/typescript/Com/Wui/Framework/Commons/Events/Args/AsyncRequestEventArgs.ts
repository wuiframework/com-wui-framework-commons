/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events.Args {
    "use strict";
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    /**
     * AsyncRequestEventArgs class provides args connected with asynchronous request events.
     */
    export class AsyncRequestEventArgs extends HttpRequestEventArgs {
        private result : any;

        /**
         * @param {string} $url Set url, which should be asynchronously resolved.
         * @param {ArrayList<any>} [$POST] Specify data, which should be passed to request resolver.
         */
        constructor($url : string, $POST? : ArrayList<any>) {
            super($url, $POST);
        }

        /**
         * @param {any} $value Set result of asynchronous request.
         * @return {any} Returns result of asynchronous request.
         */
        public Result($value? : any) : any {
            if (ObjectValidator.IsSet($value)) {
                this.result = $value;
            }
            return this.result;
        }
    }
}
