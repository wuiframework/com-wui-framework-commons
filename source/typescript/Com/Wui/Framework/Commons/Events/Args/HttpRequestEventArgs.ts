/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events.Args {
    "use strict";
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import Property = Com.Wui.Framework.Commons.Utils.Property;

    /**
     * HttpRequestEventArgs class provides args connected with http request events.
     */
    export class HttpRequestEventArgs extends EventArgs {
        private readonly url : string;
        private getData : ArrayList<string>;
        private postData : ArrayList<any>;
        private status : HttpStatusType;

        /**
         * @param {string} $url Set url, which should be synchronously resolved.
         * @param {ArrayList<any>} [$POST] Specify data, which should be passed to request resolver.
         */
        constructor($url : string, $POST? : ArrayList<any>) {
            super();
            this.url = StringUtils.Remove($url, "/#", "#");
            this.getData = new ArrayList<string>();
            this.postData = new ArrayList<any>();
            this.POST($POST);
            this.status = HttpStatusType.SUCCESS;
        }

        /**
         * @return {string} Returns url of resolver, which should be or has been execute.
         */
        public Url() : string {
            return this.url;
        }

        /**
         * @param {HttpStatusType} $value Set async load result status code.
         * @return {HttpStatusType} Returns status code of resolver result.
         */
        public Status($value? : HttpStatusType) : HttpStatusType {
            return this.status = Property.Integer(this.status, $value);
        }

        /**
         * @param {ArrayList<string>} [$GET] Set http get data.
         * @return {ArrayList<string>} Returns http get data provided by requester.
         */
        public GET($GET? : ArrayList<string>) : ArrayList<string> {
            if (!ObjectValidator.IsEmptyOrNull($GET)) {
                this.getData = $GET;
            }
            return this.getData;
        }

        /**
         * @param {ArrayList<any>} [$POST] Set post data.
         * @return {ArrayList<any>} Returns post data provided by requester.
         */
        public POST($POST? : ArrayList<any>) : ArrayList<any> {
            if (!ObjectValidator.IsEmptyOrNull($POST)) {
                this.postData = $POST;
            }
            return this.postData;
        }

        public ToString($prefix : string = "", $htmlTag : boolean = true) : string {
            let output : string = "";
            output += $prefix + this.getClassName() + StringUtils.NewLine($htmlTag);
            output += $prefix + "[\"Url\"] " + this.Url() + StringUtils.NewLine($htmlTag);
            output += $prefix + "[\"Owner\"] ";
            if (ObjectValidator.IsObject(this.Owner())) {
                output += this.Owner().getClassName() + StringUtils.NewLine($htmlTag);
            } else {
                output += Convert.ObjectToString(this.Owner()) + StringUtils.NewLine($htmlTag);
            }
            output += $prefix + "[\"Status\"] " + this.Status() + StringUtils.NewLine($htmlTag);
            output += $prefix + "[\"GET\"] " + StringUtils.NewLine($htmlTag) +
                Convert.ObjectToString(this.getData, StringUtils.Tab(1, $htmlTag), $htmlTag) + StringUtils.NewLine($htmlTag);
            output += $prefix + "[\"POST\"] " + StringUtils.NewLine($htmlTag) +
                Convert.ObjectToString(this.postData, StringUtils.Tab(1, $htmlTag), $htmlTag);
            return output;
        }
    }
}
