/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Events.Args {
    "use strict";
    import Property = Com.Wui.Framework.Commons.Utils.Property;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IEventArgs = Com.Wui.Framework.Commons.Interfaces.IEventArgs;

    /**
     * EventArgs class provides basic events structure.
     */
    export class EventArgs extends Com.Wui.Framework.Commons.Primitives.BaseArgs implements IEventArgs {
        private owner : any;
        private type : string;
        private nativeEventArgs : any;

        constructor() {
            super();
            this.owner = this;
        }

        /**
         * @param {any} [$owner] Specified object, which owns current args.
         * @return {any} Returns current event args owner object.
         */
        public Owner($owner? : any) : any {
            if (!ObjectValidator.IsEmptyOrNull($owner)) {
                this.owner = $owner;
            }
            return this.owner;
        }

        /**
         * @param {string} [$value] If specified, set type of event, which handles current args.
         * @return {string} Returns current event args type of.
         */
        public Type($value? : string) : string {
            return this.type = Property.String(this.type, $value);
        }

        /**
         * @param {Event} [$value] If specified, set event args of native event.
         * @return {Event} Returns native event args provided by native event handler.
         */
        public NativeEventArgs($value? : Event) : Event {
            if (ObjectValidator.IsSet($value)) {
                this.nativeEventArgs = $value;
            }
            return this.nativeEventArgs;
        }

        /**
         * Skip execution of native event
         * @return {void}
         */
        public PreventDefault() : void {
            if (ObjectValidator.IsSet(this.nativeEventArgs)) {
                if (ObjectValidator.IsSet(this.nativeEventArgs.preventDefault)) {
                    this.nativeEventArgs.preventDefault();
                } else {
                    (<any>this.nativeEventArgs).returnValue = false;
                }
            }
        }

        /**
         * Stop event bubbling
         * @return {void}
         */
        public StopAllPropagation() : void {
            this.PreventDefault();
            if (ObjectValidator.IsSet(this.nativeEventArgs)) {
                if (ObjectValidator.IsSet(this.nativeEventArgs.stopPropagation)) {
                    this.nativeEventArgs.stopPropagation();
                    this.nativeEventArgs.stopImmediatePropagation();
                } else {
                    (<any>this.nativeEventArgs).cancelBubble = true;
                }
            }
        }

        public toString() : string {
            return this.ToString();
        }

        protected toStringFilter($methodName : string) : boolean {
            if ($methodName === "Owner") {
                return ObjectValidator.IsString(this.owner);
            }
            return super.toStringFilter($methodName);
        }
    }
}
