/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import LogLevel = Com.Wui.Framework.Commons.Enums.LogLevel;
    import HttpResolver = Com.Wui.Framework.Commons.HttpProcessor.HttpResolver;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import HttpManager = Com.Wui.Framework.Commons.HttpProcessor.HttpManager;
    import ThreadPool = Com.Wui.Framework.Commons.Events.ThreadPool;
    import Counters = Com.Wui.Framework.Commons.Utils.Counters;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import IProject = Com.Wui.Framework.Commons.Interfaces.IProject;

    /**
     * Loader class provides handling of application content singleton.
     */
    export class Loader extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        private static singleton : Loader;
        private environment : EnvironmentArgs;
        private resolver : HttpResolver;

        /**
         * Create new application instance from this entry point.
         * @param {string} $appConfig Specify project configuration in JSON format.
         */
        public static Load($appConfig : string | IProject) : void {
            try {
                Loader.singleton = new this();
                Loader.singleton.Process($appConfig);
            } catch (ex) {
                LogIt.Error("Application start error.", ex);
            }
        }

        /**
         * @return {Loader} Returns application singleton
         */
        public static getInstance() : Loader {
            return Loader.singleton;
        }

        /**
         * Start application process
         * @param {string} $appConfig Specify project configuration in JSON format.
         * @return {void}
         */
        public Process($appConfig : string | IProject) : void {
            this.environment = this.initEnvironment();
            this.environment.Load($appConfig, () : void => {
                delete (<any>Reflection).singleton;
                Reflection.setInstanceNamespaces.apply(Reflection, this.environment.getNamespaces());
                Reflection.getInstance();
                if (this.environment.IsProductionMode()) {
                    LogIt.setLevel(LogLevel.ERROR);
                }
                Counters.Clear();
                ExceptionsManager.Clear();
                ThreadPool.Clear();
                this.resolver = this.initResolver();
                this.resolver.getEvents().Subscribe();
                if (this.environment.HtmlOutputAllowed()) {
                    this.resolver.ResolveRequest();
                }
            });
        }

        /**
         * @return {EnvironmentArgs} Returns singleton of EnvironmentArgs used by current application.
         */
        public getEnvironmentArgs() : EnvironmentArgs {
            return this.environment;
        }

        /**
         * @return {HttpResolver} Returns singleton of HttpResolver used by current application.
         */
        public getHttpResolver() : HttpResolver {
            return this.resolver;
        }

        /**
         * @return {HttpResolver} Returns singleton of HttpManager used by current application.
         */
        public getHttpManager() : HttpManager {
            return this.resolver.getManager();
        }

        protected initResolver() : HttpResolver {
            return new HttpResolver(this.environment.getProjectName());
        }

        protected initEnvironment() : EnvironmentArgs {
            return new EnvironmentArgs();
        }
    }
}
