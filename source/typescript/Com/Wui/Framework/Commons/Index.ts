/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons {
    "use strict";
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;

    /**
     * Index request resolver class provides handling of web index page.
     */
    export class Index extends Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver {

        protected resolver() : void {
            /* dev:start */
            Echo.Print("<H3>Runtime tests</H3>");
            Echo.Print("<a href=\"#" + this.createLink("/web/HttpRequestParserTest") + "\">HttpRequestParserTest</a>");
            Echo.Println("<a href=\"#" + this.createLink("/web/HttpManagerTest") + "\">HttpManagerTest</a>");
            Echo.Println("<a href=\"#" + this.createLink("/web/PersistenceApiTest") + "\">PersistenceApiTest</a>");
            Echo.Println("<a href=\"#" + this.createLink("/web/EventsManagerTest") + "\">EventsManagerTest</a>");
            Echo.Println("<a href=\"#" + this.createLink("/web/ExceptionsManagerTest") + "\">ExceptionsManagerTest</a>");
            Echo.Println("<a href=\"#" + this.createLink("/web/TimeoutTest") + "\">TimeoutTest</a>");
            Echo.Println("<a href=\"#" + this.createLink("/web/ChromiumConnectorTest") + "\">ChromiumConnectorTest</a>");
            Echo.Println("<a href=\"#" + this.createLink("/web/CoverageTest") + "\">CoverageTest</a>");
            Echo.Println("<a href=\"#" + this.createLink("/web/AsyncRuntimeTest") + "\">AsyncRuntimeTest</a>");
            Echo.Println("<a href=\"#" + this.createLink("/web/JsonpFileReaderTest") + "\">JsonpFileReaderTest</a>");
            Echo.Println("<a href=\"#" + this.createLink("/web/JxbrowserBridgeClientTest") + "\">JxbrowserBridgeClientTest</a>");
            /* dev:end */
            Echo.Print("<H3>About resolvers</H3>");
            Echo.Print("<a href=\"#" + this.createLink("/about/Cache") + "\">/about/Cache</a>");
            Echo.Println("<a href=\"#" + this.createLink("/about/Env") + "\">/about/Env</a>");
        }
    }
}
