/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    import NewLineType = Com.Wui.Framework.Commons.Enums.NewLineType;

    export interface IOHandler extends IBaseObject {

        /**
         * Provides initialization, which is specific for each type of output handler.
         * @return {void}
         */
        Init() : void;

        /**
         * @return {string} Returns handler's name used as unique identifier for IO factory.
         */
        Name() : string;

        /**
         * @return {string} Returns string representation of encoding specific for each type of handler.
         */
        Encoding() : string;

        /**
         * @return {NewLineType} Returns type of line ending specific for each type of handler.
         */
        NewLineType() : NewLineType;

        /**
         * @param {string} $message Value which should be printed to resource handled by handler.
         * @return {void}
         */
        Print($message : string) : void;

        /**
         * @param {Function} $handler Callback, which should be called in time on message print.
         * @return {void}
         */
        setOnPrint($handler : ($message : string) => void) : void;

        /**
         * Clean up resource handled by handler.
         * @return {void}
         */
        Clear() : void;
    }
}
