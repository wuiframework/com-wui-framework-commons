/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import IWebServiceClientEvents = Com.Wui.Framework.Commons.Interfaces.Events.IWebServiceClientEvents;

    export interface IWebServiceClient extends IBaseObject {

        /**
         * @return {number} Returns client's id, which can be used for events handling.
         */
        getId() : number;

        /**
         * @return {string} Returns target service url.
         */
        getServerUrl() : string;

        /**
         * @param {function} $handler Specify async handler for handling of server path data.
         * @return {void}
         */
        getServerPath($handler : ($path : string) => void) : void;

        /**
         * @return {IWebServiceClientEvents} Returns events manager interface.
         */
        getEvents() : IWebServiceClientEvents;

        /**
         * Start Communication with target service
         * @return {boolean} Returns true, if communication has been started, otherwise false.
         */
        StartCommunication() : void;

        /**
         * Stop Communication with target service
         * @return {boolean} Returns true, if communication has been stopped, otherwise false.
         */
        StopCommunication() : void;

        /**
         * @return {boolean} Returns true, if communication with target service is live, otherwise false.
         */
        CommunicationIsRunning() : boolean;

        /**
         * @param {string} $data Specify request command.
         * @param {IWebServiceResponseHandler} [$handler] Specify response handler.
         * @return {void}
         */
        Send($data : any, $handler? : IWebServiceResponseHandler) : void;

        /**
         * @param {Function} $formatter Specify request formatter function
         * @return {void}
         */
        setRequestFormatter($formatter : ($data : IWebServiceRequestFormatterData) => void) : void;

        /**
         * @param {Function} $formatter Specify response formatter function
         * @return {void}
         */
        setResponseFormatter($formatter : ($data : any, $owner : IWebServiceClient,
                                           $onSuccess : ($value : any, $key? : number) => void,
                                           $onError : ($message : string | Error | Exception) => void) => void) : void;
    }

    /**
     * @param {any} $response Specify response function
     * @return {void}
     */
    export type IWebServiceResponseHandler = ($response? : any) => void;

    export abstract class IWebServiceRequestFormatterData {
        public value : any;
        public key : number;
    }
}
