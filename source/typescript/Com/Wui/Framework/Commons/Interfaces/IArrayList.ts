/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";

    export interface IArrayList<T> extends IBaseObject {
        Add($value : T, $key? : string | number) : void;

        getKeys() : Array<string | number>;

        getKey($value : T) : string | number;

        KeyExists($value : string | number) : boolean;

        Contains($value : T) : boolean;

        IndexOf($value : T) : number;

        getAll() : T[];

        getItem($key : string | number) : T;

        getFirst() : T;

        getLast() : T;

        Equal($compareWith : Com.Wui.Framework.Commons.Primitives.ArrayList<any>) : boolean;

        Length() : number;

        IsEmpty() : boolean;

        Clear() : void;

        Copy($copyFrom : Com.Wui.Framework.Commons.Primitives.ArrayList<T>, $count? : number,
             $clearBefore? : boolean) : void;

        RemoveAt($index : number) : void;

        RemoveLast() : void;

        SortByKeyUp() : void;

        SortByKeyDown() : void;

        foreach($handler : ($value : T, $key? : string | number) => void | boolean) : void;

        ToArray() : T[];
    }
}
