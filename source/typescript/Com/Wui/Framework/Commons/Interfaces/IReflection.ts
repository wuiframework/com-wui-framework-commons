/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";

    export interface IReflection extends IBaseObject {
        getAllClasses() : string[];

        Exists($className : string) : boolean;

        getClass($className : string) : any;

        IsInstanceOf($instance : IBaseObject, $className : string | ClassName) : boolean;

        ClassHasInterface($className : string | ClassName, $interface : string | Interface) : boolean;

        IsMemberOf($instance : any, $className? : string | ClassName) : boolean;

        Implements($instance : any, $className? : Interface | string) : boolean;
    }
}
