/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    import FileSystemItemType = Com.Wui.Framework.Commons.Enums.FileSystemItemType;

    export abstract class IFileSystemItemProtocol {
        public name : string;
        public type : string | FileSystemItemType;
        public value : string;
        public attributes : Array<string | FileSystemItemType>;
        public map : IFileSystemItemProtocol[] | string;
    }

    export abstract class IBaseFileSystemItem {
        public name : string;
        public type : string | FileSystemItemType;
    }

    export abstract class IFileSystemFileProtocol extends IBaseFileSystemItem {
        public attributes : Array<string | FileSystemItemType>;
    }

    export abstract class IFileSystemDirectoryProtocol extends IFileSystemFileProtocol {
        public map : Array<IFileSystemDirectoryProtocol | IFileSystemFileProtocol> | string;
    }

    export abstract class IFileSystemDriveProtocol extends IBaseFileSystemItem {
        public value : string;
        public map : Array<IFileSystemDirectoryProtocol | IFileSystemFileProtocol> | string;
    }
}
