/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";
    import HttpResolversCollection = Com.Wui.Framework.Commons.HttpProcessor.HttpResolversCollection;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;

    export interface IHttpManager extends IBaseObject {
        RegisterResolver($httpRequest : string, $resolver : any, $ignoreCase? : boolean) : void;

        OverrideResolver($httpRequest : string, $resolver : any, $ignoreCase? : boolean) : void;

        getResolverClassName($requireLink : string) : string;

        getResolverParameters() : ArrayList<string>;

        getResolversCollection() : HttpResolversCollection;
    }
}
