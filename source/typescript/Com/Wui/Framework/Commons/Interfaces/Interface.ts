/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";

    /**
     * Interface class is abstract class providing declaration of methods accessible from implemented interfaces.
     */
    export class Interface {
        /**
         * @param {string[]} $declarations Specify names of methods and properties, which should be declared by the interface.
         * @param {Interface} [$parent] Specify parent interface, which should be extended.
         * @return {Interface} Returns class representing required interface.
         */
        public static getInstance($declarations : string[], $parent? : Interface) : Interface;

        public static getInstance($declarations : string[], $parent : any = null) : Interface {
            /* istanbul ignore next : interface will never have an instance */
            /* tslint:disable: only-arrow-functions */
            const child : object = function () : void {
                // default constructor with empty body
            };
            /* tslint:enable */
            /* istanbul ignore if : interface will never have an instance */
            if ($parent === null) {
                /* tslint:disable: only-arrow-functions */
                $parent = function () : void {
                    // default constructor with empty body
                };
                /* tslint:enable */
                $parent.classNamespace = "";
                $parent.className = "";
                $parent.ClassName = function () : string {
                    return this.classNamespace + "." + this.className;
                };
                $parent.NamespaceName = function () : string {
                    return this.classNamespace;
                };
                $parent.ClassNameWithoutNamespace = function () : string {
                    return this.className;
                };
                $parent.ToString = $parent.ClassName;
                $parent.toString = $parent.ToString;
            }
            let property : any;
            for (property in $parent) {
                /* istanbul ignore else */
                if (!child.hasOwnProperty(property)) {
                    child[property] = $parent[property];
                }
            }
            let index : number;
            for (index = 0; index < $declarations.length; index++) {
                if (!child.hasOwnProperty($declarations[index])) {
                    child[$declarations[index]] = null;
                }
            }
            return <Interface>child;
        }

        /**
         * @return {string} Returns class name with namespace in string format.
         */
        public ClassName() : string {
            return null;
        }

        /**
         * @return {string} Returns namespace of the class in string format.
         */
        public NamespaceName() : string {
            return null;
        }

        /**
         * @return {string} Returns only the class name without namespace in string format.
         */
        public ClassNameWithoutNamespace() : string {
            return null;
        }

        public ToString() : string {
            return null;
        }

        public toString() : string {
            return null;
        }
    }
}

/* tslint:disable: interface-name no-empty-interface */
interface ClassName {
}

/* tslint:enable */
