/* ********************************************************************************************************* *
 *
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";

    export interface ICefQuery extends IBaseObject {

        /**
         * @param  {ICefQueryMessage} $message Data for cefQuery
         * @return {number} Returns cefQuery id.
         */
        ($message : ICefQueryMessage) : number;
    }

    export abstract class ICefQueryMessage {
        public request : string;
        public persistent : boolean;
        public onSuccess : ($response : string) => void;
        public onFailure : ($code : number, $message? : string) => void;
    }
}
