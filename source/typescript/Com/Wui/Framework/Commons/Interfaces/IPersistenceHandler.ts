/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";

    export interface IPersistenceHandler extends IBaseObject {
        getSessionId() : string;

        getSize() : number;

        getLoadTime() : number;

        getRawData($asyncHandler? : ($data : string) => void) : string;

        LoadPersistenceAsynchronously($asyncHandler : () => void, $sourceFilePath? : string) : void;

        DisableCRC() : void;

        ExpireTime($value? : string | number) : number;

        Variable($name : string | IArrayList<any>, $value? : any, $asyncHandler? : () => void) : any;

        Exists($name : string) : boolean;

        Destroy($name : string | IArrayList<string>) : void;

        Clear() : void;
    }
}
