/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces {
    "use strict";

    export interface IBaseObject {
        getClassName() : string;

        getNamespaceName() : string;

        getClassNameWithoutNamespace() : string;

        getProperties() : string[];

        getMethods() : string[];

        IsTypeOf($className : ClassName | string) : boolean;

        IsMemberOf($className : ClassName | string) : boolean;

        Implements($className : Interface | string) : boolean;

        ToString($prefix? : string, $htmlTag? : boolean) : string;

        toString() : string;

        SerializationData() : object;

        getHash() : number;

        getUID() : string;
    }
}
