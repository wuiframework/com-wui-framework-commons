/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Interfaces.Events {
    "use strict";
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import EventArgs = Com.Wui.Framework.Commons.Events.Args.EventArgs;

    export interface IWebServiceClientEvents {
        OnStart($eventHandler : ($eventArgs? : EventArgs) => void) : void;

        OnClose($eventHandler : ($eventArgs? : EventArgs) => void) : void;

        OnTimeout($eventHandler : ($eventArgs? : EventArgs) => void) : void;

        OnError($eventHandler : ($eventArgs? : ErrorEventArgs) => void) : void;
    }
}
