/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Connectors {
    "use strict";
    import IWebServiceClient = Com.Wui.Framework.Commons.Interfaces.IWebServiceClient;
    import ErrorEventArgs = Com.Wui.Framework.Commons.Events.Args.ErrorEventArgs;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IWebServiceClientEvents = Com.Wui.Framework.Commons.Interfaces.Events.IWebServiceClientEvents;
    import WebServiceClientFactory = Com.Wui.Framework.Commons.WebServiceApi.WebServiceClientFactory;
    import WebServiceClientType = Com.Wui.Framework.Commons.Enums.WebServiceClientType;
    import IWebServiceRequestFormatterData = Com.Wui.Framework.Commons.Interfaces.IWebServiceRequestFormatterData;
    import Exception = Com.Wui.Framework.Commons.Exceptions.Type.Exception;
    import ObjectDecoder = Com.Wui.Framework.Commons.Utils.ObjectDecoder;
    import WebServiceClientEventType = Com.Wui.Framework.Commons.Enums.Events.WebServiceClientEventType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectEncoder = Com.Wui.Framework.Commons.Utils.ObjectEncoder;
    import IWebServiceResponseHandler = Com.Wui.Framework.Commons.Interfaces.IWebServiceResponseHandler;
    import IWuiBuilderEvents = Com.Wui.Framework.Commons.Interfaces.Events.IWuiBuilderEvents;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    /**
     * WuiBuilderConnector class provides bidirectional interface to instance of WUI Builder server.
     */
    export class WuiBuilderConnector extends Com.Wui.Framework.Commons.Primitives.BaseObject {
        private static singleton : WuiBuilderConnector;
        private subscribers : ArrayList<ArrayList<any>>;
        private readonly client : IWebServiceClient;

        /**
         * @return {WuiBuilderConnector} Returns singleton of connector instance.
         */
        public static Connect() : WuiBuilderConnector {
            if (!ObjectValidator.IsSet(WuiBuilderConnector.singleton)) {
                WuiBuilderConnector.singleton = new WuiBuilderConnector();
            }
            return WuiBuilderConnector.singleton;
        }

        /**
         * @param {boolean} [$reconnect=true] Specify, if connection should be automatically reconnected in case of
         * lose of the connection or unavailability of WUI Builder server.
         */
        constructor($reconnect : boolean = true) {
            super();

            this.subscribers = new ArrayList<ArrayList<any>>();

            this.client = WebServiceClientFactory.getClient(WebServiceClientType.WEB_SOCKETS,
                "resource/data/Com/Wui/Framework/Builder/connector.config.jsonp");

            if (!ObjectValidator.IsEmptyOrNull(this.client)) {
                this.client.setRequestFormatter(($data : IWebServiceRequestFormatterData) : void => {
                    if (ObjectValidator.IsObject($data.value)) {
                        $data.value.id = StringUtils.getCrc(this.getUID());
                        $data.value.status = 201;
                        $data.key = $data.value.id;
                    }
                });

                this.client.setResponseFormatter(($data : any, $owner : IWebServiceClient,
                                                  $onSuccess : ($value : any, $key? : number) => void,
                                                  $onError : ($message : string | Error | Exception) => void) : void => {
                    if (ObjectValidator.IsString($data)) {
                        $onSuccess($data);
                    } else {
                        if ($data.status === 200) {
                            $onSuccess($data, $data.id);
                        } else if ($data.status === 500) {
                            if ($data.type === "Server.Exception") {
                                $onError(new Exception(ObjectDecoder.Base64($data.data)));
                            } else if ($data.type === "Server.Timeout") {
                                EventsManager.getInstanceSingleton().FireEvent("" + $owner.getId(), WebServiceClientEventType.ON_TIMEOUT);
                            } else if ($data.type === "Request.Exception") {
                                $onError(ObjectDecoder.Base64(<string>$data.data));
                            } else {
                                $onError("Unsupported response error type: " + $data.type);
                            }
                        } else if ($data.status !== 201) {
                            $onError("Unsupported response status: " + $data.status);
                        }
                    }
                });

                if ($reconnect) {
                    this.client.getEvents().OnClose(() : void => {
                        this.client.StartCommunication();
                    });
                    this.client.getEvents().OnTimeout(() : void => {
                        this.client.StartCommunication();
                    });
                }
                this.client.getEvents().OnError(($eventArgs : ErrorEventArgs) : void => {
                    if (this.client.CommunicationIsRunning()) {
                        if (Loader.getInstance().getEnvironmentArgs().IsProductionMode()) {
                            ExceptionsManager.Throw(WuiBuilderConnector.ClassName(), $eventArgs.Exception());
                        } else {
                            LogIt.Error($eventArgs.Exception().ToString("", false));
                            Echo.Printf($eventArgs.Exception());
                        }
                    }
                });
            } else {
                ExceptionsManager.Throw(WuiBuilderConnector.ClassName(),
                    "Client for WUI Builder service is not supported by runtime environment.");
            }
        }

        /**
         * @return {number} Returns connector id suitable for events handling and connectors factory.
         */
        public getId() : number {
            return this.client.getId();
        }

        /**
         * @return {IWuiBuilderEvents} Returns events interface connected with connector instance.
         */
        public getEvents() : IWuiBuilderEvents {
            const superEvents : IWebServiceClientEvents = this.client.getEvents();
            this.extendClientEvents(superEvents, "OnBuildStart");
            this.extendClientEvents(superEvents, "OnBuildComplete");
            this.extendClientEvents(superEvents, "OnWarning", "OnBuilderWarning");
            this.extendClientEvents(superEvents, "OnFail", "OnBuilderFail");
            this.extendClientEvents(superEvents, "OnMessage");
            return <IWuiBuilderEvents>superEvents;
        }

        /**
         * @param {string} $type Specify data type value.
         * @param {any} $data Specify data, which should be send to WUI Builder server.
         * @param {IWebServiceResponseHandler} [$handler] Specify handler, which should be used for handling of response data.
         * @return {void}
         */
        public Send($type : string, $data : any, $handler? : IWebServiceResponseHandler) : void {
            this.client.Send(this.getProtocol($type, $data), $handler);
        }

        /**
         * @param {string} $name Specify event name value.
         * @param {IWebServiceResponseHandler} [$handler] Specify handler, which should be used for handling of event data.
         * @return {void}
         */
        public AddEventListener($name : string, $handler : IWebServiceResponseHandler) : void {
            this.client.Send(
                this.getProtocol("AddEventListener", {
                    name: $name
                }),
                ($data : any) : void => {
                    if ($data.type === "AddEventListener") {
                        if (ObjectDecoder.Base64($data.data) === "true") {
                            if (!this.subscribers.KeyExists($name)) {
                                this.subscribers.Add(new ArrayList<any>(), $name);
                            }
                            this.subscribers.getItem($name).Add($handler, $data.id);
                        }
                    } else if ($data.type === "FireEvent") {
                        const data : any = JSON.parse(ObjectDecoder.Base64($data.data));
                        if (this.subscribers.KeyExists(data.name)) {
                            const handlers : ArrayList<any> = this.subscribers.getItem(data.name);
                            handlers.foreach(($handler : any, $id : number) : void => {
                                if ($id === $data.id) {
                                    $handler(JSON.parse(ObjectDecoder.Base64(data.args)));
                                }
                            });
                        }
                    }
                });
        }

        /**
         * @param {string} $name Specify event name value, which should be removed from events pool.
         * @return {void}
         */
        public RemoveEventListener($name : string) : void {
            if (this.subscribers.KeyExists($name)) {
                this.subscribers.RemoveAt(this.subscribers.getKeys().indexOf($name));
            }
        }

        private getProtocol($type : string, $data : any) : any {
            return {
                data: ObjectEncoder.Base64(JSON.stringify($data)),
                type: $type
            };
        }

        private extendClientEvents($container : IWebServiceClientEvents, $eventType : string, $interfaceName? : string) : void {
            if (!ObjectValidator.IsSet($interfaceName)) {
                $interfaceName = $eventType;
            }
            $container[$eventType] = ($handler : IWebServiceResponseHandler) : void => {
                this.AddEventListener($interfaceName, $handler);
            };
        }
    }
}
