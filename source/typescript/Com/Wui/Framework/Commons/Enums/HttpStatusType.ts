/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Enums {
    "use strict";

    export enum HttpStatusType {
        SUCCESS = 200,
        CONTINUE = 201,
        MOVED = 301,
        CACHED = 304,
        NOT_AUTHORIZED = 401,
        FORBIDDEN = 403,
        NOT_FOUND = 404,
        ERROR = 500
    }
}
