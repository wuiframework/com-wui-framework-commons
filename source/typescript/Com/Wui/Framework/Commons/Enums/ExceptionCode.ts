/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Enums {
    "use strict";

    export enum ExceptionCode {
        EXIT = -1,
        GENERAL = 0,
        FATAL_ERROR = 1,
        NULL_POINTER = 2,
        ILLEGAL_ARGUMENT = 3,
        OUT_OF_RANGE = 4,
        ERROR_PAGE_EXCEPTION = 5,
        RESOLVER_EXCEPTION = 6
    }
}
