/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Enums.Events {
    "use strict";

    export class FileHandlerEventType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly ON_START : string = "OnStart";
        public static readonly ON_COMPLETE : string = "OnComplete";
        public static readonly ON_CHANGE : string = "OnChange";
        public static readonly ON_ERROR : string = "OnError";
        public static readonly ON_ABOARD : string = "OnAboard";
    }
}
