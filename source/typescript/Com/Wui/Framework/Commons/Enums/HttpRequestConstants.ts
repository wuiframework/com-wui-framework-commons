/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Enums {
    "use strict";

    export class HttpRequestConstants extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly EXCEPTION_TYPE : string = "ErrorType";
        public static readonly EXCEPTIONS_LIST : string = "ErrorsList";
        public static readonly HTTP301_LINK : string = "Http301Link";
        public static readonly HTTP404_FILE_PATH : string = "Http404FilePath";
        public static readonly APP_NAME : string = "AppName";
        public static readonly APP_PID : string = "AppPid";
        public static readonly WUI_DESIGNER : string = "WuiDesigner";
        public static readonly RELEASE_NAME : string = "ReleaseName";
        public static readonly PLATFORM : string = "Platform";
    }
}
