/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Enums {
    "use strict";

    export class HttpMethodType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly OPTIONS : string = "OPTIONS";
        public static readonly GET : string = "GET";
        public static readonly HEAD : string = "HEAD";
        public static readonly POST : string = "POST";
        public static readonly PUT : string = "PUT";
        public static readonly DELETE : string = "DELETE";
        public static readonly TRACE : string = "TRACE";
        public static readonly CONNECT : string = "CONNECT";
    }
}
