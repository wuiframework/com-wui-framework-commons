/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Enums {
    "use strict";

    export class FileSystemItemType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly MY_COMPUTER : string = "MyComputer";
        public static readonly FAVORITES : string = "Favorites";
        public static readonly NETWORK : string = "Network";
        public static readonly RECENT : string = "Recent";
        public static readonly PINNED : string = "Pinned";
        public static readonly DRIVE : string = "Drive";
        public static readonly DIRECTORY : string = "Directory";
        public static readonly FILE : string = "File";
        public static readonly LINK : string = "FSLink";
        public static readonly HIDDEN : string = "Hidden";
        public static readonly READONLY : string = "Readonly";
        public static readonly PERMISSION_DENIED : string = "PermissionDenied";
    }
}
