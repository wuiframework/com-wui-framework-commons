/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.Enums {
    "use strict";

    export class WebServiceClientType extends Com.Wui.Framework.Commons.Primitives.BaseEnum {
        public static readonly AJAX : string = "Ajax";
        public static readonly AJAX_SYNCHRONOUS : string = "AjaxSync";
        public static readonly IFRAME : string = "Iframe";
        public static readonly POST_MESSAGE : string = "PostMessage";
        public static readonly HTTP : string = "Http";
        public static readonly WEB_SOCKETS : string = "WebSockets";
        public static readonly CEF_QUERY : string = "CefQuery";
        public static readonly JXBROWSER_BRIDGE : string = "JxbrowserBridge";
    }
}
