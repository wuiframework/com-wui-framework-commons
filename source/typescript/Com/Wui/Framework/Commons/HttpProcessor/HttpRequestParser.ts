/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.HttpProcessor {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import Convert = Com.Wui.Framework.Commons.Utils.Convert;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import HttpMethodType = Com.Wui.Framework.Commons.Enums.HttpMethodType;

    /**
     * HttpRequestParser class provides parsing methods of current http request.
     */
    export class HttpRequestParser extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        protected parsedInfo : any;
        private readonly owner : any;

        /**
         * @param {string} [$urlBase] Force url base to parsed request info.
         * @param {any} [$owner] Specify request owner.
         */
        constructor($urlBase? : string, $owner? : any) {
            super();
            this.parsedInfo = {
                /* tslint:disable: object-literal-sort-keys */
                uri                : "",
                url                : "",
                scheme             : "http://",
                hostName           : "",
                port               : 80,
                relativeRoot       : "",
                relativeDirectory  : "",
                scriptFileName     : "",
                args               : new ArrayList<string>(),
                headers            : new ArrayList<string>(),
                eTags              : new ArrayList<string>(),
                lastModifiedTime   : Convert.TimeToGMTformat(new Date()),
                hostIpAddress      : "127.0.0.1",
                clientIpAddress    : "127.0.0.1",
                language           : "",
                platform           : "",
                userAgent          : "",
                browserType        : BrowserType.UNKNOWN,
                browserVersion     : -1,
                cookieEnabled      : false,
                isOnServer         : false,
                isOnLine           : false,
                isSearchBot        : false,
                isMobileDevice     : false,
                isWuiJre           : false,
                isWuiJreSimulator  : false,
                isWuiConnector     : false,
                isWuiConnectorDebug: false,
                isWuiPlugin        : false,
                isIdeaHost         : false,
                isWuiHosting       : false
                /* tslint:enable */
            };

            if (ObjectValidator.IsSet($urlBase)) {
                this.parsedInfo.relativeRoot = $urlBase;
            }
            this.owner = ObjectValidator.IsSet($owner) ? $owner : window;

            this.parseUri();
            this.parseUserAgent();
            this.parseHeaders();
        }

        /**
         * @return {any} Returns requested owner.
         */
        public getOwner() : any {
            return this.owner;
        }

        /**
         * @return {string} Returns requested URI.
         */
        public getUri() : string {
            return this.parsedInfo.uri;
        }

        /**
         * @return {string} Returns requested URL.
         */
        public getUrl() : string {
            return this.parsedInfo.url;
        }

        /**
         * @return {string} Returns scheme + host name + port, ends with /.
         */
        public getHostUrl() : string {
            return this.parsedInfo.hostName;
        }

        /**
         * @return {string} Returns host URL + relative directory, ends with /.
         */
        public getBaseUrl() : string {
            let url : string = this.parsedInfo.hostName;
            if (!ObjectValidator.IsEmptyOrNull(this.parsedInfo.relativeDirectory)) {
                url += this.parsedInfo.relativeDirectory;
            }
            return url + "/";
        }

        /**
         * @return {string} Returns relative path to required script file name.
         */
        public getScriptPath() : string {
            return this.parsedInfo.scriptFileName;
        }

        /**
         * @return {ArrayList} Returns array of passed arguments by required URL.
         */
        public getUrlArgs() : ArrayList<string> {
            return this.parsedInfo.args;
        }

        /**
         * @return {string} Returns directory relative to URL root path.
         */
        public getRelativeDirectory() : string {
            return this.parsedInfo.relativeDirectory;
        }

        /**
         * @return {string} Returns root path of required URL.
         */
        public getRelativeRoot() : string {
            return this.parsedInfo.relativeRoot;
        }

        /**
         * @return {string} Returns client IP address.
         */
        public getClientIP() : string {
            return this.parsedInfo.clientIpAddress;
        }

        /**
         * @param {boolean} [$simulator=false] Specify, if should be validated WUI hosting simulator.
         * @return {boolean} Returns true, if runtime environment is type of server, otherwise false.
         */
        public IsOnServer($simulator : boolean = false) : boolean {
            if ($simulator) {
                return this.parsedInfo.isWuiHosting;
            }
            return this.parsedInfo.isOnServer;
        }

        /**
         * @return {boolean} Returns true, if client has been detected as search bot, otherwise false.
         */
        public IsSearchBot() : boolean {
            return this.parsedInfo.isSearchBot;
        }

        /**
         * @return {boolean} Returns true, if client device has been detected as mobile device, otherwise false.
         */
        public IsMobileDevice() : boolean {
            return this.parsedInfo.isMobileDevice;
        }

        /**
         * @param {boolean} [$simulator=false] Specify, if should be validated WUI JRE simulator.
         * @return {boolean} Returns true, if client device has been detected as WUI JRE, otherwise false.
         */
        public IsWuiJre($simulator : boolean = false) : boolean {
            if ($simulator) {
                return this.parsedInfo.isWuiJreSimulator;
            } else {
                return this.parsedInfo.isWuiJre;
            }
        }

        /**
         * @return {boolean} Returns true, if runtime environment has been detected as WUI Plugin, otherwise false.
         */
        public IsWuiPlugin() : boolean {
            return this.parsedInfo.isWuiPlugin;
        }

        /**
         * @param {boolean} [$remote=false] Specify, if should be validated Remote WUI Connector instance.
         * @return {boolean} Returns true, if server has been detected as WUI Connector, otherwise false.
         */
        public IsWuiConnector($remote : boolean = false) : boolean {
            if ($remote) {
                return this.parsedInfo.isWuiConnectorDebug;
            } else {
                return this.parsedInfo.isWuiConnector;
            }
        }

        /**
         * @return {boolean} Returns true, if server has been detected as IntelliJ IDEA, otherwise false.
         */
        public IsIdeaHost() : boolean {
            return this.parsedInfo.isIdeaHost;
        }

        /**
         * @return {boolean} Returns true, if server has been detected as WUI Localhost, otherwise false.
         */
        public IsWuiHost() : boolean {
            return this.parsedInfo.isWuiHosting;
        }

        /**
         * @return {boolean} Returns true, if cookies are enabled at client browser, otherwise false.
         */
        public IsCookieEnabled() : boolean {
            return this.parsedInfo.cookieEnabled;
        }

        /**
         * @return {string} Returns user agent string provided by the browser.
         */
        public getUserAgent() : string {
            return this.parsedInfo.userAgent;
        }

        /**
         * @return {BrowserType} Returns detected type of browser.
         */
        public getBrowserType() : BrowserType {
            return this.parsedInfo.browserType;
        }

        /**
         * @return {number} Returns detected version number of client browser.
         */
        public getBrowserVersion() : number {
            return this.parsedInfo.browserVersion;
        }

        /**
         * @return {ArrayList} Returns list of all headers passed by current request.
         */
        public getHeaders() : ArrayList<string> {
            return this.parsedInfo.headers;
        }

        /**
         * @return {ArrayList} Returns list of Etags filtered from current headers.
         */
        public getEtags() : ArrayList<string> {
            return this.parsedInfo.eTags;
        }

        /**
         * @return {string} Returns last modified time parsed from headers in GMT format.
         */
        public getLastModifiedTime() : string {
            return this.parsedInfo.lastModifiedTime;
        }

        /**
         * @return {ArrayList<string>} Returns ArrayList of all cookies stored in the browser.
         */
        public getCookies() : ArrayList<string> {
            const cookies : ArrayList<string> = new ArrayList<string>();
            if (this.IsCookieEnabled() && !ObjectValidator.IsEmptyOrNull(document.cookie)) {
                const tmp : ArrayList<string> = ArrayList.ToArrayList(StringUtils.Split(document.cookie, ";"));
                tmp.foreach((value : string) : void => {
                    const pair : string[] = StringUtils.Split(value, "=");
                    pair[0] = StringUtils.Remove(pair[0], " ");
                    if (!ObjectValidator.IsEmptyOrNull(pair[0])) {
                        cookies.Add(pair[1], pair[0]);
                    }
                });
            }
            return cookies;
        }

        /**
         * @param {string} $name Cookie name, which should be found.
         * @return {string} Returns string value subscribed to cookie name, if name has been found,
         * otherwise returns empty string.
         */
        public getCookie($name : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($name)) {
                const cookies : ArrayList<string> = this.getCookies();
                if (cookies.KeyExists($name)) {
                    return cookies.getItem($name);
                }
            }
            return "";
        }

        /**
         * @return {ArrayList<string>} Returns ArrayList of all locally stored items.
         */
        public getStorageItems() : ArrayList<string> {
            const items : ArrayList<string> = new ArrayList<string>();
            if (Com.Wui.Framework.Commons.PersistenceApi.Handlers.StorageHandler.IsSupported()) {
                let index : number;
                for (index = 0; index < localStorage.length; index++) {
                    const key : string = localStorage.key(index);
                    items.Add(localStorage.getItem(key), key);
                }
            }
            return items;
        }

        /**
         * @param {string} $name Storage item name, which should be found.
         * @return {string} Returns string value subscribed to storage item name, if name has been found,
         * otherwise returns empty string.
         */
        public getStorageItem($name : string) : string {
            if (!ObjectValidator.IsEmptyOrNull($name)) {
                const items : ArrayList<string> = this.getStorageItems();
                if (items.KeyExists($name)) {
                    return items.getItem($name);
                }
            }
            return "";
        }

        /**
         * @param {string} $uri Uri, which should be validated.
         * @return {boolean} Returns true, if uri has same origin as host otherwise false.
         */
        public IsSameOrigin($uri : string) : boolean {
            const server : Location = this.getServerLocation();
            const parser : HTMLAnchorElement = document.createElement("a");
            parser.href = $uri;
            return server.protocol === parser.protocol && server.hostname === parser.hostname && server.port === parser.port;
        }

        /**
         * @return {string} Returns formatted string with all parsed information in HTML format.
         */
        public ToString() : string {
            let output : string = "uri: " + this.parsedInfo.uri;
            output += StringUtils.NewLine() + "url: " + this.parsedInfo.url;
            output += StringUtils.NewLine() + "scheme: " + this.parsedInfo.scheme;
            output += StringUtils.NewLine() + "host: " + this.parsedInfo.hostName;
            output += StringUtils.NewLine() + "port: " + Convert.ObjectToString(this.parsedInfo.port);
            output += StringUtils.NewLine() + "script: " + this.parsedInfo.scriptFileName;
            output += StringUtils.NewLine() + "args: " + Convert.ObjectToString(this.parsedInfo.args);
            output += StringUtils.NewLine() + "relative dir: " + this.parsedInfo.relativeDirectory;
            output += StringUtils.NewLine() + "relative root: " + this.parsedInfo.relativeRoot;

            output += StringUtils.NewLine() + StringUtils.NewLine() + "server IP: " + this.parsedInfo.hostIpAddress;
            output += StringUtils.NewLine() + "client IP: " + this.parsedInfo.clientIpAddress;

            output += StringUtils.NewLine();
            output += StringUtils.NewLine() + "cookieEnabled: " + Convert.BooleanToString(this.parsedInfo.cookieEnabled);
            output += StringUtils.NewLine() + "language: " + this.parsedInfo.language;
            output += StringUtils.NewLine() + "onServer: " + Convert.BooleanToString(this.parsedInfo.isOnServer);
            output += StringUtils.NewLine() + "onLine: " + Convert.BooleanToString(this.parsedInfo.isOnLine);
            output += StringUtils.NewLine() + "platform: " + this.parsedInfo.platform;
            output += StringUtils.NewLine() + "user agent: " + this.parsedInfo.userAgent;
            output += StringUtils.NewLine() + "is bot: " + Convert.BooleanToString(this.parsedInfo.isSearchBot);
            output += StringUtils.NewLine() + "is mobile: " + Convert.BooleanToString(this.parsedInfo.isMobileDevice);
            output += StringUtils.NewLine() + "is WUI JRE: " + Convert.BooleanToString(this.parsedInfo.isWuiJre);
            output += StringUtils.NewLine() + "is WUI Plugin: " + Convert.BooleanToString(this.parsedInfo.isWuiPlugin);
            output += StringUtils.NewLine() + "browser type: " + BrowserType[this.parsedInfo.browserType];
            output += StringUtils.NewLine() + "browser version: " + Convert.ObjectToString(this.parsedInfo.browserVersion);

            output += StringUtils.NewLine() + StringUtils.NewLine() + "headers: " + Convert.ObjectToString(this.parsedInfo.headers);
            output += StringUtils.NewLine() + "is WUI Connector: " + Convert.BooleanToString(this.parsedInfo.isWuiConnector);
            output += StringUtils.NewLine() + "is IDEA Hosting: " + Convert.BooleanToString(this.parsedInfo.isIdeaHost);
            output += StringUtils.NewLine() + "eTags: " + Convert.ObjectToString(this.parsedInfo.eTags);
            output += StringUtils.NewLine() + "last modified time: " + Convert.ObjectToString(this.parsedInfo.lastModifiedTime);

            output += StringUtils.NewLine() + StringUtils.NewLine() + "document cookies: " + Convert.ObjectToString(document.cookie);
            output += StringUtils.NewLine() + "parsed cookies: " + Convert.ObjectToString(this.getCookies());

            return output;
        }

        public toString() : string {
            return this.ToString();
        }

        protected getServerLocation() : Location {
            return window.location;
        }

        protected getNavigator() : Navigator {
            return window.navigator;
        }

        protected getRawHeaders() : ArrayList<string> {
            let request : XMLHttpRequest;
            if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed() && (
                this.parsedInfo.scheme !== "file://" ||
                (this.parsedInfo.scheme === "file://" &&
                    this.parsedInfo.browserType !== BrowserType.INTERNET_EXPLORER &&
                    this.parsedInfo.browserType !== BrowserType.GOOGLE_CHROME))) {
                try {
                    // firefox, opera 8.0+, safari
                    request = new XMLHttpRequest();
                } catch (e) {
                    // internet explorer
                    try {
                        request = new ActiveXObject("Msxml2.XMLHTTP");
                    } catch (e) {
                        try {
                            request = new ActiveXObject("Microsoft.XMLHTTP");
                        } catch (e) {
                            ExceptionsManager.Throw("parser", e);
                        }
                    }
                }
            }
            const headers : ArrayList<string> = new ArrayList<string>();
            try {
                if (ObjectValidator.IsSet(request)) {
                    request.open(HttpMethodType.HEAD, window.location.href, false);
                    request.send(null);
                    const tmp : ArrayList<string> = ArrayList.ToArrayList(StringUtils.Split(request.getAllResponseHeaders(), "\n"));

                    tmp.foreach((value : string) : void => {
                        const index : number = StringUtils.IndexOf(value, ":");
                        const key : string = StringUtils.Substring(value, 0, index);
                        value = StringUtils.Substring(value, index + 1);
                        if (StringUtils.StartsWith(value, " ")) {
                            value = StringUtils.Substring(value, 1);
                        }
                        if (!ObjectValidator.IsEmptyOrNull(value)) {
                            headers.Add(value, StringUtils.ToLowerCase(StringUtils.Replace(StringUtils.Remove(key, "HTTP_"), "_", "-")));
                        }
                    });
                }
            } catch (e) {
                // unable to read or parse headers
            }
            return headers;
        }

        protected parseUri() : void {
            const server : Location = this.getServerLocation();

            if (!ObjectValidator.IsEmptyOrNull(server.port)) {
                this.parsedInfo.port = StringUtils.ToInteger(server.port);
            } else {
                this.parsedInfo.port = 80;
            }
            this.parsedInfo.uri = server.href;
            while (StringUtils.Contains(this.parsedInfo.uri, "//")) {
                this.parsedInfo.uri = StringUtils.Replace(this.parsedInfo.uri, "//", "/");
            }
            this.parsedInfo.uri = StringUtils.Replace(this.parsedInfo.uri, server.protocol + "/", server.protocol + "//");
            this.parsedInfo.scheme = server.protocol + "//";

            this.parsedInfo.isOnServer = !StringUtils.Contains(StringUtils.ToLowerCase(this.parsedInfo.scheme), "file");

            this.parsedInfo.hostName = this.parsedInfo.uri;
            if (StringUtils.Contains(this.parsedInfo.uri, "#")) {
                this.parsedInfo.url = StringUtils.Split(this.parsedInfo.uri, "#")[1];
                this.parsedInfo.hostName = StringUtils.Remove(this.parsedInfo.hostName, "#" + this.parsedInfo.url);
            } else {
                this.parsedInfo.url = "";
            }

            this.parsedInfo.scriptFileName = this.parsedInfo.url;

            this.parsedInfo.args = new ArrayList<string>();
            if (StringUtils.Contains(this.parsedInfo.uri, "?")) {
                let query : string = StringUtils.Split(this.parsedInfo.uri, "?")[1];
                if (StringUtils.Contains(query, "#")) {
                    query = StringUtils.Split(query, "#")[0];
                }
                const args : string[] = StringUtils.Split(query, "&", "=");
                let key : string;
                let value : string;
                let index : number;
                for (index = 0; index < args.length; index += 2) {
                    key = args[index];
                    value = args[index + 1];
                    if (!ObjectValidator.IsEmptyOrNull(key)) {
                        this.parsedInfo.args.Add(value, key);
                    }
                }
                this.parsedInfo.hostName = StringUtils.Remove(this.parsedInfo.hostName, "?" + query);
                this.parsedInfo.scriptFileName = StringUtils.Remove(this.parsedInfo.scriptFileName, "?" + query);
            }

            this.parsedInfo.relativeDirectory = StringUtils.Replace(StringUtils.Substring(this.parsedInfo.url, 0,
                StringUtils.IndexOf(this.parsedInfo.url, "/", false)), "\\", "/");

            this.parsedInfo.isWuiJreSimulator = StringUtils.ContainsIgnoreCase(this.parsedInfo.args.getItem("debug"), "JRESimulator");
            this.parsedInfo.isWuiConnectorDebug = StringUtils.ContainsIgnoreCase(this.parsedInfo.args.getItem("debug"), "WUIConnector");
            this.parsedInfo.isWuiHosting = StringUtils.ContainsIgnoreCase(this.parsedInfo.args.getItem("debug"), "Hosting");
        }

        protected parseUserAgent() : void {
            const navigator : Navigator = this.getNavigator();

            this.parsedInfo.cookieEnabled = navigator.cookieEnabled;
            this.parsedInfo.language = navigator.language;
            this.parsedInfo.isOnLine = navigator.onLine;
            this.parsedInfo.platform = navigator.platform;

            this.parsedInfo.userAgent = navigator.userAgent;
            this.parsedInfo.isMobileDevice = StringUtils.Contains(this.parsedInfo.userAgent, "Mobile", "iPad");
            this.parsedInfo.isWuiJre = false;
            this.parsedInfo.isWuiPlugin = false;
            this.parsedInfo.isSearchBot = false;
            this.parsedInfo.browserType = BrowserType.UNKNOWN;
            this.parsedInfo.browserVersion = -1;
            let browserVersion : string = "-1";

            if (StringUtils.Contains(this.parsedInfo.userAgent, "com-wui-framework-plugin")) {
                this.parsedInfo.isWuiPlugin = true;
            }
            if (StringUtils.Contains(this.parsedInfo.userAgent, "com-wui-framework-jre")) {
                this.parsedInfo.isWuiJre = true;
                this.parsedInfo.browserType = BrowserType.WUIJRE;
            } else if (StringUtils.Contains(this.parsedInfo.userAgent, "bot")) {
                this.parsedInfo.isSearchBot = true;
            } else if (StringUtils.Contains(this.parsedInfo.userAgent, ".NET", "MSIE") ||
                StringUtils.Contains(this.parsedInfo.userAgent, "Mozilla/5.0") &&
                StringUtils.Contains(this.parsedInfo.userAgent, "rv:") &&
                !StringUtils.Contains(this.parsedInfo.userAgent, "Firefox")) {
                this.parsedInfo.browserType = BrowserType.INTERNET_EXPLORER;
                if (StringUtils.Contains(this.parsedInfo.userAgent, "MSIE")) {
                    browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "MSIE ")[1];
                } else {
                    browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "rv:")[1];
                }
            } else if (StringUtils.Contains(this.parsedInfo.userAgent, "Opera", "OPR")) {
                this.parsedInfo.browserType = BrowserType.OPERA;
                if (StringUtils.Contains(this.parsedInfo.userAgent, "OPR")) {
                    browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "OPR/")[1];
                }
            } else if (StringUtils.Contains(this.parsedInfo.userAgent, "Edge")) {
                this.parsedInfo.browserType = BrowserType.EDGE;
                browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "Edge/")[1];
            } else if (StringUtils.Contains(this.parsedInfo.userAgent, "Chrome")) {
                this.parsedInfo.browserType = BrowserType.GOOGLE_CHROME;
                browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "Chrome/")[1];
            } else if (StringUtils.Contains(this.parsedInfo.userAgent, "Safari")) {
                this.parsedInfo.browserType = BrowserType.SAFARI;
            } else if (StringUtils.Contains(this.parsedInfo.userAgent, "Firefox", "Gecko")) {
                this.parsedInfo.browserType = BrowserType.FIREFOX;
                browserVersion = StringUtils.Split(this.parsedInfo.userAgent, "Firefox/")[1];
            }

            if (browserVersion === "-1" && StringUtils.Contains(this.parsedInfo.userAgent, "Version")) {
                browserVersion = StringUtils.Split(StringUtils.Split(this.parsedInfo.userAgent, "Version")[1], "/")[1];
            }

            if (StringUtils.Contains(browserVersion, " ")) {
                browserVersion = StringUtils.Split(browserVersion, " ")[0];
            }
            if (StringUtils.Contains(browserVersion, ")")) {
                browserVersion = StringUtils.Split(browserVersion, ")")[0];
            }
            browserVersion = StringUtils.Remove(browserVersion, ";");
            this.parsedInfo.browserVersion = StringUtils.ToInteger(browserVersion);
        }

        protected parseHeaders() : void {
            this.parsedInfo.headers = this.getRawHeaders();
            if (this.parsedInfo.headers.KeyExists("if-none-match")) {
                this.parsedInfo.eTags = ArrayList.ToArrayList(
                    StringUtils.Split(StringUtils.StripSlashes(this.parsedInfo.headers.getItem("if-none-match")), ", "));
            }
            if (this.parsedInfo.headers.KeyExists("if-modified-since")) {
                this.parsedInfo.lastModifiedTime = StringUtils.StripSlashes(this.parsedInfo.headers.getItem("if-modified-since"));
            }
            const server : string = this.parsedInfo.headers.getItem("server");
            this.parsedInfo.isWuiConnector = StringUtils.ContainsIgnoreCase(server, "WuiConnector");
            this.parsedInfo.isIdeaHost = StringUtils.ContainsIgnoreCase(server, "IntelliJ IDEA");
            if (!this.parsedInfo.isWuiHosting) {
                this.parsedInfo.isWuiHosting = StringUtils.ContainsIgnoreCase(server, "WuiLocalhost");
            }
        }
    }
}
