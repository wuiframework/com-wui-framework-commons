/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.HttpProcessor {
    "use strict";
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import StringUtils = Com.Wui.Framework.Commons.Utils.StringUtils;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;
    import IHttpRequestResolver = Com.Wui.Framework.Commons.Interfaces.IHttpRequestResolver;
    import BaseHttpResolver = Com.Wui.Framework.Commons.HttpProcessor.Resolvers.BaseHttpResolver;
    import HttpRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import Reflection = Com.Wui.Framework.Commons.Utils.Reflection;
    import RuntimeTests = Com.Wui.Framework.Commons.RuntimeTests;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;

    /**
     * HttpResolver class provides handling of current http request and hash on change events.
     */
    export class HttpResolver extends Com.Wui.Framework.Commons.Primitives.BaseObject {

        private static baseUrl : string;
        private readonly manager : HttpManager;
        private readonly events : EventsManager;

        /**
         * @param {string} [$baseUrl] Set baseUrl for http resolver
         */
        constructor($baseUrl? : string) {
            super();

            if (!ObjectValidator.IsEmptyOrNull($baseUrl) && $baseUrl !== "#") {
                if (Loader.getInstance().getEnvironmentArgs().HtmlOutputAllowed() && ObjectValidator.IsEmptyOrNull(window.location.hash)) {
                    if (!StringUtils.StartsWith($baseUrl, "/")) {
                        $baseUrl = "/" + $baseUrl;
                    }
                    if (!StringUtils.EndsWith($baseUrl, "/")) {
                        $baseUrl += "/";
                    }
                    window.location.hash = $baseUrl;
                }
            }

            const httpManger : any = this.getHttpManagerClass();
            this.manager = new httpManger(this.CreateRequest($baseUrl));
            const eventsManger : any = this.getEventsManagerClass();
            this.events = new eventsManger();
            this.registerResolvers();
        }

        /**
         * @param {string} [$baseUrl] Specify base url for new instance of request parser.
         * @return {HttpRequestParser} Returns new instance of request parser based on HttpResolver definition.
         */
        public CreateRequest($baseUrl? : string) : HttpRequestParser {
            if (!ObjectValidator.IsEmptyOrNull($baseUrl) && HttpResolver.baseUrl !== $baseUrl) {
                HttpResolver.baseUrl = $baseUrl;
            }
            const parserClass : any = this.getRequestParserClass();
            return new parserClass(HttpResolver.baseUrl);
        }

        /**
         * @return {EventsManager} Returns EventsManager instance used for management of request events.
         */
        public getEvents() : EventsManager {
            return this.events;
        }

        /**
         * @return {HttpManager} Returns HttpManager instance used for management of resolved requests.
         */
        public getManager() : HttpManager {
            return this.manager;
        }

        /**
         * Find out appropriate request resolver class based on current http request and execute resolver Process method.
         * @param {HttpRequestEventArgs|AsyncRequestEventArgs} [$eventArgs] Specify request args, which should be resolved.
         * @return {void}
         */
        public ResolveRequest($eventArgs? : HttpRequestEventArgs | AsyncRequestEventArgs) : void {
            try {
                if (ObjectValidator.IsEmptyOrNull($eventArgs) ||
                    !Reflection.getInstance().IsMemberOf($eventArgs, <ClassName>HttpRequestEventArgs)) {
                    $eventArgs = new HttpRequestEventArgs(this.manager.getRequest().getScriptPath());
                }

                if (!Reflection.getInstance().IsMemberOf($eventArgs, <ClassName>AsyncRequestEventArgs)) {
                    let lastTimeoutId : any = setTimeout(
                        /* istanbul ignore next : callback placeholder for ability to get lastTimeoutId */
                        () : void => {
                            // force invoke creation of last timeout id
                        }, 0);
                    do {
                        clearTimeout(lastTimeoutId);
                    }
                    while (lastTimeoutId--);
                    let lastIntervalId : any = setInterval(
                        /* istanbul ignore next : callback placeholder for ability to get lastIntervalId */
                        () : void => {
                            // force invoke creation of last interval id
                        }, 0);
                    do {
                        clearInterval(lastIntervalId);
                    }
                    while (lastIntervalId--);

                    window.location.hash = $eventArgs.Url();
                    this.manager.RefreshWithoutReload();
                }
                let resolver : IHttpRequestResolver;
                let resolverName : any = this.manager.getResolverClassName($eventArgs.Url());

                if (ObjectValidator.IsEmptyOrNull(resolverName)) {
                    resolverName = this.manager.getResolverClassName("/ServerError/Http/DefaultPage");
                    if (ObjectValidator.IsEmptyOrNull(resolverName)) {
                        resolverName = BaseHttpResolver;
                    }
                } else {
                    const getData : ArrayList<string> = this.manager.getResolverParameters();
                    getData.Copy(this.manager.getRequest().getUrlArgs());
                    $eventArgs.GET(getData);
                }
                resolver = new resolverName();
                resolver.RequestArgs($eventArgs);
                resolver.Process();
            } catch (ex) {
                ExceptionsManager.HandleException(ex);
            }
        }

        public toString() : string {
            return this.ToString();
        }

        protected getEventsManagerClass() : any {
            return EventsManager;
        }

        protected getHttpManagerClass() : any {
            return HttpManager;
        }

        protected getRequestParserClass() : any {
            return HttpRequestParser;
        }

        protected registerResolvers() : void {
            const resolvers : any = this.getStartupResolvers();
            let httpPattern : string;
            for (httpPattern in resolvers) {
                /* istanbul ignore else : bulletproof condition */
                if (ObjectValidator.IsSet(resolvers[httpPattern])) {
                    const resolver : any = resolvers[httpPattern];
                    if (!ObjectValidator.IsEmptyOrNull(resolver) && ObjectValidator.IsSet(resolver.ClassName)) {
                        if (this.manager.HttpPatternExists(httpPattern)) {
                            this.manager.OverrideResolver(httpPattern, resolver);
                        } else {
                            this.manager.RegisterResolver(httpPattern, resolver);
                        }
                    }
                }
            }
        }

        protected getStartupResolvers() : any {
            const resolvers : any = {
                "/"         : Com.Wui.Framework.Commons.Index,
                "/web"      : Com.Wui.Framework.Commons.Index,
                "/web/index": Com.Wui.Framework.Commons.Index
            };
            /* dev:start */
            resolvers["/web/HttpManagerTest"] = RuntimeTests.HttpManagerTest;
            resolvers["/web/HttpRequestParserTest"] = RuntimeTests.HttpRequestParserTest;
            resolvers["/web/PersistenceApiTest"] = RuntimeTests.PersistenceApiTest;
            resolvers["/web/EventsManagerTest"] = RuntimeTests.EventsManagerTest;
            resolvers["/web/ExceptionsManagerTest"] = RuntimeTests.ExceptionsManagerTest;
            resolvers["/web/SynchronousRequestTest"] = RuntimeTests.SyncRequestTest;
            resolvers["/web/AsyncRequestTest"] = RuntimeTests.AsyncRequestTest;
            resolvers["/web/TimeoutTest"] = RuntimeTests.TimeoutTest;
            resolvers["/web/ChromiumConnectorTest"] = RuntimeTests.ChromiumConnectorTest;
            resolvers["/web/CoverageTest"] = RuntimeTests.CoverageTest;
            resolvers["/web/AsyncRuntimeTest"] = RuntimeTests.AsyncRuntimeTestCase;
            resolvers["/web/JsonpFileReaderTest"] = RuntimeTests.JsonpFileReaderTest;
            resolvers["/web/JxbrowserBridgeClientTest"] = RuntimeTests.JxbrowserBridgeClientTest;
            /* dev:end */
            return resolvers;
        }
    }
}
