/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.HttpProcessor.Resolvers {
    "use strict";
    import IHttpRequestResolver = Com.Wui.Framework.Commons.Interfaces.IHttpRequestResolver;
    import HttpRequestParser = Com.Wui.Framework.Commons.HttpProcessor.HttpRequestParser;
    import HttpManager = Com.Wui.Framework.Commons.HttpProcessor.HttpManager;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import LogIt = Com.Wui.Framework.Commons.Utils.LogIt;
    import ExceptionsManager = Com.Wui.Framework.Commons.Exceptions.ExceptionsManager;
    import Exit = Com.Wui.Framework.Commons.Exceptions.Type.Exit;
    import BrowserType = Com.Wui.Framework.Commons.Enums.BrowserType;
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import HttpRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs;
    import EventsManager = Com.Wui.Framework.Commons.Events.EventsManager;
    import ArrayList = Com.Wui.Framework.Commons.Primitives.ArrayList;
    import ObjectValidator = Com.Wui.Framework.Commons.Utils.ObjectValidator;

    /**
     * BaseHttpResolver class is abstract class providing basic http request resolving and
     * each http resolver should be extending this class for security reason.
     */
    export class BaseHttpResolver extends Com.Wui.Framework.Commons.Primitives.BaseObject implements IHttpRequestResolver {
        private readonly events : EventsManager;
        private readonly environment : EnvironmentArgs;
        private readonly httpManager : HttpManager;
        private request : HttpRequestParser;
        private requestArgs : HttpRequestEventArgs;

        constructor() {
            super();
            this.environment = Loader.getInstance().getEnvironmentArgs();
            this.httpManager = Loader.getInstance().getHttpManager();
            this.request = this.httpManager.getRequest();
            this.events = Loader.getInstance().getHttpResolver().getEvents();
            this.requestArgs = new HttpRequestEventArgs(this.httpManager.getRequest().getScriptPath());
        }

        /**
         * @param {HttpRequestEventArgs} $value Specify request arguments, which should be processed by resolver instance.
         * @return {HttpRequestEventArgs} Returns current request arguments
         */
        public RequestArgs($value? : HttpRequestEventArgs) : HttpRequestEventArgs {
            if (ObjectValidator.IsSet($value)) {
                this.requestArgs = $value;
                if ($value.Owner().IsMemberOf(HttpRequestParser)) {
                    this.request = $value.Owner();
                    this.requestArgs.GET().Copy(this.request.getUrlArgs());
                }
                this.argsHandler(this.requestArgs.GET(), this.requestArgs.POST());
            }
            return this.requestArgs;
        }

        /**
         * Execute resolver implementation.
         * @return {void}
         */
        public Process() : void {
            if (!this.browserValidator()) {
                LogIt.Error("Unsupported browser. User Agent: " + this.request.getUserAgent());
                this.httpManager.ReloadTo("/ServerError/Browser", null, true);
                this.stop();
            } else {
                Echo.ClearAll();
                this.resolver();
            }
            this.events.FireEvent(GeneralEventOwner.BODY, EventType.ON_LOAD);
        }

        public toString() : string {
            return this.ToString();
        }

        /**
         * @return {boolean} Returns true, if browser is valid for execution the resolver, otherwise false.
         */
        protected browserValidator() : boolean {
            return !(
                this.request.getBrowserType() === BrowserType.UNKNOWN ||
                (this.request.getBrowserType() === BrowserType.INTERNET_EXPLORER && this.request.getBrowserVersion() < 5));
        }

        protected argsHandler($GET : ArrayList<string>, $POST : ArrayList<any>) : void {
            // handle request args
        }

        protected resolver() : void {
            Echo.Println("This is abstract BaseHttpResolver, which has been executed at: " + this.request.getUrl());
        }

        protected stop() : void {
            ExceptionsManager.Throw(this.getClassName(), new Exit());
        }

        protected exit() : void {
            this.stop();
        }

        protected getRequest() : HttpRequestParser {
            return this.request;
        }

        protected getHttpManager() : HttpManager {
            return this.httpManager;
        }

        protected getEventsManager() : EventsManager {
            return this.events;
        }

        protected getEnvironmentArgs() : EnvironmentArgs {
            return this.environment;
        }

        protected createLink($link : string) : string {
            return this.httpManager.CreateLink($link);
        }

        protected registerResolver($httpRequest : string, $resolver : any, $ignoreCase : boolean = true) : void {
            this.httpManager.RegisterResolver($httpRequest, $resolver, $ignoreCase);
        }

        protected overrideResolver($httpRequest : string, $resolver : any, $ignoreCase : boolean = true) : void {
            this.httpManager.OverrideResolver($httpRequest, $resolver, $ignoreCase);
        }
    }
}
