/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2018 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */
namespace Com.Wui.Framework.Commons.HttpProcessor.Resolvers {
    "use strict";
    import EventType = Com.Wui.Framework.Commons.Enums.Events.EventType;
    import GeneralEventOwner = Com.Wui.Framework.Commons.Enums.Events.GeneralEventOwner;
    import AsyncRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.AsyncRequestEventArgs;
    import HttpStatusType = Com.Wui.Framework.Commons.Enums.HttpStatusType;
    import Echo = Com.Wui.Framework.Commons.Utils.Echo;
    import HttpRequestEventArgs = Com.Wui.Framework.Commons.Events.Args.HttpRequestEventArgs;

    /**
     * AsyncHttpResolver class is abstract class providing basic asynchronous request.
     */
    export abstract class AsyncHttpResolver extends BaseHttpResolver {

        /**
         * @param {AsyncRequestEventArgs} $value Specify request arguments, which should be processed by async resolver instance.
         * @return {AsyncRequestEventArgs} Returns current request arguments
         */
        public RequestArgs($value? : AsyncRequestEventArgs) : AsyncRequestEventArgs {
            return <AsyncRequestEventArgs>super.RequestArgs($value);
        }

        /**
         * Execute asynchronous resolver implementation.
         * @return {void}
         */
        public Process() : void {
            if (!this.RequestArgs().IsMemberOf(AsyncRequestEventArgs)) {
                const origArgs : HttpRequestEventArgs = this.RequestArgs();
                const newArgs : AsyncRequestEventArgs = this.RequestArgs(new AsyncRequestEventArgs(origArgs.Url(), origArgs.POST()));
                this.argsHandler(newArgs.GET(), newArgs.POST());
            }
            this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_START, this.RequestArgs());
            this.resolver();
        }

        protected success() : void {
            this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_SUCCESS, this.RequestArgs());
            this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE, this.RequestArgs());
        }

        protected error() : void {
            this.RequestArgs().Status(HttpStatusType.ERROR);
            this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_ERROR, this.RequestArgs());
            this.getEventsManager().FireEvent(GeneralEventOwner.ASYNC_REQUEST, EventType.ON_COMPLETE, this.RequestArgs());
        }

        protected resolver() : void {
            this.success();
        }
    }
}
