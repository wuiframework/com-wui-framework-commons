/* ********************************************************************************************************* *
 *
 * Copyright (c) 2014-2016 Freescale Semiconductor, Inc.
 * Copyright (c) 2017-2019 NXP
 *
 * SPDX-License-Identifier: BSD-3-Clause
 * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
 * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
 *
 * ********************************************************************************************************* */

var appExceptionHandler = function () {
    var exception = "";
    var stackIndex = 0;
    var print = function ($message, $stack) {
        if (exception === "") {
            console.log("Package \"<? @var clientConfig.packageName ?>\" has not been loaded.");
            exception =
                "<div class=\"Exception\">" +
                "   <h1>FATAL Error!</h1>" +
                "   <div class=\"Message\">";
        }
        console.log($message);
        exception += $message;
        if ($stack !== undefined && $stack !== "" && $stack !== null) {
            console.log($stack);
            exception +=
                "<br>" +
                "       <span onclick=\"" +
                "document.getElementById('exceptionItem_" + stackIndex + "').style.display=" +
                "document.getElementById('exceptionItem_" + stackIndex + "').style.display===" +
                "'block'?'none':'block';\" " +
                "style=\"CURSOR: pointer; CURSOR: hand; FONT-SIZE: 10px; COLOR: #BE0000; FONT-FAMILY: Verdana;\">" +
                "Stack trace" +
                "</span>" +
                "       <div id=\"exceptionItem_" + stackIndex + "\" style=\"border: 0 solid black; display: none;\">" +
                $stack +
                "</div>";
            stackIndex++;
        }
        exception += "<br><br>";

        var content = document.getElementById("Content");
        try {
            if (!content) {
                content = document.body;
            }
            content.innerHTML =
                exception +
                "   </div>" +
                "</div>";
        } catch (ex) {
            console.log(ex.message);
            if (ex.stack) {
                console.log(ex.stack);
            }
        }
    };
    return print;
};
var appLoader = function ($loaderClass) {
    var loaderClass = $loaderClass;
    if (loaderClass === "") {
        loaderClass = "Com.Wui.Framework.Commons.Loader";
    }

    window.onerror = function (message, filename, lineno, colno, error) {
        message =
            "       <b>" + message + "</b><br>" +
            "       file: " + filename + "<br>" +
            "       at line: " + lineno;
        var stack = null;
        if (error) {
            message += ":" + colno;
            stack = error.stack;
        }
        appExceptionHandler()(message, stack);
        return true;
    };
    window.onload = function () {
        var loaderClassParts = loaderClass.split(".");
        var classObject = window;
        for (var index = 0; index < loaderClassParts.length; index++) {
            if (classObject[loaderClassParts[index]] !== undefined) {
                classObject = classObject[loaderClassParts[index]];
            }
        }
        try {
            Com.Wui.Framework.Commons.Primitives.String = Com.Wui.Framework.Commons.Utils.StringUtils;
            classObject.Load("<? @var clientConfig ?>");
        } catch (ex) {
            appExceptionHandler()(ex.message, ex.stack);
        }
    };
};
try {
    appLoader("<? @var clientConfig.loaderClass ?>");
} catch (ex) {
    appExceptionHandler()(ex.message, ex.stack);
}
